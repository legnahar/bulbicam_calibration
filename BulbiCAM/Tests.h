#pragma once
#ifndef _TESTS_H_
#define _TESTS_H_

#include "eyetracking.h"
#include <nlohmann\json.hpp>
#include "display.h"
#include "GLFW/glfw3.h"
#include "WebCommunicate.h"
#include "lensdistortion.h"

extern CDisplay display;
extern WebCommunicate *webComm;
extern bool eyetrackingOn;

extern void startTest(int);
extern std::chrono::time_point<std::chrono::system_clock> calib_prevts;
extern Lens *lensDist;
extern string versionInfoForHub;


struct patientInformation
{
	int age;
	string examID;
	string diagnosis;
};


extern patientInformation patientInfo;

struct frameForAI
{
	cv::Mat picture;
	uint64_t ts;
};

enum MESSAGE_TYPE {
	START_TEST = 0,
	STOP_TEST = 1,
	START_PUPIL_DATA_TRANSMISSION = 2,
	STOP_PUPIL_DATA_TRANSMISSION = 3,
	START_BACKGROUND_DATA_TRANSMISSION = 4,
	STOP_BACKGROUND_DATA_TRANSMISSION = 5,
	DATA_PACKAGE = 6,
	START_NPC_DATA_TRANSMISSION = 7,
	STOP_NPC_DATA_TRANSMISSION = 8,
	START_RECOVERY_DATA_TRANSMISSION = 9,
	STOP_RECOVERY_DATA_TRANSMISSION = 10,
	START_PERIPHERAL_SHOWING = 13,
	STOP_PERIPHERAL_SHOWING = 14,
};

#pragma region VLV_CALIBRATION_TESTS

void calibrationProcessor();

#pragma endregion VLV_CALIBRATION_TESTS


class CTest
{
public:
CTest()
{
}

~CTest()
{
}
virtual void drawStimuli() {};
virtual void initStimuli() {};
virtual void ProcessETData(vector<ETData> etData) {};

#pragma region VLV_CALIBRATION_TESTS

virtual void inCaseOfCalibration() {};
virtual void finishCalibration(bool forced) {};

#pragma endregion VLV_CALIBRATION_TESTS


	string testType;
double mmtopixels(double mm)
{
	return 1920. * mm / 121.;
}

double pixelstomm(double px)
{
	return 121. * px / 1920.;
}private:

};


#endif