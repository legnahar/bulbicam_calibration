#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <mutex>
#include <atomic>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <atomic>
#define displayversion "0.9.20210805"

enum StimuliObjectType
{
	CIRCLE = 0
};

enum DisplayStimuliType
{
	DRAGONFLY = 0, //!
	STIMULI = 1,   //!
	VERGENCE = 2,  //!
	AMSLER = 3,    //!
	NYSTAGMUS = 8, //!
	CONTRAST = 9,  //!
	SACCADETASK = 10,  //!
};

struct StimuliObjects
{
	int objectID;
	StimuliObjectType objectType;
	GLfloat objectCoordinates[2];
	GLfloat objectColor[3];
	GLfloat objectSize;
	GLfloat numTexture;
};

class CDisplay
{
public:
	// TECHSTAFF STUFF
	CDisplay() {};
	~CDisplay();
	void initGL(int framewidth, int frameheight);
	void initWindows(bool, bool);
	void displayProcessor();
	void setDragonfly();

	int lback, rback;
	double maxLuminosityOS, maxLuminosityOD;
	int frameWidth, frameHeight;

	int acuity_showtarget;
	int verg_targettype;

	void updateCamPicture(cv::Mat newCamPicture);
	void updateQIPicture(cv::Mat newQIPicture);

	void(*errorCallback)(int error, const char* description);
	void (*keyCallback)(GLFWwindow* window, int key, int scancode, int action, int mode);
	void(*mouseCallback)(GLFWwindow* window, int button, int action, int mods);
	void(*scrollCallback)(GLFWwindow* window, double xoffset, double yoffset);

	// STIMULI STUFF

	void drawMCross(float x, float y, float size);
	void redrawCross();
	void loadGrayAnimals();
	void loadBlackAnimals();
	int addObject(StimuliObjects object);
	void deleteObject(int objectID);
	void moveObject(int objectID, int x, int y);
	void resizeObject(int objectID, int size);
	void paintObject(int objectID, float R, float G, float B);
	void setBackground(float R, float G, float B);
	void setBackgroundAA(float R, float G, float B);
	void setBackgroundL(float R, float G, float B);
	void setBackgroundR(float R, float G, float B);
	void drawAmsler();
	void drawContrast();
	void drawNystagmus();
	void drawSaccadeTask();
	void amsler(int eye, int type, float ipd);
	void clearObjects();
	void setTextures(cv::Mat leftTex, cv::Mat rightTex);
	void saveScreenshot();
	void changeTextures();

	std::atomic<long> frameNumber;

	int amse, amst, aipd;

	cv::Point st_greenTargetCoords, st_redTargetCoords;
	bool st_showRed, st_showGreen;
	GLfloat st_redTargetColor[3];

	std::atomic<int> stimuliType;
	std::atomic<bool> redrawPending;

	cv::Mat acImg;
	bool drCircleR;
	bool drCircleG;
	int ddist;

	int p4_texL, p4_texR;
	cv::Mat bgTexOS[257], bgTexOD[257];
	bool p4;



	cv::Point acPos;
	int bPx, wPx;
	float bClr, wClr;
	//contv
	int contract_tPx;
	GLfloat contrast_tClr[3];
	cv::Point2d contrast_tPosition;
	bool contrast_showCircle;
	int contrast_bleachFramesL;
	int contrast_bleachFramesR;
	int contrast_amdstage;
	float contrast_logmarindex;
	bool contrast_bdecrease;
	float contrast_gbclr;
	float contrast_gwclr;
	GLfloat contrast_acuityBGClr[3];
	bool vfbleech,afterbleech;
	std::chrono::time_point<std::chrono::system_clock> vf_srtstart;
	bool hideAcodaptStimulus;

	int contrast_bPx, contrast_wPx;
	GLfloat contrast_bClr, contrast_wClr;

	int acolapt_darkPixel;
	int acolapt_darkBorder;
	int acolapt_brightPixel;
	int acolapt_brightBorder;
	int acolapt_pixels;
	int acolapt_background;
	int acolapt_hertz;


	bool drCircle;
	int acType;
	GLuint acTexture;
	int coordL, coordR;
	bool backtex;
	cv::Mat lBGTex, rBGTex;

	std::vector<cv::Point> acuity2_distractors;
	cv::Point acuity2_target;
	float acuity2_diameter;

	int p2Type;
	bool lW, rW;
	GLfloat acuityobjects[360][2];

	int nystagmusType;
	cv::Point nCoordL;
	cv::Point nCoordR;
	bool animatenystagmus;
	bool showvftex;
	int vftexnum;
	bool nShowL, nShowR;
	int nTargetType;
	int nAnilmalNum;
	bool nysLarge;

	int osblues[16];
	int odblues[16];
	bool redon;
	bool dotsz;
	int jw;
	float aminitpos[2];

	cv::Mat whiteCirclePic;

	bool p2_showl, p2_showr;

	bool hollowCircle;
	cv::Point hollowCircleCoords;
	float hollowCircleR1, hollowCircleR2;
	bool vfnumbers;

	bool ac_hollowCircle;
	float ac_hollowCircleR1, ac_hollowCircleR2;

private:
	// TECHSTAFF STUFF
	std::mutex mtx;
	GLFWwindow* techstaffWindow;
	unsigned int techstaffProgramID;
	unsigned int TSVBO, TSVAO, TSEBO;
	unsigned int camTexture;
	const char* TSvertexSource =
		"	#version 330 core\n"
		"	layout(location = 0) in vec2 aPos;\n"
		"	layout(location = 1) in vec3 aTexCoord;\n"
		"	out vec3 TexCoord;\n"
		"	void main()\n"
		"	{\n"
		"		gl_Position = vec4(aPos, 0.0, 1.0);\n"
		"		TexCoord = aTexCoord;\n"
		"	}\n";
	const char* TSfragmentSource =
		"#version 330 core\n"
		"out vec4 FragColor;\n"
		"in vec3 TexCoord;\n"
		"uniform sampler2DArray ourTexture;\n"
		"void main()\n"
		"{\n"
		"	if(TexCoord.z==0)\n"
		"	FragColor = texture(ourTexture, TexCoord).rrrr;\n"
		"	else\n"
		"	FragColor = texture(ourTexture, TexCoord);\n"
		"}\n";

	const float TSVertices[100] = {
		957.5f / (1920.f * 0.5f) - 1.f,  1080.f / (1080.f * 0.5f) - 1.f,  0.5f, 1.0f, 0.f,
		957.5f / (1920.f * 0.5f) - 1.f,  330.f / (1080.f * 0.5f) - 1.f,     0.0f, 1.0f, 0.f,
		0.f / (1920.f * 0.5f) - 1.f,     330.f / (1080.f * 0.5f) - 1.f,     0.0f, 0.0f, 0.f,
		0.f / (1920.f * 0.5f) - 1.f,     1080.f / (1080.f * 0.5f) - 1.f,  0.5f, 0.0f, 0.f,

		1920.f / (1920.f * 0.5f) - 1.f,  1080.f / (1080.f * 0.5f) - 1.f,  0.5f, 0.0f, 0.f,
		1920.f / (1920.f * 0.5f) - 1.f,  330.f / (1080.f * 0.5f) - 1.f,     1.0f, 0.0f, 0.f,
		962.5f / (1920.f * 0.5f) - 1.f,     330.f / (1080.f * 0.5f) - 1.f,    1.0f, 1.0f, 0.f,
		962.5f / (1920.f * 0.5f) - 1.f,     1080.f / (1080.f * 0.5f) - 1.f,  0.5f, 1.0f, 0.f,

		576.f / (1920.f * 0.5f) - 1.f,  324.f / (1080.f * 0.5f) - 1.f,  1.f, 0.0f, 2.f,
		576.f / (1920.f * 0.5f) - 1.f,  0.f / (1080.f * 0.5f) - 1.f,     1.f, 1.0f, 2.f,
		0.f / (1920.f * 0.5f) - 1.f,     0.f / (1080.f * 0.5f) - 1.f,    0.5f, 1.0f, 2.f,
		0.f / (1920.f * 0.5f) - 1.f,     324.f / (1080.f * 0.5f) - 1.f,  0.5f, 0.0f, 2.f,

		1920.f / (1920.f * 0.5f) - 1.f,  324.f / (1080.f * 0.5f) - 1.f,  0.5f, 0.0f, 2.f,
		1920.f / (1920.f * 0.5f) - 1.f,  0.f / (1080.f * 0.5f) - 1.f,     0.5f, 1.0f, 2.f,
		1344.f / (1920.f * 0.5f) - 1.f,     0.f / (1080.f * 0.5f) - 1.f,    0.f, 1.0f, 2.f,
		1344.f / (1920.f * 0.5f) - 1.f,     324.f / (1080.f * 0.5f) - 1.f,  0.f, 0.0f, 2.f,

		1344.f / (1920.f * 0.5f) - 1.f,  324.f / (1080.f * 0.5f) - 1.f,  1.f, 0.0f, 1.f,
		1344.f / (1920.f * 0.5f) - 1.f,  0.f / (1080.f * 0.5f) - 1.f,     1.f, 1.0f, 1.f,
		576.f / (1920.f * 0.5f) - 1.f,     0.f / (1080.f * 0.5f) - 1.f,    0.0f, 1.0f, 1.f,
		576.f / (1920.f * 0.5f) - 1.f,     324.f / (1080.f * 0.5f) - 1.f,  0.f, 0.0f, 1.f,
	};
	const float P2Verticestur[80] = {

0.f,	1.f,	1.f,	0.f,	3.f,
0.f,	-1.f,	1.f,	1.f,	3.f,
-1.f,	-1.f,	0.f,	1.f,	3.f,
-1.f,	1.f,	0.f,	0.f,	3.f,

1.f,	1.f,	1.f,	0.f,	4.f,
1.f,	-1.f,	1.f,	1.f,	4.f,
0.f,	-1.f,	0.f,	1.f,	4.f,
0.f,	1.f,	0.f,	0.f,	4.f,

957.5f / (1920.f * 0.5f) - 1.f,  1080.f / (1080.f * 0.5f),  1.f, 0.f, 1.f,
957.5f / (1920.f * 0.5f) - 1.f,  330.f / (1080.f * 0.5f),     1.f, 1.f, 1.f,
0.f / (1920.f * 0.5f) - 1.f,     330.f / (1080.f * 0.5f),     0.f, 1.f, 1.f,
0.f / (1920.f * 0.5f) - 1.f,     1080.f / (1080.f * 0.5f),  0.f, 0.f, 1.f,

1920.f / (1920.f * 0.5f),  1080.f / (1080.f * 0.5f),  1.f, 0.f, 1.f,
1920.f / (1920.f * 0.5f),  330.f / (1080.f * 0.5f),     1.f, 1.f, 1.f,
962.5f / (1920.f * 0.5f),     330.f / (1080.f * 0.5f),    0.f, 1.f, 1.f,
962.5f / (1920.f * 0.5f),     1080.f / (1080.f * 0.5f),  0.f, 0.f, 1.f
	};
	const float P2Verticesbf[80] = {

	0.f,	1.f,	1.f,	0.f,	3.f,
	0.f,	-1.f,	1.f,	1.f,	3.f,
	-1.f,	-1.f,	0.f,	1.f,	3.f,
	-1.f,	1.f,	0.f,	0.f,	3.f,

	1.f,	1.f,	1.f,	0.f,	4.f,
	1.f,	-1.f,	1.f,	1.f,	4.f,
	0.f,	-1.f,	0.f,	1.f,	4.f,
	0.f,	1.f,	0.f,	0.f,	4.f,

	957.5f / (1920.f * 0.5f) - 1.f,  1080.f / (1080.f * 0.5f),  1.f, 0.f, 0.f,
	957.5f / (1920.f * 0.5f) - 1.f,  330.f / (1080.f * 0.5f),     1.f, 1.f, 0.f,
	0.f / (1920.f * 0.5f) - 1.f,     330.f / (1080.f * 0.5f),     0.f, 1.f, 0.f,
	0.f / (1920.f * 0.5f) - 1.f,     1080.f / (1080.f * 0.5f),  0.f, 0.f, 0.f,

	1920.f / (1920.f * 0.5f),  1080.f / (1080.f * 0.5f),  1.f, 0.f, 0.f,
	1920.f / (1920.f * 0.5f),  330.f / (1080.f * 0.5f),     1.f, 1.f, 0.f,
	962.5f / (1920.f * 0.5f),     330.f / (1080.f * 0.5f),    0.f, 1.f, 0.f,
	962.5f / (1920.f * 0.5f),     1080.f / (1080.f * 0.5f),  0.f, 0.f, 0.f
	};

	const unsigned int P2Indices[24] = {
		0, 1, 3,
		1, 2, 3,

		4, 5, 7,
		5, 6, 7,

		8, 9, 11,
		9, 10, 11,

		12, 13, 15,
		13, 14, 15

	};


	const unsigned int TSIndices[30] = {
		0, 1, 3,
		1, 2, 3,

		4, 5, 7,
		5, 6, 7,

		8, 9, 11,
		9, 10, 11,

		12, 13, 15,
		13, 14, 15,

		16, 17, 19,
		17, 18, 19
	};

	unsigned int mcIndices[60] = {
		0,1,5,
		0,1,6,
		0,2,7,
		0,2,8,
		0,3,9,
		0,3,10,
		0,4,11,
		0,4,12,

		13,14,18,
		13,14,19,
		13,15,20,
		13,15,21,
		13,16,22,
		13,16,23,
		13,17,24,
		13,17,25,

		26,27,29,
		27,28,29,

		30,31,33,
		31,32,33
	};

	float mcVertices[170];
	cv::Mat checkCalibTex;
	cv::Mat camPicture;
	cv::Mat screenPicture;
	cv::Mat screenTex;
	cv::Mat qiTex;
	cv::Mat numTex[16];
	cv::Mat numTexVF[9];
	std::mutex camMutex, screensMutex, mcMutex, warnMutex;
	void initTSWindow();

	// STIMULI STUFF

	const float dfVertices[40] = {
		0.f,	1.f,	1.f,	0.f,	0.f,
		0.f,	-1.f,	1.f,	1.f,	0.f,
		-1.f,	-1.f,	0.f,	1.f,	0.f,
		-1.f,	1.f,	0.f,	0.f,	0.f,

		1.f,	1.f,	1.f,	0.f,	1.f,
		1.f,	-1.f,	1.f,	1.f,	1.f,
		0.f,	-1.f,	0.f,	1.f,	1.f,
		0.f,	1.f,	0.f,	0.f,	1.f
	};
	const int dfIndices[12] = {
		0, 1, 3,
		1, 2, 3,
		4, 5, 7,
		5, 6, 7
	};
	const float stimVertices[8] = {
				1.f,  1.f,
				1.f, -1.f,
				-1.f, -1.f,
				-1.f,  1.f
	};
	const unsigned int stimIndices[6] = {
		0, 1, 3,
		1, 2, 3,
	};
	int objCounter;

	void redraw();
	void initStimuliWindow();
	GLubyte* doScreenshot();
	void switchShowTex(int id, int val);
	void showDragonfly();
	void drawP2();

	float resolutionX, resolutionY;
	float physicalSizeX, physicalSizeY;
	float distanceToScreen;
	bool dragonflyOpened;
	float mmToPix, degToPix, pdToPix, pdTomm, pxTomm;

	GLfloat backgroundColorL[3];
	GLfloat backgroundColorR[3];
	GLfloat backgroundColorA[3];
	GLfloat backgroundColorAA[3];
	GLubyte* ssrawdata;
	GLFWwindow* stimuliWindow;
	cv::VideoCapture dfL, dfR;
	cv::VideoCapture bfVideo;
	cv::VideoCapture animalVideos[10];
	cv::VideoCapture eyeBlink;

	unsigned int VBOC, VAOC, EBOC;
	unsigned int VBOCA, VAOCA, EBOCA;
	unsigned int VBOM, VAOM, EBOM;
	unsigned int VBOA, VAOA, EBOA;
	unsigned int VBODF, VAODF, EBODF;
	unsigned int VBOF, VAOF, EBOF;
	unsigned int VBOAC, VAOAC, EBOAC;
	unsigned int VBOAC2, VAOAC2, EBOAC2;
	unsigned int VBOP2, VAOP2, EBOP2;
	unsigned int VBOR, VAOR, EBOR;
	unsigned int VBON, VAON, EBON;
	unsigned int VBOST, VAOST, EBOST;
	std::vector<StimuliObjects> stimuliObjects;
	unsigned int stimuliProgramID;
	unsigned int stimuliMProgramID;
	unsigned int stimuliAProgramID;
	unsigned int stimuliCAProgramID;
	unsigned int stimuliDFProgramID;
	unsigned int stimuliACProgramID;
	unsigned int stimuliAC2ProgramID;
	unsigned int stimuliFlyProgramID;
	unsigned int stimuliP2ProgramID;
	unsigned int stimuliRAPDProgramID;
	unsigned int stimuliNProgramID;
	unsigned int stimuliSTProgramID;

	GLuint dfTextures;
	GLuint ebTexture;
	GLuint bgTextures;
	GLuint vfnumTextures;
	GLuint p2Textures;
	GLuint nTexture;
	GLuint wcTextures;
	GLuint numTextures;
	cv::Mat dfLtex, dfRtex;
	cv::Mat bfTex, turTex;
	cv::Mat ebTex;
	cv::Mat ebTex1;
	cv::Mat amd_bgtexpic;
	GLuint amd_bgtex;
	std::chrono::time_point<std::chrono::system_clock> shaderTime;
	cv::Mat bflvtex;

	const char* stimuli_VertexSource =
		"	#version 330 core\n"
		"	layout(location = 0) in vec2 aPos;\n"
		"	out vec2 fPosition;\n"
		"	void main()\n"
		"	{\n"
		"		fPosition = aPos;\n"
		"		gl_Position = vec4(aPos, 0.0, 1.0);\n"
		"	}\n";

	const char* stimuli_FragmentSource =
		"#version 330\n"
		"out vec4 fColor;\n"
		"uniform int numc;\n"
		"uniform vec3 backclrl;\n"
		"uniform vec3 backclrr;\n"
		"uniform vec2 circles[10];\n"
		"uniform vec3 colors[10];\n"
		"uniform int stex[10];\n"
		"uniform float sizes[10];\n"
		"uniform sampler2DArray ourTexture;\n"
		"uniform sampler2D vfTextures;\n"
		"uniform bool backTex;\n"
		"uniform bool vfTex;\n"
		"uniform bool hollowCircle;\n"
		"uniform vec2 hcCorrds;\n"
		"uniform float hcR1;\n"
		"uniform float hcR2;\n"
		"void main() {\n"
		"	if(!backTex){\n"
		"		if(gl_FragCoord.x<1920)\n"
		"			fColor = vec4(backclrl, 1.0);\n"
		"		if(gl_FragCoord.x>1920)\n"
		"			fColor = vec4(backclrr, 1.0);\n"
		"	} else {\n"
		"		if(gl_FragCoord.x<=1920)\n"
		"			fColor = texture(ourTexture, vec3(gl_FragCoord.x / 1920, 1 - gl_FragCoord.y / 1080, 0));\n"
		"		if(gl_FragCoord.x>1920)\n"
		"			fColor = texture(ourTexture, vec3(gl_FragCoord.x / 1920 - 1, 1 - gl_FragCoord.y / 1080, 1));\n"
		"	}\n"
		"	for (int i = numc-1; i > -1; i--) {\n"
		"		float dist = distance(gl_FragCoord.xy, circles[i]);\n"
		"		float alpha = smoothstep(sizes[i]-sizes[i]/10.0, sizes[i], dist);\n"
		"		fColor = mix(vec4(colors[i],1.0), fColor, alpha);\n"
		"		if(dist<=sizes[i]&&vfTex&&stex[i]>0){\n"
		"			vec2 tCoord = (gl_FragCoord.xy - vec2(circles[i].x-sizes[i],circles[i].y-sizes[i]))/vec2(sizes[i]*2,sizes[i]*2);"
		"			vec4 tc = texture(vfTextures, -tCoord);\n"
		"		if(tc.x<0.8)\n"
		"			fColor = tc;\n"
		"		}\n"
		"	}\n"
		"	if(hollowCircle&&((gl_FragCoord.x<=1920&&hcCorrds.x<=1920)||(gl_FragCoord.x>1920&&hcCorrds.x>1920))){\n"
		"		float disthc = distance(gl_FragCoord.xy, hcCorrds);\n"
		"		if(disthc<hcR2&&disthc>=hcR1)\n"
		"			fColor = vec4(1.0,0.0,0.0,1.0);\n"
		"	}\n"
		"}\n";

	const char* saccadetask_FragmentSource =
		"#version 330\n"
		"out vec4 fColor;\n"
		"uniform vec3 redTargetColor;\n"
		"uniform vec3 backgroundColorL;\n"
		"uniform vec3 backgroundColorR;\n"
		"uniform vec2 greenTargetCoords;\n"
		"uniform vec2 redTargetCoords;\n"
		"uniform bool showRedTarget;\n"
		"uniform bool showGreenTarget;\n"
		"uniform float targetSize;\n"
		"void main() {\n"
		"	vec3 backClr;\n"
		"	float linewidth=targetSize/9.f;\n"
		"	if(gl_FragCoord.x<=1920)\n"
		"		backClr = backgroundColorL;\n"
		"	else\n"
		"		backClr = backgroundColorR;\n"
		"	fColor=vec4(backClr,1.0);\n"
		"	float greenDist = distance(gl_FragCoord.xy, greenTargetCoords);\n"
		"	if(greenDist<=targetSize&&showGreenTarget){\n"
		"		if(greenDist<=targetSize/7.f)\n"
		"			fColor=vec4(0.0,1.0,0.0,1.0);\n"
		"		else{\n"
		"			if((gl_FragCoord.x>greenTargetCoords.x-linewidth&&gl_FragCoord.x<greenTargetCoords.x+linewidth)||(gl_FragCoord.y>greenTargetCoords.y-linewidth&&gl_FragCoord.y<greenTargetCoords.y+linewidth))\n"
		"				fColor=vec4(backClr,1.0);\n"
		"			else\n"
		"				fColor=vec4(0.0,1.0,0.0,1.0);\n"
		"		}\n"
		"	}\n"
		"	float redDist = distance(gl_FragCoord.xy, redTargetCoords);\n"
		"	if(redDist<=targetSize&&showRedTarget){\n"
		"		if(redDist<=targetSize/7.f)\n"
		"			fColor=vec4(redTargetColor,1.0);\n"
		"		else{\n"
		"			if((gl_FragCoord.x>redTargetCoords.x-linewidth&&gl_FragCoord.x<redTargetCoords.x+linewidth)||(gl_FragCoord.y>redTargetCoords.y-linewidth&&gl_FragCoord.y<redTargetCoords.y+linewidth))\n"
		"				fColor=vec4(backClr,1.0);\n"
		"			else\n"
		"				fColor=vec4(redTargetColor,1.0);\n"
		"		}\n"
		"	}\n"
		"}\n";

	const char* vergence_VertexSource =
		"	#version 330 core\n"
		"	layout(location = 0) in vec2 aPos;\n"
		"	layout(location = 1) in vec3 aClr;\n"
		"	out vec4 aColor;\n"
		"	void main()\n"
		"	{\n"
		"		aColor = vec4(aClr, 1.0);\n"
		"		gl_Position = vec4(aPos, 0.0, 1.0);\n"
		"	}\n";

	const char* vergence_FragmentSource =
		"#version 330\n"
		"in vec4 aColor;\n"
		"out vec4 fColor;\n"
		"uniform int ntype;\n"
		"uniform int coordlx;\n"
		"uniform int coordly;\n"
		"uniform int coordrx;\n"
		"uniform int coordry;\n"
		"uniform sampler2D centralPic;\n"
		"void main()\n"
		"{\n"
		"	if(ntype==-1)\n"
		"		fColor = aColor;\n"
		"	else {\n"
		"		fColor = vec4(0.0,0.0,0.0,1.0);\n"
		"		if(gl_FragCoord.x>coordlx-140 && gl_FragCoord.x<coordlx+140 && gl_FragCoord.y>461&&gl_FragCoord.y<619){\n"
		"			vec2 tCoord = (gl_FragCoord.xy - vec2(coordlx-140,461))/vec2(280,158);"
		"			tCoord.y = 1-tCoord.y;\n"
		"			vec4 texColor = texture(centralPic, tCoord);\n"
		"			fColor = mix(fColor, texColor, texColor.w);\n"
		"		}\n"
		"		if(gl_FragCoord.x>coordrx-140 && gl_FragCoord.x<coordrx+140 && gl_FragCoord.y>461&&gl_FragCoord.y<619){\n"
		"			vec2 tCoord = (gl_FragCoord.xy - vec2(coordrx-140,461))/vec2(280,158);"
		"			tCoord.y = 1-tCoord.y;\n"
		"			vec4 texColor = texture(centralPic, tCoord);\n"
		"			fColor = mix(fColor, texColor, texColor.w);\n"
		"		}\n"
		"	}\n"
		"}\n";

	const char* dragonfly_VertexSource =
		"	#version 330 core\n"
		"	layout(location = 0) in vec2 aPos;\n"
		"	layout(location = 1) in vec3 aTexCoord;\n"
		"	out vec4 ourColor;\n"
		"	out vec3 TexCoord;\n"
		"	void main()\n"
		"	{\n"
		"		gl_Position = vec4(aPos, 0.0, 1.0);\n"
		"		TexCoord = aTexCoord;\n"
		"	}\n";

	const char* dragonfly_FragmentSource =
		"#version 330 core\n"
		"out vec4 FragColor;\n"
		"in vec3 TexCoord;\n"
		"uniform sampler2DArray ourTexture;\n"
		"void main()\n"
		"{\n"
		"	FragColor = texture(ourTexture, TexCoord);\n"
		"}\n";

	const char* pupil2_VertexSource =
		"	#version 330 core\n"
		"	layout(location = 0) in vec2 aPos;\n"
		"	layout(location = 1) in vec3 aTexCoord;\n"
		"	out vec4 ourColor;\n"
		"	out vec3 TexCoord;\n"
		"	void main()\n"
		"	{\n"
		"		gl_Position = vec4(aPos, 0.0, 1.0);\n"
		"		TexCoord = aTexCoord;\n"
		"	}\n";

	const char* pupil2_FragmentSource =
		"#version 330 core\n"
		"out vec4 FragColor;\n"
		"uniform int coordl;\n"
		"uniform int coordr;\n"
		"uniform float texnum;\n"
		"uniform int lW;\n"
		"uniform int rW;\n"
		"uniform int shl;\n"
		"uniform int shr;\n"
		"uniform vec3 backclrl;\n"
		"uniform vec3 backclrr;\n"
		"uniform sampler2D ourTexture;\n"
		"void main()\n"
		"{\n"
		"FragColor = vec4(0.0,0.0,0.0,1.0);\n"
		"if(lW==1&&gl_FragCoord.x<1920){\n"
		"	float dist = distance(gl_FragCoord.xy, vec2(coordl,540.0));\n"
		"	float alpha = smoothstep(500.0, 540.0, dist);\n"
		"	FragColor = mix(vec4(1.0,1.0,1.0,1.0), vec4(0.0,0.0,0.0,1.0), alpha);\n"
		"}\n"
		"if(rW==1&&gl_FragCoord.x>1920){\n"
		"	float dist = distance(gl_FragCoord.xy, vec2(coordr,540.0));\n"
		"	float alpha = smoothstep(500.0, 540.0, dist);\n"
		"	FragColor = mix(vec4(1.0,1.0,1.0,1.0), vec4(0.0,0.0,0.0,1.0), alpha);\n"
		"}\n"
		"if(gl_FragCoord.x>coordl-64 && gl_FragCoord.x<coordl+64 && gl_FragCoord.y>675&&gl_FragCoord.y<803&&shl==1){\n"
		"	vec2 tCoord = (gl_FragCoord.xy - vec2(coordl-64,675)) / vec2(128,128);"
		"	tCoord.y = 1-tCoord.y;\n"
		"	vec4 texColor = texture(ourTexture, tCoord);\n"
		"	if(texColor.x>0.4&&texColor.x<0.6&&texColor.y>0.4&&texColor.y<0.6&&texColor.z>0.4&&texColor.z<0.6)\n"
		"		texColor.w = 0.0;\n"
		"	FragColor = mix(FragColor, texColor, texColor.w);\n"
		"}\n"
		"if(gl_FragCoord.x>coordr-64 && gl_FragCoord.x<coordr+64 && gl_FragCoord.y>675&&gl_FragCoord.y<803&&shr==1){\n"
		"	vec2 tCoord = (gl_FragCoord.xy - vec2(coordr-64,675)) / vec2(128,128);"
		"	tCoord.y = 1-tCoord.y;\n"
		"	vec4 texColor = texture(ourTexture, tCoord);\n"
		"	if(texColor.x>0.4&&texColor.x<0.6&&texColor.y>0.4&&texColor.y<0.6&&texColor.z>0.4&&texColor.z<0.6)\n"
		"		texColor.w = 0.0;\n"
		"	FragColor = mix(FragColor, texColor, texColor.w);\n"
		"}\n"
		"}\n";

	const char* nystagmus_VertexSource =
		"	#version 330 core\n"
		"	layout(location = 0) in vec2 aPos;\n"
		"	layout(location = 1) in vec3 aTexCoord;\n"
		"	out vec4 ourColor;\n"
		"	out vec3 TexCoord;\n"
		"	void main()\n"
		"	{\n"
		"		gl_Position = vec4(aPos, 0.0, 1.0);\n"
		"		TexCoord = aTexCoord;\n"
		"	}\n";

	const char* nystagmus_FragmentSource =
		"#version 330 core\n"
		"out vec4 FragColor;\n"
		"uniform int coordlx;\n"
		"uniform int coordly;\n"
		"uniform int coordrx;\n"
		"uniform int coordry;\n"
		"uniform int ntype;\n"
		"uniform int nshowl;\n"
		"uniform int nshowr;\n"
		"uniform int tszx;\n"
		"uniform int tszy;\n"
		"uniform sampler2D centralPic;\n"
		"void main()\n"
		"{\n"
		"FragColor = vec4(0.5,0.5,0.5,1.0);\n"
		"float distal = distance(gl_FragCoord.xy,vec2(coordlx,coordly));\n"
		"float distar = distance(gl_FragCoord.xy,vec2(coordrx,coordry));\n"
		"if(gl_FragCoord.x>coordlx-tszx && gl_FragCoord.x<coordlx+tszx && gl_FragCoord.y>coordly-tszy && gl_FragCoord.y<coordly+tszy && ntype==1 && nshowl==1){\n"
		"	vec2 tCoord = (gl_FragCoord.xy - vec2(coordlx-tszx,coordly-tszy))/vec2(tszx*2,tszy*2);"
		"	tCoord.y = 1-tCoord.y;\n"
		"	vec4 texColor = texture(centralPic, tCoord);\n"
		"	FragColor = mix(FragColor, texColor, texColor.w);\n"
		"}\n"
		"if(gl_FragCoord.x>coordrx-tszx && gl_FragCoord.x<coordrx+tszx && gl_FragCoord.y>coordry-tszy && gl_FragCoord.y<coordry+tszy && ntype==1 && nshowr==1){\n"
		"	vec2 tCoord = (gl_FragCoord.xy - vec2(coordrx-tszx,coordry-tszy))/vec2(tszx*2,tszy*2);"
		"	tCoord.y = 1-tCoord.y;\n"
		"	vec4 texColor = texture(centralPic, tCoord);\n"
		"	FragColor = mix(FragColor, texColor, texColor.w);\n"
		"}\n"
		"if (nshowl == 1 && ntype==0 && distal <= tszx)\n"
		"	FragColor = vec4(0.0, 1.0, 0.0, 1.0);\n"
		"if (nshowr == 1 && ntype==0 && distar <= tszx)\n"
		"	FragColor = vec4(0.0, 1.0, 0.0, 1.0);\n"
		"}\n";
	const char* amsler_VertexSource =
		"	#version 330 core\n"
		"	layout(location = 0) in vec2 aPos;\n"
		"	out vec2 fPosition;\n"
		"	void main()\n"
		"	{\n"
		"		fPosition = aPos;\n"
		"		gl_Position = vec4(aPos, 0.0, 1.0);\n"
		"	}\n";

	const char* amsler_FragmentSource = 
		"#version 330\n"
		"out vec4 fColor;\n"
		"uniform sampler2DArray ourTexture;\n"
		"uniform int dtp;\n"
		"uniform int jw;\n"
		"uniform int cds;\n"
		"uniform int redon;\n"
		"uniform vec2 initpos;\n"
		"uniform int blues[16];\n"
		"void main()\n"
		"{\n"
		"	fColor = vec4(1.0, 1.0, 1.0, 1.0);\n"
		"   if(jw==1)\n"
		"	{\n"
		"		if (gl_FragCoord.x > initpos.x - dtp * 10.0 - 4.0 && gl_FragCoord.x < initpos.x + dtp * 10.0 + 4.0 && gl_FragCoord.y<initpos.y + dtp * 10.0 + 4.0 && gl_FragCoord.y>initpos.y - dtp * 10.0 - 4.0)\n"
		"		{\n"
		"			if(redon==1)\n"
		"			{\n"
		"				for (int i = -2; i < 2; i++)\n"
		"				{\n"
		"					for (int j = -2; j < 2; j++)\n"
		"					{\n"
		"						float vert = initpos.x + dtp * 5 * float(i) + dtp * 2.5;\n"
		"						float hor = initpos.y + dtp * 5 * float(j) + dtp * 2.5;\n"
		"						if ((gl_FragCoord.x > vert - 30.0 && gl_FragCoord.x < vert + 30.0) && (gl_FragCoord.y > hor - 30.0 && gl_FragCoord.y < hor + 30.0))\n"
		"						{\n"
		"							vec2 tCoord = (gl_FragCoord.xy - vec2(vert-30,hor-30))/vec2(60,60);"
		"							tCoord.y = 1 - tCoord.y;\n"
		"							tCoord.x = 1 - tCoord.x;\n"
		"							float zc = 15 - ((j + 2) * 4 + i + 2);\n"
		"							fColor = texture(ourTexture, vec3(tCoord,zc));\n"
		"						}\n"
		"					}\n"
		"				}\n"
		"				for (int i = -2; i <= 2; i++)\n"
		"				{\n"
		"					for (int j = -2; j <= 2; j++)\n"
		"					{\n"
		"						float vert = initpos.x + dtp * 5 * float(i);\n"
		"						float hor = initpos.y + dtp * 5 * float(j);\n"
		"						if ((gl_FragCoord.x > vert - 2.0 && gl_FragCoord.x < vert + 2.0) || (gl_FragCoord.y > hor - 2.0 && gl_FragCoord.y < hor + 2.0))\n"
		"						{\n"
		"							fColor = vec4(1.0, 0.0, 0.0, 1.0);\n"
		"						}\n"
		"					}\n"
		"				}\n"
		"				for (int i = -2; i < 2; i++)\n"
		"				{\n"
		"					for (int j = -2; j < 2; j++)\n"
		"					{\n"
		"						float vert = initpos.x + dtp * 5 * float(i) + dtp * 2.5;\n"
		"						float hor = initpos.y + dtp * 5 * float(j) + dtp * 2.5;\n"
		"						if (((gl_FragCoord.x > vert+dtp*2.5 - 4.0 && gl_FragCoord.x < vert+dtp*2.5 + 4.0) && (gl_FragCoord.y > hor - dtp*2.5 - 4.0 && gl_FragCoord.y < hor + dtp*2.5 + 4.0))||\n"
		"						((gl_FragCoord.x > vert-dtp*2.5 - 4.0 && gl_FragCoord.x < vert-dtp*2.5 + 4.0) && (gl_FragCoord.y > hor - dtp*2.5 - 4.0 && gl_FragCoord.y < hor + dtp*2.5 + 4.0))||\n"
		"						((gl_FragCoord.x > vert-dtp*2.5 - 4.0 && gl_FragCoord.x < vert+dtp*2.5 + 4.0) && (gl_FragCoord.y > hor - dtp*2.5 - 4.0 && gl_FragCoord.y < hor - dtp*2.5 + 4.0))||\n"
		"						((gl_FragCoord.x > vert-dtp*2.5 - 4.0 && gl_FragCoord.x < vert+dtp*2.5 + 4.0) && (gl_FragCoord.y > hor + dtp*2.5 - 4.0 && gl_FragCoord.y < hor + dtp*2.5 + 4.0)))\n"
		"						{\n"
		"							int zc = 15 - ((j + 2) * 4 + i + 2);\n"
		"							if(blues[zc]==1)"
		"								fColor = vec4(0.0, 0.0, 1.0, 1.0);\n"
		"						}\n"
		"					}\n"
		"				}\n"
		"			}\n"
		"		}\n"
		"		if (gl_FragCoord.x > initpos.x - dtp * 10.0 - 1.0 && gl_FragCoord.x < initpos.x + dtp * 10.0 + 1.0 && gl_FragCoord.y<initpos.y + dtp * 10.0 + 1.0 && gl_FragCoord.y>initpos.y - dtp * 10.0 - 1.0)\n"
		"		{\n"
		"			for (int i = -10; i <= 10; i++)\n"
		"			{\n"
		"				for (int j = -10; j <= 10; j++)\n"
		"				{\n"
		"					float vert = initpos.x + dtp * float(i);\n"
		"					float hor = initpos.y + dtp * float(j);\n"
		"					if ((gl_FragCoord.x > vert - 1.0 && gl_FragCoord.x < vert + 1.0) || (gl_FragCoord.y > hor - 1.0 && gl_FragCoord.y < hor + 1.0))\n"
		"					{\n"
		"						fColor = vec4(0.0, 0.0, 0.0, 1.0);\n"
		"					}\n"
		"				}\n"
		"			}\n"
		"		}\n"
		"		float dista = distance(vec2(initpos.x, initpos.y), gl_FragCoord.xy);\n"
		"		if (dista < cds)\n"
		"			fColor = vec4(0.0, 0.0, 0.0, 1.0);\n"
		"	}\n"
		"}\n";


	const char* contrast_FragmentSource =
		"#version 330\n"
		"out vec4 fColor;\n"
		"uniform int purebpx;\n"
		"uniform int purewpx;\n"
		"uniform float gbclr;\n"
		"uniform float gwclr;\n"
		"uniform vec3 bclr;\n"
		"uniform vec3 wclr;\n"
		"uniform vec3 backClrL;\n"
		"uniform vec3 backClrR;\n"
		"uniform vec2 tPosition;\n"
		"uniform int showRim;\n"
		"uniform bool hollowCircle;\n"
		"uniform float hcR1;\n"
		"uniform float hcR2;\n"

		"void main()\n"
		"{\n"
		"	int graybpx = purebpx+1;\n"
		"	int graywpx = purewpx+1;\n"
		"	float dista = distance(gl_FragCoord.xy,tPosition.xy);\n"
		"	if(gl_FragCoord.x<=1920.0)\n"
	"				fColor = vec4(backClrL, 1.0);\n"
		"	else \n"
		"			fColor = vec4(backClrR, 1.0);\n"
//		"	if(hidetarget==0){\n"
		"	if (gl_FragCoord.x < tPosition.x + 0.5\n"
		"		&&gl_FragCoord.x >= tPosition.x + 0.5 - float(graybpx)\n"
		"		&&gl_FragCoord.y >= tPosition.y - 0.5\n"
		"		&&gl_FragCoord.y < tPosition.y - 0.5 + float(graybpx)&&showRim>0)\n"
		"		fColor = vec4(gbclr, gbclr, gbclr, 1.0);\n"
		"	if (gl_FragCoord.x < tPosition.x + 0.5\n"
		"		&&gl_FragCoord.x >= tPosition.x + 0.5 - float(purebpx)\n"
		"		&&gl_FragCoord.y >= tPosition.y - 0.5\n"
		"		&&gl_FragCoord.y < tPosition.y - 0.5 + float(purebpx))\n"
		"		fColor = vec4(bclr, 1.0);\n"
		"	if (gl_FragCoord.x >= tPosition.x + 0.5\n"
		"		&&gl_FragCoord.x < tPosition.x + 0.5 + float(graywpx)\n"
		"		&&gl_FragCoord.y >= tPosition.y - 0.5\n"
		"		&&gl_FragCoord.y < tPosition.y - 0.5 + float(graywpx)&&showRim>0)\n"
		"		fColor = vec4(gwclr, gwclr, gwclr, 1.0);\n"
		"	if (gl_FragCoord.x >= tPosition.x + 0.5\n"
		"		&&gl_FragCoord.x < tPosition.x + 0.5 + float(purewpx)\n"
		"		&&gl_FragCoord.y >= tPosition.y - 0.5\n"
		"		&&gl_FragCoord.y < tPosition.y - 0.5 + float(purewpx))\n"
		"		fColor = vec4(wclr, 1.0);\n"
//		"	}\n"
//		"	if (showCircle == 1 && dista >= 49 && dista <= 51)\n"
//		"		fColor = vec4(1.0, 0.0, 0.0, 1.0);\n"
		"	if(hollowCircle&&((gl_FragCoord.x<=1920&&tPosition.x<=1920)||(gl_FragCoord.x>1920&&tPosition.x>1920))){\n"
		"		float disthc = distance(gl_FragCoord.xy, tPosition);\n"
		"		if(disthc<hcR2&&disthc>=hcR1)\n"
		"			fColor = vec4(1.0,0.0,0.0,1.0);\n"
		"	}\n"
		"}\n";
};