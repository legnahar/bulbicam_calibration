#pragma once

#include "Tests.h"
#include <chrono>

#define pursuitssaccadesversion "0.9.20211021"

extern CTest* currentTest;
extern ofstream qualityLog;
extern ofstream testLog;
extern string testLogsPath;

enum pss_sacstates
{
	PSS_VERTICAL,
	PSS_HORSHORT,
	PSS_HORLONG
};

struct ps_sacTarget
{
	Point2d OS_PX, OD_PX;
	Point2f OS_D, OD_D;
	bool showOS, showOD;
};

class CSaccadesTest : public CTest
{
public:
	CSaccadesTest();
	void ProcessETData(vector<ETData> etData);
private:
	ps_sacTarget ps_stimLS, ps_stimC, ps_stimLL, ps_stimRL, ps_stimRS, ps_stimU, ps_stimD;
	Point2d ps_greenOS, ps_greenOD;
	Point2f ps_degstimos, ps_degstimod;
	Point2d ps_stimOS, ps_stimOD;
	int ps_greenOSID, ps_greenODID;
	int ps_state;
	float ps_calibrationcoords[4];
	int ps_calibcount;
	int extremex;
	vector<ps_sacTarget> targets;
	float nextdelay;
	int sacamount;
};

CSaccadesTest::CSaccadesTest()
{
	webComm->openLog();
	nlohmann::json js221;
	js221["chartTypeString"] = "PURSUITS_AND_SACCADES";
	js221["message_type"] = MESSAGE_TYPE::START_TEST;
	js221["metadata"] = versionInfoForHub;
	js221["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	js221["target"] = "SACCADES";
	webComm->sendJSON(js221);

	eyetrackingOn = true;

	std::ostringstream vffn;
	vffn << testLogsPath << "sac_" << patientInfo.examID << "_" << std::chrono::system_clock::now().time_since_epoch().count() << ".csv";
	std::string vffns = vffn.str();
	testLog.open(vffns);


	int ctocOD = lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOD / 7.);
	int ctocOS = lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOD / 7.);

	ps_stimC.OD_PX = Point(2880, 540);
	ps_stimC.OS_PX = Point(960, 540);
	ps_stimC.OD_D = Point2d(0, 0);
	ps_stimC.OS_D = Point2d(0, 0);
	ps_stimC.showOD = true;
	ps_stimC.showOS = true;

	ps_stimLS.OD_PX = lensDist->jumpTo(ps_stimC.OD_PX, Point2d(7.5, 0));
	ps_stimLS.OS_PX = lensDist->jumpTo(ps_stimC.OS_PX, Point2d(-7.5, 0));
	ps_stimLS.OD_D = Point2d(-7.5, 0);
	ps_stimLS.OS_D = Point2d(-7.5, 0);
	ps_stimLS.showOD = true;
	ps_stimLS.showOS = true;

	ps_stimRS.OD_PX = lensDist->jumpTo(ps_stimC.OD_PX, Point2d(-7.5, 0));
	ps_stimRS.OS_PX = lensDist->jumpTo(ps_stimC.OS_PX, Point2d(7.5, 0));
	ps_stimRS.OD_D = Point2d(7.5, 0);
	ps_stimRS.OS_D = Point2d(7.5, 0);
	ps_stimRS.showOD = true;
	ps_stimRS.showOS = true;

	ps_stimLL.OD_PX = lensDist->jumpTo(ps_stimC.OD_PX, Point2d(20, 0));
	ps_stimLL.OS_PX = lensDist->jumpTo(ps_stimC.OS_PX, Point2d(-20, 0));
	ps_stimLL.OD_D = Point2d(-20, 0);
	ps_stimLL.OS_D = Point2d(-20, 0);
	ps_stimLL.showOD = true;
	ps_stimLL.showOS = false;

	ps_stimRL.OD_PX = lensDist->jumpTo(ps_stimC.OD_PX, Point2d(-20, 0));
	ps_stimRL.OS_PX = lensDist->jumpTo(ps_stimC.OS_PX, Point2d(20, 0));
	ps_stimRL.OD_D = Point2d(20, 0);
	ps_stimRL.OS_D = Point2d(20, 0);
	ps_stimRL.showOD = false;
	ps_stimRL.showOS = true;

	ps_stimU.OD_PX = lensDist->jumpTo(ps_stimC.OD_PX, Point2d(0, 9));
	ps_stimU.OS_PX = lensDist->jumpTo(ps_stimC.OS_PX, Point2d(0, 9));
	ps_stimU.OD_D = Point2d(0, 9);
	ps_stimU.OS_D = Point2d(0, 9);
	ps_stimU.showOD = true;
	ps_stimU.showOS = true;

	ps_stimD.OD_PX = lensDist->jumpTo(ps_stimC.OD_PX, Point2d(0, -9));
	ps_stimD.OS_PX = lensDist->jumpTo(ps_stimC.OS_PX, Point2d(0, -9));
	ps_stimD.OD_D = Point2d(0, -9);
	ps_stimD.OS_D = Point2d(0, -9);
	ps_stimD.showOD = true;
	ps_stimD.showOS = true;

	ps_stimC.OD_PX.x -= lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOD / 7.);
	ps_stimC.OS_PX.x += lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOS / 7.);

	ps_stimLS.OD_PX.x -= lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOD / 7.);
	ps_stimLS.OS_PX.x += lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOS / 7.);

	ps_stimRS.OD_PX.x -= lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOD / 7.);
	ps_stimRS.OS_PX.x += lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOS / 7.);

	ps_stimLL.OD_PX.x -= lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOD / 7.);
	ps_stimLL.OS_PX.x += lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOS / 7.);

	ps_stimRL.OD_PX.x -= lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOD / 7.);
	ps_stimRL.OS_PX.x += lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOS / 7.);

	ps_stimU.OD_PX.x -= lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOD / 7.);
	ps_stimU.OS_PX.x += lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOS / 7.);

	ps_stimD.OD_PX.x -= lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOD / 7.);
	ps_stimD.OS_PX.x += lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOS / 7.);

	StimuliObjects s1;

	s1.objectColor[0] = 0;
	s1.objectColor[1] = 1;
	s1.objectColor[2] = 0;

	ps_greenOD = ps_stimC.OD_PX;

	s1.objectCoordinates[0] = ps_greenOD.x;
	s1.objectCoordinates[1] = ps_greenOD.y;

	s1.objectSize = 27.f;
	ps_greenODID = display.addObject(s1);

	StimuliObjects s2;

	ps_greenOS = ps_stimC.OS_PX;

	s2.objectColor[0] = 0;
	s2.objectColor[1] = 1;
	s2.objectColor[2] = 0;
	s2.objectCoordinates[0] = ps_greenOS.x;
	s2.objectCoordinates[1] = ps_greenOS.y;

	s2.objectSize = 27.f;
	ps_greenOSID = display.addObject(s2);

	display.redrawPending = true;

	ps_state = PSS_VERTICAL;

	nextdelay = 2 + (rand() % 2000) / 1000.;

	ps_degstimod = Point(0, 0);
	ps_degstimos = Point(0, 0);

	sacamount = 0;

	targets.push_back(ps_stimU);
	targets.push_back(ps_stimD);
	targets.push_back(ps_stimU);
	targets.push_back(ps_stimD);
	targets.push_back(ps_stimU);
	targets.push_back(ps_stimD);
	targets.push_back(ps_stimC);

	glfwSetTime(0);
}

void CSaccadesTest::ProcessETData(vector<ETData> etData)
{
	double ggt = glfwGetTime();
	Point2d eyeOS = Point(0, 0);
	Point2d eyeOD = Point(0, 0);

	eyeOS = (etData[1].tandemGlints.bpG + etData[1].tandemGlints.dpG) / 2.;
	eyeOD = (etData[0].tandemGlints.bpG + etData[0].tandemGlints.dpG) / 2.;

	testLog << "1" << ";"
		<< eyeOS.x << ";"
		<< eyeOS.y << ";"
		<< eyeOD.x << ";"
		<< eyeOD.y << ";"
		<< ps_degstimos.x << ";"
		<< ps_degstimos.y << ";"
		<< ps_degstimod.x << ";"
		<< ps_degstimod.y << ";"
		<< etData[0].tandemGlints.bpG.x << ";"
		<< etData[0].tandemGlints.bpG.y << ";"
		<< etData[0].tandemGlints.dpG.x << ";"
		<< etData[0].tandemGlints.dpG.y << ";"
		<< etData[1].tandemGlints.bpG.x << ";"
		<< etData[1].tandemGlints.bpG.y << ";"
		<< etData[1].tandemGlints.dpG.x << ";"
		<< etData[1].tandemGlints.dpG.y << ";" 
		<< ggt << endl;

	nlohmann::json pas_data;
	pas_data["chartTypeString"] = "PURSUITS_AND_SACCADES";
	pas_data["message_type"] = MESSAGE_TYPE::DATA_PACKAGE;
	pas_data["stimuliOSx"] = ps_degstimos.x;
	pas_data["stimuliOSy"] = ps_degstimos.y;
	pas_data["stimuliODx"] = ps_degstimod.x;
	pas_data["stimuliODy"] = ps_degstimod.y;
	pas_data["eyeOSx"] = eyeOS.x;
	pas_data["eyeOSy"] = 1000-eyeOS.y;
	pas_data["eyeODx"] = eyeOD.x;
	pas_data["eyeODy"] = 1000-eyeOD.y;
	pas_data["message_type"] = MESSAGE_TYPE::DATA_PACKAGE;
	pas_data["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	pas_data["target"] = "SACCADES";
	webComm->sendJSON(pas_data);

	if (ggt>=nextdelay)
	{
		glfwSetTime(0);
		if (targets.size() > 0)
		{
			ps_greenOS = targets[0].OS_PX;
			ps_greenOD = targets[0].OD_PX;
			ps_degstimos = targets[0].OS_D;
			ps_degstimod = targets[0].OD_D;
			if(targets[0].showOS)
				display.moveObject(ps_greenOSID, targets[0].OS_PX.x, targets[0].OS_PX.y);
			else
				display.moveObject(ps_greenOSID, 9999, 9999);
			if (targets[0].showOD)
				display.moveObject(ps_greenODID, targets[0].OD_PX.x, targets[0].OD_PX.y);
			else
				display.moveObject(ps_greenODID, 9999, 9999);
			targets.erase(targets.begin());
			nextdelay = 2 + (rand() % 2000) / 1000.;
			display.redrawPending = true;
		}
		else
		{
			if (ps_state==PSS_VERTICAL)
			{
				targets.push_back(ps_stimLS);
				targets.push_back(ps_stimRS);
				targets.push_back(ps_stimLS);
				targets.push_back(ps_stimRS);
				targets.push_back(ps_stimLS);
				targets.push_back(ps_stimRS);
				targets.push_back(ps_stimC);
				nextdelay = 0;
				ps_state = PSS_HORSHORT;
			}
			else if (ps_state == PSS_HORSHORT)
			{
				targets.push_back(ps_stimLL);
				targets.push_back(ps_stimRL);
				targets.push_back(ps_stimLL);
				targets.push_back(ps_stimRL);
				targets.push_back(ps_stimLL);
				targets.push_back(ps_stimRL);
				nextdelay = 0;
				ps_state = PSS_HORLONG;
			}
			else if (ps_state == PSS_HORLONG)
			{
				nlohmann::json pas_stop;
				pas_stop["chartTypeString"] = "PURSUITS_AND_SACCADES";
				pas_stop["message_type"] = MESSAGE_TYPE::STOP_TEST;
				pas_stop["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
				pas_stop["target"] = "SACCADES";
				webComm->sendJSON(pas_stop);
				webComm->closeLog();
				qualityLog.close();
				eyetrackingOn = false;
				display.setDragonfly();
				currentTest = nullptr;
				testLog.close();
			}
		}
		return;
	}
}