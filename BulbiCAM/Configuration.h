#pragma once

#include <string>
#include <map>
#include <iostream>

class Configuration
{
public:
	Configuration();
	~Configuration();


	std::string configurationIP;

	std::string GetConfigSetting(const std::string& key) const 
	{ 
		auto p = m_configurations.find(key);
		if (p != m_configurations.end())
			return p->second; 
		std::cerr << "CONFIG ERROR : " << key << " not found" << std::endl;
		return "0"; 
	}

protected:
	std::map<std::string, std::string> m_configurations;
};

