#include "Configuration.h"

//#define WIN32_LEAN_AND_MEAN
//#include <Windows.h>
#include <Shlwapi.h>

#include <fstream>

#include <iostream>

#include <regex>


Configuration::Configuration()
{
	wchar_t path[MAX_PATH];
	HMODULE hModule = GetModuleHandleW(NULL);
	GetModuleFileName(hModule, path, MAX_PATH);
	PathRemoveFileSpec(path);

	std::wstring configFilePath(path);
	configFilePath += L"\\bulbicam.cfg";

	
	std::ifstream infile(configFilePath);

	std::regex regx("(.*)=(.*)");

	std::string str;
	std::string strIP;

	if (infile.is_open()) {
		while (infile >> str)
		{
			if (!str.empty())
			{
				if(str.substr(0,1)!="[")
					if (std::regex_match(str, regx))
					{
						std::smatch match;
						if (std::regex_search(str, match, regx) && match.size() > 2) 
						{
							const std::string key = match.str(1);
							const std::string val = match.str(2);

							m_configurations[key] = val;
						}
				}
			}
		}
	}	
}


Configuration::~Configuration()
{
}
