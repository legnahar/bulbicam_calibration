#pragma once

#include "Tests.h"
#include "pupiltest4.h"

#define pupil2version "0.9.20210827"

extern atomic<bool> ptosis_pic1sent, ptosis_pic2sent, ptosis_pic3sent, ptosis_needpic;
extern bool picBP;
extern CTest *currentTest;
extern ofstream testLog;
extern bool saveFrameSignal;
extern int savedFramesCurrentID;
extern vector<frameForAI> savedFrames;

class CPupil2Test : public CTest
{
public:
	CPupil2Test(bool secondround, bool ispupil4);
	void ProcessETData(vector<ETData> etData);
	int testNum = 888;
private:
	const int pupil_bgc[25][2] =
	{ { 0,0 },
	{ 0,0 },
	{ 0,0 },
	{ 1,1 },
	{ 1,1 },
	{ 0,0 },
	{ 0,0 },
	{ 0,0 },
	{ 1,0 },
	{ 1,0 },
	{ 0,0 },
	{ 0,0 },
	{ 0,0 },
	{ 0,1 },
	{ 0,1 },
	{ 1,1 },
	{ 1,1 },
	{ 1,0 },
	{ 1,0 },
	{ 1,0 },
	{ 1,1},
	{ 1,1},
	{ 0,1},
	{ 0,1},
	{ 0,1}
	};
	float pupil_stimDuration;
	int pupil_state;
	bool tworounds;
	bool thirdround;
	string charttypestring;
	float convertToMM(RotatedRect pupil, int eye);
	int pupil_whitecircleIDs[2];
	int pupil_redcircleIDs[2];
	float ppd2;
	bool picSavedThisRound;
};

CPupil2Test::CPupil2Test(bool isPupil3, bool ispupil4)
{
	savedFrames.clear();
	display.stimuliType = STIMULI;
	tworounds = true;
	picSavedThisRound = false;
	if (isPupil3)
		charttypestring = "PUPILLARY_EVALUATION3";
	else
		charttypestring = "PUPILLARY_EVALUATION2";

	webComm->openLog();
	nlohmann::json pupil_starttestmessage;
	pupil_starttestmessage["chartTypeString"] = charttypestring;
	pupil_starttestmessage["message_type"] = MESSAGE_TYPE::START_TEST;
	pupil_starttestmessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	pupil_starttestmessage["metadata"] = versionInfoForHub;
	webComm->sendJSON(pupil_starttestmessage);

	nlohmann::json pupil_startpupildatamessage;
	pupil_startpupildatamessage["chartTypeString"] = charttypestring;
	pupil_startpupildatamessage["message_type"] = MESSAGE_TYPE::START_PUPIL_DATA_TRANSMISSION;
	pupil_startpupildatamessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	webComm->sendJSON(pupil_startpupildatamessage);

	nlohmann::json pupil_startbgdatamessage;
	pupil_startbgdatamessage["chartTypeString"] = charttypestring;
	pupil_startbgdatamessage["message_type"] = MESSAGE_TYPE::START_BACKGROUND_DATA_TRANSMISSION;
	pupil_startbgdatamessage["backgroundColorOS"] = pupil_bgc[0][0];
	pupil_startbgdatamessage["backgroundColorOD"] = pupil_bgc[0][1];
	pupil_startbgdatamessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	webComm->sendJSON(pupil_startbgdatamessage);

	display.setBackground(0, 0, 0);
	//ppd2 = (lensDist->PPD / 2.) - 0.5;
	ppd2 = 15;
	StimuliObjects t1;

	t1.objectColor[0] = 1;
	t1.objectColor[1] = 0;
	t1.objectColor[2] = 0;

	t1.objectCoordinates[0] = 960 + mmtopixels(ppd2);
	t1.objectCoordinates[1] = 540;

	t1.objectSize = 20;

	pupil_redcircleIDs[0] = display.addObject(t1);

	StimuliObjects t2;

	t2.objectColor[0] = 1;
	t2.objectColor[1] = 0;
	t2.objectColor[2] = 0;

	t2.objectCoordinates[0] = 2880 - mmtopixels(ppd2);
	t2.objectCoordinates[1] = 540;

	t2.objectSize = 20;

	pupil_redcircleIDs[1] = display.addObject(t2);

	display.redrawPending = true;
	pupil_state = 0;
	pupil_stimDuration = 0;

	ptosis_pic1sent = false;
	ptosis_pic2sent = false;
	ptosis_pic3sent = false;
	ptosis_needpic = false;

	glfwSetTime(0);
	eyetrackingOn = true;
}

float CPupil2Test::convertToMM(RotatedRect pupil, int eye)
{
	float sz = MAX(pupil.size.height,pupil.size.width)/14.5;
	float lll = 0;
	if (eye == 0)
		lll = -lensDist->dioptersOD;
	else
		lll = -lensDist->dioptersOS;
	float perc = 0;
	if (lll > 0)
		perc = 0.0232;
	else if (lll < 0)
		perc = 0.0178;
	float k = perc * lll;
	return sz - sz * k;
}

void CPupil2Test::ProcessETData(vector<ETData> etData)
{
	pupil_stimDuration = glfwGetTime();
	if (savedFrames.size() > 0)
	{
		std::ostringstream fnShort;
		fnShort << "pupil_" << patientInfo.examID << "_" << savedFrames[0].ts << format("_%d", savedFramesCurrentID) << ".png";
		std::ostringstream fnFull;
		fnFull << picturesLogsPath << fnShort.str();
		if(savedFrames[0].picture.rows&& savedFrames[0].picture.cols)
			cv::imwrite(fnFull.str(), savedFrames[0].picture);
		nlohmann::json pupil_picforai;
		pupil_picforai["chartTypeString"] = charttypestring;
		pupil_picforai["message_type"] = 22;
		pupil_picforai["filename"] = fnShort.str();
		pupil_picforai["timestamp"] = savedFrames[0].ts;
		webComm->sendJSON(pupil_picforai);
		savedFrames.erase(savedFrames.begin());


	}
	if ((pupil_stimDuration > 1 && !(pupil_state == 27 && (!ptosis_pic1sent || !ptosis_pic2sent || !ptosis_pic3sent)))|| pupil_stimDuration > 7)
	{
		pupil_state++;
		picSavedThisRound = false;
		saveFrameSignal = false;
		if (pupil_bgc[pupil_state][0] == 1)
			display.setBackgroundL(display.maxLuminosityOS, display.maxLuminosityOS, display.maxLuminosityOS);
		else
			display.setBackgroundL(0, 0, 0);
		if (pupil_bgc[pupil_state][1] == 1)
			display.setBackgroundR(display.maxLuminosityOD, display.maxLuminosityOD, display.maxLuminosityOD);
		else
			display.setBackgroundR(0, 0, 0);

//		display.setBackground(0.0, 0.0, 0.0);
		display.redrawPending = true;

		nlohmann::json pupil_stopbgdatamessage;
		pupil_stopbgdatamessage["chartTypeString"] = charttypestring;
		pupil_stopbgdatamessage["message_type"] = MESSAGE_TYPE::STOP_BACKGROUND_DATA_TRANSMISSION;
		pupil_stopbgdatamessage["pulseduration"] = 2.5;
		pupil_stopbgdatamessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
		webComm->sendJSON(pupil_stopbgdatamessage);

		if (pupil_state > 24)
		{
			nlohmann::json pupil_stoppupildatamessage;
			pupil_stoppupildatamessage["chartTypeString"] = charttypestring;
			pupil_stoppupildatamessage["message_type"] = MESSAGE_TYPE::STOP_PUPIL_DATA_TRANSMISSION;
			pupil_stoppupildatamessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
			webComm->sendJSON(pupil_stoppupildatamessage);

			if (!tworounds)
			{
				display.clearObjects();
				currentTest = new CPupil4Test();
				currentTest->testType = "PUPILLARY_EVALUATION2";
				return;
			}
			else
			{
				nlohmann::json pupil_startpupildatamessage;
				pupil_startpupildatamessage["chartTypeString"] = charttypestring;
				pupil_startpupildatamessage["message_type"] = MESSAGE_TYPE::START_PUPIL_DATA_TRANSMISSION;
				pupil_startpupildatamessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
				webComm->sendJSON(pupil_startpupildatamessage);

				nlohmann::json pupil_startbgdatamessage;
				pupil_startbgdatamessage["chartTypeString"] = charttypestring;
				pupil_startbgdatamessage["message_type"] = MESSAGE_TYPE::START_BACKGROUND_DATA_TRANSMISSION;
				pupil_startbgdatamessage["backgroundColorOS"] = pupil_bgc[0][0];
				pupil_startbgdatamessage["backgroundColorOD"] = pupil_bgc[0][1];
				pupil_startbgdatamessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
				webComm->sendJSON(pupil_startbgdatamessage);

//				display.setBackground(0, 0, 0);

				display.redrawPending = true;
				pupil_state = 0;
				pupil_stimDuration = 0;

				ptosis_pic1sent = false;
				ptosis_pic2sent = false;
				ptosis_pic3sent = false;
				ptosis_needpic = false;

				display.stimuliType = STIMULI;

				glfwSetTime(0);
				eyetrackingOn = true;
				tworounds = false;
				return;
			}
		}

		nlohmann::json pupil_startbgdatamessage;
		pupil_startbgdatamessage["chartTypeString"] = charttypestring;
		pupil_startbgdatamessage["message_type"] = MESSAGE_TYPE::START_BACKGROUND_DATA_TRANSMISSION;
		pupil_startbgdatamessage["backgroundColorOS"] = pupil_bgc[pupil_state][0];
		pupil_startbgdatamessage["backgroundColorOD"] = pupil_bgc[pupil_state][1];
		pupil_startbgdatamessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
		webComm->sendJSON(pupil_startbgdatamessage);

		glfwSetTime(0);
		display.redrawPending = true;

	}

	nlohmann::json js;

	float ossize = 0, odsize = 0;
	odsize = convertToMM(etData[0].rawPupilEllipse, 0);
	ossize = convertToMM(etData[1].rawPupilEllipse, 1);

	if (pupil_state == 27 && !ptosis_needpic)
	{
		if ((pupil_stimDuration > 0.5 && !ptosis_pic1sent) || (pupil_stimDuration > 1 && !ptosis_pic2sent) || (pupil_stimDuration > 1.5 && !ptosis_pic3sent))
			ptosis_needpic = true;
	}

	if (pupil_stimDuration > 0.5 && !picSavedThisRound)
	{
		picSavedThisRound = true;
		saveFrameSignal = true;
	}

	js["pupilxOS"] = 0;
	js["pupilyOS"] = 0;
	js["bpgxOS"] = 0;
	js["bpgyOS"] = 0;
	js["dpgxOS"] = 0;
	js["dpgyOS"] = 0;
	js["pupilxOD"] = 0;
	js["pupilyOD"] = 0;
	js["bpgxOD"] = 0;
	js["bpgyOD"] = 0;
	js["dpgxOD"] = 0;
	js["dpgyOD"] = 0;

	if (etData[0].rawPupilEllipse.center.x > 0 && etData[0].rawPupilEllipse.center.y > 0)
	{
		js["pupilxOD"] = etData[0].pupilEllipse.center.x;
		js["pupilyOD"] = etData[0].pupilEllipse.center.y;
	}
	if (etData[1].rawPupilEllipse.center.x > 0 && etData[1].rawPupilEllipse.center.y > 0)
	{
		js["pupilxOS"] = etData[1].pupilEllipse.center.x;
		js["pupilyOS"] = etData[1].pupilEllipse.center.y;
	}
	if (picBP)
	{
		if (etData[0].tandemGlints.bpGraw.x > 0 && etData[0].tandemGlints.bpGraw.y > 0)
		{
			js["bpgxOD"] = etData[0].tandemGlints.bpG.x;
			js["bpgyOD"] = etData[0].tandemGlints.bpG.y;
		}
		if (etData[1].tandemGlints.bpGraw.x > 0 && etData[1].tandemGlints.bpGraw.y > 0)
		{
			js["bpgxOS"] = etData[1].tandemGlints.bpG.x;
			js["bpgyOS"] = etData[1].tandemGlints.bpG.y;
		}
	}
	else
	{
		if (etData[0].tandemGlints.dpGraw.x > 0 && etData[0].tandemGlints.dpGraw.y > 0)
		{
			js["dpgxOD"] = etData[0].tandemGlints.dpG.x;
			js["dpgyOD"] = etData[0].tandemGlints.dpG.y;
		}
		if (etData[1].tandemGlints.dpGraw.x > 0 && etData[1].tandemGlints.dpGraw.y > 0)
		{
			js["dpgxOS"] = etData[1].tandemGlints.dpG.x;
			js["dpgyOS"] = etData[1].tandemGlints.dpG.y;
		}
	}

	js["chartTypeString"] = charttypestring;
	js["message_type"] = MESSAGE_TYPE::DATA_PACKAGE;
	js["backgroundColorOS"] = pupil_bgc[pupil_state][1];
	js["measurementOD"] = odsize;
	js["measurementOS"] = ossize;
	js["backgroundColorOD"] = pupil_bgc[pupil_state][0];
	js["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	webComm->sendJSON(js);
}