#pragma once
#include <fstream>
#include <chrono>
#include <filesystem>
#include <mutex>
#include "Tests.h"

extern CPTGCamera* ptgreycam;
extern CTest* currentTest;
extern atomic<bool> g_calibrationIsGoing;
extern atomic<bool> g_calibrationForcedStop;
extern int offset;

#define PATTERNTEST_VERSION "1.0.20221027"

class PatternTest : public CTest
{
public:
	PatternTest();
	void inCaseOfCalibration();
	void finishCalibration(bool forced);

private:

	struct CurrentCircles
	{
		cv::RotatedRect bigRects[2];
		cv::Point2f verticles[2][4];
	};

	struct DetectedCircles
	{
		cv::RotatedRect bigRects[2];
		cv::RotatedRect circles[8];
		int contrastSum{};
	};

	ofstream m_history;

	std::vector<DetectedCircles> m_extCircles_bp;
	std::vector<DetectedCircles> m_extCircles_dp;

	string m_calibrationPath;
	string m_resourcesPath;
	Mat m_dpImg, m_bpImg, m_toDrawOn;
	Point2f m_centerResults[2];
	float m_angleResults[2];

	Rect m_areas[2];
	float m_requiredDist = 3;
	float m_minBigSquaresSize[2];
	float m_maxBigSquaresSize[2];
	float m_minAngle[2];
	float m_maxAngle[2];
	float m_minCirclesRad[2];
	float m_maxCirclesRad[2];
	float m_radInc[2];
	float m_xstep[2];
	float m_ystep[2];

	void acquirePictures();
	void detectShapes();
	void drawResults();
	void setCamBack();
};

PatternTest::PatternTest()
{
	webComm->openLog();
	g_calibrationIsGoing = true;
	cout << "Calibration is executing. PatternTest module" << endl;

	char path[MAX_PATH];
	HMODULE hModule = GetModuleHandleA(NULL);
	GetModuleFileNameA(hModule, path, MAX_PATH);
	PathRemoveFileSpecA(path);
	m_calibrationPath = path;
	m_calibrationPath += "\\calibration";
	m_resourcesPath = m_calibrationPath + "\\resources";

	m_areas[0] = cv::Rect(270, 520, 170, 170);
	m_areas[1] = cv::Rect(910, 520, 170, 170);

	for (int idx = 0; idx < 2; idx++)
	{
		m_minBigSquaresSize[idx] = 225;
		m_maxBigSquaresSize[idx] = 230;
		m_minAngle[idx] = 0;
		m_maxAngle[idx] = 30;
		m_minCirclesRad[idx] = 16;
		m_maxCirclesRad[idx] = 18;
		m_radInc[idx] = 1;
		m_xstep[idx] = 1;
		m_ystep[idx] = 1;
	}

	nlohmann::json js;
	js["test"] = "PATTERN_TEST";
	js["message_type"] = "START";
	webComm->sendJSON(js);
}

void PatternTest::inCaseOfCalibration()
{
	acquirePictures();
	detectShapes();
	if (!g_calibrationForcedStop)
	{
		drawResults();
	}
	finishCalibration(g_calibrationForcedStop);
}

void PatternTest::acquirePictures()
{
	ptgreycam->expandROI();
	ptgreycam->setExposure(2000);

	ptgreycam->switchDP();
	display.setBackground(1, 1, 1);
	display.redrawPending = true;
	this_thread::sleep_for(1s);
	ptgreycam->getImage().copyTo(m_dpImg);

	ptgreycam->switchBP();
	display.setBackground(0, 0, 0);
	display.redrawPending = true;
	this_thread::sleep_for(1s);
	ptgreycam->getImage().copyTo(m_bpImg);

	imwrite(m_resourcesPath + "\\last_dp_pattern.bmp", m_dpImg);
	imwrite(m_resourcesPath + "\\last_bp_pattern.bmp", m_bpImg);
}

void PatternTest::drawResults()
{
	cout << "Drawing." << endl;
	cvtColor(m_dpImg, m_toDrawOn, COLOR_GRAY2BGR);
	m_toDrawOn(Rect(500, 0, 280, m_toDrawOn.rows)) = 0;

	rectangle(m_toDrawOn, Rect(30, 30, 10, 10), Scalar(255, 255, 0), -1);
	putText(m_toDrawOn, " - BP LED RESULT", Point(40, 40), FONT_HERSHEY_PLAIN, 1.0, Scalar(255, 255, 0));
	rectangle(m_toDrawOn, Rect(30, 50, 10, 10), Scalar(0, 255, 0), -1);
	putText(m_toDrawOn, " - DP LED RESULT", Point(40, 60), FONT_HERSHEY_PLAIN, 1.0, Scalar(0, 255, 0));
	rectangle(m_toDrawOn, Rect(30, 70, 10, 10), Scalar(0, 255, 255), -1);
	putText(m_toDrawOn, " - AVERAGE RESULT", Point(40, 80), FONT_HERSHEY_PLAIN, 1.0, Scalar(0, 255, 255));

	putText(m_toDrawOn, "PATTERN CENTERS:", Point(510, 520), FONT_HERSHEY_PLAIN, 1.0, Scalar(200, 200, 255));
	putText(m_toDrawOn, "PATTERN ANGLES:", Point(510, 640), FONT_HERSHEY_PLAIN, 1.0, Scalar(200, 200, 255));

	for (int eye = 0; eye < 2; eye++)
	{
		//Centers calculation
		Point2f l_overallCenterBp = Point2f(0, 0);
		for (const RotatedRect& circle : m_extCircles_bp.at(eye).circles)
		{
			ellipse(m_toDrawOn, circle, Scalar(255, 255, 0), 2);
			l_overallCenterBp += circle.center;
		}
		l_overallCenterBp /= 8.f;

		Point2f l_overallCenterDp = Point2f(0, 0);
		for (const RotatedRect& circle : m_extCircles_dp.at(eye).circles)
		{
			ellipse(m_toDrawOn, circle, Scalar(0, 255, 0), 2);
			l_overallCenterDp += circle.center;
		}
		l_overallCenterDp /= 8.f;

		stringstream l_ss;
		l_ss << l_overallCenterBp;
		putText(m_toDrawOn, l_ss.str(), Point(510 + 130 * eye, 540), FONT_HERSHEY_PLAIN, 1.0, Scalar(255, 255, 0));
		
		l_ss.str("");
		l_ss << l_overallCenterDp;
		putText(m_toDrawOn, l_ss.str(), Point(510 + 130 * eye, 570), FONT_HERSHEY_PLAIN, 1.0, Scalar(0, 255, 0));

		m_centerResults[eye] = (l_overallCenterBp + l_overallCenterDp) / 2.f;
		l_ss.str("");
		l_ss << m_centerResults[eye];
		putText(m_toDrawOn, l_ss.str(), Point(510 + 130 * eye, 600), FONT_HERSHEY_PLAIN, 1.0, Scalar(0, 255, 255));

		//Angles calculation
		std::sort(m_extCircles_bp.at(eye).circles, m_extCircles_bp.at(eye).circles + 8,
			[](RotatedRect& a, RotatedRect& b) {return a.center.y < b.center.y; });

		std::sort(m_extCircles_dp.at(eye).circles, m_extCircles_dp.at(eye).circles + 8,
			[](RotatedRect& a, RotatedRect& b) {return a.center.y < b.center.y; });

		float l_bpAngle = abs(pAng(m_extCircles_bp.at(eye).circles[3].center, m_extCircles_bp.at(eye).circles[2].center));
		line(m_toDrawOn, m_extCircles_bp.at(eye).circles[3].center, m_extCircles_bp.at(eye).circles[2].center, Scalar(255, 0, 0), 2);

		float l_dpAngle = abs(pAng(m_extCircles_dp.at(eye).circles[3].center, m_extCircles_dp.at(eye).circles[2].center));
		line(m_toDrawOn, m_extCircles_dp.at(eye).circles[3].center, m_extCircles_dp.at(eye).circles[2].center, Scalar(255, 0, 0), 2);

		if (eye == 0)
		{
			l_bpAngle -= 180;
			l_dpAngle -= 180;
		}
		else
		{
			l_bpAngle = 360.f - l_bpAngle;
			l_dpAngle = 360.f - l_dpAngle;
		}

		l_ss.str("");
		l_ss << l_bpAngle;
		putText(m_toDrawOn, l_ss.str(), Point(510 + 130 * eye, 660), FONT_HERSHEY_PLAIN, 1.0, Scalar(255, 255, 0));

		l_ss.str("");
		l_ss << l_dpAngle;
		putText(m_toDrawOn, l_ss.str(), Point(510 + 130 * eye, 690), FONT_HERSHEY_PLAIN, 1.0, Scalar(0, 255, 0));

		m_angleResults[eye] = (l_bpAngle + l_dpAngle) / 2.f;
		l_ss.str("");
		l_ss << m_angleResults[eye];
		putText(m_toDrawOn, l_ss.str(), Point(510 + 130 * eye, 720), FONT_HERSHEY_PLAIN, 1.0, Scalar(0, 255, 255));

		rectangle(m_toDrawOn, m_areas[eye], Scalar(0, 255, 255), 1);
	}

	string imwrite_path = m_calibrationPath + "\\last_detected_patterns.bmp";

	imwrite(imwrite_path, m_toDrawOn);

	nlohmann::json js;
	js["test"] = "PATTERN_TEST";
	js["message_type"] = "DATA_PACKAGE";
	js["picturePath"] = imwrite_path;
	webComm->sendJSON(js);
}

void PatternTest::detectShapes()
{
	//Scan DP picture
	Rect l_dpRect = Rect(0, 0, m_dpImg.cols, m_dpImg.rows);

	for (int eye = 0; eye <= 1; eye++)
	{
		int l_maxContrastStorage = -9999;
		DetectedCircles l_dc;

		for (int x = m_areas[eye].x; x <= m_areas[eye].x + m_areas[eye].width; x++)
			for (int y = m_areas[eye].y; y <= m_areas[eye].y + m_areas[eye].height; y++)
				for (int bigsize = m_minBigSquaresSize[eye]; bigsize <= m_maxBigSquaresSize[eye]; bigsize++)
					for (int angle = m_minAngle[eye]; angle <= m_maxAngle[eye]; angle++)
					{
						if (g_calibrationForcedStop)
						{
							return;
						}
						CurrentCircles cc;
						cc.bigRects[0] = cv::RotatedRect(cv::Point(x, y), cv::Size(bigsize, bigsize), angle);
						cc.bigRects[1] = cv::RotatedRect(cv::Point(x, y), cv::Size(bigsize, bigsize), angle + 45);
						cc.bigRects[0].points(cc.verticles[0]);
						cc.bigRects[1].points(cc.verticles[1]);

						for (int smallrad = m_minCirclesRad[eye]; smallrad <= m_maxCirclesRad[eye]; smallrad++)
						{
							int l_localContrastSum = 0;

							cv::Point2f pointsIn[2][4][8];
							cv::Point2f pointsOut[2][4][8];

							//Get points in and out
							for (int i = 0; i <= 1; i++)
							{
								//In case of DP led
								//We have to avoid certain holes
								if (eye == 0)
								{
									std::sort(cc.verticles[i], cc.verticles[i] + 4, [](const cv::Point2f& a, const cv::Point2f& b) {
										return a.x < b.x;
									});
								}
								else
								{
									std::sort(cc.verticles[i], cc.verticles[i] + 4, [](const cv::Point2f& a, const cv::Point2f& b) {
										return a.x > b.x;
									});
								}

								for (int k = 1; k < 4; k++)
								{
									pointsIn[i][k][0] = cv::Point2f(cc.verticles[i][k].x + smallrad - m_requiredDist, cc.verticles[i][k].y);
									pointsIn[i][k][1] = cv::Point2f(cc.verticles[i][k].x - smallrad + m_requiredDist, cc.verticles[i][k].y);
									pointsIn[i][k][2] = cv::Point2f(cc.verticles[i][k].x, cc.verticles[i][k].y + smallrad - m_requiredDist);
									pointsIn[i][k][3] = cv::Point2f(cc.verticles[i][k].x, cc.verticles[i][k].y - smallrad + m_requiredDist);

									pointsIn[i][k][4] = cv::Point2f(cc.verticles[i][k].x + smallrad * 0.71 - m_requiredDist, cc.verticles[i][k].y + smallrad * 0.71 - m_requiredDist);
									pointsIn[i][k][5] = cv::Point2f(cc.verticles[i][k].x - smallrad * 0.71 + m_requiredDist, cc.verticles[i][k].y - smallrad * 0.71 + m_requiredDist);
									pointsIn[i][k][6] = cv::Point2f(cc.verticles[i][k].x + smallrad * 0.71 - m_requiredDist, cc.verticles[i][k].y - smallrad * 0.71 + m_requiredDist);
									pointsIn[i][k][7] = cv::Point2f(cc.verticles[i][k].x - smallrad * 0.71 + m_requiredDist, cc.verticles[i][k].y + smallrad * 0.71 - m_requiredDist);

									pointsOut[i][k][0] = cv::Point2f(cc.verticles[i][k].x + smallrad + m_requiredDist, cc.verticles[i][k].y);
									pointsOut[i][k][1] = cv::Point2f(cc.verticles[i][k].x - smallrad - m_requiredDist, cc.verticles[i][k].y);
									pointsOut[i][k][2] = cv::Point2f(cc.verticles[i][k].x, cc.verticles[i][k].y + smallrad + m_requiredDist);
									pointsOut[i][k][3] = cv::Point2f(cc.verticles[i][k].x, cc.verticles[i][k].y - smallrad - m_requiredDist);

									pointsOut[i][k][4] = cv::Point2f(cc.verticles[i][k].x + smallrad * 0.71 + m_requiredDist, cc.verticles[i][k].y + smallrad * 0.71 + m_requiredDist);
									pointsOut[i][k][5] = cv::Point2f(cc.verticles[i][k].x - smallrad * 0.71 - m_requiredDist, cc.verticles[i][k].y - smallrad * 0.71 - m_requiredDist);
									pointsOut[i][k][6] = cv::Point2f(cc.verticles[i][k].x + smallrad * 0.71 + m_requiredDist, cc.verticles[i][k].y - smallrad * 0.71 - m_requiredDist);
									pointsOut[i][k][7] = cv::Point2f(cc.verticles[i][k].x - smallrad * 0.71 - m_requiredDist, cc.verticles[i][k].y + smallrad * 0.71 + m_requiredDist);
								}
							}
							for (int i = 0; i <= 1; i++)
								for (int k = 1; k < 4; k++)
									for (int l = 0; l < 8; l++)
									{
										if (l_dpRect.contains(pointsOut[i][k][l]) && l_dpRect.contains(pointsIn[i][k][l]))
											l_localContrastSum += abs(m_dpImg.at<uchar>(pointsOut[i][k][l]) - m_dpImg.at<uchar>(pointsIn[i][k][l]));
									}

							if (l_localContrastSum > l_maxContrastStorage)
							{
								l_maxContrastStorage = l_localContrastSum;
								l_dc.bigRects[0] = cc.bigRects[0];
								l_dc.bigRects[1] = cc.bigRects[1];

								for (int n = 0; n < 4; n++)
								{
									l_dc.circles[n] = cv::RotatedRect(cc.verticles[0][n], cv::Size(smallrad * 2, smallrad * 2), 0);
									l_dc.circles[4 + n] = cv::RotatedRect(cc.verticles[1][n], cv::Size(smallrad * 2, smallrad * 2), 0);
								}

								l_dc.contrastSum = l_localContrastSum;
							}
						}
					}

		m_extCircles_dp.push_back(l_dc);
	}

	//Scan BP picture
	Rect l_bpRect = Rect(0, 0, m_bpImg.cols, m_bpImg.rows);

	for (int eye = 0; eye <= 1; eye++)
	{
		int l_maxContrastStorage = -9999;
		DetectedCircles l_dc;

		for (int x = m_areas[eye].x; x <= m_areas[eye].x + m_areas[eye].width; x++)
			for (int y = m_areas[eye].y; y <= m_areas[eye].y + m_areas[eye].height; y++)
				for (int bigsize = m_minBigSquaresSize[eye]; bigsize <= m_maxBigSquaresSize[eye]; bigsize++)
					for (int angle = m_minAngle[eye]; angle <= m_maxAngle[eye]; angle++)
					{
						//Test is interrupted from outside
						if (g_calibrationForcedStop)
						{
							return;
						}
						CurrentCircles cc;
						cc.bigRects[0] = cv::RotatedRect(cv::Point(x, y), cv::Size(bigsize, bigsize), angle);
						cc.bigRects[1] = cv::RotatedRect(cv::Point(x, y), cv::Size(bigsize, bigsize), angle + 45);
						cc.bigRects[0].points(cc.verticles[0]);
						cc.bigRects[1].points(cc.verticles[1]);

						for (int smallrad = m_minCirclesRad[eye]; smallrad <= m_maxCirclesRad[eye]; smallrad++)
						{
							int l_localContrastSum = 0;

							cv::Point2f pointsIn[2][4][8];
							cv::Point2f pointsOut[2][4][8];

							//Get points in and out
							for (int i = 0; i <= 1; i++)
							{
								for (int k = 0; k < 4; k++)
								{
									pointsIn[i][k][0] = cv::Point2f(cc.verticles[i][k].x + smallrad - m_requiredDist, cc.verticles[i][k].y);
									pointsIn[i][k][1] = cv::Point2f(cc.verticles[i][k].x - smallrad + m_requiredDist, cc.verticles[i][k].y);
									pointsIn[i][k][2] = cv::Point2f(cc.verticles[i][k].x, cc.verticles[i][k].y + smallrad - m_requiredDist);
									pointsIn[i][k][3] = cv::Point2f(cc.verticles[i][k].x, cc.verticles[i][k].y - smallrad + m_requiredDist);

									pointsIn[i][k][4] = cv::Point2f(cc.verticles[i][k].x + smallrad * 0.71 - m_requiredDist, cc.verticles[i][k].y + smallrad * 0.71 - m_requiredDist);
									pointsIn[i][k][5] = cv::Point2f(cc.verticles[i][k].x - smallrad * 0.71 + m_requiredDist, cc.verticles[i][k].y - smallrad * 0.71 + m_requiredDist);
									pointsIn[i][k][6] = cv::Point2f(cc.verticles[i][k].x + smallrad * 0.71 - m_requiredDist, cc.verticles[i][k].y - smallrad * 0.71 + m_requiredDist);
									pointsIn[i][k][7] = cv::Point2f(cc.verticles[i][k].x - smallrad * 0.71 + m_requiredDist, cc.verticles[i][k].y + smallrad * 0.71 - m_requiredDist);

									pointsOut[i][k][0] = cv::Point2f(cc.verticles[i][k].x + smallrad + m_requiredDist, cc.verticles[i][k].y);
									pointsOut[i][k][1] = cv::Point2f(cc.verticles[i][k].x - smallrad - m_requiredDist, cc.verticles[i][k].y);
									pointsOut[i][k][2] = cv::Point2f(cc.verticles[i][k].x, cc.verticles[i][k].y + smallrad + m_requiredDist);
									pointsOut[i][k][3] = cv::Point2f(cc.verticles[i][k].x, cc.verticles[i][k].y - smallrad - m_requiredDist);

									pointsOut[i][k][4] = cv::Point2f(cc.verticles[i][k].x + smallrad * 0.71 + m_requiredDist, cc.verticles[i][k].y + smallrad * 0.71 + m_requiredDist);
									pointsOut[i][k][5] = cv::Point2f(cc.verticles[i][k].x - smallrad * 0.71 - m_requiredDist, cc.verticles[i][k].y - smallrad * 0.71 - m_requiredDist);
									pointsOut[i][k][6] = cv::Point2f(cc.verticles[i][k].x + smallrad * 0.71 + m_requiredDist, cc.verticles[i][k].y - smallrad * 0.71 - m_requiredDist);
									pointsOut[i][k][7] = cv::Point2f(cc.verticles[i][k].x - smallrad * 0.71 - m_requiredDist, cc.verticles[i][k].y + smallrad * 0.71 + m_requiredDist);
								}
							}
							for (int i = 0; i <= 1; i++)
								for (int k = 0; k < 4; k++)
									for (int l = 0; l < 8; l++)
									{
										if (l_bpRect.contains(pointsOut[i][k][l]) && l_bpRect.contains(pointsIn[i][k][l]))
											l_localContrastSum += abs(m_bpImg.at<uchar>(pointsOut[i][k][l]) - m_bpImg.at<uchar>(pointsIn[i][k][l]));
									}

							if (l_localContrastSum > l_maxContrastStorage)
							{
								l_maxContrastStorage = l_localContrastSum;
								l_dc.bigRects[0] = cc.bigRects[0];
								l_dc.bigRects[1] = cc.bigRects[1];

								for (int n = 0; n < 4; n++)
								{
									l_dc.circles[n] = cv::RotatedRect(cc.verticles[0][n], cv::Size(smallrad * 2, smallrad * 2), 0);
									l_dc.circles[4 + n] = cv::RotatedRect(cc.verticles[1][n], cv::Size(smallrad * 2, smallrad * 2), 0);
								}

								l_dc.contrastSum = l_localContrastSum;
							}
						}
					}

		m_extCircles_bp.push_back(l_dc);
	}
}

void PatternTest::setCamBack()
{
	ptgreycam->shrinkROI(offset);
	ptgreycam->setExposure(2000);
	ptgreycam->switchDPBP();
}

void PatternTest::finishCalibration(bool forced)
{
	setCamBack();

	m_history.open(m_resourcesPath + "\\history.txt", ios_base::app);
	if (!m_history.is_open() || forced)
	{
		cout << "ERROR: Cannot open history.txt file or forced stop" << endl;
	}
	else
	{
		uint64_t l_stopTime = std::chrono::system_clock::now().time_since_epoch().count();
		m_history << "PatternTest;" << l_stopTime << ";" << 
			m_centerResults[0].x << ";" << m_centerResults[0].y << ";" << m_angleResults[0] << 
			";" << m_centerResults[1].x << ";" << m_centerResults[1].y << ";" << m_angleResults[1] << endl;

		m_history.close();
	}

	nlohmann::json js;
	js["test"] = "PATTERN_TEST";
	js["message_type"] = "STOP";
	webComm->sendJSON(js);

	webComm->closeLog();
	cout << "Calibration is finished. PatternTest module" << endl;
}

