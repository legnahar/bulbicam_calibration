#pragma once
#include "Spinnaker.h"
#include "SpinGenApi/SpinnakerGenApi.h"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <thread>
#include <mutex>

using namespace cv;
using namespace Spinnaker;
using namespace Spinnaker::GenApi;
using namespace Spinnaker::GenICam;
using namespace std;

#define cameraversion "0.9.202000825"

class CPTGCamera
{
private:
	SystemPtr system;
	CameraList camList;
	CameraPtr pCam;
	CCommandPtr ptrTrigger;
	INodeMap *nodeMap;

	CFloatPtr ptrLevel;
	CFloatPtr ptrGamma;
	CFloatPtr ptrGain;
	CFloatPtr ptrExposureTime;
	CIntegerPtr ptrTimestamp;
	CIntegerPtr ptrHeight;
	CIntegerPtr ptrWidth;
	CIntegerPtr ptrXOffset;
	CIntegerPtr ptrYOffset;
	CCommandPtr ptrTimestampLatch;

	int expT;
	int frameHeight, frameWidth;
public:
	string sn;
	int yOffset;
	CPTGCamera(int offset);
	~CPTGCamera();
	Mat getImage(uint64_t& lineStatus, uint64_t& timestamp, uint64_t& framenumber);
	void shrinkROI(int height);
	void startAcquisition();
	void endAcquisition();
	bool setExposure(int time);
	bool setOffset(int xoffset, int yoffset);
	void switchDPBP();
	int getExposure();

#pragma region VLV_CALIBRATION_TESTS

	Mat getImage();
	void switchDP();
	void switchBP();
	void expandROI();
	bool setHeight(int height);
	bool setWidth(int width);

#pragma endregion VLV_CALIBRATION_TESTS

};
