#pragma once

#include "Tests.h"
#include "LuxConverter.h"
#include <chrono>

#define visualfieldversion "0.9.20211008"

extern bool canceltraining;
extern int vftrainresults[8];
extern CTest* currentTest;
extern string testLogsPath;

extern int vf_greenFixationFixedCount;
extern int vf_greenFixationRandomCount;
extern float vf_greenFixationRadius;
extern int vf_whiteFixationCount;
extern float vf_whiteFixationRadius;
extern float vf_srtRadius;
extern ofstream qualityLog;
extern ofstream testLog;
extern LuxConverter luxConverter[2];

enum vfm_states
{
	VFM_PAUSE,
	VFM_ONLYGREEN,
	VFM_WHITE,
	VFM_MOVING,
	VFM_PTOSIS,
	VFM_WAITINGTIME
};

struct vfm_anglediffs
{
	float dist;
	long camTS;
};

struct CVFMTarget
{
	Point2d greenTarget;
	Point2d whiteTarget;
	bool repeat;
	bool training;
};

class CVFMTest : public CTest
{
public:
	CVFMTest(int oculus, int stimuliPositions, bool training, int diameter, int bgr, int bgg, int bgb, int str, int stg, int stb, int onsetType, float onsetTime, bool stFlickerStatus, float stFlickerFrequency, bool hollowCircle, bool animateColor, bool animateSize, bool animatePos, bool animateLetter, float greenAnimationTime, bool onsetAdditionalWaitingStatus, float onsetAdditionalWaitingLength, float fullintensitylux, float bglux);
	void ProcessETData(vector<ETData> etData, uint64_t camTS, uint64_t frameid);
	int testNum = 22;

private:
	float compareAng(float a1, float a2);
	float lcDist(Point2d p1, Point2d p2);
	Point2d lcSubs(Point2d p1, Point2d p2);
	vector<float> movingAvg(vector<float> inArr, int window);

	float curBGColor[2];

	void animateGreen();
	int vf_targettype;
	int vf_targettypecnt;
	void nexttarget(bool repeat);
	void hidewhite(float distpx);
	void movepoint();
	void showgreen();

	bool peripheralShown, centralShown;
	double vf_fullIntensity, vf_bgIntensity;

	vector<float> rands;

	bool wshown;

	void showwhite(uint64_t fid, uint64_t camTS);

	float vf_screening_od_fix[26][2] = { {5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	-10.5},
{5,	-10.5},
{5,	-10.5},
{5,	-10.5},
{5,	-10.5},
{5,	-10.5},
{5,	10.5},
{5,	10.5},
{5,	10.5},
{5,	10.5},
{5,	10.5},
{5,	10.5},
{-4,	0},
{-4,	0},
{-4,	0},
{-4,	0},
{-4,	0},
{-4,	0},
{-4,	0},
{-4,	0},
{-4,	0},
{-4,	0} };
	float vf_screening_od_per[26][2] = {
{-6,	6},
{-6,	-6},
{6,	6},
{6,	-6},
{-3,	15},
{3,	15},
{9,	15},
{-9,	15},
{-3,	21},
{3,	21},
{3,	-15},
{9,	-15},
{-9,	-15},
{-3,	-15},
{3,	-21},
{-3,	-21},
{-15,	3},
{-15,	-3},
{-15,	9},
{-15,	-9},
{-21,	3},
{-21,	-3},
{-21,	9},
{-21,	-9},
{-27,	3},
{-27,	-3} };
	float vf_screening_os_fix[26][2] = { {5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	-10.5},
{5,	-10.5},
{5,	-10.5},
{5,	-10.5},
{5,	-10.5},
{5,	-10.5},
{5,	10.5},
{5,	10.5},
{5,	10.5},
{5,	10.5},
{5,	10.5},
{5,	10.5},
{-4,	0},
{-4,	0},
{-4,	0},
{-4,	0},
{-4,	0},
{-4,	0},
{-4,	0},
{-4,	0},
{-4,	0},
{-4,	0} };
	float vf_screening_os_per[26][2] = {
	{-6,	6},
	{-6,	-6},
	{6,	6},
	{6,	-6},
	{-3,	15},
	{3,	15},
	{9,	15},
	{-9,	15},
	{-3,	21},
	{3,	21},
	{3,	-15},
	{-3,	-15},
	{-9,	-15},
	{9,	-15},
	{3,	-21},
	{-3,	-21},
	{15,	3},
	{15,	-3},
	{15,	9},
	{15,	-9},
	{21,	3},
	{21,	-3},
	{21,	9},
	{21,	-9},
	{27,	3},
	{27,	-3} };
/*
	float vf_training_od_fix[16][2] = { {5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0} };
	float vf_training_od_per[16][2] = {
{-3,	9},
{14, -1.5},
{3,	9},
{14, -1.5},
{9,	3},
{14, -1.5},
{9,	-3},
{14, -1.5},
{3,	-9},
{14, -1.5},
{-3,	-9},
{14, -1.5},
{-9,	-3},
{14, -1.5},
{-9,	3},
{14, -1.5}
	};
	float vf_training_os_fix[16][2] = { {5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0} };
	float vf_training_os_per[16][2] = {
{-3,	9},
{-14, -1.5},
{3,	9},
{-14, -1.5},
{9,	3},
{-14, -1.5},
{9,	-3},
{-14, -1.5},
{3,	-9},
{-14, -1.5},
{-3,	-9},
{-14, -1.5},
{-9,	-3},
{-14, -1.5},
{-9,	3},
{-14, -1.5}
	};
	*/
	float vf_training_od_fix[8][2] = { 
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0} };
	float vf_training_od_per[8][2] = {
{-3,	9},
{3,	9},
{9,	3},
{9,	-3},
{3,	-9},
{-3,	-9},
{-9,	-3},
{-9,	3}
	};
	float vf_training_os_fix[8][2] = { 
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0},
{5,	0} };
	float vf_training_os_per[8][2] = {
{-3,	9},
{3,	9},
{9,	3},
{9,	-3},
{3,	-9},
{-3,	-9},
{-9,	-3},
{-9,	3}
	};

	float vf_neurofield_od_16_fix[16][2] = {
{15,-10.5},
{15,-10.5},
{15,0},
{15,0},
{15,10.5},
{15,10.5},
{5,10.5},
{5,10.5},
{-5,10.5},
{-5,10.5},
{-5,0},
{-5,0},
{-5,-10.5},
{-5,-10.5},
{5,-10.5},
{5,-10.5},

	};
	float vf_neurofield_od_16_per[16][2] = {
{21,21},
{10.5,10.5},
{21,0},
{10.5,0},
{21,-21},
{10.5,-10.5},
{0,-21},
{0,-10.5},
{-21,-21},
{-10.5,-10.5},
{-21,0},
{-10.5,0},
{-21,21},
{-10.5,10.5},
{0,21},
{0,10.5},

	};

	float vf_neurofield_os_16_fix[16][2] = {
{-5,-10.5},
{-5,-10.5},
{-5,0},
{-5,0},
{-5,10.5},
{-5,10.5},
{5,10.5},
{5,10.5},
{15,10.5},
{15,10.5},
{15,0},
{15,0},
{15,-10.5},
{15,-10.5},
{5,-10.5},
{5,-10.5},
	};
	float vf_neurofield_os_16_per[16][2] = {
{21,21},
{10.5,10.5},
{21,0},
{10.5,0},
{21,-21},
{10.5,-10.5},
{0,-21},
{0,-10.5},
{-21,-21},
{-10.5,-10.5},
{-21,0},
{-10.5,0},
{-21,21},
{-10.5,10.5},
{0,21},
{0,10.5},

	};

	float vf_neurofield_os_64_fix[64][2] = {
{5,0},	//0
{5,0},	//1
{5,0},	//2
{5,0},	//3
{5,0},	//4
{5,0},	//5
{5,0},	//6
{5,0},	//7
{5,0},	//8
{5,0},	//9
{5,0},	//10
{5,0},	//11
{5,0},	//12
{5,0},	//13
{5,0},	//14
{5,0},	//15
{-6,-10.5},	//16
{-6,-10.5},	//17
{-6,-10.5},	//18
{-6,-10.5},	//19
{-6,-10.5},	//20
{-6,-10.5},	//21
{-6,-10.5},	//22
{-6,-10.5},	//23
{-6,-10.5},	//24
{-6,-10.5},	//25
{-6,-10.5},	//26
{-6,-10.5},	//27
{-6,10.5},	//28
{-6,10.5},	//29
{-6,10.5},	//30
{-6,10.5},	//31
{-6,10.5},	//32
{-6,10.5},	//33
{-6,10.5},	//34
{-6,10.5},	//35
{-6,10.5},	//36
{-6,10.5},	//37
{-6,10.5},	//38
{-6,10.5},	//39
{21,10.5},	//40
{21,10.5},	//41
{21,10.5},	//42
{21,10.5},	//43
{21,10.5},	//44
{21,10.5},	//45
{21,10.5},	//46
{21,10.5},	//47
{21,10.5},	//48
{21,10.5},	//49
{21,10.5},	//50
{21,10.5},	//51
{21,-10.5},	//52
{21,-10.5},	//53
{21,-10.5},	//54
{21,-10.5},	//55
{21,-10.5},	//56
{21,-10.5},	//57
{21,-10.5},	//58
{21,-10.5},	//59
{21,-10.5},	//60
{21,-10.5},	//61
{21,-10.5},	//62
{21,-10.5},	//63
	};
	float vf_neurofield_os_64_per[64][2] = {
{3,3},			//0
{-3,-3},	//1
{3,-3},		//2
{-3,3},		//3
{9,9},			//4
{-9,-9},	//5
{9,-9},		//6
{-9,9},		//7
{3,9},			//8
{-3,-9},	//9
{3,-9},		//10
{-3,9},		//11
{9,3},			//12
{-9,-3},	//13
{9,-3},		//14
{-9,3},		//15
{3,15},		//16
{3,21},		//17
{9,15},		//18
{9,21},		//19
{15,3},			//20
{15,9},			//21
{15,15},		//22
{21,3},			//23
{21,9},			//24
{21,9},			//25
{27,3},			//26
{27,9},			//27
{3,-15},		//28
{3,-21},		//29
{9,-15},		//30
{9,-21},		//31
{15,-3},		//32
{15,-9},		//33
{15,-15},		//34
{21,-3},		//35
{21,-9},		//36
{21,-9},		//37
{27,-3},		//38
{27,-9},		//39
{-3,-15},		//40
{-3,-21},		//41
{-9,-15},		//42
{-9,-21},		//43
{-15,-3},			//44
{-15,-9},			//45
{-15,-15},			//46
{-21,-3},			//47
{-21,-9},			//48
{-21,-9},			//49
{-27,-3},			//50
{-27,-9},			//51
{-3,15},		//52
{-3,21},		//53
{-9,15},		//54
{-9,21},		//55
{-15,3},			//56
{-15,9},			//57
{-15,15},			//58
{-21,3},			//59
{-21,9},			//60
{-21,9},			//61
{-27,3},			//62
{-27,9},			//63
	};

	float vf_neurofield_od_64_fix[64][2] = {
{5,0},	//0
{5,0},	//1
{5,0},	//2
{5,0},	//3
{5,0},	//4
{5,0},	//5
{5,0},	//6
{5,0},	//7
{5,0},	//8
{5,0},	//9
{5,0},	//10
{5,0},	//11
{5,0},	//12
{5,0},	//13
{5,0},	//14
{5,0},	//15
{21,-10.5},	//16
{21,-10.5},	//17
{21,-10.5},	//18
{21,-10.5},	//19
{21,-10.5},	//20
{21,-10.5},	//21
{21,-10.5},	//22
{21,-10.5},	//23
{21,-10.5},	//24
{21,-10.5},	//25
{21,-10.5},	//26
{21,-10.5},	//27
{21,10.5},	//28
{21,10.5},	//29
{21,10.5},	//30
{21,10.5},	//31
{21,10.5},	//32
{21,10.5},	//33
{21,10.5},	//34
{21,10.5},	//35
{21,10.5},	//36
{21,10.5},	//37
{21,10.5},	//38
{21,10.5},	//39
{-6,10.5},	//40
{-6,10.5},	//41
{-6,10.5},	//42
{-6,10.5},	//43
{-6,10.5},	//44
{-6,10.5},	//45
{-6,10.5},	//46
{-6,10.5},	//47
{-6,10.5},	//48
{-6,10.5},	//49
{-6,10.5},	//50
{-6,10.5},	//51
{-6,-10.5},	//52
{-6,-10.5},	//53
{-6,-10.5},	//54
{-6,-10.5},	//55
{-6,-10.5},	//56
{-6,-10.5},	//57
{-6,-10.5},	//58
{-6,-10.5},	//59
{-6,-10.5},	//60
{-6,-10.5},	//61
{-6,-10.5},	//62
{-6,-10.5},	//63

	};
	float vf_neurofield_od_64_per[64][2] = {
{3,3},	//0
{-3,-3},	//1
{3,-3},	//2
{-3,3},	//3
{9,9},	//4
{-9,-9},	//5
{9,-9},	//6
{-9,9},	//7
{3,9},	//8
{-3,-9},	//9
{3,-9},	//10
{-3,9},	//11
{9,3},	//12
{-9,-3},	//13
{9,-3},	//14
{-9,3},	//15
{3,15},	//16
{3,21},	//17
{9,15},	//18
{9,21},	//19
{15,3},	//20
{15,9},	//21
{15,15},	//22
{21,3},	//23
{21,9},	//24
{21,9},	//25
{27,3},	//26
{27,9},	//27
{3,-15},	//28
{3,-21},	//29
{9,-15},	//30
{9,-21},	//31
{15,-3},	//32
{15,-9},	//33
{15,-15},	//34
{21,-3},	//35
{21,-9},	//36
{21,-9},	//37
{27,-3},	//38
{27,-9},	//39
{-3,-15},	//40
{-3,-21},	//41
{-9,-15},	//42
{-9,-21},	//43
{-15,-3},	//44
{-15,-9},	//45
{-15,-15},	//46
{-21,-3},	//47
{-21,-9},	//48
{-21,-9},	//49
{-27,-3},	//50
{-27,-9},	//51
{-3,15},	//52
{-3,21},	//53
{-9,15},	//54
{-9,21},	//55
{-15,3},	//56
{-15,9},	//57
{-15,15},	//58
{-21,3},	//59
{-21,9},	//60
{-21,9},	//61
{-27,3},	//62
{-27,9},	//63
	};

	float vf_period_show_only_green_in_beginning = 0.7f;
	float vf_period_move_green = 2.f;
	float vf_period_white_timeout = 1.2f;
	float vf_period_pause = 0.7f;
	float vf_period_show_only_green_in_beginning_big = 3.f;

	bool ptosistimeout;
	int vf_fixationcount;
	bool vf_fixation;
	int lfrepeatcounter;

	vector<CVFMTarget> vf_targets;

	Point2f vf_prevInit;

	bool frameWasLost;
	int lostFramesCount;

	Point2d vf_prevOS, vf_prevOD;
	uint64_t prevTS;

	Point2f vf_prev_eye, vf_init, vf_prev_fix_pos;
	Point2f vf_fix_target_prev, vf_per_target_prev;
	Point2f vf_fix_point;
	Point2f vf_curGPx;
	Point2f vf_prevGPx;
	Point2f vf_curWPx;

	int lostframecounter;

	Point2f vf_preveye;
	float vf_randomp;

	Point2d aninpoint, anendpoint;

	int vf_seen;

	bool vf_secondrun;
	int vf_srt;
	int vf_fixations, vf_samplesInSector;
	float vf_maxDistance, vf_maxAngle;
	long tsDif;
	int vf_whitePointID;
	int vf_greenPointID;


	int vf_state;
	Point2f vf_eye;
	float vf_distpx;
	int vf_bigFixCount;
	Point2f vf_SRTPoint;
	uint64_t prevfid;
//	float trainingScore;

	vector<vfm_anglediffs> vf_andiffOS, vf_andiffOD;
	float getPeakVelocity(vector<vfm_anglediffs> andifs, float ratio);

	string charttypestring;
	int nf_dotcnt;

	float stColor[3];
	float bgColor[3];
	int vf_onsetType;
	float vf_diameter;
	float vf_onsetTime;
	bool vf_stFlickerStatus;
	float vf_stFlickerFrequency;
	bool vf_whitedoton;
	float curColor[3];
	std::chrono::time_point<std::chrono::system_clock> vf_bltime;
	std::chrono::time_point<std::chrono::system_clock> vf_srtstart;

	bool vf_showHollowCircle;
	bool vf_animateColor;
	bool vf_animateSize;
	bool vf_animateRead;
	bool vf_animatePosition;

	float vf_greenAnimationTime;
	float vf_waitingTime;
	float vf_distToTargetPx;
	nlohmann::json startTestMessage;

};

CVFMTest::CVFMTest(int oculus, int stimuliPositions, bool training, int diameter, int bgr, int bgg, int bgb, int str, int stg, int stb, int onsetType, float onsetTime, bool stFlickerStatus, float stFlickerFrequency, bool hollowCircle, bool animateColor, bool animateSize, bool animatePos, bool animateLetter, float greenAnimationTime, bool onsetAdditionalWaitingStatus, float onsetAdditionalWaitingLength, float fullintensitylux, float bglux)
{
	vf_bgIntensity = bglux;
	vf_fullIntensity = fullintensitylux;
	vf_waitingTime = onsetAdditionalWaitingLength/1000.;
	vf_showHollowCircle = hollowCircle;
	vf_animateColor = animateColor;
	vf_animatePosition = animatePos;
	vf_animateSize = animateSize;
	vf_greenAnimationTime = greenAnimationTime;
	vf_animateRead = animateLetter;
	vf_diameter = diameter;
//	trainingScore = 0;
	canceltraining = !training;
	nf_dotcnt = stimuliPositions;

	stColor[0] = str / 255.;
	stColor[1] = stg / 255.;
	stColor[2] = stb / 255.;

	bgColor[0] = bgr / 255.;
	bgColor[1] = bgg / 255.;
	bgColor[2] = bgb / 255.;

	vf_onsetType = onsetType;
	vf_onsetTime = 1.2f;
	if(vf_onsetType!=0)
		vf_onsetTime = onsetTime/1000.;
	vf_stFlickerStatus = stFlickerStatus;
	vf_stFlickerFrequency = 0;
	if (vf_stFlickerStatus)
		vf_stFlickerFrequency = stFlickerFrequency;

	if (oculus == 2)
	{
		vf_targettype = 0;
		vf_targettypecnt = 0;
		charttypestring = "VISUAL_FIELD_MERGED";
	}
	else
	{
		vf_targettype = oculus;
		vf_targettypecnt = 1;
		charttypestring = "VISUAL_FIELD_MERGED";
	}

	centralShown = false;
	peripheralShown = false;

	tsDif = 0;
	webComm->openLog();
	eyetrackingOn = true;
	std::ostringstream vffn;
	vffn << testLogsPath << "vf_" << patientInfo.examID << "_" << std::chrono::system_clock::now().time_since_epoch().count() << ".csv";
	std::string vffns = vffn.str();
	testLog.open(vffns);
	
	int framecount = 0;
	int fixationcount = 0;
	glfwSetTime(0);
	vf_secondrun = false;

	glfwSetTime(0);
	vf_srt = 0;
	vf_fixation = false;
	vf_fixationcount = 0;

	vf_seen = 2;

	vf_prev_eye.x = 0;
	vf_prev_eye.y = 0;
	vf_bigFixCount = 0;
	vf_targets.clear();

	startTestMessage["chartTypeString"] = charttypestring;
	startTestMessage["message_type"] = MESSAGE_TYPE::START_TEST;
	startTestMessage["metadata"] = versionInfoForHub;
	startTestMessage["stimuliPositions"] = stimuliPositions;
	if (oculus == 0)
		startTestMessage["oculus"] = "OD";
	else if (oculus == 1)
		startTestMessage["oculus"] = "OS";
	else if (oculus == 2)
		startTestMessage["oculus"] = "OU";
	startTestMessage["training"] = training;
	startTestMessage["diameter"] = diameter;
	startTestMessage["bgr"] = bgr;
	startTestMessage["bgg"] = bgg;
	startTestMessage["bgb"] = bgb;
	startTestMessage["str"] = str;
	startTestMessage["stg"] = stg;
	startTestMessage["stb"] = stb;
	if (onsetType == 0)
		startTestMessage["onsetType"] = "Instant";
	else if (onsetType == 1)
		startTestMessage["onsetType"] = "Linear";
	else if (onsetType == 2)
		startTestMessage["onsetType"] = "Logarithmic";
	startTestMessage["onsetTime"] = onsetTime;
	startTestMessage["onsetAdditionalWaitingStatus"] = onsetAdditionalWaitingStatus;
	startTestMessage["onsetAdditionalWaitingLength"] = onsetAdditionalWaitingLength;
	startTestMessage["stFlickerStatus"] = stFlickerStatus;
	startTestMessage["greenDotAnimationStatus"] = hollowCircle;
	startTestMessage["greenDotAnimationConcentric"] = greenAnimationTime;
	startTestMessage["color"] = animateColor;
	startTestMessage["movement"] = animatePos;
	startTestMessage["size"] = animateSize;
	startTestMessage["reading"] = animateLetter;
	startTestMessage["stFlickerFrequency"] = stFlickerFrequency;
	startTestMessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();


	if (training)
	{
		if (vf_targettype == 0)
		{
			for (int vf_i = 0; vf_i < 8; vf_i++)
			{
				CVFMTarget tmpt;
				tmpt.greenTarget.x = vf_training_od_fix[vf_i][0];
				tmpt.greenTarget.y = vf_training_od_fix[vf_i][1];
				tmpt.whiteTarget.x = vf_training_od_per[vf_i][0];
				tmpt.whiteTarget.y = vf_training_od_per[vf_i][1];
				tmpt.repeat = false;
				tmpt.training = true;
				vf_targets.push_back(tmpt);
			}
		}
		else
		{
			for (int vf_i = 0; vf_i < 8; vf_i++)
			{
				CVFMTarget tmpt;
				tmpt.greenTarget.x = vf_training_os_fix[vf_i][0];
				tmpt.greenTarget.y = vf_training_os_fix[vf_i][1];
				tmpt.whiteTarget.x = vf_training_os_per[vf_i][0];
				tmpt.whiteTarget.y = vf_training_os_per[vf_i][1];
				tmpt.repeat = false;
				tmpt.training = true;
				vf_targets.push_back(tmpt);
			}

		}
	}
	else 
	{
		webComm->sendJSON(startTestMessage);
		if (vf_targettype == 0)
		{
			if (nf_dotcnt == 64)
			{
				for (int vf_i = 0; vf_i < 64; vf_i++)
				{
					CVFMTarget tmpt;
					tmpt.greenTarget.x = vf_neurofield_od_64_fix[vf_i][0];
					tmpt.greenTarget.y = vf_neurofield_od_64_fix[vf_i][1];
					tmpt.whiteTarget.x = vf_neurofield_od_64_per[vf_i][0];
					tmpt.whiteTarget.y = vf_neurofield_od_64_per[vf_i][1];
					tmpt.repeat = false;
					tmpt.training = false;
					vf_targets.push_back(tmpt);
				}

			}
			else if (nf_dotcnt == 16)
			{
				for (int vf_i = 0; vf_i < 16; vf_i++)
				{
					CVFMTarget tmpt;
					tmpt.greenTarget.x = vf_neurofield_od_16_fix[vf_i][0];
					tmpt.greenTarget.y = vf_neurofield_od_16_fix[vf_i][1];
					tmpt.whiteTarget.x = vf_neurofield_od_16_per[vf_i][0];
					tmpt.whiteTarget.y = vf_neurofield_od_16_per[vf_i][1];
					tmpt.repeat = false;
					tmpt.training = false;
					vf_targets.push_back(tmpt);
				}
			}
			else
			{
				for (int vf_i = 0; vf_i < 26; vf_i++)
				{
					CVFMTarget tmpt;
					tmpt.greenTarget.x = vf_screening_od_fix[vf_i][0];
					tmpt.greenTarget.y = vf_screening_od_fix[vf_i][1];
					tmpt.whiteTarget.x = vf_screening_od_per[vf_i][0];
					tmpt.whiteTarget.y = vf_screening_od_per[vf_i][1];
					tmpt.repeat = false;
					tmpt.training = false;
					vf_targets.push_back(tmpt);
				}
			}
		}
		if (vf_targettype == 1)
		{
			if (nf_dotcnt == 16)
			{
				for (int vf_i = 0; vf_i < 16; vf_i++)
				{
					CVFMTarget tmpt;
					tmpt.greenTarget.x = vf_neurofield_os_16_fix[vf_i][0];
					tmpt.greenTarget.y = vf_neurofield_os_16_fix[vf_i][1];
					tmpt.whiteTarget.x = vf_neurofield_os_16_per[vf_i][0];
					tmpt.whiteTarget.y = vf_neurofield_os_16_per[vf_i][1];
					tmpt.repeat = false;
					tmpt.training = false;
					vf_targets.push_back(tmpt);
				}
			}
			else if (nf_dotcnt == 64)
			{
				for (int vf_i = 0; vf_i < 64; vf_i++)
				{
					CVFMTarget tmpt;
					tmpt.greenTarget.x = vf_neurofield_os_64_fix[vf_i][0];
					tmpt.greenTarget.y = vf_neurofield_os_64_fix[vf_i][1];
					tmpt.whiteTarget.x = vf_neurofield_os_64_per[vf_i][0];
					tmpt.whiteTarget.y = vf_neurofield_os_64_per[vf_i][1];
					tmpt.repeat = false;
					tmpt.training = false;
					vf_targets.push_back(tmpt);
				}
			}
			else
			{
				for (int vf_i = 0; vf_i < 26; vf_i++)
				{
					CVFMTarget tmpt;
					tmpt.greenTarget.x = vf_screening_os_fix[vf_i][0];
					tmpt.greenTarget.y = vf_screening_os_fix[vf_i][1];
					tmpt.whiteTarget.x = vf_screening_os_per[vf_i][0];
					tmpt.whiteTarget.y = vf_screening_os_per[vf_i][1];
					tmpt.repeat = false;
					tmpt.training = false;
					vf_targets.push_back(tmpt);
				}
			}
		}

	}
	vf_fix_target_prev = vf_targets[0].greenTarget;

	if (vf_targettype == 0)
	{
		vf_curGPx = lensDist->mmtopix(lensDist->posfromanglOD(vf_targets[0].greenTarget));
		vf_curGPx.x = 3840 - vf_curGPx.x;
	}
	else
	{
		vf_curGPx = lensDist->mmtopix(lensDist->posfromanglOS(vf_targets[0].greenTarget));
		vf_curGPx.x = 1920 - vf_curGPx.x;
	}

	if (!vf_targets[0].training)
		vf_randomp = rand() % vf_greenFixationRandomCount;
	else
		vf_randomp = 0;
	if (vf_bgIntensity > 0)
	{
		curBGColor[0] = luxConverter[0].luxToGrayScale(vf_bgIntensity);
		curBGColor[1] = luxConverter[1].luxToGrayScale(vf_bgIntensity);
	}
	else
	{
		curBGColor[0] = bgColor[0];
		curBGColor[1] = bgColor[0];
		display.setBackground(bgColor[0], bgColor[1], bgColor[2]);
	}
	display.setBackgroundL(curBGColor[1], curBGColor[1], curBGColor[1]);
	display.setBackgroundR(curBGColor[0], curBGColor[0], curBGColor[0]);
	showgreen();
	glfwSetTime(0);
	vf_state = VFM_PAUSE;

}

void CVFMTest::showgreen()
{
	StimuliObjects s1;

	s1.objectColor[0] = 0;
	s1.objectColor[1] = 1;
	s1.objectColor[2] = 0;

	if (vf_targettype == 0)
	{
		vf_curGPx = lensDist->mmtopix(lensDist->posfromanglOD(vf_targets[0].greenTarget));
		vf_curGPx.x = 3840 - vf_curGPx.x;
	}
	else
	{
		vf_curGPx = lensDist->mmtopix(lensDist->posfromanglOS(vf_targets[0].greenTarget));
		vf_curGPx.x = 1920 - vf_curGPx.x;
	}

	s1.objectCoordinates[0] = vf_curGPx.x;
	s1.objectCoordinates[1] = vf_curGPx.y;
	s1.numTexture = 1;
	s1.objectSize = 27.f;
	vf_greenPointID = display.addObject(s1);
	centralShown = true;
	display.redrawPending = true;

}

void CVFMTest::animateGreen()
{
	display.showvftex = vf_animateRead;
	float ggt = glfwGetTime();
	if (vf_showHollowCircle)
	{
		float r1 = vf_distToTargetPx - vf_distToTargetPx * ggt / vf_greenAnimationTime;
		float r2 = r1 + 27;
		display.hollowCircle = true;
		display.hollowCircleCoords = vf_curGPx;
		display.hollowCircleR1 = max(0.f, r1);
		display.hollowCircleR2 = max(0.f, r2);
	}
	if (vf_animateColor)
	{
		display.paintObject(vf_greenPointID, (abs(cos(ggt*5)*rands[0]))/255., (255.f - abs(cos(ggt * 5) * rands[1])) / 255., (abs(cos(ggt * 5) * rands[2])) / 255.);
	}
	if (vf_animateSize)
	{
		display.resizeObject(vf_greenPointID, 27. + cos(ggt * 5) * 13.5);
	}
	if (vf_animatePosition)
	{
		display.moveObject(vf_greenPointID, vf_curGPx.x + sin(ggt * 5) * 27. * sin(rands[3]), vf_curGPx.y + cos(ggt * 5) * 27. * cos(rands[3]));
	}
	display.redrawPending = true;

}

void CVFMTest::showwhite(uint64_t fid, uint64_t camTS)
{

	display.vftexnum = rand() % 8;
	if (vf_onsetType == 0)
	{
		if (vf_fullIntensity > 0)
		{
			curColor[0] = stColor[0];
			curColor[1] = stColor[1];
			curColor[2] = stColor[2];
		}
		else
		{
			curColor[0] = stColor[0];
			curColor[1] = stColor[1];
			curColor[2] = stColor[2];
		}
	}
	else
	{
		if (vf_bgIntensity > 0)
		{
			curColor[0] = bgColor[0];
			curColor[1] = bgColor[1];
			curColor[2] = bgColor[2];
		}
		else
		{
			curColor[0] = bgColor[0];
			curColor[1] = bgColor[1];
			curColor[2] = bgColor[2];
		}
	}
	StimuliObjects s1;

	s1.objectColor[0] = curColor[0];
	s1.objectColor[1] = curColor[1];
	s1.objectColor[2] = curColor[2];

	if (vf_targettype == 0)
	{
		vf_curWPx = lensDist->jumpTo(vf_curGPx, Point2d(-vf_targets[0].whiteTarget.x, vf_targets[0].whiteTarget.y));
	}
	else
	{
		vf_curWPx = lensDist->jumpTo(vf_curGPx, vf_targets[0].whiteTarget);
	}

	s1.objectCoordinates[0] = vf_curWPx.x;
	s1.objectCoordinates[1] = vf_curWPx.y;
	vf_distToTargetPx = sqrt(pow(vf_curWPx.x- vf_curGPx.x, 2) + pow(vf_curWPx.y - vf_curGPx.y, 2));

	s1.objectSize = vf_diameter/2.;

/*	int r = rand() % 3;
	if (r != 1)
	{*/
	s1.numTexture = 0;
	vf_whitePointID = display.addObject(s1);
	peripheralShown = true;
/*		wshown = true;
	}
	else
		wshown = false;*/
	display.vfbleech = false;
	display.afterbleech = true;
	vf_whitedoton = true;
	vf_bltime = std::chrono::system_clock::now();

	lostframecounter=0;
	prevfid = fid;
	display.redrawPending = true;

	nlohmann::json vf_start_peripheral_showing;
	vf_start_peripheral_showing["chartTypeString"] = charttypestring;
	vf_start_peripheral_showing["message_type"] = MESSAGE_TYPE::START_PERIPHERAL_SHOWING;
	vf_start_peripheral_showing["targettype"] = vf_targettype;
	vf_start_peripheral_showing["targetx"] = vf_targets[0].whiteTarget.x;
	vf_start_peripheral_showing["targety"] = vf_targets[0].whiteTarget.y;
	vf_start_peripheral_showing["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	if (!vf_targets[0].training)
		webComm->sendJSON(vf_start_peripheral_showing);

	nlohmann::json s_start_peripheral_showing;
	s_start_peripheral_showing["chartTypeString"] = "SACCADES";
	s_start_peripheral_showing["message_type"] = MESSAGE_TYPE::START_PERIPHERAL_SHOWING;
	s_start_peripheral_showing["targettype"] = vf_targettype;
	s_start_peripheral_showing["targetx"] = vf_targets[0].whiteTarget.x;
	s_start_peripheral_showing["targety"] = vf_targets[0].whiteTarget.y;
	s_start_peripheral_showing["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	if (!vf_targets[0].training)
		webComm->sendJSON(s_start_peripheral_showing);

	glfwSetTime(0);
	vf_srt = 0;
	vf_srtstart = std::chrono::system_clock::now();
	vf_maxDistance = 0;
	vf_maxAngle = 0;
	vf_fixations = 0;
	vf_samplesInSector = 0;
	vf_fixation = false;
	vf_fixationcount = 0;
	vf_state = VFM_WHITE;
	vf_seen = 2;
	testLog << 111
		<< ";" << vf_init.x
		<< ";" << vf_init.y
		<< ";" << vf_targets[0].whiteTarget.x
		<< ";" << vf_targets[0].whiteTarget.y
		<< ";" << vf_targettype
		<< ";" << std::chrono::system_clock::now().time_since_epoch().count()
		<< ";" << lensDist->dioptersOD
		<< ";" << lensDist->dioptersOS
		<< endl;

	vf_andiffOD.clear();
	vf_andiffOS.clear();
}

void CVFMTest::movepoint()
{
	float ggt = glfwGetTime();
	float dur = vf_period_move_green / ggt;

	if (dur < 1)
		dur = 1;

	display.moveObject(vf_greenPointID,
		vf_prevGPx.x + (vf_curGPx.x - vf_prevGPx.x) / dur,
		vf_prevGPx.y + (vf_curGPx.y - vf_prevGPx.y) / dur);
	display.redrawPending = true;
}

void CVFMTest::nexttarget(bool repeat = false)
{
	lfrepeatcounter = 0;
	bool training = vf_targets[0].training;
	if (repeat && !vf_targets[0].repeat && !training)
	{
		vf_targets.push_back(vf_targets[0]);
		vf_targets[vf_targets.size() - 1].repeat = true;
	}
//	if(training)
//		vf_targets.push_back(vf_targets[0]);
	vf_targets.erase(vf_targets.begin());

	if (training && (canceltraining|| vf_targets.size()==0))
	{
		webComm->sendJSON(startTestMessage);

		if (vf_targettype == 0)
		{
			vf_targets.clear();
			if (nf_dotcnt == 64)
			{
				for (int vf_i = 0; vf_i < 64; vf_i++)
				{
					CVFMTarget tmpt;
					tmpt.greenTarget.x = vf_neurofield_od_64_fix[vf_i][0];
					tmpt.greenTarget.y = vf_neurofield_od_64_fix[vf_i][1];
					tmpt.whiteTarget.x = vf_neurofield_od_64_per[vf_i][0];
					tmpt.whiteTarget.y = vf_neurofield_od_64_per[vf_i][1];
					tmpt.repeat = false;
					tmpt.training = false;
					vf_targets.push_back(tmpt);
				}

			}
			else if (nf_dotcnt == 16)
			{
				for (int vf_i = 0; vf_i < 16; vf_i++)
				{
					CVFMTarget tmpt;
					tmpt.greenTarget.x = vf_neurofield_od_16_fix[vf_i][0];
					tmpt.greenTarget.y = vf_neurofield_od_16_fix[vf_i][1];
					tmpt.whiteTarget.x = vf_neurofield_od_16_per[vf_i][0];
					tmpt.whiteTarget.y = vf_neurofield_od_16_per[vf_i][1];
					tmpt.repeat = false;
					tmpt.training = false;
					vf_targets.push_back(tmpt);
				}
			}
			else
			{
				for (int vf_i = 0; vf_i < 26; vf_i++)
				{
					CVFMTarget tmpt;
					tmpt.greenTarget.x = vf_screening_od_fix[vf_i][0];
					tmpt.greenTarget.y = vf_screening_od_fix[vf_i][1];
					tmpt.whiteTarget.x = vf_screening_od_per[vf_i][0];
					tmpt.whiteTarget.y = vf_screening_od_per[vf_i][1];
					tmpt.repeat = false;
					tmpt.training = false;
					vf_targets.push_back(tmpt);
				}
			}
			ptosis_pic1sent = false;
			ptosis_pic2sent = false;
			ptosis_pic3sent = false;
//			vf_fix_target_prev = Point2d(99, 99);
		}
		if (vf_targettype == 1)
		{
			vf_targets.clear();
			if (nf_dotcnt == 16)
			{
				for (int vf_i = 0; vf_i < 16; vf_i++)
				{
					CVFMTarget tmpt;
					tmpt.greenTarget.x = vf_neurofield_os_16_fix[vf_i][0];
					tmpt.greenTarget.y = vf_neurofield_os_16_fix[vf_i][1];
					tmpt.whiteTarget.x = vf_neurofield_os_16_per[vf_i][0];
					tmpt.whiteTarget.y = vf_neurofield_os_16_per[vf_i][1];
					tmpt.repeat = false;
					tmpt.training = false;
					vf_targets.push_back(tmpt);
				}
			}
			else if (nf_dotcnt == 64)
			{
				for (int vf_i = 0; vf_i < 64; vf_i++)
				{
					CVFMTarget tmpt;
					tmpt.greenTarget.x = vf_neurofield_os_64_fix[vf_i][0];
					tmpt.greenTarget.y = vf_neurofield_os_64_fix[vf_i][1];
					tmpt.whiteTarget.x = vf_neurofield_os_64_per[vf_i][0];
					tmpt.whiteTarget.y = vf_neurofield_os_64_per[vf_i][1];
					tmpt.repeat = false;
					tmpt.training = false;
					vf_targets.push_back(tmpt);
				}
			}
			else
			{
				for (int vf_i = 0; vf_i < 26; vf_i++)
				{
					CVFMTarget tmpt;
					tmpt.greenTarget.x = vf_screening_os_fix[vf_i][0];
					tmpt.greenTarget.y = vf_screening_os_fix[vf_i][1];
					tmpt.whiteTarget.x = vf_screening_os_per[vf_i][0];
					tmpt.whiteTarget.y = vf_screening_os_per[vf_i][1];
					tmpt.repeat = false;
					tmpt.training = false;
					vf_targets.push_back(tmpt);
				}
			}

			
			ptosis_pic1sent = false;
			ptosis_pic2sent = false;
			ptosis_pic3sent = false;
		}

	}

	if (vf_targets.size() == 0&&!training)
	{
		if (vf_targettype == 0)
		{
			vf_targets.clear();
			if (nf_dotcnt == 16)
			{
				for (int vf_i = 0; vf_i < 16; vf_i++)
				{
					CVFMTarget tmpt;
					tmpt.greenTarget.x = vf_neurofield_os_16_fix[vf_i][0];
					tmpt.greenTarget.y = vf_neurofield_os_16_fix[vf_i][1];
					tmpt.whiteTarget.x = vf_neurofield_os_16_per[vf_i][0];
					tmpt.whiteTarget.y = vf_neurofield_os_16_per[vf_i][1];
					tmpt.repeat = false;
					tmpt.training = false;
					vf_targets.push_back(tmpt);
				}
			}
			else if (nf_dotcnt == 64)
			{
				for (int vf_i = 0; vf_i < 64; vf_i++)
				{
					CVFMTarget tmpt;
					tmpt.greenTarget.x = vf_neurofield_os_64_fix[vf_i][0];
					tmpt.greenTarget.y = vf_neurofield_os_64_fix[vf_i][1];
					tmpt.whiteTarget.x = vf_neurofield_os_64_per[vf_i][0];
					tmpt.whiteTarget.y = vf_neurofield_os_64_per[vf_i][1];
					tmpt.repeat = false;
					tmpt.training = false;
					vf_targets.push_back(tmpt);
				}
			}
			else
			{
				for (int vf_i = 0; vf_i < 26; vf_i++)
				{
					CVFMTarget tmpt;
					tmpt.greenTarget.x = vf_screening_os_fix[vf_i][0];
					tmpt.greenTarget.y = vf_screening_os_fix[vf_i][1];
					tmpt.whiteTarget.x = vf_screening_os_per[vf_i][0];
					tmpt.whiteTarget.y = vf_screening_os_per[vf_i][1];
					tmpt.repeat = false;
					tmpt.training = false;
					vf_targets.push_back(tmpt);
				}
			}

			ptosis_pic1sent = false;
			ptosis_pic2sent = false;
			ptosis_pic3sent = false;
			vf_fix_target_prev = Point2d(99, 99);
		}
		if (vf_targettype == 1 || vf_targettypecnt == 1)
		{
			nlohmann::json me1;
			me1["chartType"] = 22;
			me1["chartTypeString"] = charttypestring;
			me1["message_type"] = MESSAGE_TYPE::STOP_TEST;
			me1["timestamp"] = 1 / 100;
			webComm->sendJSON(me1);
			webComm->closeLog();
			qualityLog.close();
			eyetrackingOn = false;
			display.setDragonfly();
			currentTest = nullptr;

			testLog.close();
		}
		vf_targettype = 1;
	}
	cout << vf_fix_target_prev.x << " -> " << vf_targets[0].greenTarget.x << " ; " << vf_fix_target_prev.y << " -> " << vf_targets[0].greenTarget.y << endl;
	if (vf_fix_target_prev.x == vf_targets[0].greenTarget.x && vf_fix_target_prev.y == vf_targets[0].greenTarget.y)
	{
		vf_state = VFM_PAUSE;
	}
	else
	{
		vf_prevGPx = vf_curGPx;
		vf_state = VFM_MOVING;
		if (vf_targettype == 0)
		{
			vf_curGPx = lensDist->mmtopix(lensDist->posfromanglOD(vf_targets[0].greenTarget));
			vf_curGPx.x = 3840 - vf_curGPx.x;
		}
		else
		{
			vf_curGPx = lensDist->mmtopix(lensDist->posfromanglOS(vf_targets[0].greenTarget));
			vf_curGPx.x = 1920 - vf_curGPx.x;
		}
	}
	vf_fix_target_prev = vf_targets[0].greenTarget;
	glfwSetTime(0);
}

float CVFMTest::getPeakVelocity(vector<vfm_anglediffs> andifs, float ratio)
{
	double maxvv = 0;
	for (int vv = 0; vv < andifs.size(); vv++)
	{
		if (ratio > 0 && andifs[vv].dist > 0 && andifs[vv].camTS > 0)
		{
			double cursp = abs((andifs[vv].dist * 10000. * ratio) / (andifs[vv].camTS / 100000.));
			if(cursp<1200)
				maxvv = MAX(cursp, maxvv);
		}
	}
	return maxvv;
}

void CVFMTest::hidewhite(float distpx)
{
	rands.clear();
	for (int i = 0; i < 5; i++)
		rands.push_back(rand() % 255);
	display.deleteObject(vf_whitePointID);
	peripheralShown = false;
	display.redrawPending = true;
	float ttdist = sqrt(pow(vf_targets[0].whiteTarget.x, 2) + pow(vf_targets[0].whiteTarget.y, 2));
	if (vf_srt < 110)
		vf_seen = 2;
	if (distpx > vf_srtRadius)
	{
		nlohmann::json vf_srtpointdata;
		vf_srtpointdata["chartType"] = 22;
		vf_srtpointdata["chartTypeString"] = charttypestring;
		vf_srtpointdata["message_type"] = MESSAGE_TYPE::DATA_PACKAGE;
		vf_srtpointdata["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
		Point2f srtp = lcSubs(vf_init, vf_SRTPoint);
		vf_srtpointdata["x"] = srtp.x;
		vf_srtpointdata["y"] = srtp.y;
		vf_srtpointdata["lineType"] = 1;
		vf_srtpointdata["lineColor"] = "#FF0F00";
		if (!vf_targets[0].training)
			webComm->sendJSON(vf_srtpointdata);

		nlohmann::json s_srtpointdata;
		s_srtpointdata["chartTypeString"] = "SACCADES";
		s_srtpointdata["message_type"] = MESSAGE_TYPE::DATA_PACKAGE;
		s_srtpointdata["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
		s_srtpointdata["x"] = srtp.x;
		s_srtpointdata["y"] = srtp.y;
		s_srtpointdata["lineType"] = 1;
		s_srtpointdata["lineColor"] = "#FF0F00";
		if (!vf_targets[0].training)
			webComm->sendJSON(s_srtpointdata);
	}
	else
	{
		vf_srt = 0;
		vf_seen = 1;
	}
	lostframecounter = 0;
	if (vf_targets[0].training)
	{
		int curtarg = 0;
		if (vf_targettype == 0)
		{
			for (int i = 0; i < 16; i++)
			{
				if (vf_targets[0].whiteTarget.x == vf_training_od_per[i][0]
					&& vf_targets[0].whiteTarget.y == vf_training_od_per[i][1])
					curtarg = i;
			}
		}
		else
		{
			for (int i = 0; i < 16; i++)
			{
				if (vf_targets[0].whiteTarget.x == vf_training_os_per[i][0]
					&& vf_targets[0].whiteTarget.y == vf_training_os_per[i][1])
					curtarg = i;
			}
		}
		vftrainresults[curtarg] = vf_seen;
	}

	int dir = 0;
	int tx = vf_targets[0].whiteTarget.x;
	int ty = vf_targets[0].whiteTarget.y;
	float ang = fastAtan2(ty, tx);
	if (ang >= 45 && ang < 135)
		dir = 3;
	if (ang >= 135 && ang < 225)
		dir = 1;
	if (ang >= 225 && ang < 315)
		dir = 2;
	if (ang < 45 || ang > 315)
		dir = 0;

	float mvod=0, mvos=0, vmvod=0, vmvos=0, osr=0, odr=0, odd=0,osd=0;

	if (vf_seen == 3 || vf_secondrun || vf_targets[0].repeat )
	{
		if (vf_seen == 3)
		{
			float ttdpx = lcDist(aninpoint, anendpoint);
			if (vf_targettype == 0)
			{
				odd = distpx;
				odr = ttdist / distpx;

				osd = ttdpx;
				osr = ttdist / ttdpx;
			}
			else
			{
				osd = distpx;
				osr = ttdist / distpx;

				odd = ttdpx;
				odr = ttdist / ttdpx;
			}

			mvod = getPeakVelocity(vf_andiffOD, odr);
			vf_andiffOD.clear();
			mvos = getPeakVelocity(vf_andiffOS, osr);
			vf_andiffOS.clear();

			nlohmann::json saccade_data_glint2;
			saccade_data_glint2["chartType"] = 13;
			saccade_data_glint2["chartTypeString"] = "SACCADES";
			saccade_data_glint2["message_type"] = MESSAGE_TYPE::DATA_PACKAGE;
			saccade_data_glint2["targettype"] = vf_targettype;
			saccade_data_glint2["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
			saccade_data_glint2["direction"] = dir;
			saccade_data_glint2["targetx"] = vf_targets[0].whiteTarget.x;
			saccade_data_glint2["targety"] = vf_targets[0].whiteTarget.y;
			//			saccade_data_glint2["velocity"] = mvod;
			saccade_data_glint2["velocity"] = mvod;
			saccade_data_glint2["eye"] = 0;
			saccade_data_glint2["seen"] = vf_seen;
			saccade_data_glint2["distance"] = ttdist;
			if (!vf_targets[0].training && (vf_targettype == 0 || (vf_targettype == 1 && aninpoint.x > 0 && aninpoint.y > 0 && anendpoint.x > 0 && anendpoint.y > 0)))
				webComm->sendJSON(saccade_data_glint2);

			nlohmann::json saccade_data_glint3;
			saccade_data_glint3["chartType"] = 13;
			saccade_data_glint3["chartTypeString"] = "SACCADES";
			saccade_data_glint3["message_type"] = MESSAGE_TYPE::DATA_PACKAGE;
			saccade_data_glint3["targettype"] = vf_targettype;
			saccade_data_glint3["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
			saccade_data_glint3["targetx"] = vf_targets[0].whiteTarget.x;
			saccade_data_glint3["targety"] = vf_targets[0].whiteTarget.y;
			saccade_data_glint3["direction"] = dir;
//			saccade_data_glint3["velocity"] = mvos;
			saccade_data_glint3["velocity"] = mvos;
			saccade_data_glint3["eye"] = 1;
			saccade_data_glint3["seen"] = vf_seen;
			saccade_data_glint3["distance"] = ttdist;
			if (!vf_targets[0].training && (vf_targettype == 1 || (vf_targettype == 0 && aninpoint.x > 0 && aninpoint.y > 0 && anendpoint.x > 0 && anendpoint.y > 0)))
				webComm->sendJSON(saccade_data_glint3);

		}
	}
	if (vf_secondrun||lfrepeatcounter>9 || vf_targets[0].training)
	{
		vf_secondrun = false;
		//nexttarget();
		vf_state = VFM_WAITINGTIME;
		glfwSetTime(0);
	}
	else
	{
		if (vf_seen != 3)
		{
			if(!vf_targets[0].training)
				vf_secondrun = true;
			vf_state = VFM_WAITINGTIME;

			if (!vf_targets[0].training)
				vf_randomp = rand() % vf_greenFixationRandomCount;
			else
				vf_randomp = 0;
		}
		else
		{
			vf_secondrun = false;
			//nexttarget();
			vf_state = VFM_WAITINGTIME;
			glfwSetTime(0);
		}
	}

	testLog << "333;"
		<< vf_srt
		<< ";" << vf_seen
		<< ";" << distpx
		<< ";" << ttdist
		<< ";" << ttdist / distpx
		<< ";" << mvod
		<< ";" << mvos
		<< ";" << lcDist(aninpoint, anendpoint)
		<< ";" << ttdist / lcDist(aninpoint, anendpoint)
		<< endl;


/*	if (vf_targets[0].training)
	{
		if (vf_targets[0].whiteTarget.x == 14 || vf_targets[0].whiteTarget.x == -14)
		{
			if (vf_seen == 1)
				trainingScore += 1;
			if (vf_seen == 2)
				trainingScore -= 1;
			if (vf_seen == 3)
				trainingScore -= 1;
		}
		else
		{
			if (vf_seen == 3)
				trainingScore += 1;
			if (vf_seen == 2)
				trainingScore -= 1;
			if (vf_seen == 1)
				trainingScore -= 0.5;
		}
	}
	if (trainingScore >= 3)
		canceltraining = true;*/
	glfwSetTime(0);
}

float CVFMTest::lcDist(Point2d p1, Point2d p2)
{

	float dist = sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));
	float lll = 0;
	if (vf_targettype == 0)
		lll = -lensDist->dioptersOD;
	else
		lll = -lensDist->dioptersOS;
	float perc = 0;
	if (lll > 0)
		perc = 0.0232;
	else if (lll < 0)
		perc = -0.0178;
	float k = perc * lll;

	return dist + dist*k;

}

Point2d CVFMTest::lcSubs(Point2d p1, Point2d p2)
{

	float lll = 0;
	if (vf_targettype == 0)
		lll = -lensDist->dioptersOD;
	else
		lll = -lensDist->dioptersOS;
	float perc = 0;
	if (lll > 0)
		perc = 0.0232;
	else if (lll < 0)
		perc = -0.0178;
	float k = perc * lll;
	Point2d ret = p2-p1;
	ret.x += ret.x*k;
	ret.y += ret.y*k;

	return ret;

}

float CVFMTest::compareAng(float a1, float a2)
{
	return 180 - abs(abs(a1 - a2) - 180);
}

void CVFMTest::ProcessETData(vector<ETData> etData, uint64_t camTS, uint64_t frameid)
{
	float ggt = glfwGetTime();
	prevfid = frameid;

	nlohmann::json rawInfo;
	rawInfo["chartTypeString"] = charttypestring;
	rawInfo["message_type"] = 99;
	rawInfo["bpgOSX"] = etData[1].tandemGlints.bpG.x;
	rawInfo["bpgOSY"] = etData[1].tandemGlints.bpG.y;
	rawInfo["dpgOSX"] = etData[1].tandemGlints.dpG.x;
	rawInfo["dpgOSY"] = etData[1].tandemGlints.dpG.y;
	rawInfo["bpgODX"] = etData[0].tandemGlints.bpG.x;
	rawInfo["bpgODY"] = etData[0].tandemGlints.bpG.y;
	rawInfo["bpgODX"] = etData[0].tandemGlints.dpG.x;
	rawInfo["bpgODY"] = etData[0].tandemGlints.dpG.y;
	rawInfo["centralTargetShown"] = centralShown;
	rawInfo["centralTargetX"] = vf_curGPx.x;
	rawInfo["centralTargetY"] = vf_curGPx.y;
	rawInfo["peripheralTargetShown"] = peripheralShown;
	rawInfo["peripheralTargetX"] = vf_curWPx.x;
	rawInfo["peripheralTargetY"] = vf_curWPx.y;
	rawInfo["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	webComm->sendJSON(rawInfo);

	if (vf_state == VFM_MOVING)
	{
		if (ggt < vf_period_move_green)
		{
			movepoint();
			return;
		}
		else
		{
			movepoint();

			vf_prevInit.x = 0;
			vf_prevInit.y = 0;

			vf_state = VFM_PAUSE;
			glfwSetTime(0);
			return;
		}
	}

	if (vf_state == VFM_PAUSE)
	{
		if (ggt < vf_period_pause)
		{
			display.moveObject(vf_greenPointID,
				vf_curGPx.x,
				vf_curGPx.y);
			display.redrawPending = true;
			return;
		}
		else
		{
			vf_state = VFM_ONLYGREEN;
			glfwSetTime(0);
			return;
		}
	}

	if (etData[vf_targettype].tandemGlints.dpG.x == 0 && etData[vf_targettype].tandemGlints.bpG.x == 0)
	{
		if ((vf_state == VFM_WHITE || vf_state == VFM_WAITINGTIME) && etData[1-vf_targettype].tandemGlints.dpG.x != 0 || etData[1-vf_targettype].tandemGlints.bpG.x != 0 && !vf_targets[0].training)
		{
			nlohmann::json s_rawdata;
			s_rawdata["chartTypeString"] = "SACCADES";
			s_rawdata["message_type"] = MESSAGE_TYPE::DATA_PACKAGE;
			s_rawdata["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
			s_rawdata["x"] = 0;
			s_rawdata["y"] = 0;
			Point2f rdp2 = lcSubs(aninpoint, (etData[1 - vf_targettype].tandemGlints.bpG + etData[1 - vf_targettype].tandemGlints.dpG) / 2);
			s_rawdata["x2"] = rdp2.x;
			s_rawdata["y2"] = rdp2.y;
			s_rawdata["lineType"] = 0;
			s_rawdata["lineColor"] = "#FFFF00";
			webComm->sendJSON(s_rawdata);
		}
		return;
	}
	else
	{
		tsDif = camTS - prevTS;
		prevTS = camTS;

		vf_eye = (etData[vf_targettype].tandemGlints.dpG + etData[vf_targettype].tandemGlints.bpG)/2;
		vf_preveye = vf_eye;
		if (lcDist(vf_eye, vf_fix_point) < vf_whiteFixationRadius)
		{
			if (vf_fixationcount < 40)
			{
				vf_fixationcount++;
			}
			else
			{
				vf_prev_fix_pos = vf_eye;
				anendpoint = (etData[1 - vf_targettype].tandemGlints.bpG + etData[1 - vf_targettype].tandemGlints.dpG) / 2;

				vf_fixationcount = 0;
				vf_fixation = true;
			}
		}
		else
		{
			vf_fix_point = vf_eye;
			vf_fixation = false;
			vf_fixationcount = 0;
		}

		//ANALYSIS
		float ddistpx = lcDist(vf_init, vf_eye);
		float vf_angle_target, vf_angle_eyes;
		int tx = vf_targets[0].whiteTarget.x;
		int ty = vf_targets[0].whiteTarget.y;
		vf_angle_target = fastAtan2(ty, -tx);
		Point2d ae = vf_init - vf_eye;
		vf_angle_eyes = fastAtan2(ae.y, ae.x);
		float anDif = compareAng(vf_angle_target, vf_angle_eyes);
		if (anDif < 30)
		{
			vf_samplesInSector++;
		}
		if (ddistpx > vf_maxDistance)
		{
			vf_maxDistance = ddistpx;
			vf_maxAngle = anDif;
		}

	}


	if (vf_state == VFM_WAITINGTIME)
	{
		if (ggt < vf_greenAnimationTime)
		{
			animateGreen();
		}
		else
		{
			display.showvftex = false;
			display.paintObject(vf_greenPointID, 0,1,0);
			display.resizeObject(vf_greenPointID, 27.);
			display.moveObject(vf_greenPointID, vf_curGPx.x, vf_curGPx.y);
			display.hollowCircle = false;
			display.redrawPending = true;
		}
		if (ggt < 0.4)
		{
			nlohmann::json vf_rawdata;
			vf_rawdata["chartType"] = 22;
			vf_rawdata["chartTypeString"] = charttypestring;
			vf_rawdata["message_type"] = MESSAGE_TYPE::DATA_PACKAGE;
			vf_rawdata["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
			Point2f rdp = lcSubs(vf_init, vf_eye);
			vf_rawdata["x"] = rdp.x;
			vf_rawdata["y"] = rdp.y;
			vf_rawdata["lineType"] = 0;
			vf_rawdata["lineColor"] = "#FFFF00";
			if (!vf_targets[0].training)
				webComm->sendJSON(vf_rawdata);

			nlohmann::json s_rawdata;
			s_rawdata["chartType"] = 22;
			s_rawdata["chartTypeString"] = "SACCADES";
			s_rawdata["message_type"] = MESSAGE_TYPE::DATA_PACKAGE;
			s_rawdata["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
			s_rawdata["x"] = rdp.x;
			s_rawdata["y"] = rdp.y;
			Point2f rdp2 = lcSubs(aninpoint, (etData[1 - vf_targettype].tandemGlints.bpG + etData[1 - vf_targettype].tandemGlints.dpG) / 2);
			s_rawdata["x2"] = rdp2.x;
			s_rawdata["y2"] = rdp2.y;
			s_rawdata["lineType"] = 0;
			s_rawdata["lineColor"] = "#FFFF00";
			if (!vf_targets[0].training)
				webComm->sendJSON(s_rawdata);
			return;
		}
		if(ggt>max(vf_waitingTime, vf_greenAnimationTime))
		{
			nlohmann::json vf_stop_peripheral_showing;
			vf_stop_peripheral_showing["chartType"] = 22;
			vf_stop_peripheral_showing["chartTypeString"] = charttypestring;
			vf_stop_peripheral_showing["message_type"] = MESSAGE_TYPE::STOP_PERIPHERAL_SHOWING;
			vf_stop_peripheral_showing["targettype"] = vf_targettype;
			vf_stop_peripheral_showing["fixations"] = vf_fixations;
			vf_stop_peripheral_showing["maxdistance"] = vf_maxDistance;
			vf_stop_peripheral_showing["maxangle"] = vf_maxAngle;
			vf_stop_peripheral_showing["samplesinsector"] = vf_samplesInSector;
			vf_stop_peripheral_showing["targetx"] = vf_targets[0].whiteTarget.x;
			vf_stop_peripheral_showing["targety"] = vf_targets[0].whiteTarget.y;
			vf_stop_peripheral_showing["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
			vf_stop_peripheral_showing["seen"] = vf_seen;
			vf_stop_peripheral_showing["srt"] = vf_srt;
			if (!vf_targets[0].training)
				webComm->sendJSON(vf_stop_peripheral_showing);

			nlohmann::json s_stop_peripheral_showing;
			s_stop_peripheral_showing["chartType"] = 22;
			s_stop_peripheral_showing["chartTypeString"] = "SACCADES";
			s_stop_peripheral_showing["message_type"] = MESSAGE_TYPE::STOP_PERIPHERAL_SHOWING;
			s_stop_peripheral_showing["targettype"] = vf_targettype;
			s_stop_peripheral_showing["fixations"] = vf_fixations;
			s_stop_peripheral_showing["maxdistance"] = vf_maxDistance;
			s_stop_peripheral_showing["maxangle"] = vf_maxAngle;
			s_stop_peripheral_showing["samplesinsector"] = vf_samplesInSector;
			s_stop_peripheral_showing["targetx"] = vf_targets[0].whiteTarget.x;
			s_stop_peripheral_showing["targety"] = vf_targets[0].whiteTarget.y;
			s_stop_peripheral_showing["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
			s_stop_peripheral_showing["fromTest"] = charttypestring;
			s_stop_peripheral_showing["dotsCount"] = nf_dotcnt;
			s_stop_peripheral_showing["seen"] = vf_seen;
			s_stop_peripheral_showing["srt"] = vf_srt;

			if (vf_seen == 2)
				cout << "MD" << endl;

			if (!vf_targets[0].training)
			{
				webComm->sendJSON(s_stop_peripheral_showing);
				vf_randomp = rand() % vf_greenFixationRandomCount;
			}
			else
				vf_randomp = 0;

			if (!vf_secondrun)
				nexttarget();
			else
				vf_state = VFM_PAUSE;

			glfwSetTime(0);
			return;
		}
	}

	if (vf_state == VFM_ONLYGREEN && vf_bigFixCount > vf_greenFixationFixedCount + vf_randomp)
	{
		vf_bigFixCount = 0;
		if ((!ptosis_pic1sent || !ptosis_pic2sent || !ptosis_pic3sent) && vf_targets[0].greenTarget.y == 0 && !ptosistimeout && !vf_targets[0].training)
		{
			vf_state = VFM_PTOSIS;
			ptosis_needpic = true;
			ptosistimeout = false;
			glfwSetTime(0);
		}
		else
		{
			showwhite(frameid, camTS);
			ptosistimeout = false;
		}
		return;
	}

	if (vf_state == VFM_PTOSIS && (ggt > 2 || (ptosis_pic1sent && ptosis_pic2sent && ptosis_pic3sent)))
	{
		ptosistimeout = true;
		ptosis_needpic = false;
		vf_state = VFM_ONLYGREEN;
		glfwSetTime(0);
	}

	if (vf_state == VFM_ONLYGREEN && ggt > vf_period_show_only_green_in_beginning_big)
	{
		nlohmann::json vf_start_peripheral_showing;
		vf_start_peripheral_showing["chartType"] = 22;
		vf_start_peripheral_showing["chartTypeString"] = charttypestring;
		vf_start_peripheral_showing["message_type"] = MESSAGE_TYPE::START_PERIPHERAL_SHOWING;
		vf_start_peripheral_showing["targettype"] = vf_targettype;
		vf_start_peripheral_showing["targetx"] = vf_targets[0].whiteTarget.x;
		vf_start_peripheral_showing["targety"] = vf_targets[0].whiteTarget.y;
		vf_start_peripheral_showing["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
		if(!vf_targets[0].training)
			webComm->sendJSON(vf_start_peripheral_showing);

		nlohmann::json vf_stop_peripheral_showing;
		vf_stop_peripheral_showing["chartType"] = 22;
		vf_stop_peripheral_showing["chartTypeString"] = charttypestring;
		vf_stop_peripheral_showing["message_type"] = MESSAGE_TYPE::STOP_PERIPHERAL_SHOWING;
		vf_stop_peripheral_showing["targettype"] = vf_targettype;
		vf_stop_peripheral_showing["fixations"] = 0;
		vf_stop_peripheral_showing["maxdistance"] = 0;
		vf_stop_peripheral_showing["maxangle"] = 0;
		vf_stop_peripheral_showing["samplesinsector"] = 0;
		vf_stop_peripheral_showing["targetx"] = vf_targets[0].whiteTarget.x;
		vf_stop_peripheral_showing["targety"] = vf_targets[0].whiteTarget.y;
		vf_stop_peripheral_showing["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
		vf_stop_peripheral_showing["seen"] = 2;
		vf_stop_peripheral_showing["srt"] = 0;
		if (!vf_targets[0].training)
			webComm->sendJSON(vf_stop_peripheral_showing);

		nlohmann::json s_start_peripheral_showing;
		s_start_peripheral_showing["chartType"] = 22;
		s_start_peripheral_showing["chartTypeString"] = "SACCADES";
		s_start_peripheral_showing["message_type"] = MESSAGE_TYPE::START_PERIPHERAL_SHOWING;
		s_start_peripheral_showing["targettype"] = vf_targettype;
		s_start_peripheral_showing["targetx"] = vf_targets[0].whiteTarget.x;
		s_start_peripheral_showing["targety"] = vf_targets[0].whiteTarget.y;
		s_start_peripheral_showing["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
		if (!vf_targets[0].training)
			webComm->sendJSON(s_start_peripheral_showing);

		nlohmann::json s_stop_peripheral_showing;
		s_stop_peripheral_showing["chartTypeString"] = "SACCADES";
		s_stop_peripheral_showing["message_type"] = MESSAGE_TYPE::STOP_PERIPHERAL_SHOWING;
		s_stop_peripheral_showing["targettype"] = vf_targettype;
		s_stop_peripheral_showing["fixations"] = 0;
		s_stop_peripheral_showing["maxdistance"] = 0;
		s_stop_peripheral_showing["maxangle"] = 0;
		s_stop_peripheral_showing["samplesinsector"] = 0;
		s_stop_peripheral_showing["targetx"] = vf_targets[0].whiteTarget.x;
		s_stop_peripheral_showing["targety"] = vf_targets[0].whiteTarget.y;
		s_stop_peripheral_showing["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
		s_stop_peripheral_showing["fromTest"] = charttypestring;
		s_stop_peripheral_showing["dotsCount"] = nf_dotcnt;
		s_stop_peripheral_showing["seen"] = 2;
		s_stop_peripheral_showing["srt"] = 0;
		if (!vf_targets[0].training)
			webComm->sendJSON(vf_stop_peripheral_showing);

		nexttarget(true);
		if (!vf_targets[0].training)
			vf_randomp = rand() % vf_greenFixationRandomCount;
		else
			vf_randomp = 0;
		glfwSetTime(0);
	}

	if (vf_state == VFM_WHITE && ggt > vf_onsetTime)
	{
		hidewhite(lcDist(vf_init, vf_eye));
		return;
	}

	if (vf_state == VFM_ONLYGREEN)
	{
		if (vf_fixation)
		{
			vf_init.x = vf_eye.x;
			vf_init.y = vf_eye.y;
			aninpoint = (etData[1 - vf_targettype].tandemGlints.bpG + etData[1 - vf_targettype].tandemGlints.dpG) / 2;
			vf_prevOS = (etData[1].tandemGlints.bpG + etData[1].tandemGlints.dpG) / 2;
			vf_prevOD = (etData[0].tandemGlints.bpG + etData[0].tandemGlints.dpG) / 2;
		}
		if (lcDist(vf_eye, vf_init) < vf_greenFixationRadius)
			vf_bigFixCount++;
		else
			vf_bigFixCount = 0;
	}

	if (vf_state == VFM_WHITE)
	{
		if (vf_stFlickerStatus && std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - vf_bltime).count() >= 1000. / vf_stFlickerFrequency)
		{
			vf_whitedoton = !vf_whitedoton;
			if (vf_whitedoton)
			{
				if (vf_fullIntensity > 0)
				{
					double cc = luxConverter[vf_targettype].luxToGrayScale(vf_fullIntensity);
					display.paintObject(vf_whitePointID, cc, cc, cc);
					display.redrawPending = true;
				}
				else
				{
					display.paintObject(vf_whitePointID, curColor[0], curColor[1], curColor[2]);
					display.redrawPending = true;
				}
			}
			else
			{
				display.paintObject(vf_whitePointID, curBGColor[vf_targettype], curBGColor[vf_targettype], curBGColor[vf_targettype]);
				display.redrawPending = true;
			}
			vf_bltime = std::chrono::system_clock::now();
		}
		if (vf_onsetType > 0 && vf_whitedoton)
		{
			float a = ggt / vf_onsetTime;

			if (vf_fullIntensity > 0)
			{
				double cC = 1;
				if (vf_onsetType == 1)
				{
					cC = luxConverter[vf_targettype].luxToGrayScale(vf_bgIntensity + (vf_fullIntensity - vf_bgIntensity) * a);
				}
				if (vf_onsetType == 2)
				{
					cC = bgColor[0] + (1 - bgColor[0]) * a;
				}
				curColor[0] = cC;
				curColor[1] = cC;
				curColor[2] = cC;
			}
			else
			{
				double cC = 1;
				if (vf_onsetType == 1)
				{
					cC = bgColor[0] + (1 - bgColor[0]) * a;
				}
				if (vf_onsetType == 2)
				{
					a = pow(33, ggt / vf_onsetTime) / 33;
					cC = bgColor[0] + (1 - bgColor[0]) * a;
				}
				curColor[0] = cC;
				curColor[1] = cC;
				curColor[2] = cC;
			}

			display.paintObject(vf_whitePointID, curColor[0], curColor[1], curColor[2]);

			display.redrawPending = true;
		}

		if (lcDist(vf_init, vf_eye) < vf_srtRadius)
		{
			vf_srt = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - vf_srtstart).count();
			vf_SRTPoint = vf_eye;
		}

		vfm_anglediffs andtmpOS, andtmpOD;

		andtmpOS.dist = lcDist(vf_prevOS, (etData[1].tandemGlints.bpG + etData[1].tandemGlints.dpG) / 2);
		andtmpOS.camTS = tsDif;

		vf_andiffOS.push_back(andtmpOS);

		andtmpOD.dist = lcDist(vf_prevOD, (etData[0].tandemGlints.bpG + etData[0].tandemGlints.dpG) / 2);
		andtmpOD.camTS = tsDif;

		vf_andiffOD.push_back(andtmpOD);

		vf_prevOS = (etData[1].tandemGlints.bpG + etData[1].tandemGlints.dpG) / 2;
		vf_prevOD = (etData[0].tandemGlints.bpG + etData[0].tandemGlints.dpG) / 2;

		if (frameWasLost)
		{
			cout << lostFramesCount << " frames lost ! " << endl;
			lostframecounter += lostFramesCount;
			frameWasLost = false;
		}
		
		nlohmann::json vf_rawdata;
		vf_rawdata["chartType"] = 22;
		vf_rawdata["chartTypeString"] = charttypestring;
		vf_rawdata["message_type"] = MESSAGE_TYPE::DATA_PACKAGE;
		vf_rawdata["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
		Point2f rdp = lcSubs(vf_init, vf_eye);
		vf_rawdata["x"] = rdp.x;
		vf_rawdata["y"] = rdp.y;
		vf_rawdata["lineType"] = 0;
		vf_rawdata["lineColor"] = "#FFFF00";
		if (!vf_targets[0].training)
			webComm->sendJSON(vf_rawdata);

		nlohmann::json s_rawdata;
		s_rawdata["chartTypeString"] = "SACCADES";
		s_rawdata["message_type"] = MESSAGE_TYPE::DATA_PACKAGE;
		s_rawdata["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
		s_rawdata["x"] = rdp.x;
		s_rawdata["y"] = rdp.y;
		Point2f rdp2 = lcSubs(aninpoint, (etData[1 - vf_targettype].tandemGlints.bpG + etData[1 - vf_targettype].tandemGlints.dpG)/2);
		s_rawdata["x2"] = rdp2.x;
		s_rawdata["y2"] = rdp2.y;
		s_rawdata["lineType"] = 0;
		s_rawdata["lineColor"] = "#FFFF00";
		if (!vf_targets[0].training)
			webComm->sendJSON(s_rawdata);

		if (vf_fixation)
		{
			vf_fixations++;
			vf_fixation = false;
			nlohmann::json vf_fixpointdata;
			vf_fixpointdata["chartType"] = 22;
			vf_fixpointdata["chartTypeString"] = charttypestring;
			vf_fixpointdata["message_type"] = MESSAGE_TYPE::DATA_PACKAGE;
			vf_fixpointdata["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
			Point2f fdp = lcSubs(vf_prev_fix_pos, vf_eye);
			vf_fixpointdata["x"] = fdp.x;
			vf_fixpointdata["y"] = fdp.y;
			vf_fixpointdata["lineType"] = 1;
			vf_fixpointdata["lineColor"] = "#FFFFFF";
			if (!vf_targets[0].training)
				webComm->sendJSON(vf_fixpointdata);

			nlohmann::json s_fixpointdata;
			s_fixpointdata["chartTypeString"] = "SACCADES";
			s_fixpointdata["message_type"] = MESSAGE_TYPE::DATA_PACKAGE;
			s_fixpointdata["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
			s_fixpointdata["x"] = fdp.x;
			s_fixpointdata["y"] = fdp.y;
			s_fixpointdata["lineType"] = 1;
			s_fixpointdata["lineColor"] = "#FFFFFF";
			if (!vf_targets[0].training)
				webComm->sendJSON(s_fixpointdata);

			float distpx = lcDist(vf_init, vf_prev_fix_pos);
			float distdeg = sqrt(pow(vf_targets[0].whiteTarget.x, 2) + pow(vf_targets[0].whiteTarget.y, 2));
			if (distpx > vf_srtRadius)
			{
				if (vf_srt < 110)
				{
//					vf_srt = 0;
					hidewhite(distpx);
				}
				else
				{
					float vf_angle_target, vf_angle_eyes;
					int tx = vf_targets[0].whiteTarget.x;
					int ty = vf_targets[0].whiteTarget.y;
					vf_angle_target = fastAtan2(ty, -tx);
					Point2d ae = vf_init - vf_prev_fix_pos;
					vf_angle_eyes = fastAtan2(ae.y, ae.x);


					float anDif = compareAng(vf_angle_target, vf_angle_eyes);
					testLog << "ANG"
						<< ";" << vf_angle_target //1
						<< ";" << vf_angle_eyes //2
						<< ";" << vf_targets[0].whiteTarget.x //3
						<< ";" << vf_targets[0].whiteTarget.y //4
						<< ";" << ae.x //3
						<< ";" << ae.y //4
						<< endl;

					if (anDif < 30)
					{
						vf_seen = 3;
						hidewhite(distpx);
					}
					else
					{
						vf_seen = 2;
					}
				}
			}
		}
	}
}