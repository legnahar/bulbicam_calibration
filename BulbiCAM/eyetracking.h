/*
	Author:		VLV
	Info:		Eyetracking module declaration
	Version:	2.0.220516
	Updates:	16.05.2022
				- Dilated DPG tracking implements area rule
*/

#pragma once

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/dnn.hpp>
#include <opencv2/core/cuda.hpp>
#include <iostream>
#include <vector>
#include <deque>
#include <stdlib.h>
#include <math.h>
#include <numeric>

#define ETversion "2.0.220516"

struct TandemGlints
{
	cv::Point2f bpG = cv::Point2f();
	cv::Point2f dpG = cv::Point2f(); 
	cv::Point2f bpGraw = cv::Point2f();
	cv::Point2f dpGraw = cv::Point2f();
	cv::Rect2f bpgRect = cv::Rect2f();
	cv::Rect2f dpgRect = cv::Rect2f();
	double bpgArea = 0;
	double dpgArea = 0;
	float angle = 0;

	const TandemGlints& operator = (const TandemGlints& tandemGlints)
	{
		this->bpG = tandemGlints.bpG;
		this->dpG = tandemGlints.dpG;
		this->bpGraw = tandemGlints.bpGraw;
		this->dpGraw = tandemGlints.dpGraw;
		this->bpgRect = tandemGlints.bpgRect;
		this->dpgRect = tandemGlints.dpgRect;
		this->bpgArea = tandemGlints.bpgArea;
		this->dpgArea = tandemGlints.dpgArea;
		this->angle = tandemGlints.angle;

		return *this;
	}

};
struct PursuitValues
{
	float indicator = 0;
	float vectorAngle = 0;
	float vectorLength = 0;
};
struct ThreeDangle
{
	cv::Point2f vsPixel = cv::Point2f(0, 0);
	cv::Size2f eyeAngle = cv::Size2f(-99, -99);
};
struct ETROI
{
	cv::Rect glintsROI = cv::Rect();
	cv::Mat prevDPImg = cv::Mat();
	cv::Mat prevBPImg = cv::Mat();
	cv::Mat prevBPDPImg = cv::Mat();
	TandemGlints prevTanG = TandemGlints();
	cv::RotatedRect lastPupil = cv::RotatedRect();
	cv::Point2f pcenter = cv::Point2f(0, 0);
	std::vector<std::vector<cv::Point>> prevBContours, prevDContours, rcontoursb, rcontoursd;
	cv::Point2f rotPoint = cv::Point2f(0, 0);
	cv::Point2f prevRotPoint = cv::Point2f(0, 0);
};
struct ETData
{
	cv::RotatedRect rawPupilEllipse = cv::RotatedRect();
	cv::RotatedRect pupilEllipse = cv::RotatedRect();
	TandemGlints tandemGlints = TandemGlints();
	TandemGlints dilatedTandemGlints = TandemGlints();
	cv::Point2f rawRotPoint = cv::Point2f(0, 0);
	cv::Point2f rotPoint = cv::Point2f(0, 0);
	cv::Point2f calibShape = cv::Point2f(0, 0);
	PursuitValues pursuitValues = PursuitValues();
	ThreeDangle threeDangle = ThreeDangle();
	float posNoiseBPG = 0;
	float posNoiseDPG = 0;
	float distanceToOptimum = 0;
	float IPD = 0;
	bool isIOL = false;
	bool isPicBright = false;
	bool goodGlintsSequence = false;
	int8_t round = -1;
	uint64_t glintsTimestamp = 0;
	uint64_t pupilsTimestamp = 0;
	double glintsTimestampGL = 0;
	double pupilsTimestampGL = 0;
	uint bpgCandidates = 0;
	uint dpgCandidates = 0;
	int BPcontrast = 0;
	int DPcontrast = 0;
};

struct ETInitStructure
{
	bool inverted = true;
	int BPGthreshold = 250, DPGthreshold = 250;
	cv::Size frameSize = cv::Size(1000, 400);
	cv::Size offset = cv::Size(0, 0);
	cv::Point2f centersOfShapes[2] = { cv::Point2f(0,0), cv::Point2f(0,0) };
	float anglesOfShapes[2] = { -99, -99 };
};
class EyeTracker
{
private:
	friend class ETexam;

	struct DpgCandidate
	{
		cv::Point2f pos;
		float angle;
		float area;
		int thresh;
	};

	struct CandidateTandem
	{
		cv::Point2f bpgCand;
		cv::Point2f dpgCand;
		double bpGarea;
		double dpGarea;
		float angle;
	};

	static constexpr int _xRightEye = 50;
	static constexpr int _yRightEye = 25;
	static constexpr int _glintsRadius = 8;
	static constexpr float _pixPerMm = 14.5;
	static constexpr float _ledToRotZaxis = 197.8;

	float DPGthreshold, BPGthreshold;
	double todpleddist[2];
	bool bpgotten, dpgotten;
	bool inverted;
	cv::Point2f centersOfShapes[2];

	std::vector<cv::Point> dpgAngularContours[2];
	cv::Rect bounrect[2];
	cv::Mat drawnAngularArea[2];

	cv::RotatedRect glintsEllipses[2];
	cv::RotatedRect bigGlintsEllipses[2];

	std::vector<float> distances[2];
	std::vector<cv::Point2f> mid_points[2];
	ETData previousResult[2];
	cv::Rect deafaultImageArea;

	void buildDPGAngularArea(int curROInum);
	void moveContour(const cv::Point2f& dpgRaw, int curROInum);
	void moveMat(const cv::Point2f& dpgRaw, int curROInum);

	bool isPicBright(cv::Mat& frame);
	void prepareROIs(cv::Mat& frame, bool picIsBright);
	std::vector<TandemGlints> glintsTracking(cv::Mat& frame, bool curBright, bool needPupils);
	std::vector<PursuitValues> getPursuitValues(const std::vector<TandemGlints>& glints);
	std::pair<cv::Size2f, double> blueLineRule(cv::Point2f bpg, cv::Point2f dpg, int curROInum, bool israw);
	cv::Point2f convertCoords(cv::Point2f rawPoint, int curROInum);
	cv::Point2f convertCoords2(cv::Point2f rawPoint, int curROInum);

public:
	EyeTracker() {};
	EyeTracker(ETInitStructure etis);

	cv::Size offset;
	std::deque<cv::Point2f> dpgCands[2];
	std::deque<cv::Point2f> bpgCands[2];
	std::deque<cv::Point2f> allBpgs[2];
	std::deque<cv::Point2f> allDpgs[2];
	cv::Mat lastBPimage, lastDPimage, lastBPDPimage, lastDPBPimage, trueLastBPimage, prevLastBPimage;
	std::deque<cv::Point2f> goodBPGinarow[2];
	std::deque<cv::Point2f> goodDPGinarow[2];

	cv::Point2f initCentersOfShapes[2];
	std::vector<ETROI> ROIs;
	cv::Rect defaultBorders[2];

	int frameNum;
	int frameHeight;
	int frameWidth;
	float anglesOfShapes[2];

	bool goodGlintsSequence[2];
	bool picIsBright;
	std::vector<ETData> getTrackingData(cv::Mat& frame, int improc, bool needPupils, int interestingEye = -1);
	TandemGlints DPglintTracking(cv::Mat& DPframe, cv::Mat& trueBPframe, cv::RotatedRect& detectedPupil, int curROInum);
};