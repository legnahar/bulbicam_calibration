#include "webcommunicate.h"
#include <iostream>
#include <iomanip>
#include <sstream> 
#include <thread>
#include <regex>

extern string communicationLogsPath;

std::vector<std::string>
resplit(const std::string& s, std::string rgx_str = "<SP>") {


	std::vector<std::string> elems;

	std::regex rgx(rgx_str);

	std::sregex_token_iterator iter(s.begin(), s.end(), rgx, -1);
	std::sregex_token_iterator end;

	while (iter != end) {
		elems.push_back(*iter);
		++iter;
	}

	return elems;

}

void WebCommunicate::sendpic(vector<unsigned char> mes)
{
    if(isConnected) {
        std::ostringstream oss;
        oss << "<SP>" << std::setfill('0') << std::setw(8) << 3 << "PIC" << "<SP>" << std::setfill('0') << std::setw(8)
            << mes.size();
        std::string var = oss.str();

        write(*socket_server, buffer(oss.str().c_str(), oss.str().length()));
        write(*socket_server, buffer((char *) &mes[0], mes.size()));
    }
}

vector<pair<string,string>> WebCommunicate::SplitJSONString(std::string& str)
{
	size_t first, last = 0;
    vector<pair<string, string>> ret;
	str.erase(std::remove(str.begin(), str.end(), '\"'), str.end());
	vector<vector<string>> pointParams;
    int nystagmusarrayfirst=-1, nystagmusarraylast = -1;
    nystagmusarrayfirst = str.find_first_of('[', 0);
    nystagmusarraylast = str.find_first_of(']', nystagmusarrayfirst);
    string nystagmusarray="";
    if (nystagmusarrayfirst > 0 && nystagmusarraylast > 0)
    {
        nystagmusarray = str.substr(nystagmusarrayfirst, nystagmusarraylast- nystagmusarrayfirst+1);
        str.replace(nystagmusarrayfirst, nystagmusarraylast - nystagmusarrayfirst+1, "arr");
        cout << nystagmusarray<<endl;
    }
    cout << str << endl;
    while (last < str.length() - 2)
	{
		first = str.find_first_of('{', last);
		last = str.find_first_of('}', first);
		pointParams.push_back(resplit(str.substr(first + 1, last - first - 1),","));
	}

	for (int i = 0; i < pointParams.size(); i++)
		for (int j = 0; j < pointParams[i].size(); j++)
		{
			first = pointParams[i][j].find_first_of(':');

		}
	int tam, ttp;
	for (int i = 0; i < pointParams.size(); i++)
	{
		for (int j = 0; j < pointParams[i].size(); j++)
		{
			first = pointParams[i][j].find_first_of(':');
			string name = pointParams[i][j].substr(0, first);
			string val = pointParams[i][j].substr(first + 1, pointParams[i][j].length() - first - 1);
            if (val == "arr")
                val = nystagmusarray;
            pair<string, string> tmp;
            tmp.first = name;
            tmp.second = val;
            ret.push_back(tmp);
		}
	}
    return ret;
}

void WebCommunicate::parse(char *buf)
{
	string bufs = string(buf);
	vector<string> commands = resplit(bufs);
	
	for (int i = 1; i < commands.size(); i++)
	{
        if (commands[i].substr(0, 1) == "{")
        {
            vector<pair<string, string>> pairstoprocess = SplitJSONString(bufs);
            dataRead2(pairstoprocess, false);
        }
    }
}

void WebCommunicate::sendmes(const char*mes)
{
    std::ostringstream oss;
    oss << "<SP>" << std::setfill('0') << std::setw(8) << strlen(mes) << mes;
    std::string var = oss.str();
    if(isConnected) {
        write(*socket_server, buffer(var.c_str(), var.length()));
        cout << "SENT : " << var.c_str() << endl;
    }
//    if (logfile.is_open())
//        logfile << var.c_str() << endl;
}

void WebCommunicate::recieve(socket_ptr sock)
{
    try
    {
        for (;;)
        {
            boost::system::error_code error;
            size_t length = sock->read_some(boost::asio::buffer(recvbuf), error);
            if (error == boost::asio::error::eof)
                break;
            else if (error) {
                std::ostringstream oss;
                oss << "Recv failed with error : " << error << endl;
                break;
            }
            if (length > 0)
            {
                printf("Bytes received: %d\n", length);
                printf("%s\n", recvbuf);
                parse(recvbuf);
                for(int i=0;i<4096;i++)
                    recvbuf[i] = '\0';
            }
        }
    }
    catch (std::exception& e)
    {
        std::cerr << "Exception in web communication thread: " << e.what() << "\n";
    }
	std::thread server(&WebCommunicate::startServer, this);
	server.detach();
}

void WebCommunicate::emulaterecv(string line)
{
    parse((char*)line.c_str());
}

void WebCommunicate::startServer()
{
    isConnected = false;
    ip::tcp::acceptor acceptor_server(service_server, ip::tcp::endpoint(ip::tcp::v4(), 5000));
    socket_server = socket_ptr(new ip::tcp::socket(service_server));
    acceptor_server.accept(*socket_server);
    isConnected = true;
	recieve(socket_server);
}

void WebCommunicate::sendJSON(nlohmann::json toSend)
{
	std::ostringstream jss;
	jss << toSend << "\0";
	std::string strtosend = jss.str();

    std::ostringstream oss;
    oss << "<SP>" << std::setfill('0') << std::setw(8) << strtosend.length() << strtosend.c_str();
    std::string var = oss.str();
    if (logfile.is_open())
        logfile << var.c_str() << endl;
    if (isConnected) {
        write(*socket_server, buffer(var.c_str(), var.length()));
    }
}

void WebCommunicate::openLog()
{
	std::ostringstream oss;
	oss << communicationLogsPath << std::chrono::system_clock::now().time_since_epoch().count() << ".log";
	const char* filename = oss.str().c_str();
	if(!logfile.is_open())
		logfile.open(filename);
}

void WebCommunicate::closeLog()
{
	if (logfile.is_open())
		logfile.close();
}