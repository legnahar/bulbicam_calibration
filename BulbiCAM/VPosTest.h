#pragma once
#include <fstream>
#include <chrono>
#include <filesystem>
#include <mutex>
#include "Tests.h"

extern CPTGCamera* ptgreycam;
extern CTest* currentTest;
extern atomic<bool> g_calibrationIsGoing;

#define VPOSTEST_VERSION "1.0.20221027"

class VPosTest : public CTest
{
public:
	VPosTest();
	void inCaseOfCalibration();
	void finishCalibration(bool forced);

private:
	fstream m_history;

	string m_calibrationPath;
	string m_resourcesPath;
	Mat m_dpImg, m_bpImg, m_toDrawOn;
	Point2f m_results[2];
	Point2f m_patternCenters[2];
	Size2f m_patternAngles[2];

	void acquirePictures();
	void readLastPatterns();
	void detectStains();
	void drawResults();
};

VPosTest::VPosTest()
{
	webComm->openLog();
	g_calibrationIsGoing = true;
	cout << "Calibration is executing. VPosTest module" << endl;

	char path[MAX_PATH];
	HMODULE hModule = GetModuleHandleA(NULL);
	GetModuleFileNameA(hModule, path, MAX_PATH);
	PathRemoveFileSpecA(path);
	m_calibrationPath = path;
	m_calibrationPath += "\\calibration";
	m_resourcesPath = m_calibrationPath + "\\resources";

	readLastPatterns();

	nlohmann::json js;
	js["testType"] = "VPOS_TEST";
	js["message_type"] = "START";
	webComm->sendJSON(js);
}

void VPosTest::inCaseOfCalibration()
{
	acquirePictures();
	detectStains();
	drawResults();
	finishCalibration(false);
}

void VPosTest::readLastPatterns()
{
	m_history.open(m_resourcesPath + "\\history.txt", ios_base::in);

	if (m_history.is_open())
	{
		string l_str;
		while (getline(m_history, l_str))
		{
			//cout << l_str << endl;
			size_t l_pos = 0;
			string l_token;
			bool token_found = false;
			vector<string> shapes_params;

			//Check first word
			if ((l_pos = l_str.find(";")) != std::string::npos)
			{
				l_token = l_str.substr(0, l_pos);
				if (l_token == "PatternTest")
				{
					//cout << "FOUND" << endl;
					token_found = true;
					l_str.erase(0, l_pos + 1);
				}
			}

			if (token_found)
			{
				while ((l_pos = l_str.find(";")) != std::string::npos)
				{
					l_token = l_str.substr(0, l_pos);
					if (token_found)
					{
						shapes_params.push_back(l_token);
						l_str.erase(0, l_pos + 1);
					}
				}

				shapes_params.push_back(l_str);

				if (shapes_params.size() == 7)
				{
					m_patternCenters[0] = Point2f(atof(shapes_params[1].c_str()), atof(shapes_params[2].c_str()));
					m_patternCenters[1] = Point2f(atof(shapes_params[4].c_str()), atof(shapes_params[5].c_str()));
				}
				else
				{
					cout << "ERROR: Wrong format of calibration pattern recording." << endl;
					finishCalibration(true);
				}
			}
		}

		if (m_patternCenters[0] == Point2f() && m_patternCenters[1] == Point2f())
		{
			cout << "ERROR: No recordings of calibration pattern detected. Please, perfom pattern test at first." << endl;
			finishCalibration(true);
		}
		else
		{
			cout << "Pattern centers are: " << m_patternCenters[0] << m_patternCenters[1] << endl;
		}

		m_history.close();
	}
	else
	{
		cout << "ERROR: Cannot open history.txt file" << endl;
		finishCalibration(true);
	}
}

void VPosTest::acquirePictures()
{
	m_dpImg = imread(m_resourcesPath + "\\last_dp_pattern.bmp", ImreadModes::IMREAD_UNCHANGED);

	if (m_dpImg.size() != Size(1280, 1024) || m_dpImg.channels() != 1)
	{
		cout << "ERROR: Cannot open dp_pattern.bmp file. Please, perfom pattern test at first." << endl;
		finishCalibration(true);
	}
}

void VPosTest::detectStains()
{
	Size picSize = m_dpImg.size();

	Mat rois[2];

	for (int idx = 0; idx < 2; idx++)
	{
		rois[idx] = m_dpImg(Rect(779 * idx, 0, 499, 1024)).clone();

		Mat thresholded;
		threshold(rois[idx], thresholded, 250, 255, THRESH_BINARY);
		
		vector<vector<Point>> contours;
		findContours(thresholded, contours, RETR_EXTERNAL, CHAIN_APPROX_NONE, Point(779 * idx, 0));

		sort(contours.begin(), contours.end(), [](const vector<Point>& one, const vector<Point>& two)
		{ return contourArea(one) > contourArea(two); });

		if (contours.size() > 0)
		{
			RotatedRect stain = minAreaRect(contours[0]);
			m_results[idx] = stain.center;
		}
	}

	cout << "Stains detected" << endl;
}

void VPosTest::drawResults()
{
	Mat toDrawOn;
	cvtColor(m_dpImg, toDrawOn, ColorConversionCodes::COLOR_GRAY2BGR);

	putText(toDrawOn, "VECTOR LENGTH:", Point(510, 520), FONT_HERSHEY_PLAIN, 1.0, Scalar(200, 200, 255));
	putText(toDrawOn, "VECTOR ANGLE:", Point(510, 560), FONT_HERSHEY_PLAIN, 1.0, Scalar(200, 200, 255));
	putText(toDrawOn, "GLINT CENTERS:", Point(510, 600), FONT_HERSHEY_PLAIN, 1.0, Scalar(200, 200, 255));
	putText(toDrawOn, "PATTERN CENTERS:", Point(510, 640), FONT_HERSHEY_PLAIN, 1.0, Scalar(200, 200, 255));

	for (int idx = 0; idx < 2; idx++)
	{
		circle(toDrawOn, m_patternCenters[idx], 3, Scalar(128, 255, 0), 2);
		circle(toDrawOn, m_results[idx], 3, Scalar(255, 128, 0), 2);
		line(toDrawOn, m_patternCenters[idx], m_results[idx], Scalar(0, 255, 0), 1);

		putText(toDrawOn, to_string(pDist(m_patternCenters[idx], m_results[idx])), Point(510 + 130 * idx, 535), FONT_HERSHEY_PLAIN, 1.0, Scalar(0, 255, 0));
		putText(toDrawOn, to_string(pAng(m_patternCenters[idx], m_results[idx])), Point(510 + 130 * idx, 575), FONT_HERSHEY_PLAIN, 1.0, Scalar(0, 255, 0));

		stringstream l_ss;
		l_ss << m_results[idx];
		putText(toDrawOn, l_ss.str(), Point(510 + 130 * idx, 615), FONT_HERSHEY_PLAIN, 1.0, Scalar(0, 255, 0));

		l_ss.str("");
		l_ss << m_patternCenters[idx];
		putText(toDrawOn, l_ss.str(), Point(510 + 130 * idx, 655), FONT_HERSHEY_PLAIN, 1.0, Scalar(0, 255, 0));

	}

	string imwrite_path = m_calibrationPath + "\\last_detected_patterns.bmp";

	imwrite(imwrite_path, toDrawOn);

	nlohmann::json js;
	js["testType"] = "VPOS_TEST";
	js["message_type"] = "DATA_PACKAGE";
	js["picturePath"] = imwrite_path;
	js["OD_length"] = pDist(m_patternCenters[0], m_results[0]);
	js["OD_angle"] = pAng(m_patternCenters[0], m_results[0]);
	js["OS_length"] = pDist(m_patternCenters[1], m_results[1]);
	js["OS_angle"] = pAng(m_patternCenters[1], m_results[1]);
	webComm->sendJSON(js);
}

void VPosTest::finishCalibration(bool forced)
{
	m_history.open(m_resourcesPath + "\\history.txt", ios_base::app);

	if (!m_history.is_open() || forced)
	{
		cout << "ERROR: Cannot open history.txt file or forced stop" << endl;
	}
	else
	{
		uint64_t l_stopTime = std::chrono::system_clock::now().time_since_epoch().count();
		m_history << "VPosTest;" << l_stopTime << ";"
			<< m_patternCenters[0].x << ";" << m_patternCenters[0].y << ";" << m_results[0].x << ";" << m_results[0].y << ";"
			<< m_patternCenters[1].x << ";" << m_patternCenters[1].y << ";" << m_results[1].x << ";" << m_results[1].y << ";"
			<< pDist(m_patternCenters[0], m_results[0]) << ";" << pAng(m_patternCenters[0], m_results[0]) << ";"
			<< pDist(m_patternCenters[1], m_results[1]) << ";" << pAng(m_patternCenters[1], m_results[1]) << endl;

		m_history.close();
	}

	nlohmann::json js;
	js["testType"] = "VPOS_TEST";
	js["message_type"] = "STOP";
	webComm->sendJSON(js);

	webComm->closeLog();
	cout << "Calibration is finished. VPosTest module" << endl;
}