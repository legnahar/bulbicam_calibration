#pragma once

#include <fstream>
#include <chrono>
#include "eyetracking.h"

#define techstaff_indexes_version "0.9.20210203"

extern EyeTracker et;

//Predefinitions
uchar mask_r = 200, mask_g = 200, mask_b = 200;
float ts_mat_r = 0.2, ts_mat_g = 0.3, ts_mat_b = 0.3;
float shadow_threshold = 0.5;
unsigned int arrow_length = 80;
unsigned int zero_color_coridor = 60;
unsigned int ellipse_D = 280, ellipse_d = 230;
unsigned int shadow_rect_w = 130, shadow_rect_h = 200;
Point arrows_begin[2];

template<typename T>
float pDist(const T& p1, const T& p2)
{
	return sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));
}

template<typename T>
float pAng(const T& p1, const T& p2)
{
	return fastAtan2(p2.y - p1.y, p2.x - p1.x);
}

void drawShadow(Mat& TSdraw, const int& curROInum)
{
	if (curROInum == 0)
	{
		ellipse(TSdraw, arrows_begin[curROInum], Size(40, 40), 95, 0, 60, Scalar::all(0), 10);	 
	}
	if (curROInum == 1)
	{
		ellipse(TSdraw, arrows_begin[curROInum], Size(40, 40), 85, 0, -60, Scalar::all(0), 10);
	}
}

void drawMask(Mat& TSdraw, double maskScale, Point2f maskPos, float thic)
{
	if (maskPos.x > TSdraw.cols - 1  || maskPos.x < 0 || maskPos.y > TSdraw.rows - 1 || maskPos.y < 0)
	{
		//maskPos out of boundaries of Mat
		throw;
	}

	float thickness = thic * maskScale;
	float half_width = 100 * maskScale;
	float half_height = 60 * maskScale;
	float edge_width = half_width / 2;

	Rect2f around = Rect2f(Point2f(maskPos.x - half_width, maskPos.y - half_height), Point2f(maskPos.x + half_width, maskPos.y + half_height));
	Rect2f top = Rect2f(Point2f(maskPos.x - (half_width - edge_width), maskPos.y - half_height), Point2f(maskPos.x + (half_width - edge_width), maskPos.y - half_height + thickness));

	vector<vector<Point>> topContour { vector<Point>() };
	for (int col = top.x; col < top.x + top.width; col++)
	{
		topContour[0].push_back(Point(col, top.y));
		topContour[0].push_back(Point(col, top.y + top.height));
	}
	for (int row = top.y; row < top.y + top.height; row++)
	{
		topContour[0].push_back(Point(top.x, row));
		topContour[0].push_back(Point(top.x + top.width, row));
	}

	fillPoly(TSdraw, topContour, Scalar(mask_b, mask_g, mask_r));

	Point2f basis = Point2f(maskPos.x - half_width / 2, around.y + around.height / 2);
	arrows_begin[0] = basis;

	for (int _thck = 0; _thck <= thickness; _thck++)
	{
		for (int angle = 90; angle <= 270; angle++)
		{
			int pix_x = basis.x + (half_height - _thck) * cos(angle * CV_PI / 180.0);
			int pix_y = basis.y + (half_height - _thck) * sin(angle * CV_PI / 180.0);
			TSdraw.at<Vec3b>(Point(pix_x, pix_y)) = Vec3b(mask_b, mask_g, mask_r);
		}
	}

	Point2f basis2 = Point2f(maskPos.x + half_width / 2, around.y + around.height / 2);
	arrows_begin[1] = basis2;

	for (int _thck = 0; _thck <= thickness; _thck++)
	{
		for (int angle = 270; angle <= 450; angle++)
		{
			int pix_x = basis2.x + (half_height - _thck) * cos(angle * CV_PI / 180.0);
			int pix_y = basis2.y + (half_height - _thck) * sin(angle * CV_PI / 180.0);
			TSdraw.at<Vec3b>(Point(pix_x, pix_y)) = Vec3b(mask_b, mask_g, mask_r);
		}
	}
}

void boundariesInfo(Mat& TSdraw, const Point2f& center, const RotatedRect& _rr, const int& curROInum)
{
	float equation = pow((center.x - _rr.center.x), 2) / pow((_rr.size.height / 2), 2) + pow((center.y - _rr.center.y), 2) / pow((_rr.size.width / 2), 2);
	
	if (equation > 1)
	{
		string _element = "Pupil out of";
		Point2f _position = Size2f(TSdraw.cols * (0.05 + 0.67 * curROInum), TSdraw.rows * 0.48);
		cv::putText(TSdraw, _element, _position, HersheyFonts::FONT_HERSHEY_DUPLEX, 1, Scalar(0, 0, 230), 1.5);

		_element = "boundaries";
		_position = Size2f(TSdraw.cols * (0.05 + 0.67 * curROInum), TSdraw.rows * 0.57);
		cv::putText(TSdraw, _element, _position, HersheyFonts::FONT_HERSHEY_DUPLEX, 1, Scalar(0, 0, 230), 1.5);

		float angle = pAng(_rr.center, center);

		if (curROInum == 0)
		{
			angle -= 90;
		}
		else if (curROInum == 1)
		{
			angle += 90;
		}

		int end_x = arrows_begin[curROInum].x + arrow_length * cos(angle * CV_PI / 180.0);
		int end_y = arrows_begin[curROInum].y + arrow_length * sin(angle * CV_PI / 180.0);

		Point arrow_end = Point(end_x, end_y);

		arrowedLine(TSdraw, arrows_begin[curROInum], arrow_end, Scalar(0, 128, 255), 2, 8, 0, 0.15);
	}

	//arrowedLine(TSdraw, Point(TSdraw_size.width * 0.5, TSdraw_size.height * 0.73), Point(TSdraw_size.width * 0.5, TSdraw_size.height * 0.6), Scalar(0, 0, 255), 5, LineTypes::LINE_AA, 0, 0.35);

}

float shadowInfo(Mat& ETdraw, const int& curROInum)
{
	//Shadow detection
	Rect shadow_roi = Rect((ETdraw.cols - shadow_rect_w - 1) * curROInum, 0, shadow_rect_w, shadow_rect_h);

	//rectangle(ETdraw, shadow_roi, Scalar::all(255));

	unsigned int shadow_pixels_total = 0;
	for (int row = shadow_roi.y; row < shadow_roi.y + shadow_roi.height; ++row)
	{
		//Use pointer method because it's faster
		uchar* bp_pixel = et.trueLastBPimage.ptr(row);
		uchar* dp_pixel = et.lastDPimage.ptr(row);
		uchar* Et_pixel = ETdraw.ptr(row);

		float row_passed_ratio = (float)(row - shadow_roi.y) / shadow_roi.height;
		int col_start, col_end;

		if (curROInum == 0)
		{
			col_start = shadow_roi.x;
			col_end = shadow_roi.x + shadow_roi.width - (shadow_roi.width * row_passed_ratio);
		}
		else if (curROInum == 1)
		{
			col_start = shadow_roi.x + (shadow_roi.width * row_passed_ratio);
			col_end = shadow_roi.x + shadow_roi.width;
		}

		dp_pixel += col_start;
		bp_pixel += col_start;
		Et_pixel += col_start;

		for (int col = col_start; col < col_end; ++col)
		{
			int bp_color = *bp_pixel;
			int dp_color = *dp_pixel;

			//Draw shadow pixels
			if (abs(bp_color - dp_color) <= zero_color_coridor)
			{
				//*Et_pixel = 255;
				shadow_pixels_total++;
			}

			//Draw triangle limit
			//if (curROInum == 0 && col == col_end - 1 || curROInum == 1 && col == col_start)
			//{
			//	*Et_pixel = 0;
			//}

			bp_pixel++; dp_pixel++; Et_pixel++;
		}
	}

	float shadow_area_ratio = (float)shadow_pixels_total / (shadow_roi.area() / 2);

	return shadow_area_ratio;
}

void drawTSinfo(const vector<ETData>& etData, Mat& TSdraw, Mat& ETdraw)
{
	TSdraw.setTo(CV_RGB(ts_mat_r * 255, ts_mat_g * 255, ts_mat_b * 255));
	Size TSdraw_size = TSdraw.size();
	Size ETdraw_size = ETdraw.size();
	string _element;
	Point2f _position;

	_element = "OD: ";
	_position = Size2f(TSdraw_size.width * 0.05, TSdraw_size.height * 0.2);
	cv::putText(TSdraw, _element, _position, HersheyFonts::FONT_HERSHEY_DUPLEX, 1.5, Scalar(255, 255, 0), 1);

	_element = "OS: ";
	_position = Size2f(TSdraw_size.width * 0.55, TSdraw_size.height * 0.2);
	cv::putText(TSdraw, _element, _position, HersheyFonts::FONT_HERSHEY_DUPLEX, 1.5, Scalar(255, 255, 0), 1);

	//_element = "Position";
	//_position = Size2f(TSdraw_size.width * 0.405, TSdraw_size.height * 0.38);
	//cv::putText(TSdraw, _element, _position, HersheyFonts::FONT_HERSHEY_SIMPLEX, 1.5, Scalar::all(0), 3);

	drawMask(TSdraw, 1, Point2f(TSdraw_size.width * 0.5, TSdraw_size.height * 0.5), 15);

	bool strong_shadow = false;
	float shadow_power[2] = { false };

	for (int curROInum = 0; curROInum < etData.size(); curROInum++)
	{
		//Little eyes
		circle(TSdraw, arrows_begin[curROInum], 18, Scalar::all(140), -1);
		circle(TSdraw, arrows_begin[curROInum], 7, Scalar::all(0), -1);

		//Eyes picture ellipse
		RotatedRect _rr = RotatedRect(Point2f(ETdraw.cols / 4 + (ETdraw.cols / 2 * curROInum), ETdraw.rows / 2), Size2f(ellipse_d, ellipse_D), 0);
		ellipse(ETdraw, _rr, Scalar::all(255), 1);

		//All detection
		if (etData.at(curROInum).rawPupilEllipse.size != Size2f(0, 0) && etData.at(curROInum).tandemGlints.bpGraw != Point2f(0, 0))
		{
			_element = "eye detected";
			_position = Size2f(TSdraw_size.width * (0.15 + 0.5 * curROInum), TSdraw_size.height * 0.2);
			cv::putText(TSdraw, _element, _position, HersheyFonts::FONT_HERSHEY_DUPLEX, 1.5, Scalar(0, 230, 0), 1);
			boundariesInfo(TSdraw, etData[curROInum].rawPupilEllipse.center, _rr, curROInum);

			//shadow_power[curROInum] = shadowInfo(ETdraw, curROInum);
		}
		//Glints only
		else if (etData.at(curROInum).tandemGlints.bpGraw != Point2f(0, 0))
		{
			_element = "glints only";
			_position = Size2f(TSdraw_size.width * (0.15 + 0.5 * curROInum), TSdraw_size.height * 0.2);
			cv::putText(TSdraw, _element, _position, HersheyFonts::FONT_HERSHEY_DUPLEX, 1.5, Scalar(0, 230, 230), 1);

			//shadow_power[curROInum] = shadowInfo(ETdraw, curROInum);
		}
		//Nothing
		else
		{
			_element = "no tracking";
			_position = Size2f(TSdraw_size.width * (0.15 + 0.5 * curROInum), TSdraw_size.height * 0.2);
			cv::putText(TSdraw, _element, _position, HersheyFonts::FONT_HERSHEY_DUPLEX, 1.5, Scalar(0, 0, 230), 1);
		}

		//Strong shadow
		if (shadow_power[curROInum] >= shadow_threshold)
		{
			drawShadow(TSdraw, curROInum);
			strong_shadow = true;
		}
	}

	if (strong_shadow)
	{
		_element = "Patient too far back, use thinner mask";
		_position = Size2f(TSdraw_size.width * 0.16, TSdraw_size.height * 0.9);
		cv::putText(TSdraw, _element, _position, HersheyFonts::FONT_HERSHEY_DUPLEX, 1, Scalar(0, 0, 230), 1.5);
	}
}