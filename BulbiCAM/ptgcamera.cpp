#include "ptgcamera.h"
//#define LOCALTEST
CPTGCamera::CPTGCamera(int offset)
	: system(SystemPtr{}), camList(), pCam(0)
{
	try
	{
		system = System::GetInstance();
		camList = system->GetCameras();
		unsigned int numCameras = camList.GetSize();
		if (numCameras == 0)
		{
			camList.Clear();
			system->ReleaseInstance();
			cout << "No cameras detected!" << endl;
			return;
		}
		pCam = camList.GetByIndex(0);
		pCam->Init();
		sn = pCam->DeviceSerialNumber.GetValue();
		if (sn != "17391407")
			pCam->FactoryReset();

		numCameras = 0;
		while (numCameras == 0)
		{
			cout << "Waiting for camera after reset" << endl;
			std::this_thread::sleep_for(std::chrono::seconds(1));
			camList = system->GetCameras();
			numCameras = camList.GetSize();
		}
		pCam = camList.GetByIndex(0);
		INodeMap & nodeMapTLDevice = pCam->GetTLDeviceNodeMap();
		pCam->Init();

		nodeMap = &pCam->GetNodeMap();

		ptrXOffset = nodeMap->GetNode("OffsetX");
		if (!IsAvailable(ptrXOffset) || !IsWritable(ptrXOffset))
		{
			cout << "Unable to set Offset X (Integer node retrieval). Aborting..." << endl << endl;
		}

		ptrYOffset = nodeMap->GetNode("OffsetY");
		if (!IsAvailable(ptrYOffset) || !IsWritable(ptrYOffset))
		{
			cout << "Unable to set Offset Y (Integer node retrieval). Aborting..." << endl << endl;
		}

		CEnumerationPtr ptrExposureAuto = nodeMap->GetNode("ExposureAuto");
		if (!IsAvailable(ptrExposureAuto) || !IsWritable(ptrExposureAuto))
		{
			cout << "Can't set exposure to manual" << endl;
			return;
		}

		CEnumEntryPtr ptrExposureAutoOff = ptrExposureAuto->GetEntryByName("Off");
		if (!IsAvailable(ptrExposureAutoOff) || !IsReadable(ptrExposureAutoOff))
		{
			cout << "Can't set exposure to manual" << endl;
			return;
		}

		CEnumerationPtr ptrGainAuto = nodeMap->GetNode("GainAuto");
		if (!IsAvailable(ptrGainAuto) || !IsWritable(ptrGainAuto))
		{
			cout << "Can't set gain to manual" << endl;
			return;
		}

		CEnumEntryPtr ptrGainAutoOff = ptrGainAuto->GetEntryByName("Off");
		if (!IsAvailable(ptrGainAutoOff) || !IsReadable(ptrGainAutoOff))
		{
			cout << "Can't set gain to manual" << endl;
			return;
		}

		ptrExposureAuto->SetIntValue(ptrExposureAutoOff->GetValue());
		ptrGainAuto->SetIntValue(ptrGainAutoOff->GetValue());

		ptrExposureTime = nodeMap->GetNode("ExposureTime");
		if (!IsAvailable(ptrExposureTime) || !IsWritable(ptrExposureTime))
		{
			cout << "Can't set exposure" << endl;
			return;
		}

		ptrGain = nodeMap->GetNode("Gain");
		if (!IsAvailable(ptrGain) || !IsWritable(ptrGain))
		{
			cout << "Can't set gain" << endl;
			return;
		}

		ptrLevel = nodeMap->GetNode("BlackLevel");
		if (!IsAvailable(ptrLevel) || !IsWritable(ptrLevel))
		{
			cout << "Can't set black level" << endl;
			return;
		}

		Spinnaker::GenApi::INodeMap & sNodeMap = pCam->GetTLStreamNodeMap();

		CEnumerationPtr ptrHandlingMode = sNodeMap.GetNode("StreamBufferHandlingMode");
		if (!IsAvailable(ptrHandlingMode) || !IsWritable(ptrHandlingMode))
		{
			cout << "Unable to set Buffer Handling mode (node retrieval). Aborting..." << endl << endl;
		}

		CEnumEntryPtr ptrHandlingModeEntry = ptrHandlingMode->GetCurrentEntry();
		if (!IsAvailable(ptrHandlingModeEntry) || !IsReadable(ptrHandlingModeEntry))
		{
			cout << "Unable to set Buffer Handling mode (Entry retrieval). Aborting..." << endl << endl;
		}

		// Set stream buffer Count Mode to manual
		CEnumerationPtr ptrStreamBufferCountMode = sNodeMap.GetNode("StreamBufferCountMode");
		if (!IsAvailable(ptrStreamBufferCountMode) || !IsWritable(ptrStreamBufferCountMode))
		{
			cout << "Unable to set Buffer Count Mode (node retrieval). Aborting..." << endl << endl;
		}

		CEnumEntryPtr ptrStreamBufferCountModeManual = ptrStreamBufferCountMode->GetEntryByName("Manual");
		if (!IsAvailable(ptrStreamBufferCountModeManual) || !IsReadable(ptrStreamBufferCountModeManual))
		{
			cout << "Unable to set Buffer Count Mode entry (Entry retrieval). Aborting..." << endl << endl;
		}

		ptrStreamBufferCountMode->SetIntValue(ptrStreamBufferCountModeManual->GetValue());

		cout << "Stream Buffer Count Mode set to manual..." << endl;

		// Retrieve and modify Stream Buffer Count
		CIntegerPtr ptrBufferCount = sNodeMap.GetNode("StreamBufferCountManual");
		if (!IsAvailable(ptrBufferCount) || !IsWritable(ptrBufferCount))
		{
			cout << "Unable to set Buffer Count (Integer node retrieval). Aborting..." << endl << endl;
		}

		// Display Buffer Info
		cout << endl << "Default Buffer Handling Mode: " << ptrHandlingModeEntry->GetDisplayName() << endl;
		cout << "Default Buffer Count: " << ptrBufferCount->GetValue() << endl;
		cout << "Maximum Buffer Count: " << ptrBufferCount->GetMax() << endl;

		ptrBufferCount->SetValue(10);

		cout << "Buffer count now set to: " << ptrBufferCount->GetValue() << endl;

		ptrHandlingModeEntry = ptrHandlingMode->GetEntryByName("NewestOnly");
		ptrHandlingMode->SetIntValue(ptrHandlingModeEntry->GetValue());
		cout << endl << endl << "Buffer Handling Mode has been set to " << ptrHandlingModeEntry->GetDisplayName() << endl;

		pCam->Gain.SetValue(17);
		pCam->BlackLevel.SetValue(0);

		pCam->BeginAcquisition();
		pCam->EndAcquisition();
		ptrHeight = nodeMap->GetNode("Height");
		if (!IsAvailable(ptrHeight) || !IsWritable(ptrHeight))
		{
			cout << "Unable to set Height (Integer node retrieval). Aborting... " << IsAvailable(ptrHeight) << " " << IsWritable(ptrHeight) << endl << endl;
		}

		ptrWidth = nodeMap->GetNode("Width");
		if (!IsAvailable(ptrWidth) || !IsWritable(ptrWidth))
		{
			cout << "Unable to set Height (Integer node retrieval). Aborting... " << IsAvailable(ptrWidth) << " " << IsWritable(ptrWidth) << endl << endl;
		}

		CBooleanPtr ptrChunkModeActive = nodeMap->GetNode("ChunkModeActive");
		if (!IsAvailable(ptrChunkModeActive) || !IsWritable(ptrChunkModeActive))
		{
			cout << "Unable to activate chunk mode..." << endl << endl;
		}
		ptrChunkModeActive->SetValue(true);
		cout << "Chunk mode activated..." << endl;

		NodeList_t entries;
		// Retrieve the selector node
		CEnumerationPtr ptrChunkSelector = nodeMap->GetNode("ChunkSelector");
		if (!IsAvailable(ptrChunkSelector) || !IsReadable(ptrChunkSelector))
		{
			cout << "Unable to retrieve chunk selector..." << endl << endl;
		}
		// Retrieve entries
		CEnumEntryPtr ptrChunkSelectorEntry = ptrChunkSelector->GetEntryByName("ExposureEndLineStatusAll");
		if (!IsAvailable(ptrChunkSelectorEntry) || !IsReadable(ptrChunkSelectorEntry))
		{
			cout << "Unable to set chunk selector entry to Enabled..." << endl << endl;
		}
		ptrChunkSelector->SetIntValue(ptrChunkSelectorEntry->GetValue());
		cout << "\t" << ptrChunkSelectorEntry->GetSymbolic() << ": ";
		CBooleanPtr ptrChunkEnable = nodeMap->GetNode("ChunkEnable");
		if (!IsAvailable(ptrChunkEnable))
		{
			cout << "Node not available" << endl;
		}
		else if (ptrChunkEnable->GetValue())
		{
			cout << "Enabled" << endl;
		}
		else if (IsWritable(ptrChunkEnable))
		{
			ptrChunkEnable->SetValue(true);
			cout << "Enabled" << endl;
		}
		else
		{
			cout << "Node not writable" << endl;
		}

		CEnumEntryPtr ptrChunkSelectorEntry2 = ptrChunkSelector->GetEntryByName("Timestamp");
		if (!IsAvailable(ptrChunkSelectorEntry2) || !IsReadable(ptrChunkSelectorEntry2))
		{
			cout << "Unable to set chunk selector entry to Enabled..." << endl << endl;
		}

		ptrChunkSelector->SetIntValue(ptrChunkSelectorEntry2->GetValue());
		cout << "\t" << ptrChunkSelectorEntry2->GetSymbolic() << ": ";
		CBooleanPtr ptrChunkEnable2 = nodeMap->GetNode("ChunkEnable");
		if (!IsAvailable(ptrChunkEnable2))
		{
			cout << "Node not available" << endl;
		}
		else if (ptrChunkEnable2->GetValue())
		{
			cout << "Enabled" << endl;
		}
		else if (IsWritable(ptrChunkEnable2))
		{
			ptrChunkEnable2->SetValue(true);
			cout << "Enabled" << endl;
		}
		else
		{
			cout << "Node not writable" << endl;
		}

		CEnumEntryPtr ptrChunkSelectorEntry3 = ptrChunkSelector->GetEntryByName("FrameID");
		if (!IsAvailable(ptrChunkSelectorEntry3) || !IsReadable(ptrChunkSelectorEntry3))
		{
			cout << "Unable to set chunk selector entry to Enabled..." << endl << endl;
		}

		ptrChunkSelector->SetIntValue(ptrChunkSelectorEntry3->GetValue());
		cout << "\t" << ptrChunkSelectorEntry3->GetSymbolic() << ": ";
		CBooleanPtr ptrChunkEnable3 = nodeMap->GetNode("ChunkEnable");
		if (!IsAvailable(ptrChunkEnable3))
		{
			cout << "Node not available" << endl;
		}
		else if (ptrChunkEnable3->GetValue())
		{
			cout << "Enabled" << endl;
		}
		else if (IsWritable(ptrChunkEnable3))
		{
			ptrChunkEnable3->SetValue(true);
			cout << "Enabled" << endl;
		}
		else
		{
			cout << "Node not writable" << endl;
		}

		endAcquisition();
		ptrHeight->SetValue(400);
		ptrWidth->SetValue(1000);
		setOffset(140, offset);
		startAcquisition();
		switchDPBP();
	}
	catch (Spinnaker::Exception &e)
	{
		cout << e.what() << endl;
	}
	cout << "Cam init ok..." << endl;
}

CPTGCamera::~CPTGCamera()
{
	try
	{
		pCam->DeInit();
		camList.Clear();
		system->ReleaseInstance();
	}
	catch (Spinnaker::Exception &e)
	{
		cout << e.what() << endl;
	}
	cout << "Cam deinit ok..." << endl;
}

void CPTGCamera::startAcquisition()
{
	try
	{
		if (nodeMap)
		{


			CEnumerationPtr ptrTriggerMode = nodeMap->GetNode("TriggerMode");
			CEnumEntryPtr ptrTriggerModeOff = ptrTriggerMode->GetEntryByName("Off");
			CEnumEntryPtr ptrTriggerModeOn = ptrTriggerMode->GetEntryByName("Off");
			ptrTriggerMode->SetIntValue(ptrTriggerModeOn->GetValue());
			CEnumerationPtr ptrAcquisitionMode = nodeMap->GetNode("AcquisitionMode");
			CEnumEntryPtr ptrAcquisitionModeContinuous = ptrAcquisitionMode->GetEntryByName("Continuous");
			int64_t acquisitionModeContinuous = ptrAcquisitionModeContinuous->GetValue();
			ptrAcquisitionMode->SetIntValue(acquisitionModeContinuous);
			pCam->BeginAcquisition();
			ptrTrigger = nodeMap->GetNode("TriggerSoftware");
		}
	}
	catch (Spinnaker::Exception &e)
	{
		cout << e.what() << endl;
	}
	cout << "Cam start ok..." << endl;
}

void CPTGCamera::endAcquisition()
{
	try
	{
		pCam->EndAcquisition();
	}
	catch (Spinnaker::Exception &e)
	{
		cout << e.what() << endl;
	}
	cout << "Cam end ok..." << endl;
}

bool CPTGCamera::setExposure(int time)
{
	if (!pCam) return false;

	bool bRet = true;
	try
	{
		const double exposureTimeMin = ptrExposureTime->GetMin();
		const double exposureTimeMax = ptrExposureTime->GetMax();
		int exposureTimeToSet = time;

		if (exposureTimeToSet > exposureTimeMax)
		{
			exposureTimeToSet = int(exposureTimeMax);
		}
		if (exposureTimeToSet < exposureTimeMin)
		{
			exposureTimeToSet = int(exposureTimeMin);
		}

		ptrExposureTime->SetValue(exposureTimeToSet);
		expT = pCam->ExposureTime.GetValue();
	}
	catch (Spinnaker::Exception& e)
	{
		bRet = false;
	}

	return bRet;
}

int CPTGCamera::getExposure()
{
	if (!pCam) return false;

	bool bRet = true;
	try
	{
		expT = pCam->ExposureTime.GetValue();
		return expT;
	}
	catch (Spinnaker::Exception& e)
	{
	}
	return 0;
}

bool CPTGCamera::setOffset(int xoffset, int yoffset)
{
	if (!pCam) return false;

	bool bRet = true;
	try
	{
		if (sn == "17391407")
		{
			ptrYOffset->SetValue(yoffset);
//			pCam->OffsetY.SetValue(304);
//			yOffset = 304;
			yOffset = yoffset;
		}
		else
		{
			ptrYOffset->SetValue(yoffset);
//			pCam->OffsetY.SetValue(yoffset);
			yOffset = yoffset;
		}

		cout << "Y Offset " << ptrYOffset->GetValue() << endl;
		pCam->OffsetX.SetValue(xoffset);
		cout << "X Offset " << ptrXOffset->GetValue() << endl;
	}
	catch (Spinnaker::Exception &e)
	{
		cerr << e.what() << endl;
		bRet = false;
	}

	return bRet;
}

Mat CPTGCamera::getImage(uint64_t& lineStatus, uint64_t& timestamp, uint64_t& framenumber)
{
	ImagePtr image = pCam->GetNextImage();
	if (image->IsIncomplete())
	{
		cout << "Image incompleted ..." << endl;
		return Mat::zeros(Size(1280, 1024), CV_8UC1);
	}
	cv::Mat mat(image->GetHeight(), image->GetWidth(), CV_8UC1, image->GetData(), 0);
	ChunkData chunkdata = image->GetChunkData();
	lineStatus = chunkdata.GetExposureEndLineStatusAll();
	timestamp = chunkdata.GetTimestamp();
	framenumber = chunkdata.GetFrameID();
	//	cout << timestamp << endl;
	return mat;
}

void CPTGCamera::switchDPBP()
{
	pCam->LineSelector.SetValue(LineSelectorEnums::LineSelector_Line1);
	pCam->LineMode.SetValue(LineModeEnums::LineMode_Output);
	pCam->LineInverter.SetValue(true);
	pCam->LineSource.SetValue(LineSourceEnums::LineSource_LogicBlock0);
	if (sn != "17391407")
	{
		pCam->LineSelector.SetValue(LineSelectorEnums::LineSelector_Line2);
		pCam->LineMode.SetValue(LineModeEnums::LineMode_Input);
		pCam->LineInverter.SetValue(false);
		pCam->LineSource.SetValue(LineSourceEnums::LineSource_UserOutput0);
	}
	else
	{
		pCam->LineSelector.SetValue(LineSelectorEnums::LineSelector_Line2);
		pCam->LineMode.SetValue(LineModeEnums::LineMode_Output);
		pCam->LineInverter.SetValue(false);
		pCam->LineSource.SetValue(LineSourceEnums::LineSource_LogicBlock0);
	}

	pCam->LogicBlockSelector.SetValue(LogicBlockSelectorEnums::LogicBlockSelector_LogicBlock0);
	pCam->LogicBlockLUTSelector.SetValue(LogicBlockLUTSelectorEnums::LogicBlockLUTSelector_Value);
	pCam->LogicBlockLUTOutputValueAll.SetValue(0x2);
	pCam->LogicBlockLUTSelector.SetValue(LogicBlockLUTSelectorEnums::LogicBlockLUTSelector_Enable);
	pCam->LogicBlockLUTOutputValueAll.SetValue(0xA);

	pCam->LogicBlockLUTInputSelector.SetValue(LogicBlockLUTInputSelectorEnums::LogicBlockLUTInputSelector_Input0);
	pCam->LogicBlockLUTInputSource.SetValue(LogicBlockLUTInputSourceEnums::LogicBlockLUTInputSource_ExposureEnd);
	pCam->LogicBlockLUTInputActivation.SetValue(LogicBlockLUTInputActivationEnums::LogicBlockLUTInputActivation_RisingEdge);

	pCam->LogicBlockLUTInputSelector.SetValue(LogicBlockLUTInputSelectorEnums::LogicBlockLUTInputSelector_Input1);
	pCam->LogicBlockLUTInputSource.SetValue(LogicBlockLUTInputSourceEnums::LogicBlockLUTInputSource_LogicBlock0);
	pCam->LogicBlockLUTInputActivation.SetValue(LogicBlockLUTInputActivationEnums::LogicBlockLUTInputActivation_LevelHigh);

	pCam->LogicBlockLUTInputSelector.SetValue(LogicBlockLUTInputSelectorEnums::LogicBlockLUTInputSelector_Input2);
	pCam->LogicBlockLUTInputSource.SetValue(LogicBlockLUTInputSourceEnums::LogicBlockLUTInputSource_Zero);
}

#pragma region VLV_CALIBRATION_TESTS

void CPTGCamera::shrinkROI(int height)
{
	endAcquisition();
	setHeight(400);
	setWidth(1000);
	setOffset(140, height);
	startAcquisition();
}

bool CPTGCamera::setHeight(int height)
{
	if (!pCam) return false;

	bool bRet = true;
	try
	{
		ptrHeight->SetValue(height);
	}
	catch (Spinnaker::Exception& e)
	{
		bRet = false;
	}

	return bRet;
}

bool CPTGCamera::setWidth(int width)
{
	if (!pCam) return false;

	bool bRet = true;
	try
	{
		ptrWidth->SetValue(width);
	}
	catch (Spinnaker::Exception& e)
	{
		bRet = false;
	}

	return bRet;
}

Mat CPTGCamera::getImage()
{
	ImagePtr image = pCam->GetNextImage();
	if (image->IsIncomplete())
	{
		cout << "Image incompleted ..." << endl;
		return Mat::zeros(Size(1280, 1024), CV_8UC1);
	}
	cv::Mat mat(image->GetHeight(), image->GetWidth(), CV_8UC1, image->GetData(), 0);

	return mat;
}

void CPTGCamera::expandROI()
{
	endAcquisition();
	setOffset(0, 0);
	setHeight(1024);
	setWidth(1280);
	startAcquisition();
}

void CPTGCamera::switchDP()
{
	pCam->LineSelector.SetValue(LineSelectorEnums::LineSelector_Line1);
	pCam->LineMode.SetValue(LineModeEnums::LineMode_Output);
	pCam->LineInverter.SetValue(false);
	pCam->LineSource.SetValue(LineSourceEnums::LineSource_LogicBlock0);

	pCam->LineSelector.SetValue(LineSelectorEnums::LineSelector_Line2);
	pCam->LineMode.SetValue(LineModeEnums::LineMode_Output);
	pCam->LineInverter.SetValue(true);
	pCam->LineSource.SetValue(LineSourceEnums::LineSource_LogicBlock0);

	pCam->LogicBlockSelector.SetValue(LogicBlockSelectorEnums::LogicBlockSelector_LogicBlock0);
	pCam->LogicBlockLUTSelector.SetValue(LogicBlockLUTSelectorEnums::LogicBlockLUTSelector_Value);
	pCam->LogicBlockLUTOutputValueAll.SetValue(0x2);
	pCam->LogicBlockLUTSelector.SetValue(LogicBlockLUTSelectorEnums::LogicBlockLUTSelector_Enable);
	pCam->LogicBlockLUTOutputValueAll.SetValue(0xA);

	pCam->LogicBlockLUTInputSelector.SetValue(LogicBlockLUTInputSelectorEnums::LogicBlockLUTInputSelector_Input0);
	pCam->LogicBlockLUTInputSource.SetValue(LogicBlockLUTInputSourceEnums::LogicBlockLUTInputSource_ExposureEnd);
	pCam->LogicBlockLUTInputActivation.SetValue(LogicBlockLUTInputActivationEnums::LogicBlockLUTInputActivation_RisingEdge);

	pCam->LogicBlockLUTInputSelector.SetValue(LogicBlockLUTInputSelectorEnums::LogicBlockLUTInputSelector_Input1);
	pCam->LogicBlockLUTInputSource.SetValue(LogicBlockLUTInputSourceEnums::LogicBlockLUTInputSource_Zero);
	pCam->LogicBlockLUTInputActivation.SetValue(LogicBlockLUTInputActivationEnums::LogicBlockLUTInputActivation_LevelLow);

	pCam->LogicBlockLUTInputSelector.SetValue(LogicBlockLUTInputSelectorEnums::LogicBlockLUTInputSelector_Input2);
	pCam->LogicBlockLUTInputSource.SetValue(LogicBlockLUTInputSourceEnums::LogicBlockLUTInputSource_Zero);
}

void CPTGCamera::switchBP()
{
	pCam->LineSelector.SetValue(LineSelectorEnums::LineSelector_Line1);
	pCam->LineMode.SetValue(LineModeEnums::LineMode_Output);
	pCam->LineInverter.SetValue(true);
	pCam->LineSource.SetValue(LineSourceEnums::LineSource_LogicBlock0);

	pCam->LineSelector.SetValue(LineSelectorEnums::LineSelector_Line2);
	pCam->LineMode.SetValue(LineModeEnums::LineMode_Output);
	pCam->LineInverter.SetValue(false);
	pCam->LineSource.SetValue(LineSourceEnums::LineSource_LogicBlock0);

	pCam->LogicBlockSelector.SetValue(LogicBlockSelectorEnums::LogicBlockSelector_LogicBlock0);
	pCam->LogicBlockLUTSelector.SetValue(LogicBlockLUTSelectorEnums::LogicBlockLUTSelector_Value);
	pCam->LogicBlockLUTOutputValueAll.SetValue(0x2);
	pCam->LogicBlockLUTSelector.SetValue(LogicBlockLUTSelectorEnums::LogicBlockLUTSelector_Enable);
	pCam->LogicBlockLUTOutputValueAll.SetValue(0xA);

	pCam->LogicBlockLUTInputSelector.SetValue(LogicBlockLUTInputSelectorEnums::LogicBlockLUTInputSelector_Input0);
	pCam->LogicBlockLUTInputSource.SetValue(LogicBlockLUTInputSourceEnums::LogicBlockLUTInputSource_ExposureEnd);
	pCam->LogicBlockLUTInputActivation.SetValue(LogicBlockLUTInputActivationEnums::LogicBlockLUTInputActivation_RisingEdge);

	pCam->LogicBlockLUTInputSelector.SetValue(LogicBlockLUTInputSelectorEnums::LogicBlockLUTInputSelector_Input1);
	pCam->LogicBlockLUTInputSource.SetValue(LogicBlockLUTInputSourceEnums::LogicBlockLUTInputSource_Zero);
	pCam->LogicBlockLUTInputActivation.SetValue(LogicBlockLUTInputActivationEnums::LogicBlockLUTInputActivation_LevelLow);

	pCam->LogicBlockLUTInputSelector.SetValue(LogicBlockLUTInputSelectorEnums::LogicBlockLUTInputSelector_Input2);
	pCam->LogicBlockLUTInputSource.SetValue(LogicBlockLUTInputSourceEnums::LogicBlockLUTInputSource_Zero);
}

#pragma endregion VLV_CALIBRATION_TESTS