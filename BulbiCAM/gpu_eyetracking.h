#pragma once 

/*
	Author:		VLV
	Info:		GPU eyetracking module declarations
	Version:	2.1.220501
	Updates:	01.05.2022
				- Contrasts for Functional screening
				- Separate algorithms for dilated pupil tracking
				- Refactoring
*/

#include "eyetracking.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/dnn.hpp>
#include <opencv2/core/cuda.hpp>
#include <boost/lockfree/stack.hpp>

#define GPUETversion "2.1.220501"

#define COUPLES 8			//Equally distributed on 360 degrees circle (if 4 it means, we have 360/4 = 90 degrees between)
#define ADD_COUPLES 4		//Specially added on certain positions of the circle 
#define COUPLE_DIST 3		//3 pixels is an indent from a border of scanning sircle. We have: 3 in + 3 out + 1 circle = 7 pixels
#define GLINTSRAD 8			//Supposed glint's stain radius
#define MINRAD 9			//Minimum radius of scanning circle
#define MAXRAD 70			//Maximum radius of scanning circle
#define CMP_POINTS 9		//N of points to compare color in gpuTrackingQuality()
#define RAY_TRACES 3		//N of rays to shoot to borders in gpu_calculatePupilDeltas()
#define FRAME_WIDTH 1000	//Camera's frame width in pixels
#define FRAME_HEIGHT 400	//Camera's frame height in pixels
#define ROI_BORDER 67		//MAXRAD + COUPLE_DIST + 2
#define BIG_ROI_WIDTH 90 + ROI_BORDER * 2
#define BIG_ROI_HEIGHT 150 + ROI_BORDER * 2
#define ROI_WIDTH 90
#define ROI_HEIGHT 150
#define ARRAYMULTIPLIER (MAXRAD - MINRAD + 1) * (int)(1.0/0.125)
#define CLASSIC_ROUND_0_THREADS ROI_WIDTH * ROI_HEIGHT / 9

//DILATED PUPIL TRACKING
#define DILATED_ROI_WIDTH 400
#define DILATED_ROI_HEIGHT 300
#define DILATED_ROUND_0_THREADS DILATED_ROI_WIDTH / 2 * DILATED_ROI_HEIGHT / 2

//#define GPUET_SHOW_DEBUG
//#define GPUET_ESTIMATE_TIMING

enum class PupilTrackingType
{
};

struct gpuTrackingQuality
{
	float dPupilPos = 0;
	float dPupilSize = 0;
	int BPcontrast = 0;
	int DPcontrast = 0;
};

struct GpuPoint2f
{
	float x = 0, y = 0;
};
struct GpuRect
{
	int x = 0, y = 0, width = 0, height = 0;

	GpuRect& operator= (const cv::Rect& rect)
	{
		x = rect.x;
		y = rect.y;
		width = rect.width;
		height = rect.height;
		return *this;
	}

	__device__
	bool isPointOutside(const GpuPoint2f& gpuPt)
	{
		return (gpuPt.x <= 0 || gpuPt.x >= width - 1 || gpuPt.y <= 0 || gpuPt.y >= height - 1);
	}
};
struct gpuContrastCouple
{
	int16_t in_x = 0;
	int16_t in_y = 0;
	int16_t out_x = 0;
	int16_t out_y = 0;
	int16_t contrast = 0;
	bool jumped = false;
};

struct Pupil
{
	float center_x = 0;
	float center_y = 0;
	float radius = 0;
	int contrast = 0;
};
struct gtQueue
{
	cv::Mat trueLastBPimage;
	cv::Mat lastBPDPimage;
	cv::Mat lastDPimage;
	uint64_t timestamp;
	double glts;
	TandemGlints tgs[2];
	cv::Point2f calibShape[2];
	cv::Rect defaultBorders[2];
	bool goodGlintsSequence[2];
	bool isBP;
};
struct etQueue
{
	Pupil pupil[2];
	int BPcontrast[2];
	int DPcontrast[2];
	cv::Point2f rotPoint[2];
	TandemGlints tandemGlints[2];
	ThreeDangle threeDdata[2];
	uint64_t timestamp = 0;
	double glts;

	void eraseTrackingData(int curROInum)
	{
		pupil[curROInum] = Pupil();
		BPcontrast[curROInum] = 0;
		DPcontrast[curROInum] = 0;
		rotPoint[curROInum] = cv::Point2f();
		tandemGlints[curROInum] = TandemGlints();
		threeDdata[curROInum] = ThreeDangle();
	}
};
struct camFrame
{
	cv::Mat picture;
	uint64_t timestamp;
	uint64_t frameID;
	uint64_t linestatus;
	double glts;
	bool picBP;
}; 
struct ETResult
{
	std::vector<ETData> etData;
	camFrame frame;
};
struct AllocatedGPUmemorySpace
{
	Pupil* gpuAllocatedPupils;
	float* deviceStorage1;
	float* deviceStorage2;
	uchar* gpuAllocatedImage;
};

extern boost::lockfree::stack<ETResult, boost::lockfree::capacity<100>> filled_et_data;
extern std::vector<ETResult> entireEThistoryData;
extern std::deque<etQueue> smallEThistoryData;

extern cv::Point2f last_succesfull_rots[2];
extern int et_round[2];
extern bool filling_completed;

// if > -1: ETdataFixTiming() performed moving
// if == -1: ETdataFixTiming() didn't perform moving due to correct timestamp
// if == -2: ETdataFixTiming() didn't perform moving due to empty pupil result
extern int last_fixed_index;

__host__ AllocatedGPUmemorySpace allocateGPUmemorySpace();
__host__ void getPointsToStorage1();
__host__ void getPointsToStorage2();


__host__ __device__ bool jumpOverGlint(const TandemGlints& thisTanG, GpuPoint2f& pointIn, GpuPoint2f& pointOut);
__host__ Pupil getCandidatePupil(const AllocatedGPUmemorySpace& agms, std::deque<etQueue>& smallEThistoryData, gtQueue& tmpQueue, cv::Point2f& last_successfull_rot, uint64_t time_limit, int curROInum);
__host__ Pupil getDilatedCandidatePupil(const AllocatedGPUmemorySpace& agms, gtQueue& tmpQueue, uint64_t time_limit, float pupil_diameter, int curROInum);
__host__ cv::Point2f gpu_getRotPoint_center(const cv::Point2f& winner_center, const cv::Point2f& BPglint);
__host__ cv::Point2f gpu_getRotPoint(const Pupil& winner, const cv::Point2f& BPglint);
__host__ ThreeDangle gpu_get3DangleData(const cv::Point2f& rawRotPoint, const cv::Point2f& someCenRaw, const cv::Point2f& calibShape, int curROInum, bool usebpg = false);
__host__ cv::Point2f gpu_convertCoords2(cv::Point2f rawPoint, int curROInum);
__host__ float calculateDilatedPupilSize(int curROInum);

__host__ gpuTrackingQuality gpuQualityControl(const std::deque<etQueue>& smallEThistoryData, cv::Mat& frame, const cv::Mat& lastDPimage, cv::Mat& trueLastBPimage, const int& curROInum, const cv::Point2f& thisRot, const cv::RotatedRect& thisWin, const TandemGlints& thisTanG, const bool& dilated);
__host__ void gpu_calculatePupilDeltas(cv::Mat& frame, const cv::Mat& trueLastBPimage, const cv::RotatedRect& thisWin, const TandemGlints& thisTanG, const  const int& curROInum, gpuTrackingQuality& ret, const cv::Mat& trueLastDPimage);

// if pupil data exists at the end of tracking history storage, move it to according glints (check its timestamp)
__host__ void ETdataFixTiming();

// during the tracking process, we have holes in pupil tracking sometimes because it goes slower, than glints tracking
// this procedure does an estimation and averaging of missing data and fills it to storage
__host__ void TrackingHolesFilling();

// after the filling of tracking holes, according storage usualy has a disorder in timestamps
// this procedure arranges all filled data in a correct way
__host__ void FixStack();

template<typename T> __host__ float gpu_pDist(const T& p1, const T& p2);
template<typename T> __host__ float gpu_pAng(const T& p1, const T& p2);
__host__ float gpu_cam_pix_to_mm(float pix);
__host__ float gpu_cam_mm_to_pix(float mm);
__host__ float gpu_scr_pix_to_mm(float pix);
__host__ float gpu_scr_mm_to_pix(float mm);

__host__ void freeAllocatedGPUmemorySpace(AllocatedGPUmemorySpace& agms);