#pragma once

#include "Tests.h"


class CCalibrationLux : public CTest
{
public:
	CCalibrationLux::CCalibrationLux(int backOD, int backOS);
	void ProcessETData(vector<ETData> etData);
	void changeBackgroundColors(int backOD, int backOS);
};

CCalibrationLux::CCalibrationLux(int backOD, int backOS)
{
	display.stimuliType = DisplayStimuliType::STIMULI;
	display.setBackgroundL(backOS/255., backOS/255., backOS/255.);
	display.setBackgroundR(backOD/255., backOD/255., backOD/255.);
	display.redrawPending = true;
}

void CCalibrationLux::ProcessETData(vector<ETData> etData)
{

}

void CCalibrationLux::changeBackgroundColors(int backOD, int backOS)
{
	display.setBackgroundL(backOS / 255., backOS / 255., backOS / 255.);
	display.setBackgroundR(backOD / 255., backOD / 255., backOD / 255.);
	display.redrawPending = true;
}