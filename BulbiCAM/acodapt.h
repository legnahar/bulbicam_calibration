#pragma once

#include "Tests.h"
#include <filesystem>
#include "LuxConverter.h"
extern LuxConverter luxConverter[2];

#define contrastversion "0.9.20221019"
extern CTest* currentTest;
extern ofstream qualityLog;
extern ofstream testLog;
extern string testLogsPath;

enum AMD_DIRROT
{
	AMD_DR_CW,
	AMD_DR_CCW
};

enum AMD_STATES
{
	AMD_MOVING,
	AMD_PAUSE
};


enum AMD_TESTSTAGES
{
	AMD_ACUITY,
	AMD_CONTRAST,
	AMD_DARKADAPTATION_FC,
	AMD_DARKADAPTATION_FF
};

struct SUInfo
{
	float V1, V2;
	float targetClr[3];
	int bPx, wPx;
	float logmar;
	float pod, pos;
	float frequency;
};

extern string PathToExecutable;

extern float amd_medialLimit;
extern float amd_lateralLimit;
extern float amd_topLimit;
extern float amd_bottomLimit;

extern float amd_radius;
extern int amd_speed;

extern float amd_initNSEyeBGClrR;
extern float amd_initNSEyeBGClrG;
extern float amd_initNSEyeBGClrB;
extern int amd_trackNSEye;

extern float amd_acuityInitBGClrR;
extern float amd_acuityInitBGClrG;
extern float amd_acuityInitBGClrB;

extern float amd_contrastBGClrR;
extern float amd_contrastBGClrG;
extern float amd_contrastBGClrB;

extern float amd_darkadaptationFixedContrastClr[3];
extern float amd_darkadaptationFixedFrequencyFrequency;
extern float amd_darkadaptationBleachDurationFrames;

extern int amd_contrastPxNumber;

struct acodaptConfig
{
	float level;
	float dplux, dblux, bplux, bblux;
	float background;
	int pixels;
	float hertz;
};

extern std::chrono::time_point<std::chrono::system_clock> starttesttime;

class CAMDTest : public CTest
{
public:
	CAMDTest(bool testAcuity, bool testContrast, bool testFixedFrequency, bool testFixedContrast, bool testOD, bool testOS);
	void ProcessETData(vector<ETData> etData);
	void pickDir();
	void nextStage();
	void loadConfig();
	void setStage(int stageNum);
	double angVecsC(Point2f v1, Point2f v2);
	void resetParams();
	float ppDist(Point p1, Point p2);
	bool loadConfigLux();

	int testNum = 28;
	float convertToMM(RotatedRect pupil, int eye);

	int amd_currentEye;
	int amd_state;
	int amd_testCount;
	std::chrono::time_point<std::chrono::system_clock> amd_bleachStart;

	float amd_moveTime;

	Point2f amd_eye;
	Point2d amd_prevTargPos;
	Point2d amd_targPos;
	Point2d amd_nextTargPos;

//	bool amd_showcircle;
	bool amd_showHollowCircle;
	int amd_testStage;

	int amd_samplesInSectorG[2], amd_samplesInSectorPoS[2];
	Point2d amd_initPointGlint[2], amd_initPointPoS[2];
	float compareAng(float a1, float a2);
	Point2d lcSubs(Point2d p1, Point2d p2);
	float lcDist(Point2d p1, Point2d p2);
	float testdur;
	int currentlevel;
	acodaptConfig amd_acuityLevels[10], amd_contrastLevels[10], amd_darkadaptLevelsFC[10], amd_darkadaptLevelsFF[10];

	Point2d amd_smallNextPos, amd_smallPrevPos, amd_smallCurPos;
	float amd_smallMoveTime;
	double amd_oldTime, amd_newTime;
	float amd_smallTimeOverall;
	bool amd_smallMoveToCenter;
	float sectorangle;
	bool amd_testAcuity, amd_testContrast, amd_testFixedFrequency, amd_testFixedContrast;
	bool amd_testOD, amd_testOS;
};

float CAMDTest::convertToMM(RotatedRect pupil, int eye)
{
	float sz = MAX(pupil.size.height, pupil.size.width) / 14.5;
	float lll = 0;
	if (eye == 0)
		lll = -lensDist->dioptersOD;
	else
		lll = -lensDist->dioptersOS;
	float perc = 0;
	if (lll > 0)
		perc = 0.0232;
	else if (lll < 0)
		perc = 0.0178;
	float k = perc * lll;
	return sz - sz * k;
}

float anDifC(float a1, float a2, int dir)
{
	float anDf = a2 - a1;
	if (dir == 0 && anDf > 0)
		anDf -= CV_2PI;
	if (dir == 1 && anDf < 0)
		anDf += CV_2PI;
	return anDf;
}

double CAMDTest::angVecsC(Point2f v1, Point2f v2)
{
	double dp = v1.x * v2.x + v1.y * v2.y;
	double v1l = sqrt(pow(v1.x, 2) + pow(v1.y, 2));
	double v2l = sqrt(pow(v2.x, 2) + pow(v2.y, 2));
	double can = dp / (v1l * v2l);
	double ret = acos(can);
	return ret;
}

float convertPSToMM(RotatedRect pupil, int eye)
{
	float sz = MAX(pupil.size.height, pupil.size.width) / 14.5;
	float lll = 0;
	if (eye == 0)
		lll = -lensDist->dioptersOD;
	else
		lll = -lensDist->dioptersOS;
	float perc = 0;
	if (lll > 0)
		perc = 0.0232;
	else if (lll < 0)
		perc = 0.0178;
	float k = perc * lll;
	return sz - sz * k;
}

float CAMDTest::lcDist(Point2d p1, Point2d p2)
{

	float dist = sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));
	float lll = 0;
	if (p1.x < 1920)
		lll = -lensDist->dioptersOD;
	else
		lll = -lensDist->dioptersOS;
	float perc = 0;
	if (lll > 0)
		perc = 0.0232;
	else if (lll < 0)
		perc = -0.0178;
	float k = perc * lll;

	return dist + dist * k;

}

float CAMDTest::ppDist(Point p1, Point p2) 
{
	return sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));

}

Point2d CAMDTest::lcSubs(Point2d p1, Point2d p2)
{

	float lll = 0;
	if (p1.x<1920)
		lll = -lensDist->dioptersOD;
	else
		lll = -lensDist->dioptersOS;
	float perc = 0;
	if (lll > 0)
		perc = 0.0232;
	else if (lll < 0)
		perc = -0.0178;
	float k = perc * lll;
	Point2d ret = p2 - p1;
	ret.x += ret.x * k;
	ret.y += ret.y * k;

	return ret;

}

float CAMDTest::compareAng(float a1, float a2)
{
	return 180 - abs(abs(a1 - a2) - 180);
}

void CAMDTest::pickDir()
{
	while (true)
	{
		float amd_anDf = (rand() % 30 + 1) * CV_PI / 16.;
		amd_nextTargPos.x = amd_prevTargPos.x + cos(amd_anDf) * amd_radius;
		amd_nextTargPos.y = amd_prevTargPos.y + sin(amd_anDf) * amd_radius;

		if(amd_currentEye==0)
			if (amd_nextTargPos.x < 3940 - amd_medialLimit &&
				amd_nextTargPos.y < 1080 - amd_topLimit &&
				amd_nextTargPos.y > amd_bottomLimit &&
				amd_nextTargPos.x > amd_lateralLimit + 1920)
				break;
		if (amd_currentEye == 1)
			if (amd_nextTargPos.x < 1920 - amd_lateralLimit &&
				amd_nextTargPos.y < 1080 - amd_topLimit &&
				amd_nextTargPos.y > amd_bottomLimit &&
				amd_nextTargPos.x > amd_medialLimit)
				break;


	}
	amd_moveTime = amd_radius / amd_speed;
	cout << "(" << amd_prevTargPos.x << ";" << amd_prevTargPos.y << ")" << "  ==> " << "(" << amd_nextTargPos.x << ";" << amd_nextTargPos.y << ")" << endl;

}

void CAMDTest::nextStage()
{
	if (amd_testStage == -1)
	{
		if (amd_testAcuity)
			setStage(AMD_ACUITY);
		else if (amd_testContrast)
			setStage(AMD_CONTRAST);
		else if (amd_testFixedFrequency)
			setStage(AMD_DARKADAPTATION_FF);
		else if (amd_testFixedContrast)
			setStage(AMD_DARKADAPTATION_FC);
		else
			setStage(-1);
		return;
	}
	if (amd_testStage == AMD_ACUITY)
	{
		if (amd_testContrast)
			setStage(AMD_CONTRAST);
		else if (amd_testFixedFrequency)
			setStage(AMD_DARKADAPTATION_FF);
		else if (amd_testFixedContrast)
			setStage(AMD_DARKADAPTATION_FC);
		else
			setStage(-1);
		return;
	}
	if (amd_testStage == AMD_CONTRAST)
	{
		if (amd_testFixedFrequency)
			setStage(AMD_DARKADAPTATION_FF);
		else if (amd_testFixedContrast)
			setStage(AMD_DARKADAPTATION_FC);
		else
			setStage(-1);
		return;
	}
	if (amd_testStage == AMD_DARKADAPTATION_FC)
	{
		setStage(-1);
	}
	if (amd_testStage == AMD_DARKADAPTATION_FF)
	{
		if (amd_testFixedContrast)
			setStage(AMD_DARKADAPTATION_FC);
		else
			setStage(-1);
		return;
	}
}

void CAMDTest::setStage(int stageNum)
{
	if (stageNum == AMD_CONTRAST)
	{
		display.contrast_amdstage = 1;
		amd_testStage = AMD_CONTRAST;
	}
	else if (stageNum == AMD_DARKADAPTATION_FF)
	{
		display.contrast_amdstage = 1;
		amd_testStage = AMD_DARKADAPTATION_FF;
	}
	else if (stageNum == AMD_ACUITY)
	{
		display.contrast_amdstage = 0;
		amd_testStage = AMD_ACUITY;
	}
	else if (stageNum == AMD_DARKADAPTATION_FC)
	{
		display.contrast_amdstage = 1;
		amd_testStage = AMD_DARKADAPTATION_FC;
	}
	else if (stageNum == -1)
	{
		nlohmann::json js_stopTestMessage;
		js_stopTestMessage["chartTypeString"] = "DARK_ADAPTAION_AMD";
		js_stopTestMessage["message_type"] = STOP_TEST;
		js_stopTestMessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
		webComm->sendJSON(js_stopTestMessage);
		webComm->closeLog();
		qualityLog.close();
		testLog.close();
		display.setDragonfly();
		currentTest = nullptr;
		eyetrackingOn = false;
		return;
	}
	resetParams();
}

void CAMDTest::resetParams()
{
	currentlevel = 7;
	if (amd_currentEye == 0)
	{
		amd_prevTargPos = Point(2880, 540);
		amd_targPos = Point(2880, 540);
		amd_nextTargPos = Point(2880, 540);
	}
	else
	{
		amd_prevTargPos = Point(960, 540);
		amd_targPos = Point(960, 540);
		amd_nextTargPos = Point(960, 540);
	}
	pickDir();
	amd_smallNextPos = amd_prevTargPos;

	if (amd_testStage == AMD_ACUITY)
	{
		display.contrast_logmarindex = amd_acuityLevels[currentlevel].level;
		display.contrast_bClr = luxConverter[amd_currentEye].luxToGrayScale(amd_acuityLevels[currentlevel].dplux);
		display.contrast_wClr = luxConverter[amd_currentEye].luxToGrayScale(amd_acuityLevels[currentlevel].bplux);
		display.contrast_gbclr = luxConverter[amd_currentEye].luxToGrayScale(amd_acuityLevels[currentlevel].bblux);
		display.contrast_gwclr = luxConverter[amd_currentEye].luxToGrayScale(amd_acuityLevels[currentlevel].dblux);

		display.contrast_showCircle = true;
		if (amd_currentEye == 0)
		{
			float cc = luxConverter[0].luxToGrayScale(amd_acuityLevels[currentlevel].background);
			float ccNS = luxConverter[1].luxToGrayScale(amd_initNSEyeBGClrR);
			display.setBackgroundR(cc, cc, cc);
			display.setBackgroundL(ccNS, ccNS, ccNS);
		}
		else
		{
			float ccNS = luxConverter[0].luxToGrayScale(amd_initNSEyeBGClrR);
			float cc = luxConverter[1].luxToGrayScale(amd_acuityLevels[currentlevel].background);
			display.setBackgroundL(cc, cc, cc);
			display.setBackgroundR(ccNS, ccNS, ccNS);
		}
	}
	if (amd_testStage == AMD_CONTRAST)
	{ 
//		display.setBackground(amd_contrastBGClrR, amd_contrastBGClrG, amd_contrastBGClrB);
//		if (amd_currentEye == 1)
//			display.setBackgroundR(amd_initNSEyeBGClrR, amd_initNSEyeBGClrG, amd_initNSEyeBGClrB);
//		else
//			display.setBackgroundL(amd_initNSEyeBGClrR, amd_initNSEyeBGClrG, amd_initNSEyeBGClrB);
		if (amd_currentEye == 0)
		{
			float cc = luxConverter[0].luxToGrayScale(amd_contrastLevels[currentlevel].background);
			float ccNS = luxConverter[1].luxToGrayScale(amd_initNSEyeBGClrR);
			display.setBackgroundR(cc, cc, cc);
			display.setBackgroundL(ccNS, ccNS, ccNS);
		}
		else
		{
			float ccNS = luxConverter[0].luxToGrayScale(amd_initNSEyeBGClrR);
			float cc = luxConverter[1].luxToGrayScale(amd_contrastLevels[currentlevel].background);
			display.setBackgroundL(cc, cc, cc);
			display.setBackgroundR(ccNS, ccNS, ccNS);
		}

		display.contract_tPx = amd_contrastPxNumber;
		float cc = luxConverter[amd_currentEye].luxToGrayScale(amd_contrastLevels[currentlevel].bplux);
		display.contrast_tClr[0] = cc;
		display.contrast_tClr[1] = cc;
		display.contrast_tClr[2] = cc;
		cout << amd_contrastLevels[currentlevel].bplux << "lux->" << cc << "rgb\n";
	}
	if (amd_testStage == AMD_DARKADAPTATION_FC)
	{
//		display.setBackground(amd_contrastBGClrR, amd_contrastBGClrG, amd_contrastBGClrB);
//		if (amd_currentEye == 1)
//			display.setBackgroundR(amd_initNSEyeBGClrR, amd_initNSEyeBGClrG, amd_initNSEyeBGClrB);
//		else
//			display.setBackgroundL(amd_initNSEyeBGClrR, amd_initNSEyeBGClrG, amd_initNSEyeBGClrB);
//		display.contrast_showCircle = true;
		if (amd_currentEye == 0)
		{
			float cc = luxConverter[0].luxToGrayScale(amd_darkadaptLevelsFC[currentlevel].background);
			float ccNS = luxConverter[1].luxToGrayScale(amd_initNSEyeBGClrR);
			display.setBackgroundR(cc, cc, cc);
			display.setBackgroundL(ccNS, ccNS, ccNS);
		}
		else
		{
			float ccNS = luxConverter[0].luxToGrayScale(amd_initNSEyeBGClrR);
			float cc = luxConverter[1].luxToGrayScale(amd_darkadaptLevelsFC[currentlevel].background);
			display.setBackgroundL(cc, cc, cc);
			display.setBackgroundR(ccNS, ccNS, ccNS);
		}

		float cc = luxConverter[amd_currentEye].luxToGrayScale(amd_darkadaptLevelsFC[currentlevel].bplux);
		display.contrast_tClr[0] = cc;
		display.contrast_tClr[1] = cc;
		display.contrast_tClr[2] = cc;
	}
	if (amd_testStage == AMD_DARKADAPTATION_FF)
	{
//		display.setBackground(amd_contrastBGClrR, amd_contrastBGClrG, amd_contrastBGClrB);
//		if (amd_currentEye == 1)
//			display.setBackgroundR(amd_initNSEyeBGClrR, amd_initNSEyeBGClrG, amd_initNSEyeBGClrB);
//		else
//			display.setBackgroundL(amd_initNSEyeBGClrR, amd_initNSEyeBGClrG, amd_initNSEyeBGClrB);
		if (amd_currentEye == 0)
		{
			float cc = luxConverter[0].luxToGrayScale(amd_darkadaptLevelsFF[currentlevel].background);
			float ccNS = luxConverter[1].luxToGrayScale(amd_initNSEyeBGClrR);
			display.setBackgroundR(cc, cc, cc);
			display.setBackgroundL(ccNS, ccNS, ccNS);
		}
		else
		{
			float ccNS = luxConverter[0].luxToGrayScale(amd_initNSEyeBGClrR);
			float cc = luxConverter[1].luxToGrayScale(amd_darkadaptLevelsFF[currentlevel].background);
			display.setBackgroundL(cc, cc, cc);
			display.setBackgroundR(ccNS, ccNS, ccNS);
		}

		display.contract_tPx = amd_contrastPxNumber;


		float cc = luxConverter[amd_currentEye].luxToGrayScale(amd_darkadaptLevelsFF[currentlevel].bplux);
		display.contrast_tClr[0] = cc;
		display.contrast_tClr[1] = cc;
		display.contrast_tClr[2] = cc;
	}
	display.redrawPending = true;

	if (amd_currentEye == 0)
	{
		amd_prevTargPos = Point(2880, 540);
		amd_targPos = Point(2880, 540);
		amd_nextTargPos = Point(2880, 540);
	}
	else
	{
		amd_prevTargPos = Point(960, 540);
		amd_targPos = Point(960, 540);
		amd_nextTargPos = Point(960, 540);
	}
	pickDir();

	amd_state = AMD_PAUSE;
	glfwSetTime(0);
	amd_eye = Point(0, 0);
	amd_bleachStart = std::chrono::system_clock::now();
	starttesttime = std::chrono::system_clock::now();
	glfwSetTime(0);
}

void CAMDTest::loadConfig()
{
	ifstream datfl;
	std::string datfilename = PathToExecutable;
	datfilename.append("\\acolapt.conf");
	datfl.open(datfilename);
	string ld;
	int ln = 0;
	while (getline(datfl, ld))
	{
		vector<string> params = resplitstring(ld, ";");
//		cout << ln << endl;

		cout << luxConverter[0].luxToGrayScale(atof(params[1].c_str())) << ";"
			<< luxConverter[0].luxToGrayScale(atof(params[2].c_str())) << ";"
			<< luxConverter[0].luxToGrayScale(atof(params[3].c_str())) << ";"
			<< luxConverter[0].luxToGrayScale(atof(params[4].c_str())) << ";"
			<< atof(params[1].c_str()) << ";"
			<< atof(params[2].c_str()) << ";"
			<< atof(params[3].c_str()) << ";"
			<< atof(params[4].c_str()) << endl;

		if (ln < 10)
		{
//			cout << "a" << ln<< " " << ld << endl;
			amd_acuityLevels[ln].level = atof(params[0].c_str());
			amd_acuityLevels[ln].dplux = atof(params[1].c_str());
			amd_acuityLevels[ln].dblux = atof(params[2].c_str());
			amd_acuityLevels[ln].bplux = atof(params[3].c_str());
			amd_acuityLevels[ln].bblux = atof(params[4].c_str());
			amd_acuityLevels[ln].background = atof(params[5].c_str());
			amd_acuityLevels[ln].pixels = atof(params[6].c_str());
			amd_acuityLevels[ln].hertz = atof(params[7].c_str());
		}
		else if (ln >= 10 && ln < 20)
		{
//			cout << "b" << ln << " " << ld << endl;
			amd_contrastLevels[ln - 10].level = atof(params[0].c_str());
			amd_contrastLevels[ln - 10].dplux = atof(params[1].c_str());
			amd_contrastLevels[ln - 10].dblux = atof(params[2].c_str());
			amd_contrastLevels[ln - 10].bplux = atof(params[3].c_str());
			amd_contrastLevels[ln - 10].bblux = atof(params[4].c_str());
			amd_contrastLevels[ln - 10].background = atof(params[5].c_str());
			amd_contrastLevels[ln - 10].pixels = atof(params[6].c_str());
			amd_contrastLevels[ln - 10].hertz = atof(params[7].c_str());
		}
		else if (ln >= 20 && ln < 30)
		{
//			cout << "c" << ln << " " << ld << endl;
			amd_darkadaptLevelsFF[ln - 20].level = atof(params[0].c_str());
			amd_darkadaptLevelsFF[ln - 20].dplux = atof(params[1].c_str());
			amd_darkadaptLevelsFF[ln - 20].dblux = atof(params[2].c_str());
			amd_darkadaptLevelsFF[ln - 20].bplux = atof(params[3].c_str());
			amd_darkadaptLevelsFF[ln - 20].bblux = atof(params[4].c_str());
			amd_darkadaptLevelsFF[ln - 20].background = atof(params[5].c_str());
			amd_darkadaptLevelsFF[ln - 20].pixels = atof(params[6].c_str());
			amd_darkadaptLevelsFF[ln - 20].hertz = atof(params[7].c_str());
		}
		else if (ln >= 30)
		{
//			cout << "d" << ln << " " << ld << endl;
			amd_darkadaptLevelsFC[ln - 30].level = atof(params[0].c_str());
			amd_darkadaptLevelsFC[ln - 30].dplux = atof(params[1].c_str());
			amd_darkadaptLevelsFC[ln - 30].dblux = atof(params[2].c_str());
			amd_darkadaptLevelsFC[ln - 30].bplux = atof(params[3].c_str());
			amd_darkadaptLevelsFC[ln - 30].bblux = atof(params[4].c_str());
			amd_darkadaptLevelsFC[ln - 30].background = atof(params[5].c_str());
			amd_darkadaptLevelsFC[ln - 30].pixels = atof(params[6].c_str());
			amd_darkadaptLevelsFC[ln - 30].hertz = atof(params[7].c_str());

		}
		ln++;
	}
	vector<string> cfinfo;
	string line16 = format("amd_acuityInitBGClrR = %.2f", amd_acuityInitBGClrR);
	cfinfo.push_back(line16);
	string line17 = format("amd_acuityInitBGClrG = %.2f", amd_acuityInitBGClrG);
	cfinfo.push_back(line17);
	string line18 = format("amd_acuityInitBGClrB = %.2f", amd_acuityInitBGClrB);
	cfinfo.push_back(line18);
	string line38 = format("amd_contrastPxNumber = %i", amd_contrastPxNumber);
	cfinfo.push_back(line38);
	string line80 = format("amd_contrastBGClrR = %.2f", amd_contrastBGClrR);
	cfinfo.push_back(line80);
	string line81 = format("amd_contrastBGClrG = %.2f", amd_contrastBGClrG);
	cfinfo.push_back(line81);
	string line82 = format("amd_contrastBGClrB = %.2f", amd_contrastBGClrB);
	cfinfo.push_back(line82);
	string line32 = format("amd_medialLimit = %.2f", amd_medialLimit);
	cfinfo.push_back(line32);
	string line35 = format("amd_lateralLimit = %.2f", amd_lateralLimit);
	cfinfo.push_back(line35);
	string line36 = format("amd_topLimit = %.2f", amd_topLimit);
	cfinfo.push_back(line36);
	string line37 = format("amd_bottomLimit = %.2f", amd_bottomLimit);
	cfinfo.push_back(line37);
	string line40 = format("amd_radius = %i", amd_radius);
	cfinfo.push_back(line40);
	string line41 = format("amd_speed = %i", amd_speed);
	cfinfo.push_back(line41);
	string line52 = format("amd_initNSEyeBGClrR = %.2f", amd_initNSEyeBGClrR);
	cfinfo.push_back(line52);
	string line53 = format("amd_initNSEyeBGClrG = %.2f", amd_initNSEyeBGClrG);
	cfinfo.push_back(line53);
	string line54 = format("amd_initNSEyeBGClrB = %.2f", amd_initNSEyeBGClrB);
	cfinfo.push_back(line54);
	string line90 = format("amd_trackNSEye = %i", amd_trackNSEye);
	cfinfo.push_back(line90);
	string line92 = format("amd_darkadaptationBleachDurationFrames = %.2f", amd_darkadaptationBleachDurationFrames);
	cfinfo.push_back(line92);
	string line72 = format("amd_darkadaptationFixedContrastClrR = %.2f", amd_darkadaptationFixedContrastClr[0]);
	cfinfo.push_back(line72);
	string line56 = format("amd_darkadaptationFixedContrastClrG = %.2f", amd_darkadaptationFixedContrastClr[1]);
	cfinfo.push_back(line56);
	string line57 = format("amd_darkadaptationFixedContrastClrB = %.2f", amd_darkadaptationFixedContrastClr[2]);
	cfinfo.push_back(line57);

	string line58 = format("amd_darkadaptationFixedFrequencyFrequency = %.2f", amd_darkadaptationFixedFrequencyFrequency);
	cfinfo.push_back(line58);

	nlohmann::json js_configFileInfo;
	js_configFileInfo["chartTypeString"] = "DARK_ADAPTAION_AMD";
	js_configFileInfo["message_type"] = 4;
	js_configFileInfo["info"] = cfinfo;
	js_configFileInfo["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	webComm->sendJSON(js_configFileInfo);
}

CAMDTest::CAMDTest(bool testAcuity, bool testContrast, bool testFixedFrequency, bool testFixedContrast, bool testOD, bool testOS)
{
	amd_testAcuity = testAcuity;
	amd_testContrast = testContrast;
	amd_testFixedFrequency = testFixedFrequency;
	amd_testFixedContrast = testFixedContrast;
	amd_testOS = testOS;
	amd_testOD = testOD;
	display.contract_tPx = amd_contrastPxNumber;

	sectorangle = 16;
	amd_smallTimeOverall = 0;
	amd_oldTime = 0;
	amd_newTime = 0;
	amd_testCount = 0;
	amd_smallMoveToCenter = false;
	testdur = 0;
//	amd_radius = 480;

	std::ostringstream vffn;
	vffn << testLogsPath << "ac_" << patientInfo.examID << "_" << std::chrono::system_clock::now().time_since_epoch().count() << ".csv";
	std::string vffns = vffn.str();
	testLog.open(vffns);
	webComm->openLog();

	display.stimuliType = DisplayStimuliType::CONTRAST;

	// START MESSAGE
	nlohmann::json js_startTestMessage;
	js_startTestMessage["chartTypeString"] = "DARK_ADAPTAION_AMD";
	js_startTestMessage["message_type"] = START_TEST;
	js_startTestMessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	webComm->sendJSON(js_startTestMessage);
	amd_testStage = -1;
	if(testOD)
		amd_currentEye = 0;
	else
		amd_currentEye = 1;

	loadConfig();
	nextStage();
}

void CAMDTest::ProcessETData(vector<ETData> etData)
{
	if (amd_testStage == AMD_DARKADAPTATION_FF)
	{
		if (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - amd_bleachStart).count() > 1000.f / amd_darkadaptLevelsFF[currentlevel].hertz)
		{
			if (amd_currentEye == 1)
				display.contrast_bleachFramesL = amd_darkadaptationBleachDurationFrames;
			else
				display.contrast_bleachFramesR = amd_darkadaptationBleachDurationFrames;
			amd_bleachStart = std::chrono::system_clock::now();
		}
	}
	if (amd_testStage == AMD_DARKADAPTATION_FC)
	{
		if (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - amd_bleachStart).count() > 1000.f / amd_darkadaptLevelsFC[currentlevel].hertz)
		{
			if (amd_currentEye == 1)
				display.contrast_bleachFramesL = amd_darkadaptationBleachDurationFrames;
			else
				display.contrast_bleachFramesR = amd_darkadaptationBleachDurationFrames;
			amd_bleachStart = std::chrono::system_clock::now();
		}
	}
	display.redrawPending = true;
	float gett = glfwGetTime();
	if (!amd_trackNSEye)
		amd_eye = (etData[amd_currentEye].tandemGlints.bpG + etData[amd_currentEye].tandemGlints.dpG) / 2;
	else
		amd_eye = (etData[1 - amd_currentEye].tandemGlints.bpG + etData[1 - amd_currentEye].tandemGlints.dpG) / 2;
	testdur = gett;
	if (amd_state== AMD_PAUSE)
	{

		if(etData[0].tandemGlints.bpG.x>0 && etData[0].tandemGlints.dpG.x>0)
			amd_initPointGlint[0] = (etData[0].tandemGlints.bpG + etData[0].tandemGlints.dpG) / 2;
		if (etData[1].tandemGlints.bpG.x > 0 && etData[1].tandemGlints.dpG.x > 0)
			amd_initPointGlint[1] = (etData[1].tandemGlints.bpG + etData[1].tandemGlints.dpG) / 2;
		if (etData[0].threeDangle.vsPixel.x != 0)
			amd_initPointPoS[0] = etData[0].threeDangle.vsPixel;
		if (etData[1].threeDangle.vsPixel.x != 0)
			amd_initPointPoS[1] = etData[1].threeDangle.vsPixel;
		if (testdur < 1)
		{
			float distToTargetPx = 0;
			float maxx = 0;
			if (amd_smallNextPos.x > 1920)
				maxx = max(abs(3840 - amd_smallNextPos.x), abs(1920 - amd_smallNextPos.x));
			else
				maxx = max(abs(0 - amd_smallNextPos.x), abs(1920 - amd_smallNextPos.x));
			float maxy = max(abs(3840 - amd_smallNextPos.x), abs(1920 - amd_smallNextPos.x));
			distToTargetPx = sqrt(pow(maxx, 2) + pow(maxy, 2))/4.;
			float r1 = distToTargetPx - distToTargetPx * gett;

			display.ac_hollowCircleR1 = max(0.f, r1 - 27);
			display.ac_hollowCircleR2 = max(0.f, r1);

			if (testdur < amd_smallTimeOverall)
			{
				float d = amd_smallMoveTime - (amd_smallTimeOverall - testdur);
				float dur = (float)amd_smallMoveTime / d;
				if (dur < 1)
					dur = 1;

				amd_smallCurPos = amd_smallPrevPos + (amd_smallNextPos - amd_smallPrevPos) / dur;
				display.contrast_tPosition = amd_smallCurPos;
			}
			else
			{
				amd_smallPrevPos = amd_smallNextPos;
				float amd_anDf = (rand() % 30 + 1) * CV_PI / 16.;
				amd_smallNextPos = Point(amd_prevTargPos.x + cos(amd_anDf) * 30,
					amd_prevTargPos.y + sin(amd_anDf) * 30);
				amd_smallMoveTime = ppDist(amd_smallNextPos, amd_smallPrevPos) / (amd_speed*0.6);
				amd_smallTimeOverall += amd_smallMoveTime;
				display.contrast_tPosition = amd_smallPrevPos;
			}
			display.redrawPending = true;
		}
		if (testdur > 1)
		{
			display.ac_hollowCircle = false;
		}
		if (testdur > 1.5)
		{
			if (!amd_smallMoveToCenter && testdur >= amd_smallTimeOverall)
			{
				amd_smallPrevPos = amd_smallNextPos;
				amd_smallNextPos = amd_prevTargPos;
				amd_smallMoveTime = ppDist(amd_smallNextPos, amd_smallPrevPos) / amd_speed;
				display.contrast_tPosition = amd_smallPrevPos;
				amd_smallTimeOverall += amd_smallMoveTime;
			}
			if (amd_eye.x > 0 && amd_smallCurPos.x == amd_prevTargPos.x && amd_smallCurPos.y == amd_prevTargPos.y)
			{
				amd_smallTimeOverall = 0;
				glfwSetTime(0);
				amd_bleachStart = std::chrono::system_clock::now();
				testdur = 0;
				amd_state = AMD_MOVING;
				display.contrast_showCircle = false;
				testLog << "START" << endl;

				nlohmann::json js_startSequenceMessage;
				js_startSequenceMessage["chartTypeString"] = "DARK_ADAPTAION_AMD";
				js_startSequenceMessage["message_type"] = 15;
				js_startSequenceMessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
				js_startSequenceMessage["speed"] = amd_speed;
				js_startSequenceMessage["eye"] = amd_currentEye;
				webComm->sendJSON(js_startSequenceMessage);

				display.redrawPending = true;
			}
			else
			{
				float d = amd_smallMoveTime - (amd_smallTimeOverall - testdur);
				float dur = (float)amd_smallMoveTime / d;
				if (dur < 1)
					amd_smallCurPos = amd_smallNextPos;
				else
					amd_smallCurPos = amd_smallPrevPos + (amd_smallNextPos - amd_smallPrevPos) / dur;
				display.contrast_tPosition = amd_smallCurPos;
			}

		}
		return;
	}
	else
	{
		float angT = fastAtan2(amd_nextTargPos.y - amd_prevTargPos.y, amd_prevTargPos.x-amd_nextTargPos.x);
		testLog << amd_prevTargPos.x << ";" << amd_prevTargPos.y << ";" << amd_nextTargPos.x << ";" << amd_nextTargPos.y << ";";
		if (etData[0].tandemGlints.bpG.x > 0 && etData[0].tandemGlints.dpG.x > 0)
		{
			testLog << amd_initPointGlint[0].x << ";" << amd_initPointGlint[0].y << ";";
			Point2d tmp = (etData[0].tandemGlints.bpG + etData[0].tandemGlints.dpG) / 2;
			testLog << tmp.x << ";" << tmp.y << ";";
			if (lcDist(tmp, amd_initPointGlint[0]) > 7)
			{
				float angG = fastAtan2(amd_initPointGlint[0].y - tmp.y, tmp.x - amd_initPointGlint[0].x);
				if (compareAng(angT, angG) < sectorangle)
					amd_samplesInSectorG[0]++;
				testLog << compareAng(angT, angG) << "c;";
			}
			else
				testLog << 0 << ";";
		}
		else
		{
			testLog << 0 << ";" << 0 << ";" << 0 << ";";
		}
		if (etData[1].tandemGlints.bpG.x > 0 && etData[1].tandemGlints.dpG.x > 0)
		{
			Point2d tmp = (etData[1].tandemGlints.bpG + etData[1].tandemGlints.dpG) / 2;
			testLog << amd_initPointGlint[1].x << ";" << amd_initPointGlint[1].y << ";";
			testLog << tmp.x << ";" << tmp.y << ";";
			if (lcDist(tmp, amd_initPointGlint[1]) > 7)
			{
				float angG = fastAtan2(amd_initPointGlint[1].y - tmp.y, tmp.x - amd_initPointGlint[1].x);
				if (compareAng(angT, angG) < sectorangle)
					amd_samplesInSectorG[1]++;
				testLog << compareAng(angT, angG) << "c;";
			}
			else
				testLog << 0 << ";";
		}
		else
		{
			testLog << 0 << ";" << 0 << ";" << 0 << ";";
		}
		if (etData[0].threeDangle.vsPixel.x != 0)
		{
			testLog << amd_initPointPoS[0].x << ";" << amd_initPointPoS[0].y << ";";
			testLog << etData[0].threeDangle.vsPixel.x << ";" << etData[0].threeDangle.vsPixel.y << ";";
			if (lcDist(etData[0].threeDangle.vsPixel, amd_initPointPoS[0]) > 350)
			{
				float angG = fastAtan2(etData[0].threeDangle.vsPixel.y - amd_initPointPoS[0].y, etData[0].threeDangle.vsPixel.x - amd_initPointPoS[0].x);
				if (compareAng(angT, angG) < sectorangle)
					amd_samplesInSectorPoS[0]++;
				testLog << compareAng(angT, angG) << "c;";
			}
			else
				testLog << 0 << ";";

		}
		else
		{
			testLog << 0 << ";" << 0 << ";" << 0 << ";";
		}
		if (etData[1].threeDangle.vsPixel.x != 0)
		{
			testLog << amd_initPointPoS[1].x << ";" << amd_initPointPoS[1].y << ";";
			testLog << etData[1].threeDangle.vsPixel.x << ";" << etData[1].threeDangle.vsPixel.y << ";";
			if (lcDist(etData[1].threeDangle.vsPixel, amd_initPointPoS[1]) > 350)
			{
				float angG = fastAtan2(etData[1].threeDangle.vsPixel.y - amd_initPointPoS[1].y, etData[1].threeDangle.vsPixel.x - amd_initPointPoS[1].x);
				if (compareAng(angT, angG) < sectorangle)
					amd_samplesInSectorPoS[1]++;
				testLog << compareAng(angT, angG) << "c;";
			}
			else
				testLog << 0 << ";";
		}
		else
		{
			testLog << 0 << ";" << 0 << ";" << 0 << ";";
		}

	}
	testLog <<  endl;

	nlohmann::json js_startSequenceMessage;
	js_startSequenceMessage["chartTypeString"] = "DARK_ADAPTAION_AMD";
	js_startSequenceMessage["message_type"] = 6;
	js_startSequenceMessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	js_startSequenceMessage["eye"] = amd_currentEye;
	if (amd_testStage != AMD_DARKADAPTATION_FF)
	{
		js_startSequenceMessage["targetColorR"] = amd_contrastLevels[currentlevel].level;
		js_startSequenceMessage["targetColorG"] = amd_contrastLevels[currentlevel].level;
		js_startSequenceMessage["targetColorB"] = amd_contrastLevels[currentlevel].level;
		js_startSequenceMessage["frequency"] = amd_darkadaptLevelsFC[currentlevel].hertz;
	}
	else
	{
		js_startSequenceMessage["targetColorR"] = amd_darkadaptLevelsFF[currentlevel].level;
		js_startSequenceMessage["targetColorG"] = amd_darkadaptLevelsFF[currentlevel].level;
		js_startSequenceMessage["targetColorB"] = amd_darkadaptLevelsFF[currentlevel].level;
		js_startSequenceMessage["frequency"] = amd_darkadaptLevelsFF[currentlevel].hertz;
	}
	js_startSequenceMessage["type"] = amd_testStage;
	js_startSequenceMessage["logmar"] = amd_acuityLevels[currentlevel].level;
	js_startSequenceMessage["pod"] = convertToMM(etData[0].rawPupilEllipse, 0);
	js_startSequenceMessage["pos"] = convertToMM(etData[1].rawPupilEllipse, 1);
	webComm->sendJSON(js_startSequenceMessage);

	if (testdur > amd_moveTime || amd_samplesInSectorG[0] > 100 || amd_samplesInSectorG[1] > 100 || amd_samplesInSectorPoS[0] > 100 || amd_samplesInSectorPoS[1] > 100)
	{
		bool seen = false;
		amd_testCount++;
		testLog << "STOP;" << ";" << testdur << ";" << amd_samplesInSectorG[0] << ";" << amd_samplesInSectorG[1] << ";" << amd_samplesInSectorPoS[0] << ";" << amd_samplesInSectorPoS[1] << endl;
		if (amd_samplesInSectorG[0] < 100 && amd_samplesInSectorG[1] < 100 && amd_samplesInSectorPoS[0] < 100 && amd_samplesInSectorPoS[1] < 100)
		{
//			display.contrast_showCircle = true;
			if (currentlevel < 9)
				currentlevel++;
		}
		else
		{
			if (currentlevel > 0)
				currentlevel--;
			seen = true;
		}

		if (amd_testStage == AMD_ACUITY)
		{
			display.contrast_logmarindex = amd_acuityLevels[currentlevel].level;
			if (amd_currentEye == 0)
			{
				float cc = luxConverter[0].luxToGrayScale(amd_acuityLevels[currentlevel].background);
				float ccNS = luxConverter[1].luxToGrayScale(amd_initNSEyeBGClrR);
				display.setBackgroundR(cc, cc, cc);
				display.setBackgroundL(ccNS, ccNS, ccNS);
			}
			else
			{
				float ccNS = luxConverter[0].luxToGrayScale(amd_initNSEyeBGClrR);
				float cc = luxConverter[1].luxToGrayScale(amd_acuityLevels[currentlevel].background);
				display.setBackgroundL(cc, cc, cc);
				display.setBackgroundR(ccNS, ccNS, ccNS);
			}

		}
		if (amd_testStage == AMD_CONTRAST)
		{
			display.contrast_tClr[0] = luxConverter[amd_currentEye].luxToGrayScale(amd_contrastLevels[currentlevel].bplux);
			display.contrast_tClr[1] = luxConverter[amd_currentEye].luxToGrayScale(amd_contrastLevels[currentlevel].bplux);
			display.contrast_tClr[2] = luxConverter[amd_currentEye].luxToGrayScale(amd_contrastLevels[currentlevel].bplux);
//			cout << amd_contrastLevels[currentlevel].bplux << "lux->" << luxConverter[amd_currentEye].luxToGrayScale(amd_contrastLevels[currentlevel].bplux) << "rgb\n";
			if (amd_currentEye == 0)
			{
				float cc = luxConverter[0].luxToGrayScale(amd_contrastLevels[currentlevel].background);
				float ccNS = luxConverter[1].luxToGrayScale(amd_initNSEyeBGClrR);
				display.setBackgroundR(cc, cc, cc);
				display.setBackgroundL(ccNS, ccNS, ccNS);
			}
			else
			{
				float ccNS = luxConverter[0].luxToGrayScale(amd_initNSEyeBGClrR);
				float cc = luxConverter[1].luxToGrayScale(amd_contrastLevels[currentlevel].background);
				display.setBackgroundL(cc, cc, cc);
				display.setBackgroundR(ccNS, ccNS, ccNS);
			}

		}
		if (amd_testStage == AMD_DARKADAPTATION_FF)
		{
			display.contrast_tClr[0] = luxConverter[amd_currentEye].luxToGrayScale(amd_darkadaptLevelsFF[currentlevel].bplux);
			display.contrast_tClr[1] = luxConverter[amd_currentEye].luxToGrayScale(amd_darkadaptLevelsFF[currentlevel].bplux);
			display.contrast_tClr[2] = luxConverter[amd_currentEye].luxToGrayScale(amd_darkadaptLevelsFF[currentlevel].bplux);

			if (amd_currentEye == 0)
			{
				float cc = luxConverter[0].luxToGrayScale(amd_darkadaptLevelsFF[currentlevel].background);
				float ccNS = luxConverter[1].luxToGrayScale(amd_initNSEyeBGClrR);
				display.setBackgroundR(cc, cc, cc);
				display.setBackgroundL(ccNS, ccNS, ccNS);
			}
			else
			{
				float ccNS = luxConverter[0].luxToGrayScale(amd_initNSEyeBGClrR);
				float cc = luxConverter[1].luxToGrayScale(amd_darkadaptLevelsFF[currentlevel].background);
				display.setBackgroundL(cc, cc, cc);
				display.setBackgroundR(ccNS, ccNS, ccNS);
			}
		}
		if (amd_testStage == AMD_DARKADAPTATION_FC)
		{
			display.contrast_tClr[0] = luxConverter[amd_currentEye].luxToGrayScale(amd_darkadaptLevelsFC[currentlevel].bplux);
			display.contrast_tClr[1] = luxConverter[amd_currentEye].luxToGrayScale(amd_darkadaptLevelsFC[currentlevel].bplux);
			display.contrast_tClr[2] = luxConverter[amd_currentEye].luxToGrayScale(amd_darkadaptLevelsFC[currentlevel].bplux);

			if (amd_currentEye == 0)
			{
				float cc = luxConverter[0].luxToGrayScale(amd_darkadaptLevelsFC[currentlevel].background);
				float ccNS = luxConverter[1].luxToGrayScale(amd_initNSEyeBGClrR);
				display.setBackgroundR(cc, cc, cc);
				display.setBackgroundL(ccNS, ccNS, ccNS);
			}
			else
			{
				float ccNS = luxConverter[0].luxToGrayScale(amd_initNSEyeBGClrR);
				float cc = luxConverter[1].luxToGrayScale(amd_darkadaptLevelsFC[currentlevel].background);
				display.setBackgroundL(cc, cc, cc);
				display.setBackgroundR(ccNS, ccNS, ccNS);
			}
		}
		amd_samplesInSectorG[0] = 0;
		amd_samplesInSectorG[1] = 0;
		amd_samplesInSectorPoS[0] = 0;
		amd_samplesInSectorPoS[1] = 0;

		nlohmann::json js_stopSequenceMessage;
		js_stopSequenceMessage["chartTypeString"] = "DARK_ADAPTAION_AMD";
		js_stopSequenceMessage["message_type"] = 16;
		js_stopSequenceMessage["seen"] = seen;
		js_stopSequenceMessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
		webComm->sendJSON(js_stopSequenceMessage);


		if (amd_testCount > 14)
		{
			amd_testCount = 0;
			if (amd_currentEye == 0)
			{
				if (amd_testOS)
				{
					amd_currentEye = 1;
					resetParams();
				}
				else
				{
					nextStage();
					return;
				}
			}
			else
			{
				if (amd_testOD)
					amd_currentEye = 0;
				nextStage();
				return;
			}
		}
		
		glfwSetTime(0);
		testdur = 0;
		amd_prevTargPos = amd_targPos;
		pickDir();
		display.contrast_tPosition = amd_targPos;
		amd_state = AMD_PAUSE;
		display.ac_hollowCircle = !seen;
		amd_smallNextPos = amd_prevTargPos;
	}
	else
	{
		float dur = (float)amd_moveTime / testdur;

		if (dur < 1)
			dur = 1;

		amd_targPos = amd_prevTargPos + (amd_nextTargPos - amd_prevTargPos) / dur;
		display.contrast_tPosition = amd_targPos;
	}
}
