#pragma once

#include <vector>
#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <boost/asio.hpp>
#define webcversion "0.9.20210805"

#define DEFAULT_BUFLEN 4096
#define DEFAULT_PORT_OS "5000"

using namespace std;
using namespace boost::asio;

typedef boost::shared_ptr<ip::tcp::socket> socket_ptr;

class WebCommunicate
{
private:
	char recvbuf[DEFAULT_BUFLEN];
	char *header, *packdata;
	int recvbuflen = DEFAULT_BUFLEN;
	int dl, hl, position;
    socket_ptr socket_server;
	std::vector<char> recieved;
	std::ofstream logfile;
	io_service service_server;
	void recieve(socket_ptr sock);
	void parse(char* buf);
	vector<pair<string, string>> SplitJSONString(std::string& str);
	bool isConnected;
public:
	WebCommunicate() : header(NULL), packdata(NULL), hl(0), dl(0), position(0) {}
	~WebCommunicate() { if (header) free(header); if (packdata) free(packdata); }
	void startServer();
	void emulaterecv(string line);
	void sendJSON(nlohmann::json toSend);
	void sendmes(const char*mes);
	void (*dataRead2)(vector<pair<string, string>> commands, bool preAssesment);
	void sendpic(vector<unsigned char> mes);
	void openLog();
	void closeLog();
};