#pragma once
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include "eyetracking.h"

using namespace cv;
extern string picturesLogsPath;

struct eyelids
{
	Point upperEyelid;
	Point lowerEyelid;
	Point rawUpperEyelid;
	Point rawLowerEyelid;
	Point centerPupil;
	float pupilSize;
};

Point rotate_point(Point pointToRotate, float code, Point zeropoint, Size initSize, Point zeropointdst)
{
	Point tmp = pointToRotate - zeropoint;
	Point ret;
	if (code == ROTATE_90_COUNTERCLOCKWISE)
	{
		ret.x = tmp.y;
		ret.y = initSize.width - tmp.x;
	}
	else if (code == ROTATE_90_CLOCKWISE)
	{
		ret.x = initSize.height - tmp.y;
		ret.y = tmp.x;
	}
	return ret + zeropointdst;
}

Point bresenham(float x1, float y1, float x2, float y2, int yOfInterest)
{
	// Bresenham's line algorithm
	const bool steep = (fabs(y2 - y1) > fabs(x2 - x1));
	if (steep)
	{
		std::swap(x1, y1);
		std::swap(x2, y2);
	}

	if (x1 > x2)
	{
		std::swap(x1, x2);
		std::swap(y1, y2);
	}

	const float dx = x2 - x1;
	const float dy = fabs(y2 - y1);

	float error = dx / 2.0f;
	const int ystep = (y1 < y2) ? 1 : -1;
	int y = (int)y1;

	const int maxX = (int)x2;

	for (int x = (int)x1; x <= maxX; x++)
	{
		if (steep&&x==yOfInterest)
		{
			return Point(y, x);
		}
		if (!steep && y == yOfInterest)
		{
			return Point(x, y);
		}

		error -= dy;
		if (error < 0)
		{
			y += ystep;
			error += dx;
		}
	}
	return Point(0, 0);

}

vector<eyelids> detectEyelids(Mat frame, vector<ETData> etData)
{
	Point src_center[2] = { Point(frame.cols / 4, frame.rows / 2), Point(frame.cols * 3 / 4, frame.rows / 2) };
	Mat tos = Mat::zeros(frame.cols / 2, frame.rows * 2, CV_8UC1);

	eyelids zeroel;
	zeroel.upperEyelid = Point(0, 0);
	zeroel.lowerEyelid = Point(0, 0);
	zeroel.rawUpperEyelid = Point(0, 0);
	zeroel.rawLowerEyelid = Point(0, 0);
	vector<eyelids> Ret;
	Ret.push_back(zeroel);
	Ret.push_back(zeroel);
	if (etData[0].rawPupilEllipse.center.x == 0 || etData[1].rawPupilEllipse.center.x == 0)
		return Ret;
	Ret[0].pupilSize = max(etData[0].rawPupilEllipse.size.width, etData[0].rawPupilEllipse.size.height);
	Ret[1].pupilSize = max(etData[1].rawPupilEllipse.size.width, etData[1].rawPupilEllipse.size.height);
	Mat ROIs[2];
	Mat ROIsc[2];
	vector<Point> rLow[2];
	vector<Point> rUp[2];
	Mat mag8[2];
	float xsize = 100;
	float ysize = 50;
	for (int curEye = 0; curEye < 2; curEye++)
	{
		if (etData[curEye].rawPupilEllipse.center.x > xsize 
			&& etData[curEye].rawPupilEllipse.center.x < frame.cols-xsize 
			&& etData[curEye].rawPupilEllipse.center.y > ysize
			&& etData[curEye].rawPupilEllipse.center.y < frame.rows-ysize)
		{
			ROIs[curEye] = frame(Rect(etData[curEye].rawPupilEllipse.center.x - 100, etData[curEye].rawPupilEllipse.center.y - 50, 200, 100)).clone();
		}
		else
			return Ret;
		Mat sobelH;
		Mat sobelV;
		Mat mag;
		Mat dir = Mat::zeros(ROIs[curEye].rows, ROIs[curEye].cols, CV_32F);
		GaussianBlur(ROIs[curEye], ROIs[curEye], Size(11, 11), 0, 0, BORDER_DEFAULT);
		equalizeHist(ROIs[curEye], ROIs[curEye]);
		Sobel(ROIs[curEye], sobelH, CV_32F, 1, 0, 3);
		Sobel(ROIs[curEye], sobelV, CV_32F, 0, 1, 3);
		magnitude(sobelH, sobelV, mag);
		mag.convertTo(mag8[curEye], CV_8UC1);

		for (int i = 0; i < ROIs[curEye].rows; i++)
			for (int j = 0; j < ROIs[curEye].cols; j++)
				dir.at<float>(i, j) = fastAtan2(sobelV.at<float>(i, j), sobelH.at<float>(i, j));
		cvtColor(ROIs[curEye], ROIsc[curEye], COLOR_GRAY2BGR);
		for (int i = 0; i < ROIs[curEye].rows; i++)
		{
			for (int j = 0; j < ROIs[curEye].cols / 2; j++)
			{
				float d = dir.at<float>(i, j);
				float m = mag.at<float>(i, j);
				if ((d > 160 && d < 200) && m > 64 && m < 128)
				{
					if (curEye == 0)
					{
						rLow[curEye].push_back(Point(j, i));
						circle(ROIsc[curEye], Point(j, i), 3, CV_RGB(0, 255, 0), 2, -1);
					}
					else
					{
						rUp[curEye].push_back(Point(j, i));
						circle(ROIsc[curEye], Point(j, i), 3, CV_RGB(255, 0, 0), 2, -1);
					}
					break;
				}
			}
		}
		for (int i = 0; i < ROIs[curEye].rows; i++)
		{
			for (int j = ROIs[curEye].cols; j > ROIs[curEye].cols / 2; j--)
			{
				float d = dir.at<float>(i, j);
				float m = mag.at<float>(i, j);
				if ((d > 340 || d < 20) && m > 64 && m < 128)
				{
					if (curEye == 1)
					{
						circle(ROIsc[curEye], Point(j, i), 3, CV_RGB(0, 255, 0), 2, -1);
						rLow[curEye].push_back(Point(j, i));
					}
					else
					{
						circle(ROIsc[curEye], Point(j, i), 3, CV_RGB(255, 0, 0), 2, -1);
						rUp[curEye].push_back(Point(j, i));
					}
					break;
				}
			}
		}
		if (rUp[curEye].size() > 5)
		{
			Vec4f lnUp;
			fitLine(rUp[curEye], lnUp, DIST_HUBER, 0, 0.01, 0.01);
			int lefty = (-lnUp[2] * lnUp[1] / lnUp[0]) + lnUp[3];
			int righty = ((ROIsc[curEye].cols - lnUp[2]) * lnUp[1] / lnUp[0]) + lnUp[3];
			cv::line(ROIsc[curEye], Point(ROIsc[curEye].cols - 1, righty), Point(0, lefty), Scalar(255), 1);
			Point tmpel = bresenham(0, lefty, ROIsc[curEye].cols - 1, righty, ROIsc[curEye].rows / 2);
			circle(ROIsc[curEye], tmpel, 3, CV_RGB(0, 0, 255), 3, -1);
			Ret[curEye].rawUpperEyelid = Point(tmpel.x + etData[curEye].rawPupilEllipse.center.x - 100, etData[curEye].rawPupilEllipse.center.y);
			Ret[curEye].upperEyelid = rotate_point(Ret[curEye].rawUpperEyelid, 2 - curEye * 2, Point(frame.cols * curEye / 2, 0), Size(frame.cols / 2, frame.rows), Point(tos.cols * curEye / 2, 0));
		}
		else
		{
			Ret[curEye].rawUpperEyelid = Point(0, 0);
			Ret[curEye].upperEyelid = Point(0, 0);
		}
		if (rLow[curEye].size() > 5)
		{
			Vec4f lnLow;
			fitLine(rLow[curEye], lnLow, DIST_HUBER, 0, 0.01, 0.01);
			int lefty = (-lnLow[2] * lnLow[1] / lnLow[0]) + lnLow[3];
			int righty = ((ROIsc[curEye].cols - lnLow[2]) * lnLow[1] / lnLow[0]) + lnLow[3];
			Point tmpel = bresenham(0, lefty, ROIsc[curEye].cols - 1, righty, ROIsc[curEye].rows / 2);
			cv::line(ROIsc[curEye], Point(ROIsc[curEye].cols - 1, righty), Point(0, lefty), Scalar(255), 1);
			circle(ROIsc[curEye], tmpel, 3, CV_RGB(0, 0, 255), 3, -1);
			Ret[curEye].rawLowerEyelid = Point(tmpel.x + etData[curEye].rawPupilEllipse.center.x - 100, etData[curEye].rawPupilEllipse.center.y);
			Ret[curEye].lowerEyelid = rotate_point(Ret[curEye].rawLowerEyelid, 2 - curEye * 2, Point(frame.cols * curEye / 2, 0), Size(frame.cols / 2, frame.rows), Point(tos.cols * curEye / 2, 0));
		}
		else
		{
			Ret[curEye].rawLowerEyelid = Point(0, 0);
			Ret[curEye].lowerEyelid = Point(0, 0);
		}
		float angle = -90 + curEye * 180;
		Ret[curEye].centerPupil = rotate_point(etData[curEye].rawPupilEllipse.center, 2 - curEye * 2, Point(frame.cols * curEye / 2, 0), Size(frame.cols / 2, frame.rows), Point(tos.cols * curEye / 2, 0));
		Ret[curEye].pupilSize = etData[curEye].rawPupilEllipse.size.width;
		rotate(frame(Rect(frame.cols * curEye / 2, 0, frame.cols / 2, frame.rows)),
			tos(Rect(tos.cols * curEye / 2, 0, tos.cols / 2, tos.rows)),
			2 - curEye * 2);

//		imshow(format("tmp%d", curEye), ROIsc[curEye]);
//		waitKey(1000);

	}
	cvtColor(tos, tos, COLOR_GRAY2BGR);
	line(tos, Ret[0].lowerEyelid - Point(15, 0), Ret[0].lowerEyelid + Point(15, 0), CV_RGB(0, 255, 0), 3);
	line(tos, Ret[1].lowerEyelid - Point(15, 0), Ret[1].lowerEyelid + Point(15, 0), CV_RGB(0, 255, 0), 3);
	line(tos, Ret[0].upperEyelid - Point(15, 0), Ret[0].upperEyelid + Point(15, 0), CV_RGB(0, 255, 0), 3);
	line(tos, Ret[1].upperEyelid - Point(15, 0), Ret[1].upperEyelid + Point(15, 0), CV_RGB(0, 255, 0), 3);
	circle(tos, Ret[0].centerPupil, Ret[0].pupilSize/2, CV_RGB(255, 0, 0), 2);
	circle(tos, Ret[1].centerPupil, Ret[1].pupilSize / 2, CV_RGB(255, 0, 0), 2);
	circle(tos, Point(tos.cols*0.66,tos.rows*0.66), 3, CV_RGB(255, 0, 0), 2, -1);
//	imshow("tos", tos);

//	std::ostringstream vffn2;
//	vffn2 << picturesLogsPath << "pt_" << patientInfo.examID << "_" << std::chrono::system_clock::now().time_since_epoch().count() << ".png";
//	cv::imwrite(vffn2.str(), tos);

	return Ret;
}