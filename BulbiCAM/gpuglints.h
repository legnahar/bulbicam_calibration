#pragma once

#include "Tests.h"

extern bool picBP;
extern CTest* currentTest;

class CGGTest : public CTest
{
public:
	CGGTest();
	void ProcessETData(vector<ETData> etData);
	int testNum = 12341;
private:
	float convertToMM(RotatedRect pupil, int eye);
	ofstream ofl;
};

float CGGTest::convertToMM(RotatedRect pupil, int eye)
{
	float sz = MAX(pupil.size.height, pupil.size.width) / 14.5;
	float lll = 0;
	if (eye == 0)
		lll = -lensDist->dioptersOD;
	else
		lll = -lensDist->dioptersOS;
	float perc = 0;
	if (lll > 0)
		perc = 0.0232;
	else if (lll < 0)
		perc = 0.0178;
	float k = perc * lll;
	return sz - sz * k;
}

CGGTest::CGGTest()
{
	std::ostringstream vffn;
	vffn << "c:\\testvideos\\gpgl_" << std::chrono::system_clock::now().time_since_epoch().count() << ".csv";
	std::string vffns = vffn.str();

	ofl.open(vffns);

	glfwSetTime(0);
	eyetrackingOn = true;
	display.redrawPending = true;
}

void CGGTest::ProcessETData(vector<ETData> etData)
{
	float stimDuration = glfwGetTime();
	if (stimDuration < 10)
	{
		float odsize = convertToMM(etData[0].rawPupilEllipse, 0);
		float ossize = convertToMM(etData[1].rawPupilEllipse, 1);
		ofl << etData[0].tandemGlints.bpGraw.x << ";"
			<< etData[0].tandemGlints.bpGraw.y << ";"
			<< etData[0].tandemGlints.dpGraw.x << ";"
			<< etData[0].tandemGlints.dpGraw.y << ";"
			<< etData[1].tandemGlints.bpGraw.x << ";"
			<< etData[1].tandemGlints.bpGraw.y << ";"
			<< etData[1].tandemGlints.dpGraw.x << ";"
			<< etData[1].tandemGlints.dpGraw.y << ";"
			<< etData[0].tandemGlints.bpG.x << ";"
			<< etData[0].tandemGlints.bpG.y << ";"
			<< etData[0].tandemGlints.dpG.x << ";"
			<< etData[0].tandemGlints.dpG.y << ";"
			<< etData[1].tandemGlints.bpG.x << ";"
			<< etData[1].tandemGlints.bpG.y << ";"
			<< etData[1].tandemGlints.dpG.x << ";"
			<< etData[1].tandemGlints.dpG.y << ";"
			<< endl;
	}
	else
	{
		ofl.close();
		display.setDragonfly();
		currentTest = nullptr;

		eyetrackingOn = false;
		return;
	}
}