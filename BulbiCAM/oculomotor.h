#pragma once

#include <fstream>
#include <chrono>
#include <filesystem>
#include <mutex>
#include "Tests.h"
#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/archive/iterators/transform_width.hpp>

#define recordingtool_version "0.9.20211018"

extern CTest* currentTest;
extern ofstream qualityLog;
extern float pxToDegreeOS, pxToDegreeOD;
extern string testLogsPath;
extern vector<frameForAI> savedFrames;
extern bool saveFrameSignal;

class COculoMotorTest : public CTest
{
public:
	COculoMotorTest(int proSaccadeEye);
	void ProcessETData(vector<ETData> etData);
private:
	Point2f convertCoords(Point2f rawPoint, int curROInum);
	int omt_greenCircleID[2];
	int omt_targetSpeed = 600;
	Point2d omt_srcPos, omt_dstPos, omt_curPos;
	Point initPos[2];
	int omt_testStage;
	int omt_circleStage;
	float omt_movetime;
	float omt_src2DstDist;
	int omt_currentEye;
	int omt_proSaccadeEye;
	float ppd2;
	bool bgChanged;
	float anDifOD, anDifOS;
	Point2f centralPosOS, peripheralPosOS;
	Point2f centralPosOD, peripheralPosOD;
	int posCountCPOS, posCountCPOD;
	int posCountPPOS, posCountPPOD;
	bool saveFrameSignalSent;
};

float pixtomm(int px)
{
	return px * 121.f / 1920.f;
}

Point2d pixtomm(Point px)
{
	return Point(pixtomm(px.x), pixtomm(px.y));
}

int mmtopix(float mm)
{
	return mm * 1920.f / 121.f;
}

Point mmtopix(Point2d mm)
{
	return Point(mmtopix(mm.x), mmtopix(mm.y));
}

float p2pDist(Point2d p1, Point2d p2)
{
	return sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));
}

Point2f COculoMotorTest::convertCoords(Point2f rawPoint, int curROInum)
{
	float angos = 0;
	if (curROInum == 0)
	{
		angos = 3.5;
	}
	else
	{
		angos = 4.5;
	}

	Point2f cP = Point(1000 / 4 + 1000 / 2 * curROInum, 400 / 2);
	float shift_angle = ((90 + angos) * (2 * curROInum - 1)); 
	float this_angle = fastAtan2(rawPoint.y - cP.y, rawPoint.x - cP.x);
	float _dist = sqrt(pow(cP.x - rawPoint.x, 2) + pow(cP.y - rawPoint.y, 2));

	Point2f newcP = Point(400 / 2 + 400 * curROInum, 1000 / 4);

	Point2f rr;
	rr.x = newcP.x + _dist * cos((this_angle + shift_angle) * CV_PI / 180.0);
	rr.y = newcP.y + _dist * sin((this_angle + shift_angle) * CV_PI / 180.0);

	return rr;
}

float getDifInDegrees(Point2d p1, Point2d p2)
{
	Point2d stimpos1, stimpos2;
	if (p1.x > 1920.f)
	{
		stimpos1.x = pixtomm(3840 - p1.x);
		stimpos1.y = pixtomm(p1.y);
		stimpos2.x = pixtomm(3840 - p2.x);
		stimpos2.y = pixtomm(p2.y);
		Point2d ap1 = lensDist->anglefromposOD(stimpos1);
		Point2d ap2 = lensDist->anglefromposOD(stimpos2);
		cout << stimpos1.x << "  " << stimpos2.x << endl;
		return abs(stimpos1.x-stimpos2.x);
	}
	else
	{
		stimpos1.x = pixtomm(1920 - p1.x);
		stimpos1.y = pixtomm(p1.y);
		stimpos2.x = pixtomm(1920 - p2.x);
		stimpos2.y = pixtomm(p2.y);
		Point2d ap1 = lensDist->anglefromposOS(stimpos1);
		Point2d ap2 = lensDist->anglefromposOS(stimpos2);
		cout << stimpos1.x << "  " << stimpos2.x << endl;
		return abs(stimpos1.x - stimpos2.x);
	}
}

COculoMotorTest::COculoMotorTest(int proSaccadeEye)
{
	omt_proSaccadeEye = proSaccadeEye;
	saveFrameSignalSent = false;

	glfwSetTime(0);
	webComm->openLog();

	std::ostringstream vffn;
	vffn << testLogsPath << "fs_" << patientInfo.examID << "_" << std::chrono::system_clock::now().time_since_epoch().count() << ".csv";
	std::string vffns = vffn.str();

	testLog.open(vffns);

	nlohmann::json nystagmus_start;
	nystagmus_start["chartTypeString"] = "FUNCTIONAL_SCREENING";
	nystagmus_start["message_type"] = MESSAGE_TYPE::START_TEST;
	nystagmus_start["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	nystagmus_start["metadata"] = versionInfoForHub;
	nystagmus_start["lensOD"] = lensDist->dioptersOD;
	nystagmus_start["lensOS"] = lensDist->dioptersOS;
	webComm->sendJSON(nystagmus_start);

	nlohmann::json me1;
	me1["chartTypeString"] = "FUNCTIONAL_SCREENING";
	me1["message_type"] = 15;
	me1["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	webComm->sendJSON(me1);


	ppd2 = (lensDist->PPD / 2.) - 0.5;
	omt_srcPos = Point2d(2880 - mmtopixels(ppd2), 540);

	initPos[0] = Point2d(960 + mmtopixels(ppd2), 540);
	initPos[1] = Point2d(2880 - mmtopixels(ppd2), 540);

	StimuliObjects t1;
	t1.objectColor[0] = 0;
	t1.objectColor[1] = 1;
	t1.objectColor[2] = 0;

	t1.objectCoordinates[0] = 960 + mmtopixels(ppd2);
	t1.objectCoordinates[1] = 540;

	t1.objectSize = 27;

	omt_greenCircleID[0] = display.addObject(t1);

	StimuliObjects t2;

	t2.objectColor[0] = 0;
	t2.objectColor[1] = 1;
	t2.objectColor[2] = 0;

	t2.objectCoordinates[0] = 2880 - mmtopixels(ppd2);
	t2.objectCoordinates[1] = 540;

	t2.objectSize = 27;

	omt_greenCircleID[1] = display.addObject(t2);

	omt_testStage = 0;
	omt_circleStage = 0;
	omt_currentEye = 0;

	display.setBackgroundL(1, 1, 1);
	display.setBackgroundR(1, 1, 1);

	display.redrawPending = true;
	bgChanged = false;

	Point2d pOD = Point(3640, 540);
	Point2d pOS = Point(200, 540);

	anDifOD = getDifInDegrees(pOD, initPos[1]);
	anDifOS = getDifInDegrees(pOS, initPos[0]);

	posCountPPOS = 0;
	posCountPPOD = 0;
	posCountCPOS = 0;
	posCountCPOD = 0;

	peripheralPosOD = Point(0, 0);
	peripheralPosOS = Point(0, 0);
	centralPosOD = Point(0, 0);
	centralPosOS = Point(0, 0);

}

float convertToMM(RotatedRect pupil, int eye)
{
	float sz = MAX(pupil.size.height, pupil.size.width) / 14.5;
	float lll = 0;
	if (eye == 0)
		lll = -lensDist->dioptersOD;
	else
		lll = -lensDist->dioptersOS;
	float perc = 0;
	if (lll > 0)
		perc = 0.0232;
	else if (lll < 0)
		perc = 0.0178;
	float k = perc * lll;
	return sz - sz * k;
}

void COculoMotorTest::ProcessETData(vector<ETData> etData)
{
	float ggt = glfwGetTime();

	nlohmann::json js;

	float ossize = 0, odsize = 0;
	odsize = convertToMM(etData[0].rawPupilEllipse, 0);
	ossize = convertToMM(etData[1].rawPupilEllipse, 1);

	js["pupilxOS"] = 0;
	js["pupilyOS"] = 0;
	js["bpgxOS"] = 0;
	js["bpgyOS"] = 0;
	js["dpgxOS"] = 0;
	js["dpgyOS"] = 0;
	js["pupilxOD"] = 0;
	js["pupilyOD"] = 0;
	js["bpgxOD"] = 0;
	js["bpgyOD"] = 0;
	js["dpgxOD"] = 0;
	js["dpgyOD"] = 0;

	if (etData[0].rawPupilEllipse.center.x > 0 && etData[0].rawPupilEllipse.center.y > 0)
	{
		Point2f pe = convertCoords(etData[0].rawPupilEllipse.center, 0);
		js["pupilxOD"] = pe.x;
		js["pupilyOD"] = pe.y;
	}
	if (etData[1].rawPupilEllipse.center.x > 0 && etData[1].rawPupilEllipse.center.y > 0)
	{
		Point2f pe = convertCoords(etData[1].rawPupilEllipse.center, 1);
		js["pupilxOS"] = pe.x;
		js["pupilyOS"] = pe.y;
	}
	if (etData[0].tandemGlints.bpGraw.x > 0 && etData[0].tandemGlints.bpGraw.y > 0)
	{
		Point2f bpG = convertCoords(etData[0].tandemGlints.bpGraw, 0);
		js["bpgxOD"] = bpG.x;
		js["bpgyOD"] = bpG.y;
	}
	if (etData[1].tandemGlints.bpGraw.x > 0 && etData[1].tandemGlints.bpGraw.y > 0)
	{
		Point2f bpG = convertCoords(etData[1].tandemGlints.bpGraw, 1);
		js["bpgxOS"] = bpG.x;
		js["bpgyOS"] = bpG.y;
	}
	if (etData[0].tandemGlints.dpGraw.x > 0 && etData[0].tandemGlints.dpGraw.y > 0)
	{
		Point2f dpG = convertCoords(etData[0].tandemGlints.dpGraw, 0);
		js["dpgxOD"] = dpG.x;
		js["dpgyOD"] = dpG.y;
	}
	if (etData[1].tandemGlints.dpGraw.x > 0 && etData[1].tandemGlints.dpGraw.y > 0)
	{
		Point2f dpG = convertCoords(etData[1].tandemGlints.dpGraw, 1);
		js["dpgxOS"] = dpG.x;
		js["dpgyOS"] = dpG.y;
	}

	js["chartTypeString"] = "FUNCTIONAL_SCREENING";
	js["message_type"] = MESSAGE_TYPE::DATA_PACKAGE;

	js["measurementOD"] = odsize;
	js["measurementOS"] = ossize;

	js["IOLOS"] = etData[0].isIOL;
	js["IOLOD"] = etData[1].isIOL;

	js["IPD"] = etData[0].IPD;

	js["bpgAreaOS"] = etData[0].tandemGlints.bpgArea;
	js["dpgAreaOS"] = etData[0].tandemGlints.dpgArea;
	js["bpgAreaOD"] = etData[1].tandemGlints.bpgArea;
	js["dpgAreaOD"] = etData[1].tandemGlints.dpgArea;

	js["distToOptOS"] = etData[0].distanceToOptimum;
	js["distToOptOD"] = etData[1].distanceToOptimum;

	js["bpgCandidatesOS"] = etData[0].bpgCandidates;
	js["bpgCandidatesOD"] = etData[1].bpgCandidates;
	js["dpgCandidatesOS"] = etData[0].dpgCandidates;
	js["dpgCandidatesOD"] = etData[1].dpgCandidates;

	js["BPcontrastOS"] = etData[0].BPcontrast;
	js["BPcontrastOD"] = etData[1].BPcontrast;
	js["DPcontrastOS"] = etData[0].DPcontrast;
	js["DPcontrastOD"] = etData[1].DPcontrast;

	js["measurementODpx"] = max(etData[0].rawPupilEllipse.size.width, etData[0].rawPupilEllipse.size.height);
	js["measurementOSpx"] = max(etData[1].rawPupilEllipse.size.width, etData[1].rawPupilEllipse.size.height);

	js["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	webComm->sendJSON(js);

	if (omt_testStage == 0)
	{
		if (ggt < 5)
		{
			if (ggt > 2.5 && !saveFrameSignalSent)
			{
				saveFrameSignal = true;
				saveFrameSignalSent = true;
			}
			return;
		}
		if (ggt > 5)
		{
			omt_testStage++;
			omt_circleStage = 0;
			omt_dstPos = Point(3640, 540);
			omt_src2DstDist = p2pDist(omt_srcPos, omt_dstPos);
			omt_movetime = omt_src2DstDist / omt_targetSpeed;

			if (omt_srcPos.x > 1920)
				omt_currentEye = 1;
			else
				omt_currentEye = 0;

			display.moveObject(omt_greenCircleID[1 - omt_currentEye], 9999, 9999);
			display.moveObject(omt_greenCircleID[omt_currentEye], omt_srcPos.x, omt_srcPos.y);

			nlohmann::json me1;
			me1["chartTypeString"] = "FUNCTIONAL_SCREENING";
			me1["message_type"] = 16;
			me1["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
			webComm->sendJSON(me1);			
			
			glfwSetTime(0);
		}
	}
	else
	{
		if (ggt < 2&&omt_circleStage==0&&!bgChanged)
		{
			if (ggt >= 0.5 && ggt <= 1.5)
			{
				if (omt_testStage == 1)
				{
					float initbgOS = 1;
					float initbgOD = 1;

					float destbgOS = 0;
					float destbgOD = 1;

					float curBgOS = initbgOS + (destbgOS - initbgOS) * (ggt - 0.5);
					float curBgOD = initbgOD + (destbgOD - initbgOD) * (ggt - 0.5);

					display.setBackgroundL(curBgOS, curBgOS, curBgOS);
					display.setBackgroundR(curBgOD, curBgOD, curBgOD);

					display.redrawPending = true;
				}
				if (omt_testStage == 2)
				{
					float initbgOS = 0;
					float initbgOD = 1;

					float destbgOS = 1;
					float destbgOD = 1;

					float curBgOS = initbgOS + (destbgOS - initbgOS) * (ggt - 0.5);
					float curBgOD = initbgOD + (destbgOD - initbgOD) * (ggt - 0.5);

					display.setBackgroundL(curBgOS, curBgOS, curBgOS);
					display.setBackgroundR(curBgOD, curBgOD, curBgOD);

					display.redrawPending = true;
				}
				if (omt_testStage == 3)
				{
					float initbgOS = 1;
					float initbgOD = 1;

					float destbgOS = 1;
					float destbgOD = 0;

					float curBgOS = initbgOS + (destbgOS - initbgOS) * (ggt - 0.5);
					float curBgOD = initbgOD + (destbgOD - initbgOD) * (ggt - 0.5);

					display.setBackgroundL(curBgOS, curBgOS, curBgOS);
					display.setBackgroundR(curBgOD, curBgOD, curBgOD);

					display.redrawPending = true;
				}
				if (omt_testStage == 4)
				{
					float initbgOS = 1;
					float initbgOD = 0;

					float destbgOS = 0;
					float destbgOD = 0;

					float curBgOS = initbgOS + (destbgOS - initbgOS) * (ggt - 0.5);
					float curBgOD = initbgOD + (destbgOD - initbgOD) * (ggt - 0.5);

					display.setBackgroundL(curBgOS, curBgOS, curBgOS);
					display.setBackgroundR(curBgOD, curBgOD, curBgOD);

					display.redrawPending = true;
				}
			}
		}
		if (ggt > 1.5 && omt_circleStage == 0 && !bgChanged)
		{
			if (omt_testStage == 1 || omt_testStage == 2)
			{
				if (etData[0].tandemGlints.bpG.x > 0 && etData[0].tandemGlints.dpG.x > 0)
				{
					centralPosOD += (etData[0].tandemGlints.bpG + etData[0].tandemGlints.dpG) / 2;
					posCountCPOD++;

					testLog
						<< centralPosOD.x << ";"
						<< 0 << ";"
						<< 0 << ";"
						<< 0 << endl;

				}
			}
			else if (omt_testStage == 3 || omt_testStage == 4)
			{
				if (etData[1].tandemGlints.bpG.x > 0 && etData[1].tandemGlints.dpG.x > 0)
				{
					centralPosOS += (etData[1].tandemGlints.bpG + etData[1].tandemGlints.dpG) / 2;
					posCountCPOS++;

					testLog
						<< 0 << ";"
						<< centralPosOS.x << ";"
						<< 0 << ";"
						<< 0 << endl;
				}
			}

		}
		if (ggt > 2 && omt_circleStage == 0 && !bgChanged)
		{
			if (omt_testStage == 1)
			{
				float destbgOS = 0;
				float destbgOD = 1;

				display.setBackgroundL(destbgOS, destbgOS, destbgOS);
				display.setBackgroundR(destbgOD, destbgOD, destbgOD);

				display.redrawPending = true;
			}
			if (omt_testStage == 2)
			{
				float destbgOS = 1;
				float destbgOD = 1;

				display.setBackgroundL(destbgOS, destbgOS, destbgOS);
				display.setBackgroundR(destbgOD, destbgOD, destbgOD);

				display.redrawPending = true;
			}
			if (omt_testStage == 3)
			{
				float destbgOS = 1;
				float destbgOD = 0;

				display.setBackgroundL(destbgOS, destbgOS, destbgOS);
				display.setBackgroundR(destbgOD, destbgOD, destbgOD);

				display.redrawPending = true;
			}
			if (omt_testStage == 4)
			{
				float destbgOS = 0;
				float destbgOD = 0;

				display.setBackgroundL(destbgOS, destbgOS, destbgOS);
				display.setBackgroundR(destbgOD, destbgOD, destbgOD);

				display.redrawPending = true;
			}
			bgChanged = true;
			glfwSetTime(0);
			nlohmann::json me1;
			me1["chartTypeString"] = "FUNCTIONAL_SCREENING";
			me1["message_type"] = 15;
			me1["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
			webComm->sendJSON(me1);

			return;
		}
		if (ggt < omt_movetime + 0.2 && bgChanged)
		{
			if (omt_srcPos.x > 1920)
				omt_currentEye = 1;
			else
				omt_currentEye = 0;
			display.moveObject(omt_greenCircleID[1 - omt_currentEye], 9999, 9999);
			if (ggt < omt_movetime)
			{
				if (ggt > omt_movetime/2 && !saveFrameSignalSent&& omt_circleStage==2)
				{
					saveFrameSignal = true;
					saveFrameSignalSent = true;
				}
				float sn = sin(CV_PI / 2 * ggt / omt_movetime);
				float curX = omt_srcPos.x + (omt_dstPos.x - omt_srcPos.x) * sn;
				float curY = omt_srcPos.y + (omt_dstPos.y - omt_srcPos.y) * sn;
				display.moveObject(omt_greenCircleID[omt_currentEye], curX, curY);
				display.redrawPending = true;
			}
			else
			{
				display.moveObject(omt_greenCircleID[omt_currentEye], omt_dstPos.x, omt_dstPos.y);
				display.redrawPending = true;

				if (omt_circleStage == 0)
				{
					if (omt_testStage == 1 || omt_testStage == 2)
					{
						if (etData[0].tandemGlints.bpG.x > 0 && etData[0].tandemGlints.dpG.x > 0)
						{
							peripheralPosOD += (etData[0].tandemGlints.bpG + etData[0].tandemGlints.dpG) / 2;
							posCountPPOD++;
							testLog
								<< 0 << ";"
								<< 0 << ";"
								<< peripheralPosOD.x << ";"
								<< 0 << endl;
						}
					}
					else if (omt_testStage == 3 || omt_testStage == 4)
					{
						if (etData[1].tandemGlints.bpG.x > 0 && etData[1].tandemGlints.dpG.x > 0)
						{
							peripheralPosOS += (etData[1].tandemGlints.bpG + etData[1].tandemGlints.dpG) / 2;
							posCountPPOS++;
							testLog
								<< 0 << ";"
								<< 0 << ";"
								<< 0 << ";"
								<< peripheralPosOS.x << endl;
						}
					}
				}

			}

		}
		if (ggt >= omt_movetime + 0.2 && bgChanged)
		{
			omt_srcPos = omt_dstPos;
			omt_circleStage++;

			if (omt_circleStage == 4)
			{
				omt_testStage++;

				nlohmann::json me1;
				me1["chartTypeString"] = "FUNCTIONAL_SCREENING";
				me1["message_type"] = 16;
				me1["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
				webComm->sendJSON(me1);

				if (omt_testStage == 5)
				{
					for (int i = 0; i < savedFrames.size(); i++)
					{
						if (!savedFrames[i].picture.rows)
							continue;
						std::ostringstream vffn2;
						vffn2 << picturesLogsPath << "pupil_" << patientInfo.examID << "_" << std::chrono::system_clock::now().time_since_epoch().count() << format("_%d", i) << ".png";
						if (savedFrames[i].picture.rows && savedFrames[i].picture.cols)
							cv::imwrite(vffn2.str(), savedFrames[i].picture);
						nlohmann::json framesToSend;
						framesToSend["chartTypeString"] = "FUNCTIONAL_SCREENING";
						framesToSend["message_type"] = 33;
						framesToSend["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
						Mat picToSend = Mat::zeros(savedFrames[i].picture.cols / 2, savedFrames[i].picture.rows * 2, CV_8UC1);
						rotate(savedFrames[i].picture(Rect(0, 0, savedFrames[i].picture.cols / 2, savedFrames[i].picture.rows)),
							picToSend(Rect(0, 0, picToSend.cols / 2, picToSend.rows)),
							2);
						rotate(savedFrames[i].picture(Rect(savedFrames[i].picture.cols / 2, 0, savedFrames[i].picture.cols / 2, savedFrames[i].picture.rows)),
							picToSend(Rect(picToSend.cols / 2, 0, picToSend.cols / 2, picToSend.rows)),
							0);
						std::vector<uchar> buff;
						std::vector<int> param(2);
						param[0] = cv::IMWRITE_JPEG_QUALITY;
						param[1] = 30;
						cv::imencode(".jpg", picToSend, buff, param);
						std::string tmp = std::string(buff.begin(), buff.end());
						using namespace boost::archive::iterators;
						using It = base64_from_binary<transform_width<std::string::const_iterator, 6, 8>>;
						auto s = std::string(It(std::begin(tmp)), It(std::end(tmp)));
						s.append((3 - tmp.size() % 3) % 3, '=');

						framesToSend["picture"] = s.c_str();

						webComm->sendJSON(framesToSend);
					}
					savedFrames.clear();

//					nlohmann::json me1;
//					me1["chartType"] = 22;
//					me1["chartTypeString"] = "FUNCTIONAL_SCREENING";
//					me1["message_type"] = MESSAGE_TYPE::STOP_TEST;
//					me1["timestamp"] = 1 / 100;
//					webComm->sendJSON(me1);

					testLog
						<< peripheralPosOD.x << ";"
						<< peripheralPosOS.x << ";"
						<< centralPosOD.x << ";"
						<< centralPosOS.x << endl;

					peripheralPosOD /= posCountPPOD;
					peripheralPosOS /= posCountPPOS;
					centralPosOD /= posCountCPOD;
					centralPosOS /= posCountCPOS;

					testLog
						<< posCountPPOD << ";"
						<< posCountPPOS << ";"
						<< posCountCPOD << ";"
						<< posCountCPOS << endl;

					pxToDegreeOS = abs(3640 - initPos[1].x) / abs(centralPosOS.x - peripheralPosOS.x);
					pxToDegreeOD = abs(200 - initPos[0].x) / abs(centralPosOD.x - peripheralPosOD.x);

					testLog
						<< pxToDegreeOD << ";"
						<< pxToDegreeOS << ";"
						<< initPos[0].x << ";"
						<< initPos[1].x << endl;


					testLog.close();

					webComm->closeLog();
					qualityLog.close();
//					eyetrackingOn = false;
//					display.setDragonfly();
//					currentTest = nullptr;
					display.clearObjects();
					display.st_redTargetColor[0] = 0;
					display.st_redTargetColor[1] = 1;
					display.st_redTargetColor[2] = 0;
					currentTest = new CASTest(omt_proSaccadeEye, 2, 7, "PROSACCADE", "FUNCTIONAL_SCREENING");
					display.stimuliType = DisplayStimuliType::SACCADETASK;
					SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
					currentTest->testType = "FUNCTIONAL_SCREENING";

//					currentTest->testType = "PUPILLARY_EVALUATION2";
					return;

					return;
				}
				omt_circleStage = 0;
				bgChanged = false;

		}
			if (omt_testStage == 1)
			{
				if (omt_circleStage == 0)
				{
					omt_dstPos = Point(3640, 540);
					omt_srcPos = initPos[1];
				}
				if (omt_circleStage == 1)
					omt_dstPos = Point(3640, 1030);
				if (omt_circleStage == 2)
					omt_dstPos = Point(2880 + 490 - mmtopixels(ppd2), 1030);
				if (omt_circleStage == 3)
					omt_dstPos = Point(2880 - mmtopixels(ppd2), 540);
			}
			if (omt_testStage == 2)
			{
				if (omt_circleStage == 0)
				{
					omt_dstPos = Point(3640, 540);
					omt_srcPos = initPos[1];
				}

				if (omt_circleStage == 1)
					omt_dstPos = Point(3640, 50);
				if (omt_circleStage == 2)
					omt_dstPos = Point(2880 + 490 - mmtopixels(ppd2), 50);
				if (omt_circleStage == 3)
					omt_dstPos = Point(2880 - mmtopixels(ppd2), 540);
			}
			if (omt_testStage == 3)
			{
				if (omt_circleStage == 0)
				{
					omt_dstPos = Point(200, 540);
					omt_srcPos = initPos[0];
				}
				if (omt_circleStage == 1)
					omt_dstPos = Point(200, 1030);
				if (omt_circleStage == 2)
					omt_dstPos = Point(960 - 490 + mmtopixels(ppd2), 1030);
				if (omt_circleStage == 3)
					omt_dstPos = Point(960 + mmtopixels(ppd2), 540);
			}
			if (omt_testStage == 4)
			{
				if (omt_circleStage == 0)
				{
					omt_dstPos = Point(200, 540);
					omt_srcPos = initPos[0];
				}
				if (omt_circleStage == 1)
					omt_dstPos = Point(200, 50);
				if (omt_circleStage == 2)
					omt_dstPos = Point(960 - 490 + mmtopixels(ppd2), 50);
				if (omt_circleStage == 3)
					omt_dstPos = Point(960 + mmtopixels(ppd2), 540);
			}
			omt_src2DstDist = p2pDist(omt_srcPos, omt_dstPos);
			omt_movetime = omt_src2DstDist / omt_targetSpeed;
			if (omt_srcPos.x > 1920)
				omt_currentEye = 1;
			else
				omt_currentEye = 0;
			display.moveObject(omt_greenCircleID[1 - omt_currentEye], 9999, 9999);
			display.moveObject(omt_greenCircleID[omt_currentEye], omt_srcPos.x, omt_srcPos.y);

			glfwSetTime(0);
		}
	}
}