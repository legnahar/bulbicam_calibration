#pragma once

#include "Tests.h"

#define vergenceversion "0.9.20210805"
extern CTest* currentTest;
extern ofstream qualityLog;
extern ofstream testLog;
class CVergenceTest : public CTest
{
public:
	CVergenceTest(int ipd, int maxpixgg, int tsttype, int trialsnum);
	void ProcessETData(vector<ETData> etData);
	int testNum = 11;
private:
	int verg_ipd;
	int verg_ttdist;
	float verg_targetOS, verg_targetOD;
	int verg_state, verg_testcount;
	float verg_initOS, verg_initOD;
	bool verg_initpause;
	float verg_targetsize;
	float verg_posminOS;
	float verg_posmaxOS;
	float verg_posminOD;
	float verg_posmaxOD;
	int verg_testamount, verg_targettype;

	float verg_sizemax, verg_sizemin, verg_constsize;
//	float verg_distmax, verg_distmin;
	int verg_pausePeriod = 2, verg_movingPeriod = 7.f;
	bool verg_pause;

	float bpgavg = 0;
	int bpgavsc = 0;

	int verg_jumpstate;

	int verg_maxpixg;

	vector<float> verg_bpra;

};

CVergenceTest::CVergenceTest(int ipd, int maxpixgg, int tsttype, int trialsnum)
{
	verg_ipd = ipd;
	verg_testcount = 0;
	verg_maxpixg = maxpixgg;
	webComm->openLog();
	nlohmann::json js111;
	js111["chartTypeString"] = "CONVERGENCE";
	js111["message_type"] = MESSAGE_TYPE::START_TEST;
	js111["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	js111["metadata"] = versionInfoForHub;
	webComm->sendJSON(js111);

	nlohmann::json js113;
	js113["chartTypeString"] = "CONVERGENCE";
	js113["message_type"] = 15;
	js113["stimuli"] = tsttype;
	js113["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	webComm->sendJSON(js113);

	verg_sizemin = 5;
	verg_sizemax = 5;
	verg_constsize = 5;

//	verg_distmin = 70;
//	verg_distmax = 670;

	verg_jumpstate = 0;

	float verg_anglefar = fastAtan2(ipd / 2, 670);
//	float verg_angleclose = fastAtan2(ipd / 2, verg_distmin);

	verg_posminOS = mmtopixels(6);
	verg_posmaxOS = 1920.0 - mmtopixels(lensDist->posfromanglOS(Point2d(verg_anglefar, 0)).x);

	verg_posminOD = 1920.0*2 - mmtopixels(6);
	verg_posmaxOD = 1920.0 * 2 - mmtopixels(lensDist->posfromanglOD(Point2d(verg_anglefar, 0)).x);

	lensDist->posfromanglOD(Point2d(0, 0));

	verg_ttdist = 0;
	display.loadBlackAnimals();
	display.verg_targettype = tsttype;
	verg_targettype = tsttype;
	verg_testamount = trialsnum;
	verg_targetOS = 0;
	verg_targetOD = 0;
	verg_pause = false;
	verg_state = 0;
	verg_initOS = 0;
	verg_initOD = 0;
	verg_initpause = true;
	glfwSetTime(0);

	eyetrackingOn = true;
}

void CVergenceTest::ProcessETData(vector<ETData> etData)
{
	float verg_duration = glfwGetTime();
	float verg_pos;

	if (verg_initpause&&verg_duration < 1)
	{
		if (etData[0].tandemGlints.bpGraw.y > 0 && etData[0].tandemGlints.bpGraw.y < 500 && etData[1].tandemGlints.bpGraw.y > 0 && etData[1].tandemGlints.bpGraw.y < 500)
		{
			bpgavg += ((400 - etData[1].tandemGlints.bpGraw.y) + (400 - etData[0].tandemGlints.bpGraw.y)) / 20;
			bpgavsc++;
		}
		verg_targetsize = verg_sizemin;
		verg_targetOS = verg_posmaxOS;
		verg_targetOD = verg_posmaxOD;
		display.drawMCross(verg_targetOS, verg_targetOD, mmtopixels(verg_targetsize));
		display.nCoordL = Point(verg_targetOS,540);
		display.nCoordR = Point(verg_targetOD,540);
		display.redrawPending = true;
		return;
	}
	if (verg_initpause&&verg_duration > 1)
	{
		verg_maxpixg = bpgavg / bpgavsc + 3.f;
		glfwSetTime(0);
		verg_initpause = false;
		return;
	}


	if (verg_duration > verg_movingPeriod)
	{
		if (verg_state == 0)
		{
			verg_targetOS = verg_posminOS;
			verg_targetOD = verg_posminOD;
		}
		else
		{
			verg_targetOS = verg_posmaxOS;
			verg_targetOD = verg_posmaxOD;
		}
	}
	else
	{
		float verg_dur = verg_duration / verg_movingPeriod;
		if (verg_state == 0)
		{
			verg_targetOS = verg_posmaxOS + (verg_posminOS - verg_posmaxOS) * verg_dur;
			verg_targetOD = verg_posmaxOD + (verg_posminOD - verg_posmaxOD) * verg_dur;
		}
		else
		{
			verg_targetOS = verg_posminOS + (verg_posmaxOS - verg_posminOS) * verg_dur;
			verg_targetOD = verg_posminOD + (verg_posmaxOD - verg_posminOD) * verg_dur;
		}
	}
	if (verg_duration < verg_movingPeriod + verg_pausePeriod && verg_duration > verg_movingPeriod && !verg_pause)
	{
		if (verg_state == 0)
		{
			verg_pause = true;
			nlohmann::json message_stoprecoveryOS;
			message_stoprecoveryOS["chartTypeString"] = "CONVERGENCE";
			message_stoprecoveryOS["message_type"] = 9;
			message_stoprecoveryOS["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
			webComm->sendJSON(message_stoprecoveryOS);
		}
		if (verg_state == 1)
		{
			verg_pause = true;
			nlohmann::json message_stoprecoveryOS;
			message_stoprecoveryOS["chartTypeString"] = "CONVERGENCE";
			message_stoprecoveryOS["message_type"] = 16;
			message_stoprecoveryOS["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
			webComm->sendJSON(message_stoprecoveryOS);

			verg_testcount++;
			if(verg_targettype>-1)
				display.verg_targettype = verg_testcount;
			if (verg_testcount == verg_testamount)
			{
				nlohmann::json message_stoptest;
				message_stoptest["chartTypeString"] = "CONVERGENCE";
				message_stoptest["message_type"] = MESSAGE_TYPE::STOP_TEST;
				message_stoptest["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
				webComm->sendJSON(message_stoptest);
				webComm->closeLog();
				qualityLog.close();
				display.setDragonfly();
				currentTest = nullptr;

				eyetrackingOn = false;
				return;
			}
		}
	}

	if ((verg_duration > verg_movingPeriod + verg_pausePeriod&&verg_pause)||(verg_duration > verg_movingPeriod&&!verg_pause))
	{
		if (verg_state == 1)
		{
			nlohmann::json message_startNPCOS;
			message_startNPCOS["chartTypeString"] = "CONVERGENCE";
			message_startNPCOS["message_type"] = 15;
			if (verg_targettype > -1)
				message_startNPCOS["stimuli"] = verg_testcount;
			else
				message_startNPCOS["stimuli"] = -1;
			message_startNPCOS["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
			webComm->sendJSON(message_startNPCOS);
		}

		verg_pause = false;
		glfwSetTime(0);
		verg_state = 1 - verg_state;
	}

	nlohmann::json jsDataMessage;
	float osd, odd;
	jsDataMessage["chartTypeString"] = "CONVERGENCE";
	jsDataMessage["message_type"] = MESSAGE_TYPE::DATA_PACKAGE;
	jsDataMessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	jsDataMessage["OSeyex"] = etData[1].tandemGlints.bpG.x;
	jsDataMessage["OSeyey"] = etData[1].tandemGlints.bpG.y;
	jsDataMessage["ODeyex"] = etData[0].tandemGlints.bpG.x;
	jsDataMessage["ODeyey"] = etData[0].tandemGlints.bpG.y;
	jsDataMessage["OStargetx"] = verg_targetOS;
	jsDataMessage["ODtargetx"] = verg_targetOD;

	if (etData[1].tandemGlints.bpG.x != 0 && etData[0].tandemGlints.bpG.x != 0)
	{
		float verg_mmfactor;
		verg_mmfactor = 14.5f;
		if (verg_initOS == 0)
			verg_initOS = etData[1].tandemGlints.bpG.x;
		if (verg_initOD == 0)
			verg_initOD = etData[0].tandemGlints.bpG.x;
//		verg_middlepoint = (etData[1].tandemGlints.bpG.x + etData[0].tandemGlints.bpG.x) / 2 - MIN(etData[1].tandemGlints.bpG.x, etData[0].tandemGlints.bpG.x);


		if (etData[1].rawPupilEllipse.center.x != 0)
		{
			double td = MAX(etData[1].rawPupilEllipse.size.width, etData[1].rawPupilEllipse.size.height) / verg_mmfactor;
			jsDataMessage["OSdiameter"] = td - td * 0.03 * lensDist->dioptersOS;
		}
		if (etData[0].rawPupilEllipse.center.x != 0)
		{
			double td = MAX(etData[0].rawPupilEllipse.size.width, etData[0].rawPupilEllipse.size.height) / verg_mmfactor;
			jsDataMessage["ODdiameter"] = td - td * 0.03 * lensDist->dioptersOD;
		}

		float ppd2 = 140 - abs((((400 - etData[1].tandemGlints.bpGraw.y) + (400 - etData[0].tandemGlints.bpGraw.y)) / 20 - verg_maxpixg)*7);
//		if (ppd2 < 40&&ppd2>0)
//		{
			jsDataMessage["ppdistance"] = ppd2;
//			cout << ppd2 << endl;
//		}
//		else
//		{
//			jsDataMessage["ppdistance"] = 0;
//		}
	}
	else
	{
		jsDataMessage["OSdiameter"] = 0;
		jsDataMessage["ppdistance"] = 0;
		jsDataMessage["ODdiameter"] = 0;
	}
	float ppdist = 0.6f;

	display.drawMCross(verg_targetOS, verg_targetOD, mmtopixels(verg_targetsize));
	display.nCoordL = Point(verg_targetOS, 540);
	display.nCoordR = Point(verg_targetOD, 540);
	display.redrawPending = true;
	
	double a1 = lensDist->anglefromposOS(Point2d(pixelstomm(1920.0 - verg_targetOS), 540)).x * CV_PI / 180.;
	double a2 = lensDist->anglefromposOD(Point2d(pixelstomm(1920.0*2 - verg_targetOD), 540)).x * CV_PI / 180.;

	float Z1 = verg_ipd / (2 * tan(a1));
	float Z2 = verg_ipd / (2 * tan(a2));

	jsDataMessage["z"] = Z1;

	webComm->sendJSON(jsDataMessage);
}