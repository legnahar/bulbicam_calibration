/*
	Author:		VLV
	Info:		Eyetracking module implementations
	Version:	2.0.220516
	Updates:	16.05.2022
				- Dilated DPG tracking implements area rule
*/

#include "eyetracking.h"

using namespace cv;
using namespace std;

template<typename T>
float pDist(const T& p1, const T& p2)
{
	return sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));
}

template<typename T>
float pAng(const T& p1, const T& p2)
{
	return fastAtan2(p2.y - p1.y, p2.x - p1.x);
}
float compareAngDeg(const float& a1, const float& a2)
{
	return 180 - abs(abs(a1 - a2) - 180);
}

void getLine(double x1, double y1, double x2, double y2, double& a, double& b, double& c)
{
	// (x- p1X) / (p2X - p1X) = (y - p1Y) / (p2Y - p1Y) 
	a = y1 - y2;
	b = x2 - x1;
	c = x1 * y2 - x2 * y1;
}
double toLineDist(double pct1X, double pct1Y, double pct2X, double pct2Y, double pct3X, double pct3Y)
{
	double a, b, c;
	getLine(pct2X, pct2Y, pct3X, pct3Y, a, b, c);
	return abs(a * pct1X + b * pct1Y + c) / sqrt(a * a + b * b);
}

float cam_pix_to_mm(float pix)
{
	return pix / 14.5;
}
float cam_mm_to_pix(float mm)
{
	return mm * 14.5;
}
float scr_pix_to_mm(float pix)
{
	return pix / 15.87;
}
float scr_mm_to_pix(float mm)
{
	return mm * 15.87;
}

EyeTracker::EyeTracker(ETInitStructure etis)
{
	BPGthreshold = etis.BPGthreshold;
	DPGthreshold = etis.DPGthreshold;
	inverted = etis.inverted;
	offset = etis.offset;
	bpgotten = false;
	dpgotten = false;
	frameNum = 0;

	frameWidth = etis.frameSize.width;
	frameHeight = etis.frameSize.height;
	deafaultImageArea = Rect(Point(0, 0), etis.frameSize);
	trueLastBPimage = Mat(etis.frameSize, CV_8UC1, Scalar::all(0));
	lastBPimage = Mat(etis.frameSize, CV_8UC1, Scalar::all(255));
	lastDPimage = Mat(etis.frameSize, CV_8UC1, Scalar::all(0));

	int widthRightEye = (frameWidth - _xRightEye * 2 - 200) / 2;
	int heightRightEye = frameHeight - _yRightEye * 2;
	int widthLeftEye = widthRightEye;
	int heightLeftEye = heightRightEye;
	int xLeftEye = frameWidth / 2 + 80;
	int yLeftEye = _yRightEye;

	defaultBorders[0] = Rect(0, 0, etis.frameSize.width / 2, etis.frameSize.height);
	defaultBorders[1] = Rect(etis.frameSize.width / 2, 0, etis.frameSize.width / 2, etis.frameSize.height);

	for (int curROInum = 0; curROInum < 2; curROInum++)
	{
		ETROI tmproi;
		tmproi.glintsROI.x = defaultBorders[curROInum].x;
		tmproi.glintsROI.y = defaultBorders[curROInum].y;
		tmproi.glintsROI.width = defaultBorders[curROInum].width;
		tmproi.glintsROI.height = defaultBorders[curROInum].height;
		tmproi.rotPoint = Point2f(tmproi.glintsROI.x + tmproi.glintsROI.width / 2, tmproi.glintsROI.y + tmproi.glintsROI.height / 2);
		ROIs.push_back(tmproi);	
		
		glintsEllipses[curROInum] = RotatedRect(Point2f(frameWidth / 4 + frameWidth / 2 * curROInum, frameHeight / 2), Size2f(273, 306), 0);
		bigGlintsEllipses[curROInum] = RotatedRect(Point2f((frameWidth / 4 - 20) + (frameWidth / 2 + 40) * curROInum, frameHeight / 2), Size2f(353, 316), 0);

		todpleddist[curROInum] = 0;
		anglesOfShapes[curROInum] = etis.anglesOfShapes[curROInum];
		initCentersOfShapes[curROInum] = etis.centersOfShapes[curROInum];
		centersOfShapes[curROInum].x = initCentersOfShapes[curROInum].x - etis.offset.width;
		centersOfShapes[curROInum].y = initCentersOfShapes[curROInum].y - etis.offset.height;
		goodGlintsSequence[curROInum] = false;

		buildDPGAngularArea(curROInum);
	}
}
vector<ETData> EyeTracker::getTrackingData(Mat& frame, int improc, bool needPupils, int interestingEye)
{
	// 0 means no data from lineStatus because of using video in emulation mode
	if (improc == 0)
	{
		picIsBright = isPicBright(frame);
	}
	else
	{
		if (improc != 14)
		{
			picIsBright = true;
		}
		else
		{
			picIsBright = false;
		}
	}

	if (picIsBright)
	{
		bpgotten = true;
		trueLastBPimage = frame.clone();
		lastBPimage = frame.clone() * 2;	// BP brightness increasing produced better tracking results
	}
	else
	{
		dpgotten = true;
		lastDPimage = frame.clone();
	}
	if (bpgotten && dpgotten)
	{
		lastBPDPimage = lastBPimage - lastDPimage;
	}

	vector<ETData> ret = { ETData(), ETData() };

	prepareROIs(frame, picIsBright);

	centersOfShapes[0].x = initCentersOfShapes[0].x - offset.width;
	centersOfShapes[0].y = initCentersOfShapes[0].y - offset.height;
	centersOfShapes[1].x = initCentersOfShapes[1].x - offset.width;
	centersOfShapes[1].y = initCentersOfShapes[1].y - offset.height;

	vector<TandemGlints> tgp = glintsTracking(frame, picIsBright, needPupils);
	vector<PursuitValues> pursVals = getPursuitValues(tgp);
	vector<RotatedRect> vlvPupils = { RotatedRect(), RotatedRect() };

	for (int curROInum = 0; curROInum <= 1; curROInum++)
	{
		ETData rettmp;
		rettmp.tandemGlints = tgp[curROInum];
		rettmp.goodGlintsSequence = goodGlintsSequence[curROInum];
		rettmp.rawPupilEllipse = vlvPupils[curROInum];
		rettmp.pursuitValues = pursVals[curROInum];

//		rettmp.tandemGlints.bpG = convertCoords2(rettmp.tandemGlints.bpGraw, curROInum);
//		rettmp.tandemGlints.dpG = convertCoords2(rettmp.tandemGlints.dpGraw, curROInum);
		rettmp.calibShape = convertCoords2(centersOfShapes[curROInum], curROInum);

		rettmp.isPicBright = picIsBright;
		rettmp.bpgCandidates = bpgCands[curROInum].size();
		rettmp.dpgCandidates = dpgCands[curROInum].size();

		previousResult[curROInum] = rettmp;
		ret[curROInum] = rettmp;
	}

	return ret;
}
bool EyeTracker::isPicBright(Mat& frame)
{
	if (frame.at<uchar>(Point(7, 7)) < 128)
	{
		return 0 ^ inverted;
	}
	else
	{
		return 1 ^ inverted;
	}
}
void EyeTracker::prepareROIs(Mat& frame, bool picIsBright)
{
	for (int curROINum = 0; curROINum < ROIs.size(); curROINum++)
	{
		if (picIsBright)
			frame(ROIs[curROINum].glintsROI).copyTo(ROIs[curROINum].prevBPImg);
		else
			frame(ROIs[curROINum].glintsROI).copyTo(ROIs[curROINum].prevDPImg);
	}
}
vector<TandemGlints> EyeTracker::glintsTracking(Mat& frame, bool curBright, bool needPupils)
{
	int rectw = 80, recth = 140, step_x = 2, step_y = 2;

	Mat thresholdedBP, thresholdedDP;
	vector<TandemGlints> tgReturn = { TandemGlints(), TandemGlints() };

	for (int curROInum = 0; curROInum <= 1; curROInum++)
	{
		bool only_in_ellipse = true;
		bool gt_round = 0;
		bool found = false;
		float andel, an, expa;

		dpgCands[curROInum].clear();
		bpgCands[curROInum].clear();
		goodGlintsSequence[curROInum] = false;

		if (curROInum == 1)
			expa = 0;
		else
			expa = 180;

		vector<Vec4i> hierarchy;

		if (curBright)
		{
			ROIs[curROInum].rcontoursb.clear();
			threshold(ROIs[curROInum].prevBPImg, thresholdedBP, 250, 255, THRESH_BINARY);
			findContours(thresholdedBP, ROIs[curROInum].rcontoursb, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_NONE, Point(ROIs[curROInum].glintsROI.x, ROIs[curROInum].glintsROI.y));
		}
		else
		{
			ROIs[curROInum].rcontoursd.clear();
			threshold(ROIs[curROInum].prevDPImg, thresholdedDP, 250, 255, THRESH_BINARY);
			findContours(thresholdedDP, ROIs[curROInum].rcontoursd, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_NONE, Point(ROIs[curROInum].glintsROI.x, ROIs[curROInum].glintsROI.y));
		}

		CandidateTandem mostSuitablePair;

	gtagain:
		double errorProduct = DBL_MAX;
		double min_bpg_area_limit = 1, max_bpg_area_limit = 300, max_dist_limit = 40, max_ang_limit = 70;

		for (int i = 0; i < ROIs[curROInum].rcontoursb.size(); i++)
		{
			if (ROIs[curROInum].rcontoursb[i].size() < 3)
			{
				continue;
			}
			double barea = contourArea(ROIs[curROInum].rcontoursb[i]);
			if (barea < min_bpg_area_limit || barea > max_bpg_area_limit)
			{
				continue;
			}
			Moments m1 = moments(ROIs[curROInum].rcontoursb[i]);

			Point2f c1 = Point2f(0, 0);
			c1.x = m1.m10 / m1.m00;
			c1.y = m1.m01 / m1.m00;

			float equation_bpg = pow((c1.x - glintsEllipses[curROInum].center.x), 2) / pow((glintsEllipses[curROInum].size.width / 2), 2) + pow((c1.y - glintsEllipses[curROInum].center.y), 2) / pow((glintsEllipses[curROInum].size.height / 2), 2);
			if (equation_bpg > 1.f && only_in_ellipse)
			{
				continue;
			}

			float big_equation_bpg = pow((c1.x - bigGlintsEllipses[curROInum].center.x), 2) / pow((bigGlintsEllipses[curROInum].size.width / 2), 2) + pow((c1.y - bigGlintsEllipses[curROInum].center.y), 2) / pow((bigGlintsEllipses[curROInum].size.height / 2), 2);
			if (big_equation_bpg > 1.f)
			{
				continue;
			}

			bool darkness_met = false;
			int beginx = c1.x - (rectw / 3 + (rectw / 3 * curROInum));
			int beginy = c1.y - recth / 2;

			if (beginx < 0) beginx = 0;
			if (beginy < 0) beginy = 0;
			if (beginx + rectw > frameWidth - 1) rectw = frameWidth - beginx - 1;
			if (beginy + recth > frameHeight - 1) recth = frameHeight - beginy - 1;

			for (int i = beginx; i < beginx + rectw; i += step_x)
			{
				// pointer to the first row
				uchar* begin_ptr = lastDPimage.ptr<uchar>(beginy);
				begin_ptr += i;

				for (int j = beginy; j < beginy + recth; j += step_y)
				{
					uchar grayscale = *begin_ptr;
					begin_ptr += lastDPimage.step * step_y;

					// if at least 1 pixel is <= 70 it's a sign of DP presence around
					if (grayscale <= 70)
					{
						darkness_met = true;
						goto brokencycle;
					}
				}
			}

		brokencycle:

			if (darkness_met == false)
			{
				continue;
			}

			bpgCands[curROInum].push_back(c1);

			vector<DpgCandidate> tmpdpgCands;

			for (int j = 0; j < ROIs[curROInum].rcontoursd.size(); j++)
			{
				if (ROIs[curROInum].rcontoursd[j].size() < 3)
				{
					continue;
				}

				double darea = contourArea(ROIs[curROInum].rcontoursd[j]);
				if (darea < 1 || darea > 300)
				{
					continue;
				}

				Moments m2 = moments(ROIs[curROInum].rcontoursd[j]);
				Point2f c2;
				c2.x = m2.m10 / m2.m00;
				c2.y = m2.m01 / m2.m00;

				float equation_dpg = pow((c2.x - glintsEllipses[curROInum].center.x), 2) / pow((glintsEllipses[curROInum].size.width / 2), 2) + pow((c2.y - glintsEllipses[curROInum].center.y), 2) / pow((glintsEllipses[curROInum].size.height / 2), 2);
				if (equation_dpg > 1.f && only_in_ellipse)
				{
					continue;
				}

				float big_equation_dpg = pow((c2.x - bigGlintsEllipses[curROInum].center.x), 2) / pow((bigGlintsEllipses[curROInum].size.width / 2), 2) + pow((c2.y - bigGlintsEllipses[curROInum].center.y), 2) / pow((bigGlintsEllipses[curROInum].size.height / 2), 2);
				if (big_equation_dpg > 1.f)
				{
					continue;
				}

				if (bpgCands[curROInum].size() == 1)
				{
					dpgCands[curROInum].push_back(c2);
				}

				float d = pDist(c1, c2);
				an = pAng(c1, c2);
				andel = compareAngDeg(an, expa);
				if (d > 2 && d < max_dist_limit && andel < max_ang_limit)
				{
					DpgCandidate tmpdpgCand;
					tmpdpgCand.pos = c2;
					tmpdpgCand.angle = andel;
					tmpdpgCand.area = darea;
					tmpdpgCands.push_back(tmpdpgCand);
				}
			}

			if (tmpdpgCands.size() > 0)
			{
				float toDestDistBPG = 0, toDestDistDPG = 0;

				if (previousResult[curROInum].tandemGlints.bpGraw != Point2f(0, 0))
				{
					toDestDistBPG = (pDist(previousResult[curROInum].tandemGlints.bpGraw, c1) + 1) * (pDist(ROIs[curROInum].rotPoint, c1) + 1);
				}
				else
				{
					toDestDistBPG = pDist(ROIs[curROInum].rotPoint, c1) + 1;
				}

				for (int ccan = 0; ccan < tmpdpgCands.size(); ccan++)
				{
					if (!needPupils)
					{
						if (previousResult[curROInum].tandemGlints.dpGraw != Point2f(0, 0))
						{
							toDestDistDPG = (pDist(previousResult[curROInum].tandemGlints.dpGraw, tmpdpgCands[ccan].pos) + 1) * (pDist(ROIs[curROInum].rotPoint, tmpdpgCands[ccan].pos) + 1);
						}
						else
						{
							toDestDistDPG = pDist(ROIs[curROInum].rotPoint, tmpdpgCands[ccan].pos) + 1;
						}
					}
					else
					{
						toDestDistDPG = 1;
					}

					pair<Size2f, double> dpgToLine = blueLineRule(c1, tmpdpgCands[ccan].pos, curROInum, true);
					double this_errorProduct = (dpgToLine.second + 1) * toDestDistBPG * toDestDistDPG * (pDist(c1, tmpdpgCands[ccan].pos) + 1);

					if (this_errorProduct < errorProduct)
					{
						found = true;
						errorProduct = this_errorProduct;
						mostSuitablePair.bpgCand = c1;
						mostSuitablePair.bpGarea = barea;
						mostSuitablePair.dpGarea = tmpdpgCands[ccan].area;
						mostSuitablePair.dpgCand = tmpdpgCands[ccan].pos;
						mostSuitablePair.angle = tmpdpgCands[ccan].angle;
					}
				}
			}
		}

		if (found)
		{
			TandemGlints candidate = TandemGlints();

			goodBPGinarow[curROInum].push_back(mostSuitablePair.bpgCand);
			goodDPGinarow[curROInum].push_back(mostSuitablePair.dpgCand);
			if (goodBPGinarow[curROInum].size() > 100)
			{
				goodBPGinarow[curROInum].pop_front();
				goodDPGinarow[curROInum].pop_front();
			}

			//float maxdist = 3.0;
			//int n_stable = 16;
			float maxdist = 9.0;
			int n_stable = 10;
			bool zero_in = false;
			if (goodBPGinarow[curROInum].size() >= n_stable && goodDPGinarow[curROInum].size() >= n_stable)
			{
				float maxBPGdist = 0;

				for (int bpGlints = 2; bpGlints < n_stable; bpGlints++)
				{
					if (goodBPGinarow[curROInum].at(goodBPGinarow[curROInum].size() - bpGlints) == Point2f(0, 0))
					{
						zero_in = true;
						break;
					}

					Point2f mid1 = (goodBPGinarow[curROInum].back() + goodDPGinarow[curROInum].back()) / 2;
					Point2f mid2 = (goodBPGinarow[curROInum].at(goodBPGinarow[curROInum].size() - bpGlints) + goodDPGinarow[curROInum].at(goodDPGinarow[curROInum].size() - bpGlints)) / 2;

					float disBP = pDist(mid1, mid2);

					if (disBP > maxBPGdist)
					{
						maxBPGdist = disBP;
					}
				}
				if (maxBPGdist < maxdist && !zero_in)
				{
					goodGlintsSequence[curROInum] = true;
				}
			}

			if (curROInum == 0)
			{
				candidate.bpG.x = frameHeight - mostSuitablePair.bpgCand.y;
				candidate.bpG.y = frameWidth - mostSuitablePair.bpgCand.x;

				candidate.dpG.x = frameHeight - mostSuitablePair.dpgCand.y;
				candidate.dpG.y = frameWidth - mostSuitablePair.dpgCand.x;
			}
			else
			{
				candidate.bpG.x = mostSuitablePair.bpgCand.y;
				candidate.bpG.y = mostSuitablePair.bpgCand.x;

				candidate.dpG.x = mostSuitablePair.dpgCand.y;
				candidate.dpG.y = mostSuitablePair.dpgCand.x;
			}

			candidate.bpGraw = mostSuitablePair.bpgCand;
			candidate.dpGraw = mostSuitablePair.dpgCand;
			candidate.angle = mostSuitablePair.angle;
			candidate.dpgRect = Rect2f(mostSuitablePair.dpgCand.x - _glintsRadius, mostSuitablePair.dpgCand.y - _glintsRadius, _glintsRadius * 2, _glintsRadius * 2);
			candidate.bpgRect = Rect2f(mostSuitablePair.bpgCand.x - _glintsRadius, mostSuitablePair.bpgCand.y - _glintsRadius, _glintsRadius * 2, _glintsRadius * 2);
			candidate.bpgArea = mostSuitablePair.bpGarea;
			candidate.dpgArea = mostSuitablePair.dpGarea;

			allBpgs[curROInum].push_back(candidate.bpGraw);
			allDpgs[curROInum].push_back(candidate.dpGraw);
			if (allBpgs[curROInum].size() > 100)
			{
				allBpgs[curROInum].pop_front();
			}
			if (allDpgs[curROInum].size() > 100)
			{
				allDpgs[curROInum].pop_front();
			}

			tgReturn[curROInum] = candidate;

		}
		else if (only_in_ellipse)
		{
			bpgCands[curROInum].clear();
			dpgCands[curROInum].clear();

			only_in_ellipse = false;
			goto gtagain;
		}
		else
		{
			goodBPGinarow[curROInum].clear();
			goodDPGinarow[curROInum].clear();
		}
	}

	return tgReturn;
}
TandemGlints EyeTracker::DPglintTracking(Mat& DPframe, Mat& trueBPframe, RotatedRect& detectedPupil, int curROInum)
{
	TandemGlints tgReturn = TandemGlints();

	Point2f basePoint = detectedPupil.center;
	if (basePoint == Point2f(0, 0))
	{
		return tgReturn;
	}

	int baseOffset = 20;

	if (curROInum == 0)
	{
		basePoint.x += baseOffset;
	}
	else
	{
		basePoint.x -= baseOffset;
	}

	bool gt_round = 0;
	bool found = false;
	float expa;

	if (curROInum == 1)
		expa = 0;
	else
		expa = 180;


	Mat localDPimage, localBPimage;
	DPframe(ROIs[curROInum].glintsROI).copyTo(localDPimage);
	trueBPframe(ROIs[curROInum].glintsROI).copyTo(localBPimage);

	Mat thresholdedDP, thresholdedBP;
	threshold(localDPimage, thresholdedDP, 250, 255, THRESH_BINARY);
	threshold(localBPimage, thresholdedBP, 250, 255, THRESH_BINARY);

	vector<vector<Point>> DPGcontours, BPGcontours;
	vector<Vec4i> DPGhierarchy, BPGhierarchy;
	findContours(thresholdedDP, DPGcontours, DPGhierarchy, RETR_EXTERNAL, CHAIN_APPROX_NONE, Point(ROIs[curROInum].glintsROI.x, ROIs[curROInum].glintsROI.y));
	findContours(thresholdedBP, BPGcontours, BPGhierarchy, RETR_EXTERNAL, CHAIN_APPROX_NONE, Point(ROIs[curROInum].glintsROI.x, ROIs[curROInum].glintsROI.y));

	CandidateTandem mostSuitablePair;

	bool only_in_ellipse = true;

gtagain:
	double max_dist_limit = detectedPupil.size.width * 0.7 + baseOffset, max_ang_limit = 85;

	vector<DpgCandidate> tmpdpgCands;

	for (int dpgcontour = 0; dpgcontour < DPGcontours.size(); dpgcontour++)
	{
		if (DPGcontours[dpgcontour].size() < 3)
		{
			continue;
		}
		double darea = contourArea(DPGcontours[dpgcontour]);
		if (darea < 1 || darea > 300)
		{
			continue;
		}

		Moments m = moments(DPGcontours[dpgcontour]);
		Point2f dpgcan;
		dpgcan.x = m.m10 / m.m00;
		dpgcan.y = m.m01 / m.m00;

		float equation_dpg = pow((dpgcan.x - glintsEllipses[curROInum].center.x), 2) / pow((glintsEllipses[curROInum].size.width / 2), 2) + pow((dpgcan.y - glintsEllipses[curROInum].center.y), 2) / pow((glintsEllipses[curROInum].size.height / 2), 2);
		if (equation_dpg > 1.f && only_in_ellipse)
		{
			continue;
		}

		float big_equation_dpg = pow((dpgcan.x - bigGlintsEllipses[curROInum].center.x), 2) / pow((bigGlintsEllipses[curROInum].size.width / 2), 2) + pow((dpgcan.y - bigGlintsEllipses[curROInum].center.y), 2) / pow((bigGlintsEllipses[curROInum].size.height / 2), 2);
		if (big_equation_dpg > 1.f)
		{
			continue;
		}

		float dist = pDist(basePoint, dpgcan);
		float angl = pAng(basePoint, dpgcan);
		float andel = compareAngDeg(angl, expa);

		if (dist > 2 && dist < max_dist_limit && andel < max_ang_limit)
		{
			DpgCandidate tmpdpgCand;
			tmpdpgCand.pos = dpgcan;
			tmpdpgCand.angle = andel;
			tmpdpgCand.area = darea;
			tmpdpgCands.push_back(tmpdpgCand);
		}
	}

	if (tmpdpgCands.size() > 0)
	{
		double errorProduct = DBL_MAX;
		bool wholeStainCenterIsNear = false;

		for (int dpgcan = 0; dpgcan < tmpdpgCands.size(); dpgcan++)
		{
			// check if some bright pixel on BP frame exists >>>
			moveContour(tmpdpgCands[dpgcan].pos, curROInum);
			bounrect[curROInum].x -= ROIs[curROInum].glintsROI.x;
			bounrect[curROInum].y -= ROIs[curROInum].glintsROI.y;
			if (bounrect[curROInum].size() != drawnAngularArea[curROInum].size())
			{
				cerr << __LINE__ << " SIZES MISMATCH" << endl;
			}
			Mat intersection = thresholdedBP(bounrect[curROInum]) & drawnAngularArea[curROInum];

			vector<vector<Point>> contours;
			findContours(intersection, contours, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);

			if (contours.size() < 1)
			{
				continue;
			}

			sort(contours.begin(), contours.end(), [](const vector<Point>& one, const vector<Point>& two)
			{ return contourArea(one) > contourArea(two); });

			double darea = contourArea(contours[0]);
			if (darea < 1)
			{
				continue;
			}

			// <<< check if some bright pixel on BP frame exists

			pair<Size2f, double> dpgToLine = blueLineRule(basePoint, tmpdpgCands[dpgcan].pos, curROInum, true);
			float toDestDistDPG = pDist(ROIs[curROInum].rotPoint, tmpdpgCands[dpgcan].pos) + 1.0;
			double this_errorProduct = 1.0 * 1.0 * (pDist(basePoint, tmpdpgCands[dpgcan].pos) + 1.0);

			if (this_errorProduct < errorProduct)
			{
				found = true;
				errorProduct = this_errorProduct;
				mostSuitablePair.dpGarea = tmpdpgCands[dpgcan].area;
				mostSuitablePair.dpgCand = tmpdpgCands[dpgcan].pos;
			}
		}
	}

	if (found)
	{
		TandemGlints candidate = TandemGlints();

		if (curROInum == 0)
		{
			candidate.bpG.x = candidate.dpG.x = frameHeight - mostSuitablePair.dpgCand.y;
			candidate.bpG.y = candidate.dpG.y = frameWidth - mostSuitablePair.dpgCand.x;
		}
		else
		{
			candidate.bpG.x = candidate.dpG.x = mostSuitablePair.dpgCand.y;
			candidate.bpG.y = candidate.dpG.y = mostSuitablePair.dpgCand.x;
		}

		candidate.bpGraw = candidate.dpGraw = mostSuitablePair.dpgCand;
		candidate.bpgArea = candidate.dpgArea = mostSuitablePair.dpGarea;

		tgReturn = candidate;
	}
	else if (only_in_ellipse)
	{
		only_in_ellipse = false;
		goto gtagain;
	}

	//moveContour(tgReturn.dpGraw, curROInum);

	return tgReturn;
}
void EyeTracker::moveContour(const Point2f& dpgRaw, int curROInum)
{
	Rect _bounrect = boundingRect(dpgAngularContours[curROInum]);

	int d_x = 0; 
	int d_y = (int)(dpgRaw.y + 0.5f) - _bounrect.y - _bounrect.height / 2;
	
	if (curROInum == 0)
	{
		d_x = (int)(dpgRaw.x + 0.5f) - _bounrect.x;
	}
	else
	{
		d_x = (int)(dpgRaw.x + 0.5f) - _bounrect.x - _bounrect.width;
	}

	Point shift = Point(d_x, d_y);
	for_each(dpgAngularContours[curROInum].begin(), dpgAngularContours[curROInum].end(), [&](Point& pt) {pt += shift; });
	bounrect[curROInum] = boundingRect(dpgAngularContours[curROInum]);
}
pair<Size2f, double> EyeTracker::blueLineRule(Point2f bpg, Point2f dpg, int curROInum, bool israw)
{
	pair<Size2f, double> ret;

	if (israw)
	{
		bpg = convertCoords2(bpg, curROInum);
		dpg = convertCoords2(dpg, curROInum);
	}

	Point2f shapeCenter = convertCoords2(centersOfShapes[curROInum], curROInum);

	double y_dist = 23.545 * _pixPerMm;
	double z_dist = 61.87718 * _pixPerMm;
	double diff_y = -(bpg.y - shapeCenter.y);
	double consider_offset = (28.5 * _pixPerMm) - (1024.0 - (offset.height + frameHeight));
	double dp_led_x = frameHeight - consider_offset + (consider_offset * 2 * curROInum);
	double diff_x = bpg.x - dp_led_x;

	todpleddist[curROInum] = sqrt(pow(diff_x, 2) + pow(y_dist + diff_y, 2) + pow(z_dist, 2));

	ret.first.height = atan((y_dist + diff_y) / z_dist) * 180.0 / CV_PI;
	ret.first.width = atan(diff_x / z_dist) * 180.0 / CV_PI;

	Point2f lineBegins = bpg;
	float angl = pAng(Point2f(0, 0), Point2f(ret.first.width, ret.first.height));
	float x2 = lineBegins.x + 400 * cos(angl * CV_PI / 180.0);
	float y2 = lineBegins.y + 400 * -sin(angl * CV_PI / 180.0);

	double tolinedist = toLineDist(dpg.x, dpg.y, lineBegins.x, lineBegins.y, x2, y2);

	ret.second = tolinedist;

	return ret;
}
vector<PursuitValues> EyeTracker::getPursuitValues(const vector<TandemGlints>& glints)
{
	float orange_line = 2;
	int lookBack = 50;
	int coridor = 1;
	int movVector = 300;
	int average = 3;

	vector<PursuitValues> pv = { PursuitValues(), PursuitValues() };

	for (int curROInum = 0; curROInum <= 1; curROInum++)
	{
		Point2f mid_point = (glints[curROInum].bpGraw + glints[curROInum].dpGraw) / 2;
		if (mid_point != Point2f(0, 0))
		{
			mid_points[curROInum].push_back(mid_point);

			if (mid_points[curROInum].size() > movVector)
			{
				Point2f avgBegin = Point2f(0, 0);
				for_each(mid_points[curROInum].begin(), mid_points[curROInum].begin() + average, [&avgBegin](Point2f& _pt) {avgBegin += _pt; });
				avgBegin = avgBegin / average;

				Point2f avgEnd = Point2f(0, 0);
				for_each(mid_points[curROInum].rbegin(), mid_points[curROInum].rbegin() + average, [&avgEnd](Point2f& _pt) {avgEnd += _pt; });
				avgEnd = avgEnd / average;

				pv[curROInum].vectorAngle = pAng(avgBegin, avgEnd);
				pv[curROInum].vectorLength = pDist(avgBegin, avgEnd);

				mid_points[curROInum].erase(mid_points[curROInum].begin());
			}

			if (mid_points[curROInum].size() > lookBack)
			{
				float x_nn_dist = pDist(mid_points[curROInum].back(), mid_points[curROInum].at(mid_points[curROInum].size() - lookBack - 1));
				distances[curROInum].push_back(x_nn_dist);
			}

			if (distances[curROInum].size() > coridor)
			{
				float avg_distance_to_orange = 0;
				for_each(distances[curROInum].rbegin(), distances[curROInum].rbegin() + coridor, [&avg_distance_to_orange, &orange_line](float& _dist) {avg_distance_to_orange += abs(_dist - orange_line); });
				avg_distance_to_orange = avg_distance_to_orange / coridor;
				pv[curROInum].indicator = avg_distance_to_orange;
				distances[curROInum].erase(distances[curROInum].begin());
			}
		}
	}

	return pv;
}

Point2f EyeTracker::convertCoords(Point2f rawPoint, int curROInum)
{
	float angle = ((97 * (2 * curROInum - 1)) / 180.f) * CV_PI;
	float cosang = cos(angle);
	float sinang = sin(angle);

	Point2f cP = Point(320 + 640 * curROInum, 250);
	Point2f ret = rawPoint - cP;
	Point2f rr;
	rr.x = ret.x * cosang - ret.y * sinang;
	rr.y = ret.x * sinang + ret.y * cosang;

	return rr + cP;
}
Point2f EyeTracker::convertCoords2(Point2f rawPoint, int curROInum)
{
	float angos = 0;
	if (curROInum == 0)
	{
		angos = 3.5;
	}
	else
	{
		angos = 4.5;
	}

	Point2f cP = Point(1000 / 4 + 1000 / 2 * curROInum, 400 / 2);
	float shift_angle = ((90 + angos) * (2 * curROInum - 1)); //*-1 if OD
	float this_angle = pAng(cP, rawPoint);
	float _dist = pDist(cP, rawPoint);

	//float rad_angle = (((90 + anglesOfShapes[curROInum]) * (2 * curROInum - 1)) / 180.f) * CV_PI;
	//float cosang = cos(rad_angle);
	//float sinang = sin(rad_angle);

	Point2f newcP = Point(400 / 2 + 400 * curROInum, 1000 / 4);

	Point2f rr;
	rr.x = newcP.x + _dist * cos((this_angle + shift_angle) * CV_PI / 180.0);
	rr.y = newcP.y + _dist * sin((this_angle + shift_angle) * CV_PI / 180.0);
	//Point2f ret = rawPoint - cP;
	//Point2f rr;
	//rr.x = ret.x * cosang - ret.y * sinang - 120 - 200 * curROInum;
	//rr.y = ret.x * sinang + ret.y * cosang + 125;

	//return rr + cP;
	return rr;
}

void EyeTracker::buildDPGAngularArea(int curROInum)
{
	int min_dist = 2;
	int max_dist = 40;
	int plusminus_angle = 70;

	Mat canvas = Mat(max_dist * 3, max_dist * 3, CV_8UC1, Scalar::all(0));
\
	Point center = Point(canvas.cols / 2, canvas.rows / 2);

	circle(canvas, center, max_dist, Scalar::all(255), -1);
	circle(canvas, center, min_dist, Scalar::all(0), -1);

	float x2 = 0;
	float y2 = center.y + (max_dist + 10) * -sin((180 - plusminus_angle) * CV_PI / 180.0);
	float y3 = center.y - (max_dist + 10) * -sin((180 - plusminus_angle) * CV_PI / 180.0);

	if (curROInum == 0)
	{
		x2 = center.x - (max_dist + 10) * cos((180 + plusminus_angle) * CV_PI / 180.0);
	}
	else
	{
		x2 = center.x + (max_dist + 10) * cos((180 + plusminus_angle) * CV_PI / 180.0);
	}

	line(canvas, center, Point2f(x2, y2), Scalar::all(0), 2);
	line(canvas, center, Point2f(x2, y3), Scalar::all(0), 2);

	Mat thresholded;
	threshold(canvas, thresholded, 250, 255, THRESH_BINARY);

	vector<vector<Point>> contours;
	findContours(thresholded, contours, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE, Point(0, 0));

	if (contours.size() != 2)
	{
		cerr << "WRONG CONTOURS SIZE" << endl;
		throw;
	}

	sort(contours.begin(), contours.end(), [](const vector<Point>& one, const vector<Point>& two)
	{ return contourArea(one) < contourArea(two); });

	dpgAngularContours[curROInum] = contours[0];
	bounrect[curROInum] = boundingRect(contours[0]);
	drawnAngularArea[curROInum] = Mat(bounrect[curROInum].size(), CV_8UC1, Scalar::all(0));

	if (curROInum == 0)
	{
		moveContour(Point(0, bounrect[curROInum].height / 2), curROInum);
	}
	else
	{
		moveContour(Point(bounrect[curROInum].width, bounrect[curROInum].height / 2), curROInum);
	}

	drawContours(drawnAngularArea[curROInum], vector<vector<Point>>(1, dpgAngularContours[curROInum]), 0, Scalar::all(255), -1);
	//imwrite("D:\\small" + to_string(curROInum) + ".bmp", drawnAngularArea[curROInum]);
}





