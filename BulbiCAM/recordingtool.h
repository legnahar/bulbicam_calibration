#pragma once

#include <fstream>
#include <chrono>
#include <filesystem>
#include <mutex>
#include "Tests.h"

#define recordingtool_version "0.9.20211018"

extern CTest* currentTest;
extern ofstream qualityLog;
extern void dataRead2(vector<pair<string, string>> commands, bool fromPreassesment);
extern vector<pair<string, string>> savedCommands;
extern string recToolVideosPath;
//extern CPTGCamera *ptgreycam;

struct recToolElem
{
	Mat frame;
	Point tpos;
	int eye;
	bool bright;
	int tt;
};

class RecordingTool : public CTest
{
public:
	RecordingTool();
	void ProcessETData(Mat curPic, int lineStatus);
	int expT = 2000;

private:
	void initStimuli();
	void writeToFile();
	float degreesToPixels(float angle);

	vector<recToolElem> toWrite;
	int stimsIds[2];
	int testcount = 5; //n of dots
	int dots[5][2] = {
		{0, 0},
		{10,	10},
		{-10, 10},
		{-10,	-10},
		{10,	-10},
	};

	int dot_num = -1;
	int eye = 0;
	double stimuli_time = 1.5;
	bool first_time = true;
	bool stopAcq = false;
};

RecordingTool::RecordingTool()
{
	initStimuli();
	glfwSetTime(0);
	webComm->openLog();

	nlohmann::json nystagmus_start;
	nystagmus_start["chartTypeString"] = "RECORDING_TOOL";
	nystagmus_start["message_type"] = MESSAGE_TYPE::START_TEST;
	nystagmus_start["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	nystagmus_start["metadata"] = versionInfoForHub;
	webComm->sendJSON(nystagmus_start);

	nlohmann::json nystagmus_startdot;
	nystagmus_startdot["chartTypeString"] = "RECORDING_TOOL";
	nystagmus_startdot["message_type"] = 15;
	nystagmus_startdot["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	webComm->sendJSON(nystagmus_startdot);
}

void RecordingTool::initStimuli()
{
	for (size_t i = 0; i < 2; i++)
	{
		StimuliObjects _stim;
		_stim.objectCoordinates[0] = -99;
		_stim.objectCoordinates[1] = -99;
		_stim.objectColor[0] = 0;
		_stim.objectColor[1] = 0;
		_stim.objectColor[2] = 0;
		_stim.objectSize = 32;
		_stim.objectType = StimuliObjectType::CIRCLE;
		stimsIds[i] = display.addObject(_stim);
	}

	display.setBackground(0.8, 0.8, 0.8);
}

void RecordingTool::ProcessETData(Mat curPic, int lineStatus)
{
	//Redraw stimuli
	if (glfwGetTime() > stimuli_time || first_time)
	{
		first_time = false;
		glfwSetTime(0);
		dot_num++;
		if (dot_num >= testcount)
		{
			stopAcq = true;
			writeToFile();
		}
		else
		{
			display.setBackground(0, 0, 0);
			display.paintObject(stimsIds[eye], 0, 1, 0);
			display.moveObject(stimsIds[eye],
				960 + eye * 1920 + degreesToPixels(dots[dot_num][0]),
				540 + degreesToPixels(dots[dot_num][1]));
			display.redrawPending = true;
		}
	}

	//Save frame
	if (!stopAcq)
	{
		recToolElem tmp;
		Mat frame;

		tmp.eye = eye;
		tmp.tpos.x = dots[dot_num][0];
		tmp.tpos.y = dots[dot_num][1];
		tmp.frame = curPic.clone();
		if (lineStatus == 14)
			tmp.bright = true;
		else
			tmp.bright = false;

		toWrite.push_back(tmp);
	}
}

void RecordingTool::writeToFile()
{
	if (toWrite.size() > 0)
	{
		VideoWriter vw;
		VideoWriter vwcompressed;
		long long ts = std::chrono::system_clock::now().time_since_epoch().count();
		std::ostringstream oss;
		oss << "rec_"
			<< std::chrono::system_clock::now().time_since_epoch().count()
			<< "_id" << patientInfo.examID
			<< "_" << patientInfo.diagnosis;

		std::string filenamefull = recToolVideosPath;
		std::string filenamefullb = recToolVideosPath;
		std::string filenamefulld = recToolVideosPath;
		filenamefull.append(oss.str());

		VideoWriter vwb;
		VideoWriter vwd;

		filenamefullb.append(oss.str());
		filenamefulld.append(oss.str());
		filenamefullb.append("_onlybright.avi");
		filenamefulld.append("_onlydark.avi");

		filenamefull.append(".avi");

		std::string fn = oss.str();
		fn.append(".avi");

		std::string filenamefullcompressed = recToolVideosPath;
		filenamefullcompressed.append(oss.str());
		filenamefullcompressed.append(".mp4");

		std::string fncompressed = oss.str();
		fncompressed.append(".mp4");

		vw.open(filenamefull, 0, 100, Size(toWrite[0].frame.cols, toWrite[0].frame.rows), false);

		vwb.open(filenamefullb, 0, 100, Size(toWrite[0].frame.cols, toWrite[0].frame.rows), false);
		vwd.open(filenamefulld, 0, 100, Size(toWrite[0].frame.cols, toWrite[0].frame.rows), false);

		vwcompressed.open(filenamefullcompressed, VideoWriter::fourcc('a', 'v', 'c', '1'), 100, Size(toWrite[0].frame.cols, toWrite[0].frame.rows), false);
		for (int i = 0; i < toWrite.size(); i++)
		{
			Scalar bcol;
			Scalar ecol = Scalar(255 * toWrite[i].eye);
			if (toWrite[i].bright)
				bcol = Scalar(255);
			else
				bcol = Scalar(0);
			rectangle(toWrite[i].frame, Rect(0, 5, 5, 5), Scalar(255 * toWrite[i].tt), -1);
			rectangle(toWrite[i].frame, Rect(5, 5, 5, 5), bcol, -1);
			rectangle(toWrite[i].frame, Rect(10, 5, 5, 5), ecol, -1);
			rectangle(toWrite[i].frame, Rect(15, 5, 5, 5), Scalar(128 + toWrite[i].tpos.x * 3), -1);
			rectangle(toWrite[i].frame, Rect(20, 5, 5, 5), Scalar(128 + toWrite[i].tpos.y * 3), -1);
			rectangle(toWrite[i].frame, Rect(25, 5, 5, 5), Scalar((expT - 1000.f) / 10.f), -1);
//			rectangle(toWrite[i].frame, Rect(30, 5, 5, 5), Scalar(ptgreycam->yOffset / 4), -1);
			rectangle(toWrite[i].frame, Rect(35, 5, 5, 5), Scalar(128 + lensDist->dioptersOD * 4), -1);
			rectangle(toWrite[i].frame, Rect(40, 5, 5, 5), Scalar(128 + lensDist->dioptersOS * 4), -1);
			rectangle(toWrite[i].frame, Rect(45, 5, 5, 5), Scalar(patientInfo.age * 2), -1);
			vw.write(toWrite[i].frame);
			if (toWrite[i].bright)
				vwb.write(toWrite[i].frame);
			else
				vwd.write(toWrite[i].frame);
			vwcompressed.write(toWrite[i].frame);
		}

		toWrite.clear();
		vw.release();
		vwb.release();
		vwd.release();
		vwcompressed.release();
		cout << "Video recording completed." << endl;

		nlohmann::json nystagmus_start;
		nystagmus_start["chartTypeString"] = "RECORDING_TOOL";
		nystagmus_start["message_type"] = 6;
		nystagmus_start["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
		nystagmus_start["target"] = fncompressed;
		webComm->sendJSON(nystagmus_start);
	}

	nlohmann::json nystagmus_stop;
	nystagmus_stop["chartTypeString"] = "RECORDING_TOOL";
	nystagmus_stop["message_type"] = MESSAGE_TYPE::STOP_TEST;
	nystagmus_stop["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	webComm->sendJSON(nystagmus_stop);

	webComm->closeLog();
	qualityLog.close();
	display.setDragonfly();
	currentTest = nullptr;
	return;
}

float RecordingTool::degreesToPixels(float angle)
{
	return ((float)(1920 * (160.f * tan((float)(angle * CV_PI) / 180.f)) / 121.f));
}