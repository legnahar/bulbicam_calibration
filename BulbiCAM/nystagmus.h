#pragma once

#include "Tests.h"

#define nystagmusversion "0.9.20220805"

extern CTest* currentTest;
extern ofstream qualityLog;
extern ofstream testLog;
extern string testVideosPath;


struct nCamFrame
{
	Mat pic;
	bool BP;
	ETData etd[2];
	uint64_t timestamp;
	float secs;
};

struct nystagmusTarget
{
	Point2d OSTarget, ODTarget;
	string name;
	int nType;
	vector<nCamFrame> camFrames;
};

struct nystagmusPoints
{
	Point2d os;
	Point2d od;
	string name;
	int nType;
	bool large;
	bool showL;
	bool showR;
};

enum NystagmusState
{
	BUTTERFLY1 = 0,
	NYSDOT = 1,
	BUTTERFLY2 = 2,
	CALIBL = 3,
	CALIBR = 4
};

class CNystagmusTest : public CTest
{
public:
	CNystagmusTest(vector<nystagmusPoints> np, int testDuration);
	void ProcessETData(vector<ETData> etData, Mat frame, bool bp);
	int testNum = 31;
	Point2d upCalOS, downCalOS, leftCalOS, rightCalOS;
	Point2d upCalOD, downCalOD, leftCalOD, rightCalOD;
	int calNum;
private:
	vector<nystagmusPoints> nysp;
	float lcDist(Point2d p1, Point2d p2);
	vector<nCamFrame> camFrames[3];
	void writeData();
	void encodeVideos();
	int nystagmusState;
	int numdot;
	int nys_duration[3];
	int nys_stages;
	bool calibrated;
	vector<string> filenames;
	int animalNum;
	bool rCal;
	bool lCal;
};

void CNystagmusTest::encodeVideos()
{
	display.nShowL = false;
	display.nShowR = false;
	display.redrawPending = true;

	for (size_t i = 0; i < filenames.size(); i++)
	{
		VideoCapture vc;
		VideoWriter vw;
		VideoWriter vwrs;

		string filenamebefore = testVideosPath + filenames[i] + ".avi";
		string filenameafter = testVideosPath + filenames[i] + ".mp4";
		string filenameafterrs = testVideosPath + "fs" + filenames[i] + ".mp4";
		vc.open(filenamebefore);
		int fWidth = vc.get(CAP_PROP_FRAME_WIDTH);
		int fHeight = vc.get(CAP_PROP_FRAME_HEIGHT);
		
		vw.open(filenameafter, MAKEFOURCC('H', '2', '6', '4'), 1, Size(fHeight * 2, fWidth / 2), false);
		vwrs.open(filenameafterrs, MAKEFOURCC('H', '2', '6', '4'), 200, Size(fHeight * 2, fWidth /2), false);
		Point2f src_center(fWidth / 4, fHeight / 2);
		Mat rot_ccw = getRotationMatrix2D(src_center, -90, 1.0);
		Mat rot_cw = getRotationMatrix2D(src_center, 90, 1.0);
		Mat curframe;
		while (vc.read(curframe))
		{
			cvtColor(curframe, curframe, COLOR_BGR2GRAY);
			Mat rotatedpic = Mat::zeros(fWidth / 2, fHeight * 2, CV_8UC1);
			rotate(curframe(Rect(0, 0, fWidth / 2, fHeight)), rotatedpic(Rect(0, 0, fHeight, fWidth / 2)), ROTATE_90_COUNTERCLOCKWISE);
			rotate(curframe(Rect(fWidth / 2, 0, fWidth / 2, fHeight)), rotatedpic(Rect(fHeight, 0, fHeight, fWidth / 2)), ROTATE_90_CLOCKWISE);
			vw.write(rotatedpic);
			vwrs.write(rotatedpic);
		}
		vw.release();
		vwrs.release();
		vc.release();
	}
}

float CNystagmusTest::lcDist(Point2d p1, Point2d p2)
{

	float dist = sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));
	float lll = 0;
	lll = -lensDist->dioptersOD;
//	lll = -lensDist->dioptersOS;
	float perc = 0;
	if (lll > 0)
		perc = 0.0232;
	else if (lll < 0)
		perc = -0.0178;
	float k = perc * lll;

	return dist + dist * k;

}

CNystagmusTest::CNystagmusTest(vector<nystagmusPoints> np, int testDuration)
{
	if (testDuration == 4)
		nys_stages = 0;
	else if (testDuration == 34)
		nys_stages = 1;
	else if (testDuration == 64)
		nys_stages = 2;
	nysp = np;
	nys_duration[0] = 4;
	nys_duration[1] = 34;
	nys_duration[2] = 64;
	animalNum = 0;
	display.loadGrayAnimals();
	display.nAnilmalNum = animalNum;
	for (size_t i = 0; i < nysp.size(); i++)
	{
		if (nysp[i].name == "Central Gaze Far")
		{
			nysp[i].os.x = 960 + (lensDist->mmtopix(lensDist->PPD / 2.) - lensDist->mmtopix(1) - lensDist->mmtopix(10) * (-lensDist->dioptersOS / 7.));
			nysp[i].od.x = 2880 - (lensDist->mmtopix(lensDist->PPD / 2.) - lensDist->mmtopix(1) - lensDist->mmtopix(10) * (-lensDist->dioptersOS / 7.));
			nysp[i].os.y = 540;
			nysp[i].od.y = 540;
			nysp[i].showL = true;
			nysp[i].showR = true;
		}
		else if (nysp[i].name == "Central Gaze Near")
		{
			nysp[i].os.x = 960 + (lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOS / 7.));
			nysp[i].od.x = 2880 - (lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOS / 7.));
			nysp[i].os.y = 540;
			nysp[i].od.y = 540;
			nysp[i].showL = true;
			nysp[i].showR = true;
		}
		else if (nysp[i].name == "Up Gaze Far")
		{
			nysp[i].os.x = 960 + (lensDist->mmtopix(lensDist->PPD / 2.) - lensDist->mmtopix(1) - lensDist->mmtopix(10) * (-lensDist->dioptersOS / 7.));
			nysp[i].od.x = 2880 - (lensDist->mmtopix(lensDist->PPD / 2.) - lensDist->mmtopix(1) - lensDist->mmtopix(10) * (-lensDist->dioptersOS / 7.));
			nysp[i].os.y = 1040;
			nysp[i].od.y = 1040;
			nysp[i].showL = true;
			nysp[i].showR = true;
		}
		else if (nysp[i].name == "Up Gaze Near")
		{
			nysp[i].os.x = 960 + (lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOS / 7.));
			nysp[i].od.x = 2880 - (lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOS / 7.));
			nysp[i].os.y = 1040;
			nysp[i].od.y = 1040;
			nysp[i].showL = true;
			nysp[i].showR = true;
		}
		else if (nysp[i].name == "Down Gaze Far")
		{
			nysp[i].os.x = 960 + (lensDist->mmtopix(lensDist->PPD / 2.) - lensDist->mmtopix(1) - lensDist->mmtopix(10) * (-lensDist->dioptersOS / 7.));
			nysp[i].od.x = 2880 - (lensDist->mmtopix(lensDist->PPD / 2.) - lensDist->mmtopix(1) - lensDist->mmtopix(10) * (-lensDist->dioptersOS / 7.));
			nysp[i].os.y = 40;
			nysp[i].od.y = 40;
			nysp[i].showL = true;
			nysp[i].showR = true;
		}
		else if (nysp[i].name == "Down Gaze Near")
		{
			nysp[i].os.x = 960 + (lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOS / 7.));
			nysp[i].od.x = 2880 - (lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOS / 7.));
			nysp[i].os.y = 40;
			nysp[i].od.y = 40;
			nysp[i].showL = true;
			nysp[i].showR = true;
		}
		else if (nysp[i].name == "Left Gaze Far")
		{
			nysp[i].os.x = 1850;
			nysp[i].od.x = 3770 - 2 * (lensDist->mmtopix(lensDist->PPD / 2.) - lensDist->mmtopix(1) - lensDist->mmtopix(10) * (-lensDist->dioptersOS / 7.));
			nysp[i].os.y = 540;
			nysp[i].od.y = 540;
			nysp[i].showL = true;
			nysp[i].showR = true;
		}
		else if (nysp[i].name == "Left Gaze Near")
		{
			nysp[i].os.x = 1850;
			nysp[i].od.x = 3770 - 2 * (lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOS / 7.));
			nysp[i].os.y = 540;
			nysp[i].od.y = 540;
			nysp[i].showL = true;
			nysp[i].showR = true;
		}
		else if (nysp[i].name == "Right Gaze Far")
		{
			nysp[i].os.x = 70 + 2 * (lensDist->mmtopix(lensDist->PPD / 2.) - lensDist->mmtopix(1) - lensDist->mmtopix(10) * (-lensDist->dioptersOS / 7.));
			nysp[i].od.x = 1990;
			nysp[i].os.y = 540;
			nysp[i].od.y = 540;
			nysp[i].showL = true;
			nysp[i].showR = true;
		}
		else if (nysp[i].name == "Right Gaze Near")
		{
			nysp[i].os.x = 70 + 2 * (lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOS / 7.));
			nysp[i].od.x = 1990;
			nysp[i].os.y = 540;
			nysp[i].od.y = 540;
			nysp[i].showL = true;
			nysp[i].showR = true;
		}
		else
		{
			if (nysp[i].os.x != 777 && nysp[i].os.y != 777)
			{
				nysp[i].os = lensDist->mmtopix(lensDist->posfromanglOS(nysp[0].os));
				nysp[i].os.x = 1920 - nysp[i].os.x;
				nysp[i].showL = true;
			}
			else
			{
				nysp[i].os.x = 0;
				nysp[i].os.y = 0;
				nysp[i].showL = false;
			}
			if (nysp[i].od.x != 777 && nysp[i].od.y != 777)
			{
				nysp[i].od = lensDist->mmtopix(lensDist->posfromanglOD(nysp[0].od));
				nysp[i].od.x = 3840 - nysp[i].od.x;
				nysp[i].showR = true;
			}
			else
			{
				nysp[i].od.x = 0;
				nysp[i].od.y = 0;
				nysp[i].showR = false;
			}
		}
	}
	glfwSetTime(0);
	for (size_t i = 0; i < nysp.size(); i++)
	{
		cout << nysp[i].name << endl;
		cout << "OS x = " << 1920 - nysp[i].os.x << "  ;  OS y = " << nysp[i].os.y << endl;
		cout << "OD x = " << 3840 - nysp[i].od.x + 1920 << "  ;  OD y = " << nysp[i].od.y << endl;
	}
	eyetrackingOn = true;

	display.nCoordL.x = nysp[0].os.x;
	display.nCoordR.x = nysp[0].od.x;
	display.nCoordL.y = nysp[0].os.y;
	display.nCoordR.y = nysp[0].od.y;
	display.nTargetType = nysp[0].nType;
	display.nShowL = nysp[0].showL;
	display.nShowR = nysp[0].showR;
	display.nysLarge = nysp[0].large;

	display.nystagmusType = 0;
	display.stimuliType = DisplayStimuliType::NYSTAGMUS;
	display.redrawPending = true;
	
	nystagmusState = 0;

	upCalOS = Point2d(0, 0);
	downCalOS = Point2d(0, 0);
	leftCalOS = Point2d(0, 0);
	rightCalOS = Point2d(0, 0);

	upCalOD = Point2d(0, 0);
	downCalOD = Point2d(0, 0);
	leftCalOD = Point2d(0, 0);
	rightCalOD = Point2d(0, 0);

	calibrated = false;
	webComm->openLog();
	numdot = 0;

	nlohmann::json nystagmus_start;
	nystagmus_start["chartTypeString"] = "NYSTAGMUS_EVALUATION";
	nystagmus_start["message_type"] = MESSAGE_TYPE::START_TEST;
	nystagmus_start["lengthInSeconds"] = testDuration;
	nystagmus_start["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	nystagmus_start["metadata"] = versionInfoForHub;
	webComm->sendJSON(nystagmus_start);

}

void CNystagmusTest::writeData()
{
	if (camFrames[0].size() == 0)
		return;
	VideoWriter vw;
	long long ts = std::chrono::system_clock::now().time_since_epoch().count();
	string filenameshort = format("%d.mp4", ts);
	string filenameshortrs = format("fs%d.mp4", ts);
	filenames.push_back(format("%d", ts));

	string filenamefull = testVideosPath + format("%d.avi", ts);
	string filenamefullrs = testVideosPath + format("%d.avi", ts);
	vw.open(filenamefull, 0, 1000, Size(camFrames[0][0].pic.cols, camFrames[0][0].pic.rows), false);

	nlohmann::json nystagmus_startdot;
	nystagmus_startdot["chartTypeString"] = "NYSTAGMUS_EVALUATION";
	nystagmus_startdot["message_type"] = 15;
	nystagmus_startdot["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	nystagmus_startdot["osx"] = display.nCoordL.x;
	nystagmus_startdot["odx"] = display.nCoordR.x;
	nystagmus_startdot["osy"] = display.nCoordL.y;
	nystagmus_startdot["ody"] = display.nCoordR.y;
	nystagmus_startdot["name"] = nysp[0].name;
	webComm->sendJSON(nystagmus_startdot);

	for (int n = 0; n < 3; n++)
	{
		if (camFrames[n].size() == 0)
			continue;

		nlohmann::json nystagmus_startperiod;
		nystagmus_startperiod["chartTypeString"] = "NYSTAGMUS_EVALUATION";
		nystagmus_startperiod["message_type"] = 13;
		nystagmus_startperiod["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
		webComm->sendJSON(nystagmus_startperiod);

		for (int i = 0; i < camFrames[n].size(); i++)
		{
//			if (!camFrames[n][i].BP)
			{
				nlohmann::json nystagmus_data;
				nystagmus_data["chartTypeString"] = "NYSTAGMUS_EVALUATION";
				nystagmus_data["message_type"] = MESSAGE_TYPE::DATA_PACKAGE;
				nystagmus_data["h_os"] = camFrames[n][i].etd[1].tandemGlints.bpG.x;
				nystagmus_data["h_od"] = camFrames[n][i].etd[0].tandemGlints.bpG.x;
				nystagmus_data["v_os"] = camFrames[n][i].etd[1].tandemGlints.bpG.y;
				nystagmus_data["v_od"] = camFrames[n][i].etd[0].tandemGlints.bpG.y;
				nystagmus_data["h_os_pos"] = camFrames[n][i].etd[1].threeDangle.vsPixel.x;
				nystagmus_data["h_od_pos"] = camFrames[n][i].etd[0].threeDangle.vsPixel.x;
				nystagmus_data["v_os_pos"] = camFrames[n][i].etd[1].threeDangle.vsPixel.y;
				nystagmus_data["v_od_pos"] = camFrames[n][i].etd[0].threeDangle.vsPixel.y;
				nystagmus_data["t_os"] = 0;
				nystagmus_data["t_od"] = 0;
				nystagmus_data["darkFrame"] = !camFrames[n][i].BP;
				nystagmus_data["timestamp"] = camFrames[n][i].timestamp;
				nystagmus_data["secs"] = camFrames[n][i].secs;
				webComm->sendJSON(nystagmus_data);
				if (n == 0&&!camFrames[n][i].BP)
					vw.write(camFrames[n][i].pic);
			}
		}

		nlohmann::json nystagmus_stopperiod;
		nystagmus_stopperiod["chartTypeString"] = "NYSTAGMUS_EVALUATION";
		nystagmus_stopperiod["message_type"] = 14;
		nystagmus_stopperiod["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
		webComm->sendJSON(nystagmus_stopperiod);

		camFrames[n].clear();
	}
	vw.release();

	nlohmann::json nystagmus_stopdot;
	nystagmus_stopdot["chartTypeString"] = "NYSTAGMUS_EVALUATION";
	nystagmus_stopdot["message_type"] = 16;
	nystagmus_stopdot["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	nystagmus_stopdot["degtopxHOS"] = lcDist(leftCalOS, rightCalOS) / 8.;
	nystagmus_stopdot["degtopxHOD"] = lcDist(leftCalOD, rightCalOD) / 8.;
	nystagmus_stopdot["degtopxVOS"] = lcDist(upCalOS, downCalOS) / 8.;
	nystagmus_stopdot["degtopxVOD"] = lcDist(upCalOD, downCalOD) / 8.;
	nystagmus_stopdot["filename"] = filenameshort;
	nystagmus_stopdot["originalfilename"] = filenameshortrs;
	webComm->sendJSON(nystagmus_stopdot);

	nysp.erase(nysp.begin());
	animalNum++;
	display.nAnilmalNum = animalNum;

}

void CNystagmusTest::ProcessETData(vector<ETData> etData, Mat frame, bool bp)
{
	double testTime = glfwGetTime();
	if (nystagmusState == 0)
	{
		if (testTime > 1)
		{
			glfwSetTime(0);
			nystagmusState = 1;
			display.nystagmusType = 1;
			display.redrawPending = true;
		}
	}
	else if (nystagmusState == 1)
	{
		if (testTime < nys_duration[nys_stages])
		{
			if (testTime < nys_duration[0])
			{
				nCamFrame tmp;
				tmp.pic = frame;
				tmp.BP = bp;
				tmp.etd[0] = etData[0];
				tmp.etd[1] = etData[1];
				tmp.timestamp = std::chrono::system_clock::now().time_since_epoch().count();
				tmp.secs = testTime;
				camFrames[0].push_back(tmp);
			}
			else if (testTime >= nys_duration[0] && testTime < nys_duration[1])
			{
				nCamFrame tmp;
				tmp.etd[0] = etData[0];
				tmp.etd[1] = etData[1];
				tmp.BP = bp;
				tmp.timestamp = std::chrono::system_clock::now().time_since_epoch().count();
				tmp.secs = testTime;
				camFrames[1].push_back(tmp);
			}
			else if (testTime >= nys_duration[1] && testTime < nys_duration[2])
			{
				nCamFrame tmp;
				tmp.etd[0] = etData[0];
				tmp.etd[1] = etData[1];
				tmp.BP = bp;
				tmp.secs = testTime;
				tmp.timestamp = std::chrono::system_clock::now().time_since_epoch().count();
				camFrames[2].push_back(tmp);
			}
		}
		else
		{
			glfwSetTime(0);
			nystagmusState = 2;
			display.nystagmusType = 0;
			display.redrawPending = true;
		}
	}
	else if (nystagmusState == 2)
	{
		glfwSetTime(0);
		if (!calibrated)
		{
			display.nCoordL = lensDist->mmtopix(lensDist->posfromanglOS(Point2d(-4, 0)));
			display.nCoordL.x = 1920 - display.nCoordL.x;
			cout << display.nCoordL.x << "  " << display.nCoordL.y << endl;
			display.nShowL = true;
			display.nShowR = false;
			calNum = 0;
			nystagmusState = 3;
		}
		else
		{
			writeData();
			if (nysp.size() > 0)
			{
				numdot++;
				glfwSetTime(0);
				display.nCoordL.x = nysp[0].os.x;
				display.nCoordR.x = nysp[0].od.x;
				display.nCoordL.y = nysp[0].os.y;
				display.nCoordR.y = nysp[0].od.y;
				display.nTargetType = nysp[0].nType;
				display.nShowL = nysp[0].showL;
				display.nShowR = nysp[0].showR;
				display.nysLarge = nysp[0].large;
				nystagmusState = 0;
				display.nystagmusType = 0;
				display.redrawPending = true;
			}
			else
			{
				nlohmann::json nystagmus_stop;
				nystagmus_stop["chartTypeString"] = "NYSTAGMUS_EVALUATION";
				nystagmus_stop["message_type"] = MESSAGE_TYPE::STOP_TEST;
				nystagmus_stop["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
				webComm->sendJSON(nystagmus_stop);
				encodeVideos();
				display.setDragonfly();
				currentTest = nullptr;

				eyetrackingOn = false;
			}
		}
		display.redrawPending = true;
	}
	else if (nystagmusState == 3)
	{
		if (testTime > 0.5 && testTime < 2 && !calibrated && etData[0].tandemGlints.bpG.x>0 && etData[1].tandemGlints.bpG.x > 0)
		{
			leftCalOS += (Point2d)(etData[0].tandemGlints.bpG + etData[1].tandemGlints.bpG) / 2.;
			calNum++;
		}
		if (testTime > 2) 
		{
			leftCalOS /= calNum;
			calNum = 0;
			display.nCoordL = lensDist->mmtopix(lensDist->posfromanglOS(Point2d(4, 0)));
			display.nCoordL.x = 1920 - display.nCoordL.x;
			cout << display.nCoordL.x << "  " << display.nCoordL.y << endl;
			display.nShowL = true;
			display.nShowR = false;
			glfwSetTime(0);
			nystagmusState = 4;
			display.redrawPending = true;
		}
	}
	else if (nystagmusState == 4)
	{
		if (testTime > 0.5 && testTime < 2 && !calibrated && etData[0].tandemGlints.bpG.x > 0 && etData[1].tandemGlints.bpG.x > 0)
		{
			rightCalOS += (Point2d)(etData[0].tandemGlints.bpG + etData[1].tandemGlints.bpG) / 2.;
			calNum++;
		}
		if (testTime > 2)
		{
			rightCalOS /= calNum;
			calNum = 0;
			display.nCoordL = lensDist->mmtopix(lensDist->posfromanglOS(Point2d(0, 4)));
			display.nCoordL.x = 1920 - display.nCoordL.x;
			cout << display.nCoordL.x << "  " << display.nCoordL.y << endl;
			display.nShowL = true;
			display.nShowR = false;
			glfwSetTime(0);
			nystagmusState = 5;
			display.redrawPending = true;
		}

	}
	else if (nystagmusState == 5)
	{
		if (testTime > 0.5 && testTime < 2 && !calibrated && etData[0].tandemGlints.bpG.x > 0 && etData[1].tandemGlints.bpG.x > 0)
		{
			upCalOS += (Point2d)(etData[0].tandemGlints.bpG + etData[1].tandemGlints.bpG) / 2.;
			calNum++;
		}
		if (testTime > 2)
		{
			upCalOS /= calNum;
			calNum = 0;
			display.nCoordL = lensDist->mmtopix(lensDist->posfromanglOS(Point2d(0, -4)));
			display.nCoordL.x = 1920 - display.nCoordL.x;
			cout << display.nCoordL.x << "  " << display.nCoordL.y << endl;
			display.nShowL = true;
			display.nShowR = false;
			glfwSetTime(0);
			nystagmusState = 6;
			display.redrawPending = true;
		}
	}
	else if (nystagmusState == 6)
	{
		if (testTime > 0.5 && testTime < 2 && !calibrated && etData[0].tandemGlints.bpG.x > 0 && etData[1].tandemGlints.bpG.x > 0)
		{
			downCalOS += (Point2d)(etData[0].tandemGlints.bpG + etData[1].tandemGlints.bpG) / 2.;
			calNum++;
		}
		if (testTime > 2)
		{
			downCalOS /= calNum;
			calNum = 0;
			display.nCoordR = lensDist->mmtopix(lensDist->posfromanglOD(Point2d(4, 0)));
			display.nCoordR.x = 3840 - display.nCoordR.x;
			cout << display.nCoordR.x << "  " << display.nCoordR.y << endl;
			display.nShowL = false;
			display.nShowR = true;
			glfwSetTime(0);
			nystagmusState = 7;
			display.redrawPending = true;
		}
	}
	else if (nystagmusState == 7)
	{
		if (testTime > 0.5 && testTime < 2 && !calibrated && etData[0].tandemGlints.bpG.x > 0 && etData[1].tandemGlints.bpG.x > 0)
		{
			rightCalOD += (Point2d)(etData[0].tandemGlints.bpG + etData[1].tandemGlints.bpG) / 2.;
			calNum++;
		}
		if (testTime > 2)
		{
			rightCalOD /= calNum;
			calNum = 0;
			display.nCoordR = lensDist->mmtopix(lensDist->posfromanglOD(Point2d(-4, 0)));
			display.nCoordR.x = 3840 - display.nCoordR.x;
			cout << display.nCoordR.x << "  " << display.nCoordR.y << endl;
			display.nShowL = false;
			display.nShowR = true;
			glfwSetTime(0);
			nystagmusState = 8;
			display.redrawPending = true;
		}

	}
	else if (nystagmusState == 8)
	{
		if (testTime > 0.5 && testTime < 2 && !calibrated && etData[0].tandemGlints.bpG.x > 0 && etData[1].tandemGlints.bpG.x > 0)
		{
			leftCalOD += (Point2d)(etData[0].tandemGlints.bpG + etData[1].tandemGlints.bpG) / 2.;
			calNum++;
		}
		if (testTime > 2)
		{
			leftCalOD /= calNum;
			calNum = 0;
			display.nCoordR = lensDist->mmtopix(lensDist->posfromanglOD(Point2d(0, 4)));
			display.nCoordR.x = 3840 - display.nCoordR.x;
			cout << display.nCoordR.x << "  " << display.nCoordR.y << endl;
			display.nShowL = false;
			display.nShowR = true;
			glfwSetTime(0);
			nystagmusState = 9;
			display.redrawPending = true;
		}
	}
	else if (nystagmusState == 9)
	{
		if (testTime > 0.5 && testTime < 2 && !calibrated && etData[0].tandemGlints.bpG.x > 0 && etData[1].tandemGlints.bpG.x > 0)
		{
			upCalOD += (Point2d)(etData[0].tandemGlints.bpG + etData[1].tandemGlints.bpG) / 2.;
			calNum++;
		}
		if (testTime > 2)
		{
			upCalOD /= calNum;
			calNum = 0;
			display.nCoordR = lensDist->mmtopix(lensDist->posfromanglOD(Point2d(0, -4)));
			display.nCoordR.x = 3840 - display.nCoordR.x;
			cout << display.nCoordR.x << "  " << display.nCoordR.y << endl;
			display.nShowL = false;
			display.nShowR = true;
			glfwSetTime(0);
			nystagmusState = 10;
			display.redrawPending = true;
		}
	}
	else if (nystagmusState == 10)
	{
		if (testTime > 0.5 && testTime < 2 && !calibrated && etData[0].tandemGlints.bpG.x > 0 && etData[1].tandemGlints.bpG.x > 0)
		{
			downCalOD += (Point2d)(etData[0].tandemGlints.bpG + etData[1].tandemGlints.bpG) / 2.;
			calNum++;
		}
		if (testTime > 2)
		{
			if (!calibrated)
			{
				calibrated = true;
				downCalOD /= calNum;
			}
			writeData();
			if (nysp.size() > 0)
			{
				numdot++;

				glfwSetTime(0);
				display.nCoordL.x = nysp[0].os.x;
				display.nCoordR.x = nysp[0].od.x;
				display.nCoordL.y = nysp[0].os.y;
				display.nCoordR.y = nysp[0].od.y;
				display.nTargetType = nysp[0].nType;
				display.nShowL = nysp[0].showL;
				display.nShowR = nysp[0].showR;
				display.nysLarge = nysp[0].large;
				nystagmusState = 0;
				display.nystagmusType = 0;
				display.redrawPending = true;
			}
			else
			{
				nlohmann::json nystagmus_stop;
				nystagmus_stop["chartTypeString"] = "NYSTAGMUS_EVALUATION";
				nystagmus_stop["message_type"] = MESSAGE_TYPE::STOP_TEST;
				nystagmus_stop["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
				webComm->sendJSON(nystagmus_stop);
				encodeVideos();
				display.setDragonfly();
				currentTest = nullptr;
				qualityLog.close();
				eyetrackingOn = false;
			}
		}
	}
}