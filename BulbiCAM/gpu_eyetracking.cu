﻿#pragma once 

/*
	Author:		VLV
	Info:		GPU eyetracking module declarations
	Version:	2.1.220501
	Updates:	01.05.2022
				- Contrasts for Functional screening
				- Separate algorithms for dilated pupil tracking
				- Refactoring
*/

#include "gpu_eyetracking.h"

using namespace cv;
using namespace std;

Point2f calib_perp[2] = { Point2f(1437, 540), Point2f(483, 540) };
Point2f led_perp[2] = { Point2f(1475.9, 107.28), Point2f(444.36, 107.28) };
Size2f led_to_calib[2] = { Size2f(2.45, -27.27), Size2f(-2.43, -27.27) };
float anglesOfShapes[2] = { 3.5, 4.5 };
float led_to_rot_z = 197.8;
float dx[2] = { 0,0 }, dy[2] = { 0,0 }, ds[2] = { 0,0 };
uint64_t filled_untill = 0;
deque<float> cp[RAY_TRACES + 1][2];	//Huge deque size bug


float precalculatedStorage1[ARRAYMULTIPLIER * COUPLES * 2 * 2];
float precalculatedStorage2[2 * ARRAYMULTIPLIER * ADD_COUPLES * 2 * 2];

__host__ AllocatedGPUmemorySpace allocateGPUmemorySpace()
{
	AllocatedGPUmemorySpace ret;

	int storage1size = ARRAYMULTIPLIER * COUPLES * 2 * 2;
	int storage2size = 2 * ARRAYMULTIPLIER * ADD_COUPLES * 2 * 2;

	getPointsToStorage1();
	getPointsToStorage2();

	cudaMalloc(&(ret.deviceStorage1), storage1size * sizeof(float));
	cudaMalloc(&(ret.deviceStorage2), storage2size * sizeof(float));

	cudaMemcpy(ret.deviceStorage1, precalculatedStorage1, storage1size * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(ret.deviceStorage2, precalculatedStorage2, storage2size * sizeof(float), cudaMemcpyHostToDevice);

	cudaMalloc(&ret.gpuAllocatedPupils, DILATED_ROUND_0_THREADS * sizeof(Pupil));
	cudaMalloc(&ret.gpuAllocatedImage, FRAME_WIDTH / 2 * FRAME_HEIGHT * sizeof(uchar));

	return ret;
}
__host__ void getPointsToStorage1()
{
	int arraypos = 0;

	for (float rad = MINRAD; rad < MAXRAD + 1; rad += 0.125)
	{
		int carriage = 0;

		for (int angle = 0; angle < 360; angle += 360 / COUPLES)
		{
			float x_in = (rad - COUPLE_DIST) * cos(angle * CV_PI / 180.0);
			float y_in = (rad - COUPLE_DIST) * sin(angle * CV_PI / 180.0);

			float x_out = (rad + COUPLE_DIST) * cos(angle * CV_PI / 180.0);
			float y_out = (rad + COUPLE_DIST) * sin(angle * CV_PI / 180.0);

			//cout << "STOR1 --- " << rad << "--" << angle << "--" << arraypos * (COUPLES * 4) + carriage * 4 << endl;

			precalculatedStorage1[arraypos * (COUPLES * 4) + carriage * 4 + 0] = x_in;
			precalculatedStorage1[arraypos * (COUPLES * 4) + carriage * 4 + 1] = y_in;
			precalculatedStorage1[arraypos * (COUPLES * 4) + carriage * 4 + 2] = x_out;
			precalculatedStorage1[arraypos * (COUPLES * 4) + carriage * 4 + 3] = y_out;

			++carriage;
		}
		++arraypos;
	}
}
__host__ void getPointsToStorage2()
{
	vector<int> angles[2] = { {260,250,100,110}, {280,290,80,70} };

	for (int curROInum = 0; curROInum <= 1; curROInum++)
	{
		int arraypos = 0;

		for (float rad = MINRAD; rad < MAXRAD + 1; rad += 0.125)
		{
			int carriage = 0;

			for (const int& angle: angles[curROInum])
			{
				float x_in = (rad - COUPLE_DIST) * cos(angle * CV_PI / 180.0);
				float y_in = (rad - COUPLE_DIST) * sin(angle * CV_PI / 180.0);

				float x_out = (rad + COUPLE_DIST) * cos(angle * CV_PI / 180.0);
				float y_out = (rad + COUPLE_DIST) * sin(angle * CV_PI / 180.0);

				//cout << "STOR2 --- " << rad << "--" << angle << "--" << curROInum * (ARRAYMULTIPLIER * (ADD_COUPLES * 4)) + arraypos * (ADD_COUPLES * 4) + carriage * 4 << endl;

				precalculatedStorage2[curROInum * (ARRAYMULTIPLIER * (ADD_COUPLES * 4)) + arraypos * (ADD_COUPLES * 4) + carriage * 4 + 0] = x_in;
				precalculatedStorage2[curROInum * (ARRAYMULTIPLIER * (ADD_COUPLES * 4)) + arraypos * (ADD_COUPLES * 4) + carriage * 4 + 1] = y_in;
				precalculatedStorage2[curROInum * (ARRAYMULTIPLIER * (ADD_COUPLES * 4)) + arraypos * (ADD_COUPLES * 4) + carriage * 4 + 2] = x_out;
				precalculatedStorage2[curROInum * (ARRAYMULTIPLIER * (ADD_COUPLES * 4)) + arraypos * (ADD_COUPLES * 4) + carriage * 4 + 3] = y_out;

				++carriage;
			}
			++arraypos;
		}
	}
}

__device__ __host__ bool jumpOverGlint(const TandemGlints& thisTanG, GpuPoint2f& pointIn, GpuPoint2f& pointOut)
{
	bool pointIsIn = false;

	//if (thisTanG.dpgRect.contains(pointOut) || thisTanG.bpgRect.contains(pointOut))
	if (pointOut.x > thisTanG.dpgRect.x && pointOut.y > thisTanG.dpgRect.y &&
		pointOut.x < thisTanG.dpgRect.x + thisTanG.dpgRect.width && pointOut.y < thisTanG.dpgRect.y + thisTanG.dpgRect.height ||
		pointOut.x > thisTanG.bpgRect.x && pointOut.y > thisTanG.bpgRect.y &&
		pointOut.x < thisTanG.bpgRect.x + thisTanG.bpgRect.width && pointOut.y < thisTanG.bpgRect.y + thisTanG.bpgRect.height)
	{
		pointIsIn = true;
		int xdiff = pointIn.x - pointOut.x;		//If xdiff < 0 - east part, else - west part of couples
		int ydiff = pointIn.y - pointOut.y;		//If ydiff < 0 - south part, else - north part of couples

		//if (thisTanG.dpgRect.contains(pointOut))
		if (pointOut.x > thisTanG.dpgRect.x && pointOut.y > thisTanG.dpgRect.y &&
			pointOut.x < thisTanG.dpgRect.x + thisTanG.dpgRect.width && pointOut.y < thisTanG.dpgRect.y + thisTanG.dpgRect.height)
		{
			if (xdiff == 0 || ydiff == 0)		//Vertical or horizontal couples
			{
				if (xdiff == 0)					//Vertical
				{
					if (ydiff < 0)				//South couple
					{
						pointOut.y = thisTanG.dpGraw.y + 8 + 1;	//PointOut is outside of dpgRect now
					}
					else                        //North couple
					{
						pointOut.y = thisTanG.dpGraw.y - 8 - 1;	//PointOut is outside of dpgRect now
					}
				}

				else if (ydiff == 0)			//Horozontal
				{
					if (xdiff < 0)				//East couple
					{
						pointOut.x = thisTanG.dpGraw.x + 8 + 1;	//PointOut is outside of dpgRect now
					}
					else						//West line
					{
						pointOut.x = thisTanG.dpGraw.x - 8 - 1;	//PointOut is outside of dpgRect now;
					}
				}
			}

			else
			{
				if (xdiff < 0 && ydiff < 0)				//East - South couple
				{
					pointOut.x += thisTanG.dpGraw.x + 8 + 1 - pointOut.x;	//PointOut is outside of dpgRect now
					pointOut.y += thisTanG.dpGraw.y + 8 + 1 - pointOut.y;
				}

				else if (xdiff > 0 && ydiff < 0)		//West - South couple
				{
					pointOut.x -= pointOut.x - (thisTanG.dpGraw.x - 8 - 1);	//PointOut is outside of dpgRect now
					pointOut.y += thisTanG.dpGraw.y + 8 + 1 - pointOut.y;
				}

				else if (xdiff < 0 && ydiff > 0)		//East - North couple
				{
					pointOut.x += thisTanG.dpGraw.x + 8 + 1 - pointOut.x;	//PointOut is outside of dpgRect now
					pointOut.y -= pointOut.y - (thisTanG.dpGraw.y - 8 - 1);
				}
				else									//West - North couple
				{
					pointOut.x -= pointOut.x - (thisTanG.dpGraw.x - 8 - 1);	//PointOut is outside of dpgRect now
					pointOut.y -= pointOut.y - (thisTanG.dpGraw.y - 8 - 1);
				}
			}
		}

		//if (thisTanG.bpgRect.contains(pointOut))
		if (pointOut.x > thisTanG.bpgRect.x && pointOut.y > thisTanG.bpgRect.y &&
			pointOut.x < thisTanG.bpgRect.x + thisTanG.bpgRect.width && pointOut.y < thisTanG.bpgRect.y + thisTanG.bpgRect.height)
		{
			if (xdiff == 0 || ydiff == 0)		//Vertical or horizontal couples
			{
				if (xdiff == 0)					//Vertical
				{
					if (ydiff < 0)				//South couple
					{
						pointOut.y = thisTanG.bpGraw.y + 8 + 1;	//PointOut is outside of bpgRect now
					}
					else                        //North couple
					{
						pointOut.y = thisTanG.bpGraw.y - 8 - 1;	//PointOut is outside of bpgRect now
					}
				}

				else if (ydiff == 0)			//Horozontal
				{
					if (xdiff < 0)				//East couple
					{
						pointOut.x = thisTanG.bpGraw.x + 8 + 1;	//PointOut is outside of bpgRect now
					}
					else						//West couple
					{
						pointOut.x = thisTanG.bpGraw.x - 8 - 1;	//PointOut is outside of bpgRect now;
					}
				}
			}

			else
			{
				if (xdiff < 0 && ydiff < 0)				//East - South couple
				{
					pointOut.x += thisTanG.bpGraw.x + 8 + 1 - pointOut.x;	//PointOut is outside of bpgRect now
					pointOut.y += thisTanG.bpGraw.y + 8 + 1 - pointOut.y;
				}

				else if (xdiff > 0 && ydiff < 0)		//West - South couple
				{
					pointOut.x -= pointOut.x - (thisTanG.bpGraw.x - 8 - 1);	//PointOut is outside of bpgRect now
					pointOut.y += thisTanG.bpGraw.y + 8 + 1 - pointOut.y;
				}

				else if (xdiff < 0 && ydiff > 0)		//East - North couple
				{
					pointOut.x += thisTanG.bpGraw.x + 8 + 1 - pointOut.x;	//PointOut is outside of bpgRect now
					pointOut.y -= pointOut.y - (thisTanG.bpGraw.y - 8 - 1);
				}
				else									//West - North couple
				{
					pointOut.x -= pointOut.x - (thisTanG.bpGraw.x - 8 - 1);	//PointOut is outside of bpgRect now
					pointOut.y -= pointOut.y - (thisTanG.bpGraw.y - 8 - 1);
				}
			}
		}

	}

	//if (thisTanG.dpgRect.contains(pointIn) || thisTanG.bpgRect.contains(pointIn))																					//IN point is inside of DPGarea
	if (pointIn.x > thisTanG.dpgRect.x && pointIn.y > thisTanG.dpgRect.y &&
		pointIn.x < thisTanG.dpgRect.x + thisTanG.dpgRect.width && pointIn.y < thisTanG.dpgRect.y + thisTanG.dpgRect.height ||
		pointIn.x > thisTanG.bpgRect.x && pointIn.y > thisTanG.bpgRect.y &&
		pointIn.x < thisTanG.bpgRect.x + thisTanG.bpgRect.width && pointIn.y < thisTanG.bpgRect.y + thisTanG.bpgRect.height)
	{
		pointIsIn = true;
		int xdiff = pointIn.x - pointOut.x;		//If xdiff < 0 - east part, else - west part of couples
		int ydiff = pointIn.y - pointOut.y;		//If ydiff < 0 - south part, else - north part of couples

		//if (thisTanG.dpgRect.contains(pointIn))
		if (pointIn.x > thisTanG.dpgRect.x && pointIn.y > thisTanG.dpgRect.y &&
			pointIn.x < thisTanG.dpgRect.x + thisTanG.dpgRect.width && pointIn.y < thisTanG.dpgRect.y + thisTanG.dpgRect.height)
		{
			if (xdiff == 0 || ydiff == 0)		//Vertical or horizontal couples
			{
				if (xdiff == 0)					//Vertical
				{
					if (ydiff < 0)				//South couple
					{
						pointIn.y = thisTanG.dpGraw.y - 8 - 1;	//PointIn is outside of dpgRect now
					}
					else                        //North couple
					{
						pointIn.y = thisTanG.dpGraw.y + 8 + 1;	//PointIn is outside of dpgRect now
					}
				}

				else if (ydiff == 0)			//Horozontal
				{
					if (xdiff < 0)				//East couple
					{
						pointIn.x = thisTanG.dpGraw.x - 8 - 1;	//PointIn is outside of dpgRect now
					}
					else						//West couple
					{
						pointIn.x = thisTanG.dpGraw.x + 8 + 1;	//PointIn is outside of dpgRect now;
					}
				}
			}

			else
			{
				if (xdiff < 0 && ydiff < 0)				//East - South couple
				{
					pointIn.x -= pointIn.x - (thisTanG.dpGraw.x - 8 - 1);	//PointIn is outside of dpgRect now
					pointIn.y -= pointIn.y - (thisTanG.dpGraw.y - 8 - 1);
				}

				else if (xdiff > 0 && ydiff < 0)		//West - South couple
				{
					pointIn.x += thisTanG.dpGraw.x + 8 + 1 - pointIn.x;		//PointIn is outside of dpgRect now
					pointIn.y -= pointIn.y - (thisTanG.dpGraw.y - 8 - 1);
				}

				else if (xdiff < 0 && ydiff > 0)		//East - North couple
				{
					pointIn.x -= pointIn.x - (thisTanG.dpGraw.x - 8 - 1);	//PointIn is outside of dpgRect now
					pointIn.y += thisTanG.dpGraw.y + 8 + 1 - pointIn.y;
				}
				else									//West - North couple
				{
					pointIn.x += thisTanG.dpGraw.x + 8 + 1 - pointIn.x;		//PointIn is outside of dpgRect now
					pointIn.y += thisTanG.dpGraw.y + 8 + 1 - pointIn.y;
				}
			}
		}

		//if (thisTanG.bpgRect.contains(pointIn))
		if (pointIn.x > thisTanG.bpgRect.x && pointIn.y > thisTanG.bpgRect.y &&
			pointIn.x < thisTanG.bpgRect.x + thisTanG.bpgRect.width && pointIn.y < thisTanG.bpgRect.y + thisTanG.bpgRect.height)
		{
			if (xdiff == 0 || ydiff == 0)		//Vertical or horizontal couples
			{
				if (xdiff == 0)					//Vertical
				{
					if (ydiff < 0)				//South couple
					{
						pointIn.y = thisTanG.bpGraw.y - 8 - 1;	//PointIn is outside of bpgRect now
					}
					else                        //North couple
					{
						pointIn.y = thisTanG.bpGraw.y + 8 + 1;	//PointIn is outside of bpgRect now
					}
				}

				else if (ydiff == 0)			//Horozontal
				{
					if (xdiff < 0)				//East couple
					{
						pointIn.x = thisTanG.bpGraw.x - 8 - 1;	//PointIn is outside of bpgRect now
					}
					else						//West couple
					{
						pointIn.x = thisTanG.bpGraw.x + 8 + 1;	//PointIn is outside of bpgRect now;
					}
				}
			}

			else
			{
				if (xdiff < 0 && ydiff < 0)				//East - South couple
				{
					pointIn.x -= pointIn.x - (thisTanG.bpGraw.x - 8 - 1);	//PointIn is outside of bpgRect now
					pointIn.y -= pointIn.y - (thisTanG.bpGraw.y - 8 - 1);
				}

				else if (xdiff > 0 && ydiff < 0)		//West - South couple
				{
					pointIn.x += thisTanG.bpGraw.x + 8 + 1 - pointIn.x;		//PointIn is outside of bpgRect now
					pointIn.y -= pointIn.y - (thisTanG.bpGraw.y - 8 - 1);
				}

				else if (xdiff < 0 && ydiff > 0)		//East - North couple
				{
					pointIn.x -= pointIn.x - (thisTanG.bpGraw.x - 8 - 1);	//PointIn is outside of bpgRect now
					pointIn.y += thisTanG.bpGraw.y + 8 + 1 - pointIn.y;
				}
				else									//West - North couple
				{
					pointIn.x += thisTanG.bpGraw.x + 8 + 1 - pointIn.x;		//PointIn is outside of bpgRect now
					pointIn.y += thisTanG.bpGraw.y + 8 + 1 - pointIn.y;
				}
			}
		}
	}

	return pointIsIn;
}
__device__ void sortMinToMaxContrast(gpuContrastCouple* arr, int size)
{
	gpuContrastCouple temp;

	for (int i = 0; i < size - 1; i++) {
		for (int j = 0; j < size - i - 1; j++) {
			if (arr[j].contrast > arr[j + 1].contrast) {
				temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
		}
	}
}
__device__ void sortMaxToMinContrast(gpuContrastCouple* arr, int size)
{
	gpuContrastCouple temp;

	for (int i = 0; i < size - 1; i++) {
		for (int j = 0; j < size - i - 1; j++) {
			if (arr[j].contrast < arr[j + 1].contrast) {
				temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
		}
	}
}

__global__ void EyeTrackingRoundMinus1Dilated(AllocatedGPUmemorySpace agms, GpuRect imageArea, GpuRect trackingArea, int curROInum)
{
	int threadNumber = blockIdx.x * blockDim.x + threadIdx.x;
	int coord_x = trackingArea.x + (threadNumber % (DILATED_ROI_WIDTH / 2)) * 2;
	int coord_y = trackingArea.y + (threadNumber / (DILATED_ROI_WIDTH / 2)) * 2;

	if (threadNumber < DILATED_ROUND_0_THREADS)
	{
		agms.gpuAllocatedPupils[threadNumber] = Pupil();

		int min_contrast = INT_MAX;

#ifdef GPUET_SHOW_DEBUG
		int frame_coord = coord_y * imageArea.width + coord_x;
		agms.gpuAllocatedImage[frame_coord] = 255;
#endif

		for (float rad = MAXRAD / 2; rad <= MAXRAD; rad += 1)
		{
			int _contrast = 0;
			int arrayposs = (rad - MINRAD) * 8;
			gpuContrastCouple allCouplesTogether[COUPLES + ADD_COUPLES];

			for (int cpl = 0; cpl < COUPLES; cpl++)
			{
				GpuPoint2f in{ coord_x + agms.deviceStorage1[arrayposs * (COUPLES * 4) + cpl * 4 + 0], coord_y + agms.deviceStorage1[arrayposs * (COUPLES * 4) + cpl * 4 + 1] };
				GpuPoint2f out{ coord_x + agms.deviceStorage1[arrayposs * (COUPLES * 4) + cpl * 4 + 2], coord_y + agms.deviceStorage1[arrayposs * (COUPLES * 4) + cpl * 4 + 3] };

				if (imageArea.isPointOutside(in) || imageArea.isPointOutside(out))
				{
					allCouplesTogether[cpl] = gpuContrastCouple();
				}
				else
				{
					// round to closest int
					allCouplesTogether[cpl].in_x = in.x + 0.5f;
					allCouplesTogether[cpl].in_y = in.y + 0.5f;
					allCouplesTogether[cpl].out_x = out.x + 0.5f;
					allCouplesTogether[cpl].out_y = out.y + 0.5f;

					// color limitation from research
					uchar color_in = agms.gpuAllocatedImage[allCouplesTogether[cpl].in_y * imageArea.width + allCouplesTogether[cpl].in_x];
					uchar color_out = agms.gpuAllocatedImage[allCouplesTogether[cpl].out_y * imageArea.width + allCouplesTogether[cpl].out_x];
					if (color_in <= 70 && color_out >= 30 && color_out <= 140)
					{
						allCouplesTogether[cpl].contrast = color_in - color_out;
					}

#ifdef GPUET_SHOW_DEBUG
					if (threadNumber == 15100)
					{
						agms.gpuAllocatedImage[allCouplesTogether[cpl].out_y * imageArea.width + allCouplesTogether[cpl].out_x] = 255;
					}
#endif

				}
			}

			for (int cpl = 0; cpl < ADD_COUPLES; cpl++)
			{
				uint cplsArrayIndex = COUPLES + cpl;

				GpuPoint2f in{ coord_x + agms.deviceStorage2[curROInum * (ARRAYMULTIPLIER * (ADD_COUPLES * 4)) + arrayposs * (ADD_COUPLES * 4) + cpl * 4 + 0], coord_y + agms.deviceStorage2[curROInum * (ARRAYMULTIPLIER * (ADD_COUPLES * 4)) + arrayposs * (ADD_COUPLES * 4) + cpl * 4 + 1] };
				GpuPoint2f out{ coord_x + agms.deviceStorage2[curROInum * (ARRAYMULTIPLIER * (ADD_COUPLES * 4)) + arrayposs * (ADD_COUPLES * 4) + cpl * 4 + 2], coord_y + agms.deviceStorage2[curROInum * (ARRAYMULTIPLIER * (ADD_COUPLES * 4)) + arrayposs * (ADD_COUPLES * 4) + cpl * 4 + 3] };

				if (imageArea.isPointOutside(in) || imageArea.isPointOutside(out))
				{
					allCouplesTogether[cplsArrayIndex] = gpuContrastCouple();
				}
				else
				{
					//round to closest int
					allCouplesTogether[cplsArrayIndex].in_x = in.x + 0.5f;
					allCouplesTogether[cplsArrayIndex].in_y = in.y + 0.5f;
					allCouplesTogether[cplsArrayIndex].out_x = out.x + 0.5f;
					allCouplesTogether[cplsArrayIndex].out_y = out.y + 0.5f;

					//color limiration from research
					uchar color_in = agms.gpuAllocatedImage[allCouplesTogether[cplsArrayIndex].in_y * imageArea.width + allCouplesTogether[cplsArrayIndex].in_x];
					uchar color_out = agms.gpuAllocatedImage[allCouplesTogether[cplsArrayIndex].out_y * imageArea.width + allCouplesTogether[cplsArrayIndex].out_x];
					if (color_in <= 70 && color_out >= 30 && color_out <= 140)
					{
						allCouplesTogether[cplsArrayIndex].contrast = color_in - color_out;
					}

#ifdef GPUET_SHOW_DEBUG
					if (threadNumber == 15100)
					{
						agms.gpuAllocatedImage[allCouplesTogether[cplsArrayIndex].out_y * imageArea.width + allCouplesTogether[cplsArrayIndex].out_x] = 255;
					}
#endif

				}
			}

			sortMaxToMinContrast(allCouplesTogether, COUPLES + ADD_COUPLES);

			for (int idx = 0; idx < (COUPLES + ADD_COUPLES) / 2 + 1; idx++)
			{
				_contrast += allCouplesTogether[idx].contrast;
			}

			if (_contrast < min_contrast)
			{
				agms.gpuAllocatedPupils[threadNumber].center_x = coord_x;
				agms.gpuAllocatedPupils[threadNumber].center_y = coord_y;
				agms.gpuAllocatedPupils[threadNumber].radius = rad;
				agms.gpuAllocatedPupils[threadNumber].contrast = _contrast;
				min_contrast = _contrast;
			}
		}
	}
}
__global__ void EyeTrackingRound0Dilated(AllocatedGPUmemorySpace agms, GpuRect imageArea, GpuRect trackingArea, float min_rad, float max_rad, int curROInum)
{
	int threadNumber = blockIdx.x * blockDim.x + threadIdx.x;
	int coord_x = trackingArea.x + (threadNumber % (DILATED_ROI_WIDTH / 2)) * 2;
	int coord_y = trackingArea.y + (threadNumber / (DILATED_ROI_WIDTH / 2)) * 2;

	if (threadNumber < DILATED_ROUND_0_THREADS)
	{
		agms.gpuAllocatedPupils[threadNumber] = Pupil();

		int min_contrast = INT_MAX;

#ifdef GPUET_SHOW_DEBUG
		int frame_coord = coord_y * imageArea.width + coord_x;
		agms.gpuAllocatedImage[frame_coord] = 255;
#endif

		for (float rad = min_rad; rad <= max_rad; rad += 1)
		{
			int _contrast = 0;
			int arrayposs = (rad - MINRAD) * 8;
			gpuContrastCouple allCouplesTogether[COUPLES + ADD_COUPLES];

			for (int cpl = 0; cpl < COUPLES; cpl++)
			{
				GpuPoint2f in{ coord_x + agms.deviceStorage1[arrayposs * (COUPLES * 4) + cpl * 4 + 0], coord_y + agms.deviceStorage1[arrayposs * (COUPLES * 4) + cpl * 4 + 1] };
				GpuPoint2f out{ coord_x + agms.deviceStorage1[arrayposs * (COUPLES * 4) + cpl * 4 + 2], coord_y + agms.deviceStorage1[arrayposs * (COUPLES * 4) + cpl * 4 + 3] };

				if (imageArea.isPointOutside(in) || imageArea.isPointOutside(out))
				{
					allCouplesTogether[cpl] = gpuContrastCouple();
				}
				else
				{
					//round to closest int
					allCouplesTogether[cpl].in_x = in.x + 0.5f;
					allCouplesTogether[cpl].in_y = in.y + 0.5f;
					allCouplesTogether[cpl].out_x = out.x + 0.5f;
					allCouplesTogether[cpl].out_y = out.y + 0.5f;

					//color limitation from research
					uchar color_in = agms.gpuAllocatedImage[allCouplesTogether[cpl].in_y * imageArea.width + allCouplesTogether[cpl].in_x];
					uchar color_out = agms.gpuAllocatedImage[allCouplesTogether[cpl].out_y * imageArea.width + allCouplesTogether[cpl].out_x];
					if (color_in <= 70 && color_out >= 30 && color_out <= 140)
					{
						allCouplesTogether[cpl].contrast = color_in - color_out;
					}

#ifdef GPUET_SHOW_DEBUG
					if (threadNumber == 15100)
					{
						agms.gpuAllocatedImage[allCouplesTogether[cpl].out_y * imageArea.width + allCouplesTogether[cpl].out_x] = 255;
					}
#endif

				}
			}

			for (int cpl = 0; cpl < ADD_COUPLES; cpl++)
			{
				uint cplsArrayIndex = COUPLES + cpl;

				GpuPoint2f in{ coord_x + agms.deviceStorage2[curROInum * (ARRAYMULTIPLIER * (ADD_COUPLES * 4)) + arrayposs * (ADD_COUPLES * 4) + cpl * 4 + 0], coord_y + agms.deviceStorage2[curROInum * (ARRAYMULTIPLIER * (ADD_COUPLES * 4)) + arrayposs * (ADD_COUPLES * 4) + cpl * 4 + 1] };
				GpuPoint2f out{ coord_x + agms.deviceStorage2[curROInum * (ARRAYMULTIPLIER * (ADD_COUPLES * 4)) + arrayposs * (ADD_COUPLES * 4) + cpl * 4 + 2], coord_y + agms.deviceStorage2[curROInum * (ARRAYMULTIPLIER * (ADD_COUPLES * 4)) + arrayposs * (ADD_COUPLES * 4) + cpl * 4 + 3] };

				if (imageArea.isPointOutside(in) || imageArea.isPointOutside(out))
				{
					allCouplesTogether[cplsArrayIndex] = gpuContrastCouple();
				}
				else
				{
					//round to closest int
					allCouplesTogether[cplsArrayIndex].in_x = in.x + 0.5f;
					allCouplesTogether[cplsArrayIndex].in_y = in.y + 0.5f;
					allCouplesTogether[cplsArrayIndex].out_x = out.x + 0.5f;
					allCouplesTogether[cplsArrayIndex].out_y = out.y + 0.5f;

					//color limiration from research
					uchar color_in = agms.gpuAllocatedImage[allCouplesTogether[cplsArrayIndex].in_y * imageArea.width + allCouplesTogether[cplsArrayIndex].in_x];
					uchar color_out = agms.gpuAllocatedImage[allCouplesTogether[cplsArrayIndex].out_y * imageArea.width + allCouplesTogether[cplsArrayIndex].out_x];
					if (color_in <= 70 && color_out >= 30 && color_out <= 140)
					{
						allCouplesTogether[cplsArrayIndex].contrast = color_in - color_out;
					}

#ifdef GPUET_SHOW_DEBUG
					if (threadNumber == 15100)
					{
						agms.gpuAllocatedImage[allCouplesTogether[cplsArrayIndex].out_y * imageArea.width + allCouplesTogether[cplsArrayIndex].out_x] = 255;
					}
#endif

				}
			}

			sortMaxToMinContrast(allCouplesTogether, COUPLES + ADD_COUPLES);

			for (int idx = 0; idx < (COUPLES + ADD_COUPLES) / 2 + 1; idx++)
			{
				_contrast += allCouplesTogether[idx].contrast;
			}

			if (_contrast < min_contrast)
			{
				agms.gpuAllocatedPupils[threadNumber].center_x = coord_x;
				agms.gpuAllocatedPupils[threadNumber].center_y = coord_y;
				agms.gpuAllocatedPupils[threadNumber].radius = rad;
				agms.gpuAllocatedPupils[threadNumber].contrast = _contrast;
				min_contrast = _contrast;
			}
		}
	}
}
__global__ void EyeTrackingRound1Dilated(AllocatedGPUmemorySpace agms, GpuRect imageArea, GpuRect trackingArea, Pupil round0_winner, int curROInum)
{
	int threadNumber = blockIdx.x * blockDim.x + threadIdx.x;
	int steps_within_pixel = 10;
	int square_side = 7;
	int required_threads = square_side * square_side * steps_within_pixel * steps_within_pixel;

	if (threadNumber < required_threads)
	{
		float min_rad = round0_winner.radius - 1 > MINRAD ? round0_winner.radius - 1 : MINRAD;
		float max_rad = round0_winner.radius + 1 < MAXRAD ? round0_winner.radius + 1 : MAXRAD;

		float coord_x = imageArea.x + (round0_winner.center_x - square_side / 2) + (threadNumber % (square_side * steps_within_pixel) * (1.0 / steps_within_pixel));
		float coord_y = imageArea.y + (round0_winner.center_y - square_side / 2) + (threadNumber / (square_side * steps_within_pixel) * (1.0 / steps_within_pixel));

#ifdef GPUET_SHOW_DEBUG
		int frame_coord = (int)(coord_y + 0.5f) * imageArea.width + (int)(coord_x + 0.5f);
		agms.gpuAllocatedImage[frame_coord] = 255;
#endif

		agms.gpuAllocatedPupils[threadNumber] = Pupil();
		int min_contrast = INT_MAX;

		for (float rad = min_rad; rad <= max_rad; rad += 0.125)
		{
			int _contrast = 0;
			int arrayposs = (rad - MINRAD) * 8;
			gpuContrastCouple allCouplesTogether[COUPLES + ADD_COUPLES];

			for (int cpl = 0; cpl < COUPLES; cpl++)
			{
				GpuPoint2f in{ coord_x + agms.deviceStorage1[arrayposs * (COUPLES * 4) + cpl * 4 + 0], coord_y + agms.deviceStorage1[arrayposs * (COUPLES * 4) + cpl * 4 + 1] };
				GpuPoint2f out{ coord_x + agms.deviceStorage1[arrayposs * (COUPLES * 4) + cpl * 4 + 2], coord_y + agms.deviceStorage1[arrayposs * (COUPLES * 4) + cpl * 4 + 3] };

				if (imageArea.isPointOutside(in) || imageArea.isPointOutside(out))
				{
					allCouplesTogether[cpl] = gpuContrastCouple();
				}
				else
				{
					//round to closest int
					allCouplesTogether[cpl].in_x = in.x + 0.5f;
					allCouplesTogether[cpl].in_y = in.y + 0.5f;
					allCouplesTogether[cpl].out_x = out.x + 0.5f;
					allCouplesTogether[cpl].out_y = out.y + 0.5f;

					//color limiration from research
					uchar color_in = agms.gpuAllocatedImage[allCouplesTogether[cpl].in_y * imageArea.width + allCouplesTogether[cpl].in_x];
					uchar color_out = agms.gpuAllocatedImage[allCouplesTogether[cpl].out_y * imageArea.width + allCouplesTogether[cpl].out_x];
					if (color_in <= 70 && color_out >= 30 && color_out <= 140)
					{
						allCouplesTogether[cpl].contrast = color_in - color_out;
					}
				}
			}

			for (int cpl = 0; cpl < ADD_COUPLES; cpl++)
			{
				uint cplsArrayIndex = COUPLES + cpl;

				//prec_stor2 is 1-dimensional array of X, Y points data arranged in a certain order
				GpuPoint2f in{ coord_x + agms.deviceStorage2[curROInum * (ARRAYMULTIPLIER * (ADD_COUPLES * 4)) + arrayposs * (ADD_COUPLES * 4) + cpl * 4 + 0], coord_y + agms.deviceStorage2[curROInum * (ARRAYMULTIPLIER * (ADD_COUPLES * 4)) + arrayposs * (ADD_COUPLES * 4) + cpl * 4 + 1] };
				GpuPoint2f out{ coord_x + agms.deviceStorage2[curROInum * (ARRAYMULTIPLIER * (ADD_COUPLES * 4)) + arrayposs * (ADD_COUPLES * 4) + cpl * 4 + 2], coord_y + agms.deviceStorage2[curROInum * (ARRAYMULTIPLIER * (ADD_COUPLES * 4)) + arrayposs * (ADD_COUPLES * 4) + cpl * 4 + 3] };

				if (imageArea.isPointOutside(in) || imageArea.isPointOutside(out))
				{
					allCouplesTogether[cplsArrayIndex] = gpuContrastCouple();
				}
				else
				{
					//round to closest int
					allCouplesTogether[cplsArrayIndex].in_x = in.x + 0.5f;
					allCouplesTogether[cplsArrayIndex].in_y = in.y + 0.5f;
					allCouplesTogether[cplsArrayIndex].out_x = out.x + 0.5f;
					allCouplesTogether[cplsArrayIndex].out_y = out.y + 0.5f;

					//color limiration from research
					uchar color_in = agms.gpuAllocatedImage[allCouplesTogether[cplsArrayIndex].in_y * imageArea.width + allCouplesTogether[cplsArrayIndex].in_x];
					uchar color_out = agms.gpuAllocatedImage[allCouplesTogether[cplsArrayIndex].out_y * imageArea.width + allCouplesTogether[cplsArrayIndex].out_x];
					if (color_in <= 70 && color_out >= 30 && color_out <= 140)
					{
						allCouplesTogether[cplsArrayIndex].contrast = color_in - color_out;
					}					
				}
			}

			sortMaxToMinContrast(allCouplesTogether, COUPLES + ADD_COUPLES);

			for (int idx = 0; idx < (COUPLES + ADD_COUPLES) / 2 + 1; idx++)
			{
				_contrast += allCouplesTogether[idx].contrast;
			}

			if (_contrast < min_contrast)
			{
				agms.gpuAllocatedPupils[threadNumber].center_x = coord_x;
				agms.gpuAllocatedPupils[threadNumber].center_y = coord_y;
				agms.gpuAllocatedPupils[threadNumber].radius = rad;
				agms.gpuAllocatedPupils[threadNumber].contrast = _contrast;
				min_contrast = _contrast;
			}
		}
	}
}
__global__ void EyeTrackingRound0(AllocatedGPUmemorySpace agms, GpuRect imageArea, GpuRect trackingArea, TandemGlints thisTang, int curROInum)
{
	int threadNumber = blockIdx.x * blockDim.x + threadIdx.x;
	int coord_x = trackingArea.x + (threadNumber % (trackingArea.width / 3)) * 3;
	int coord_y = trackingArea.y + (threadNumber / (trackingArea.width / 3)) * 3;

	if (threadNumber < CLASSIC_ROUND_0_THREADS)
	{
		agms.gpuAllocatedPupils[threadNumber] = Pupil();

#ifdef GPUET_SHOW_DEBUG
		int frame_coord = coord_y * imageArea.width + coord_x;
		agms.gpuAllocatedImage[frame_coord] = 255;
#endif

		int max_contrast = INT_MIN;

		for (int rad = MINRAD; rad <= MAXRAD; rad += 2)
		{
			int _contrast = 0;
			int arrayposs = (rad - MINRAD) * 8;
			gpuContrastCouple general[COUPLES];
			gpuContrastCouple additional[ADD_COUPLES];

			for (int cpl = 0; cpl < COUPLES; cpl++)
			{
				GpuPoint2f in{ coord_x + agms.deviceStorage1[arrayposs * (COUPLES * 4) + cpl * 4 + 0], coord_y + agms.deviceStorage1[arrayposs * (COUPLES * 4) + cpl * 4 + 1] };
				GpuPoint2f out{ coord_x + agms.deviceStorage1[arrayposs * (COUPLES * 4) + cpl * 4 + 2], coord_y + agms.deviceStorage1[arrayposs * (COUPLES * 4) + cpl * 4 + 3] };

				if (imageArea.isPointOutside(in) || imageArea.isPointOutside(out))
				{
					general[cpl] = gpuContrastCouple();
				}
				else
				{
					general[cpl].jumped = jumpOverGlint(thisTang, in, out);

					//round to closest int
					general[cpl].in_x = in.x + 0.5f;
					general[cpl].in_y = in.y + 0.5f;
					general[cpl].out_x = out.x + 0.5f;
					general[cpl].out_y = out.y + 0.5f;
					general[cpl].contrast = agms.gpuAllocatedImage[general[cpl].in_y * imageArea.width + general[cpl].in_x] - agms.gpuAllocatedImage[general[cpl].out_y * imageArea.width + general[cpl].out_x];
				}
			}

			for (int cpl = 0; cpl < ADD_COUPLES; cpl++)
			{
				GpuPoint2f in{ coord_x + agms.deviceStorage2[curROInum * (ARRAYMULTIPLIER * (ADD_COUPLES * 4)) + arrayposs * (ADD_COUPLES * 4) + cpl * 4 + 0], coord_y + agms.deviceStorage2[curROInum * (ARRAYMULTIPLIER * (ADD_COUPLES * 4)) + arrayposs * (ADD_COUPLES * 4) + cpl * 4 + 1] };
				GpuPoint2f out{ coord_x + agms.deviceStorage2[curROInum * (ARRAYMULTIPLIER * (ADD_COUPLES * 4)) + arrayposs * (ADD_COUPLES * 4) + cpl * 4 + 2], coord_y + agms.deviceStorage2[curROInum * (ARRAYMULTIPLIER * (ADD_COUPLES * 4)) + arrayposs * (ADD_COUPLES * 4) + cpl * 4 + 3] };

				if (imageArea.isPointOutside(in) || imageArea.isPointOutside(out))
				{
					additional[cpl] = gpuContrastCouple();
				}
				else
				{
					additional[cpl].jumped = jumpOverGlint(thisTang, in, out);

					//round to closest int
					additional[cpl].in_x = in.x + 0.5f;
					additional[cpl].in_y = in.y + 0.5f;
					additional[cpl].out_x = out.x + 0.5f;
					additional[cpl].out_y = out.y + 0.5f;
					additional[cpl].contrast = agms.gpuAllocatedImage[additional[cpl].in_y * imageArea.width + additional[cpl].in_x] - agms.gpuAllocatedImage[additional[cpl].out_y * imageArea.width + additional[cpl].out_x];

					_contrast += additional[cpl].contrast;
				}
			}

			sortMinToMaxContrast(general, COUPLES);

			for (int idx = 0; idx < COUPLES / 2 + 1; idx++)
			{
				_contrast += general[idx].contrast;
			}

			if (_contrast > max_contrast)
			{
				agms.gpuAllocatedPupils[threadNumber].center_x = coord_x;
				agms.gpuAllocatedPupils[threadNumber].center_y = coord_y;
				agms.gpuAllocatedPupils[threadNumber].radius = rad;
				agms.gpuAllocatedPupils[threadNumber].contrast = _contrast;
				max_contrast = _contrast;
			}
		}
	}
}
__global__ void EyeTrackingRound1(AllocatedGPUmemorySpace agms, GpuRect imageArea, GpuRect trackingArea, TandemGlints thisTang, float min_rad, float max_rad, int curROInum)
{
	int threadNumber = blockIdx.x * blockDim.x + threadIdx.x;
	int steps_within_pixel = 10;
	int square_side = 9;

	if (threadNumber < ((square_side * square_side) * (steps_within_pixel * steps_within_pixel)))
	{
		float coord_x = trackingArea.x + (20 - square_side / 2) + (threadNumber % (square_side * steps_within_pixel) * (1.0 / steps_within_pixel));
		float coord_y = trackingArea.y + (20 - square_side / 2) + (threadNumber / (square_side * steps_within_pixel) * (1.0 / steps_within_pixel));

#ifdef GPUET_SHOW_DEBUG
		int frame_coord = (int)(coord_y + 0.5f) * imageArea.width + (int)(coord_x + 0.5f);
		agms.gpuAllocatedImage[frame_coord] = 255;
#endif

		int max_contrast = INT_MIN;

		for (float rad = min_rad; rad <= max_rad; rad += 0.125)
		{
			int _contrast = 0;
			int arrayposs = (rad - MINRAD) * 8;
			gpuContrastCouple general[COUPLES];
			gpuContrastCouple additional[ADD_COUPLES];

			for (int cpl = 0; cpl < COUPLES; cpl++)
			{
				GpuPoint2f in{ coord_x + agms.deviceStorage1[arrayposs * (COUPLES * 4) + cpl * 4 + 0], coord_y + agms.deviceStorage1[arrayposs * (COUPLES * 4) + cpl * 4 + 1] };
				GpuPoint2f out{ coord_x + agms.deviceStorage1[arrayposs * (COUPLES * 4) + cpl * 4 + 2], coord_y + agms.deviceStorage1[arrayposs * (COUPLES * 4) + cpl * 4 + 3] };

				if (imageArea.isPointOutside(in) || imageArea.isPointOutside(out))
				{
					general[cpl] = gpuContrastCouple();
				}
				else
				{
					general[cpl].jumped = jumpOverGlint(thisTang, in, out);

					//round to closest int
					general[cpl].in_x = in.x + 0.5f;
					general[cpl].in_y = in.y + 0.5f;
					general[cpl].out_x = out.x + 0.5f;
					general[cpl].out_y = out.y + 0.5f;
					general[cpl].contrast = agms.gpuAllocatedImage[general[cpl].in_y * imageArea.width + general[cpl].in_x] - agms.gpuAllocatedImage[general[cpl].out_y * imageArea.width + general[cpl].out_x];
				}
			}

			for (int cpl = 0; cpl < ADD_COUPLES; cpl++)
			{
				GpuPoint2f in{ coord_x + agms.deviceStorage2[curROInum * (ARRAYMULTIPLIER * (ADD_COUPLES * 4)) + arrayposs * (ADD_COUPLES * 4) + cpl * 4 + 0], coord_y + agms.deviceStorage2[curROInum * (ARRAYMULTIPLIER * (ADD_COUPLES * 4)) + arrayposs * (ADD_COUPLES * 4) + cpl * 4 + 1] };
				GpuPoint2f out{ coord_x + agms.deviceStorage2[curROInum * (ARRAYMULTIPLIER * (ADD_COUPLES * 4)) + arrayposs * (ADD_COUPLES * 4) + cpl * 4 + 2], coord_y + agms.deviceStorage2[curROInum * (ARRAYMULTIPLIER * (ADD_COUPLES * 4)) + arrayposs * (ADD_COUPLES * 4) + cpl * 4 + 3] };

				if (imageArea.isPointOutside(in) || imageArea.isPointOutside(out))
				{
					additional[cpl] = gpuContrastCouple();
				}
				else
				{
					additional[cpl].jumped = jumpOverGlint(thisTang, in, out);

					//round to closest int
					additional[cpl].in_x = in.x + 0.5f;
					additional[cpl].in_y = in.y + 0.5f;
					additional[cpl].out_x = out.x + 0.5f;
					additional[cpl].out_y = out.y + 0.5f;
					additional[cpl].contrast = agms.gpuAllocatedImage[additional[cpl].in_y * imageArea.width + additional[cpl].in_x] - agms.gpuAllocatedImage[additional[cpl].out_y * imageArea.width + additional[cpl].out_x];
					_contrast += additional[cpl].contrast;
				}
			}

			sortMinToMaxContrast(general, COUPLES);

			for (int idx = 0; idx < COUPLES / 2 + 1; idx++)
			{
				_contrast += general[idx].contrast;
			}

			if (_contrast > max_contrast)
			{
				agms.gpuAllocatedPupils[threadNumber].center_x = coord_x;
				agms.gpuAllocatedPupils[threadNumber].center_y = coord_y;
				agms.gpuAllocatedPupils[threadNumber].radius = rad;
				agms.gpuAllocatedPupils[threadNumber].contrast = _contrast;
				max_contrast = _contrast;
			}
		}
	}
}

__host__ Pupil getCandidatePupil(const AllocatedGPUmemorySpace& agms, deque<etQueue>& smallEThistoryData, gtQueue& tmpQueue, Point2f& last_successfull_rot, uint64_t time_limit, int curROInum)
{
	//First non-zero from back
	std::deque<etQueue>::reverse_iterator it;
	it = smallEThistoryData.rbegin();
	etQueue first_nonzero;
	while (it != smallEThistoryData.rend())
	{
		first_nonzero = *it;
		if (first_nonzero.pupil[curROInum].radius > 0)
		{
			break;
		}
		++it;
	}

	if (first_nonzero.timestamp == 0 || (tmpQueue.timestamp - first_nonzero.timestamp > time_limit))
	{
		et_round[curROInum] = 0;
	}
	else
	{
		et_round[curROInum] = 1;
	}

	if (tmpQueue.goodGlintsSequence[curROInum])
	{
		if (et_round[curROInum] == 0)
		{
			Rect roiForTracking = Rect(tmpQueue.tgs[curROInum].bpGraw.x - (ROI_WIDTH / 3 * (1 + curROInum)), tmpQueue.tgs[curROInum].bpGraw.y - ROI_HEIGHT / 2, ROI_WIDTH, ROI_HEIGHT);
			if (roiForTracking.x < tmpQueue.defaultBorders[curROInum].x) roiForTracking.x = tmpQueue.defaultBorders[curROInum].x;
			if (roiForTracking.y < tmpQueue.defaultBorders[curROInum].y) roiForTracking.y = tmpQueue.defaultBorders[curROInum].y;
			if (roiForTracking.x + roiForTracking.width > tmpQueue.defaultBorders[curROInum].x + tmpQueue.defaultBorders[curROInum].width) roiForTracking.width = tmpQueue.defaultBorders[curROInum].x + tmpQueue.defaultBorders[curROInum].width - roiForTracking.x;
			if (roiForTracking.y + roiForTracking.height > tmpQueue.defaultBorders[curROInum].y + tmpQueue.defaultBorders[curROInum].height) roiForTracking.height = tmpQueue.defaultBorders[curROInum].y + tmpQueue.defaultBorders[curROInum].height - roiForTracking.y;

			Rect roiForCopy = Rect(roiForTracking.x - ROI_BORDER, roiForTracking.y - ROI_BORDER, ROI_WIDTH + ROI_BORDER * 2, ROI_HEIGHT + ROI_BORDER * 2);
			if (roiForCopy.x < tmpQueue.defaultBorders[curROInum].x) roiForCopy.x = tmpQueue.defaultBorders[curROInum].x;
			if (roiForCopy.y < tmpQueue.defaultBorders[curROInum].y) roiForCopy.y = tmpQueue.defaultBorders[curROInum].y;
			if (roiForCopy.x + roiForCopy.width > tmpQueue.defaultBorders[curROInum].x + tmpQueue.defaultBorders[curROInum].width) roiForCopy.width = tmpQueue.defaultBorders[curROInum].x + tmpQueue.defaultBorders[curROInum].width - roiForCopy.x;
			if (roiForCopy.y + roiForCopy.height > tmpQueue.defaultBorders[curROInum].y + tmpQueue.defaultBorders[curROInum].height) roiForCopy.height = tmpQueue.defaultBorders[curROInum].y + tmpQueue.defaultBorders[curROInum].height - roiForCopy.y;
			
			Mat imgToCopy = tmpQueue.lastBPDPimage(roiForCopy).clone();
			GaussianBlur(imgToCopy, imgToCopy, Size(3, 3), 0, 0, BORDER_DEFAULT);

			GpuRect imageArea;
			imageArea = roiForCopy;
			GpuRect trackingArea;
			trackingArea = roiForTracking;
			trackingArea.x -= imageArea.x;
			trackingArea.y -= imageArea.y;

			//Move tandem's rects
			TandemGlints tgToCopy = tmpQueue.tgs[curROInum];
			tgToCopy.bpgRect.x -= imageArea.x;
			tgToCopy.bpgRect.y -= imageArea.y;
			tgToCopy.dpgRect.x -= imageArea.x;
			tgToCopy.dpgRect.y -= imageArea.y;
			tgToCopy.bpGraw.x -= imageArea.x;
			tgToCopy.bpGraw.y -= imageArea.y;
			tgToCopy.dpGraw.x -= imageArea.x;
			tgToCopy.dpGraw.y -= imageArea.y;

			cudaMemcpy(agms.gpuAllocatedImage, imgToCopy.ptr(), imgToCopy.cols * imgToCopy.rows * sizeof(uchar), cudaMemcpyHostToDevice);
			EyeTrackingRound0 << <(CLASSIC_ROUND_0_THREADS + 255) / 256, 256 >> > 
				(agms, imageArea, trackingArea, tgToCopy, curROInum);
			cudaDeviceSynchronize();

			Pupil cpuAllocatedPupils[CLASSIC_ROUND_0_THREADS];
			cudaMemcpy(cpuAllocatedPupils, agms.gpuAllocatedPupils, CLASSIC_ROUND_0_THREADS * sizeof(Pupil), cudaMemcpyDeviceToHost);
			sort(cpuAllocatedPupils, cpuAllocatedPupils + CLASSIC_ROUND_0_THREADS, [](const Pupil& a, const Pupil& b) {
				return a.contrast > b.contrast;
			});

#ifdef GPUET_SHOW_DEBUG
			cudaMemcpy(imgToCopy.ptr(), agms.gpuAllocatedImage, imgToCopy.cols * imgToCopy.rows, cudaMemcpyDeviceToHost);
			imshow("w", imgToCopy);
			waitKey(1);
#endif

			Pupil candidate = cpuAllocatedPupils[0];
			candidate.center_x += imageArea.x;
			candidate.center_y += imageArea.y;

			ds[curROInum] = 0;
			dx[curROInum] = 0;
			dy[curROInum] = 0;

			return candidate;
		}
		else if (et_round[curROInum] == 1)
		{
			Rect roiForTracking = Rect(first_nonzero.pupil[curROInum].center_x - 20 + dx[curROInum], first_nonzero.pupil[curROInum].center_y - 20 + dy[curROInum], 41, 41);
			if (roiForTracking.x < tmpQueue.defaultBorders[curROInum].x) roiForTracking.x = tmpQueue.defaultBorders[curROInum].x;
			if (roiForTracking.y < tmpQueue.defaultBorders[curROInum].y) roiForTracking.y = tmpQueue.defaultBorders[curROInum].y;
			if (roiForTracking.x + roiForTracking.width > tmpQueue.defaultBorders[curROInum].x + tmpQueue.defaultBorders[curROInum].width) roiForTracking.width = tmpQueue.defaultBorders[curROInum].x + tmpQueue.defaultBorders[curROInum].width - roiForTracking.x;
			if (roiForTracking.y + roiForTracking.height > tmpQueue.defaultBorders[curROInum].y + tmpQueue.defaultBorders[curROInum].height) roiForTracking.height = tmpQueue.defaultBorders[curROInum].y + tmpQueue.defaultBorders[curROInum].height - roiForTracking.y;

			Rect roiForCopy = Rect(roiForTracking.x - ROI_BORDER, roiForTracking.y - ROI_BORDER, roiForTracking.width + ROI_BORDER * 2, roiForTracking.height + ROI_BORDER * 2);
			if (roiForCopy.x < tmpQueue.defaultBorders[curROInum].x) roiForCopy.x = tmpQueue.defaultBorders[curROInum].x;
			if (roiForCopy.y < tmpQueue.defaultBorders[curROInum].y) roiForCopy.y = tmpQueue.defaultBorders[curROInum].y;
			if (roiForCopy.x + roiForCopy.width > tmpQueue.defaultBorders[curROInum].x + tmpQueue.defaultBorders[curROInum].width) roiForCopy.width = tmpQueue.defaultBorders[curROInum].x + tmpQueue.defaultBorders[curROInum].width - roiForCopy.x;
			if (roiForCopy.y + roiForCopy.height > tmpQueue.defaultBorders[curROInum].y + tmpQueue.defaultBorders[curROInum].height) roiForCopy.height = tmpQueue.defaultBorders[curROInum].y + tmpQueue.defaultBorders[curROInum].height - roiForCopy.y;

			Mat imgToCopy = tmpQueue.lastBPDPimage(roiForCopy).clone();
			GaussianBlur(imgToCopy, imgToCopy, Size(3, 3), 0, 0, BORDER_DEFAULT);

			float requiredMinRad = first_nonzero.pupil[curROInum].radius - 2 + ds[curROInum] > MINRAD ? first_nonzero.pupil[curROInum].radius - 2 + ds[curROInum] : MINRAD;
			float requiredMaxRad = requiredMinRad + 4 < MAXRAD ? requiredMinRad + 4 : MAXRAD;

			GpuRect imageArea;
			imageArea = roiForCopy;
			GpuRect trackingArea;
			trackingArea = roiForTracking;
			trackingArea.x -= imageArea.x;
			trackingArea.y -= imageArea.y;

			//Move tandem's rects
			TandemGlints tgToCopy = tmpQueue.tgs[curROInum];
			tgToCopy.bpgRect.x -= imageArea.x;
			tgToCopy.bpgRect.y -= imageArea.y;
			tgToCopy.dpgRect.x -= imageArea.x;
			tgToCopy.dpgRect.y -= imageArea.y;
			tgToCopy.bpGraw.x -= imageArea.x;
			tgToCopy.bpGraw.y -= imageArea.y;
			tgToCopy.dpGraw.x -= imageArea.x;
			tgToCopy.dpGraw.y -= imageArea.y;

			cudaMemcpy(agms.gpuAllocatedImage, imgToCopy.ptr(), imgToCopy.cols * imgToCopy.rows * sizeof(uchar), cudaMemcpyHostToDevice);
			EyeTrackingRound1 << <(8100 + 255) / 256, 256 >> > 
				(agms, imageArea, trackingArea, tgToCopy, requiredMinRad, requiredMaxRad, curROInum);
			cudaDeviceSynchronize();

			Pupil cpuAllocatedPupils[8100]; //9*9*10*10
			cudaMemcpy(cpuAllocatedPupils, agms.gpuAllocatedPupils, 8100 * sizeof(Pupil), cudaMemcpyDeviceToHost);
			sort(cpuAllocatedPupils, cpuAllocatedPupils + 8100, [](const Pupil& a, const Pupil& b) {
				return a.contrast > b.contrast;
			});

#ifdef GPUET_SHOW_DEBUG
			cudaMemcpy(imgToCopy.ptr(), agms.gpuAllocatedImage, imgToCopy.cols* imgToCopy.rows, cudaMemcpyDeviceToHost);
			imshow("w", imgToCopy);
			waitKey(1);
#endif

			Pupil candidate = cpuAllocatedPupils[0];
			candidate.center_x += imageArea.x;
			candidate.center_y += imageArea.y;

			//Moving average of n + 1 pupils
			int movaver = 1;
			float sumx = 0, sumy = 0, sumsize = 0;
			sumx += first_nonzero.pupil[curROInum].center_x;
			sumy += first_nonzero.pupil[curROInum].center_y;
			sumsize += first_nonzero.pupil[curROInum].radius;
			sumx += candidate.center_x;
			sumy += candidate.center_y;
			sumsize += candidate.radius;
			candidate.center_x = sumx / (movaver + 1);
			candidate.center_y = sumy / (movaver + 1);
			candidate.radius = sumsize / (movaver + 1);

			return candidate;
		}
		else
		{
			cerr << "Pupil tracking round != 0 or 1" << endl;
			throw;
		}
	}
	else
	{
		last_successfull_rot = Point2f();
		et_round[curROInum] = 0;
		return Pupil();
	}
}
__host__ Pupil getDilatedCandidatePupil(const AllocatedGPUmemorySpace& agms, gtQueue& tmpQueue, uint64_t time_limit, float pupil_diameter, int curROInum)
{
	Rect roiForTracking = Rect(50 + 500 * curROInum, 50, DILATED_ROI_WIDTH, DILATED_ROI_HEIGHT);
	Rect roiForCopy = Rect(0 + 500 * curROInum, 0, FRAME_WIDTH / 2, FRAME_HEIGHT);
	Mat imgToCopy = tmpQueue.lastDPimage(roiForCopy).clone();
	GaussianBlur(imgToCopy, imgToCopy, Size(3, 3), 0, 0, BORDER_DEFAULT);

	GpuRect imageArea;
	imageArea = roiForCopy;
	GpuRect trackingArea;
	trackingArea = roiForTracking;
	trackingArea.x -= imageArea.x;
	trackingArea.y -= imageArea.y;
	imageArea.x = 0;
	imageArea.y = 0;

	//Allocated memory for image is constant do we need allocate each time?
	cudaMemcpy(agms.gpuAllocatedImage, imgToCopy.ptr(), imgToCopy.cols * imgToCopy.rows * sizeof(uchar), cudaMemcpyHostToDevice);
	
	float requiredMinRad = pupil_diameter / 2 - 1 > MINRAD ? pupil_diameter / 2 - 1 : MINRAD;
	float requiredMaxRad = requiredMinRad + 2 < MAXRAD ? requiredMinRad + 2 : MAXRAD;

	Pupil cpuAllocatedPupils[DILATED_ROUND_0_THREADS];

	if (pupil_diameter > 0)
	{
		EyeTrackingRound0Dilated << <(DILATED_ROUND_0_THREADS + 255) / 256, 256 >> >
			(agms, imageArea, trackingArea, requiredMinRad, requiredMaxRad, curROInum);
		cudaDeviceSynchronize();
	}
	else
	{
		EyeTrackingRoundMinus1Dilated << <(DILATED_ROUND_0_THREADS + 255) / 256, 256 >> >
			(agms, imageArea, trackingArea, curROInum);
		cudaDeviceSynchronize();
	}
	cudaMemcpy(cpuAllocatedPupils, agms.gpuAllocatedPupils, DILATED_ROUND_0_THREADS * sizeof(Pupil), cudaMemcpyDeviceToHost);
	sort(cpuAllocatedPupils, cpuAllocatedPupils + DILATED_ROUND_0_THREADS, [](const Pupil& a, const Pupil& b) {
		return a.contrast < b.contrast;
	});
	
	//4900 is 7 * 7 * 10 * 10 in Round1Tracking
	EyeTrackingRound1Dilated << <(4900 + 255) / 256, 256 >> > 
		(agms, imageArea, trackingArea, cpuAllocatedPupils[0], curROInum);
	cudaDeviceSynchronize();
	cudaMemcpy(cpuAllocatedPupils, agms.gpuAllocatedPupils, 4900 * sizeof(Pupil), cudaMemcpyDeviceToHost);
	sort(cpuAllocatedPupils, cpuAllocatedPupils + 4900, [](const Pupil& a, const Pupil& b) {
		return a.contrast < b.contrast;
	});

#ifdef GPUET_SHOW_DEBUG
	cudaMemcpy(imgToCopy.ptr(), agms.gpuAllocatedImage, imgToCopy.cols * imgToCopy.rows * sizeof(uchar), cudaMemcpyDeviceToHost);
	imshow("w", imgToCopy);
	waitKey(1);
#endif

	Pupil candidate = cpuAllocatedPupils[0];
	candidate.center_x += roiForCopy.x;
	candidate.center_y += roiForCopy.y;

	return candidate;
}
__host__ Point2f gpu_getRotPoint_center(const Point2f& winner_center, const Point2f& BPglint)
{
	float shift_x = winner_center.x - BPglint.x;
	float shift_y = winner_center.y - BPglint.y;

	float rot_x = BPglint.x + shift_x * 44.82 - shift_x * 45.64;
	float rot_y = BPglint.y + shift_y * 44.82 - shift_y * 45.64;

	Point2f rot = Point2f(rot_x, rot_y);

	return rot;
}
__host__ Point2f gpu_getRotPoint(const Pupil& winner, const Point2f& BPglint)
{
	float shift_x = winner.center_x - BPglint.x;
	float shift_y = winner.center_y - BPglint.y;

	float rot_x = BPglint.x + shift_x * 44.82 - shift_x * 45.64;
	float rot_y = BPglint.y + shift_y * 44.82 - shift_y * 45.64;

	Point2f rot = Point2f(rot_x, rot_y);

	return rot;
}
__host__ ThreeDangle gpu_get3DangleData(const Point2f& rawRotPoint, const Point2f& someCenRaw, const Point2f& calibShape, int curROInum, bool usebpg)
{
	ThreeDangle ret;

	Point2f trueRot = gpu_convertCoords2(rawRotPoint, curROInum);
	Point2f trueSomeCen = gpu_convertCoords2(someCenRaw, curROInum);

	float C_y = -led_to_calib[curROInum].height;
	float A_y = -gpu_cam_pix_to_mm(trueRot.y - calibShape.y);
	float led_y_angl = asin((C_y + A_y) / led_to_rot_z);
	float y_shift = -gpu_cam_pix_to_mm(trueSomeCen.y - trueRot.y);

	float eye_y_angl;
	if (!usebpg)
		eye_y_angl = asin(y_shift / 12.2) - led_y_angl;
	else
		eye_y_angl = asin(y_shift / 5.6) - led_y_angl;

	float B_y = tan(eye_y_angl) * 160.5;
	float y_pix = calib_perp[curROInum].y + gpu_scr_mm_to_pix(A_y + B_y);

	float C_x = led_to_calib[curROInum].width;
	float A_x = gpu_cam_pix_to_mm(trueRot.x - calibShape.x);
	float led_x_angl = asin((C_x + A_x) / led_to_rot_z);
	float x_shift = gpu_cam_pix_to_mm(trueSomeCen.x - trueRot.x);

	float eye_x_angl;
	if (!usebpg)
		eye_x_angl = asin(x_shift / 12.2) - led_x_angl;
	else
		eye_x_angl = asin(x_shift / 5.6) - led_x_angl;

	float B_x = tan(eye_x_angl) * 160.5;
	float x_pix = calib_perp[curROInum].x - gpu_scr_mm_to_pix(A_x + B_x);

	ret.eyeAngle.width = eye_x_angl * 180 / CV_PI;
	ret.eyeAngle.height = eye_y_angl * 180 / CV_PI;
	ret.vsPixel.x = x_pix;
	ret.vsPixel.y = y_pix;

	return ret;
}
__host__ Point2f gpu_convertCoords2(Point2f rawPoint, int curROInum)
{
	Point2f cP = Point(FRAME_WIDTH / 4 + FRAME_WIDTH / 2 * curROInum, FRAME_HEIGHT / 2);
	float shift_angle = ((90 + anglesOfShapes[curROInum]) * (2 * curROInum - 1)); //*-1 if OD
	float this_angle = gpu_pAng(cP, rawPoint);
	float _dist = gpu_pDist(cP, rawPoint);

	Point2f newcP = Point(FRAME_HEIGHT / 2 + FRAME_HEIGHT * curROInum, FRAME_WIDTH / 4);

	Point2f rr;
	rr.x = newcP.x + _dist * cos((this_angle + shift_angle) * CV_PI / 180.0);
	rr.y = newcP.y + _dist * sin((this_angle + shift_angle) * CV_PI / 180.0);

	return rr;
}
__host__ float calculateDilatedPupilSize(int curROInum)
{
	vector<float> diameters;

	int maxIndex = entireEThistoryData.size() < 1000 ? entireEThistoryData.size() : 1000;
	for (int idx = 0; idx < maxIndex; idx++)
	{
		if (entireEThistoryData.at(idx).etData.at(curROInum).rawPupilEllipse.size.width != 0.f)
		{
			diameters.push_back(entireEThistoryData.at(idx).etData.at(curROInum).rawPupilEllipse.size.width);
		}
	}

	if (diameters.size() < 200)
	{
		return 0.f;
	}

	sort(diameters.begin(), diameters.end());

	float theMostPopularDiameter = 0;
	int maxRansacSamplesMet = 0;

	for (int idx = 0; idx < diameters.size(); idx++)
	{
		float candidateDiameter = diameters.at(idx);
		int ransacSamplesMet = 0;

		for (int idx2 = idx; idx2 < diameters.size(); idx2++)
		{
			if (diameters.at(idx2) - candidateDiameter < COUPLE_DIST)
			{
				ransacSamplesMet++;
			}
			else
			{
				break;
			}
		}

		if (ransacSamplesMet > maxRansacSamplesMet)
		{
			maxRansacSamplesMet = ransacSamplesMet;
			theMostPopularDiameter = candidateDiameter;
		}
	}

	return theMostPopularDiameter;
}

__host__ gpuTrackingQuality gpuQualityControl(const deque<etQueue>& smallEThistoryData, Mat& frame, const Mat& lastDPimage, Mat& trueLastBPimage, const int& curROInum, const Point2f& thisRot, const RotatedRect& thisWin, const TandemGlints& thisTanG, const bool& dilated)
{
	gpuTrackingQuality ret;
	Rect defaultImageArea = Rect(0, 0, FRAME_WIDTH, FRAME_HEIGHT);
	Rect2f aroundWIN = thisWin.boundingRect2f();

	/* DP contrast checking */
	float min_len = thisWin.size.width / 4;
	float max_len = thisWin.size.width / 4 * 3;
	float step = (max_len - min_len) / 6;		//3 vs 3 pts

	int overall_color_in = 0, overall_color_out = 0;
	int n_points_in = 0, n_points_out = 0;

	for (float angle = 250; angle <= 290; angle += 8)
	{
		for (float len = min_len; len < max_len; len += step)
		{
			Point2f pt, pt_mirrored;
			pt.x = thisWin.center.x + len * cos(angle * CV_PI / 180.0);
			pt.y = thisWin.center.y + len * -sin(angle * CV_PI / 180.0);
			pt_mirrored.x = thisWin.center.x - len * cos(angle * CV_PI / 180.0);
			pt_mirrored.y = thisWin.center.y - len * -sin(angle * CV_PI / 180.0);

			if (defaultImageArea.contains(pt))
			{
				int color = lastDPimage.at<uchar>(pt);

				if (color < 240)
				{
					if (gpu_pDist(pt, thisWin.center) > thisWin.size.width / 2)
					{
						overall_color_out += color;
						n_points_out++;
					}
					else
					{
						overall_color_in += color;
						n_points_in++;
					}
				}
			}

			if (defaultImageArea.contains(pt_mirrored))
			{
				int color = lastDPimage.at<uchar>(pt_mirrored);

				if (color < 240)
				{
					if (gpu_pDist(pt_mirrored, thisWin.center) > thisWin.size.width / 2)
					{
						overall_color_out += color;
						n_points_out++;
					}
					else
					{
						overall_color_in += color;
						n_points_in++;
					}
				}
			}
		}
	}

	if (n_points_in > 0 && n_points_out > 0)
	{
		ret.DPcontrast = (overall_color_in / n_points_in) - (overall_color_out / n_points_out);
	}

	/* BP contrast checking */
	overall_color_in = 0; overall_color_out = 0;
	n_points_in = 0; n_points_out = 0;

	for (float angle = 250; angle <= 290; angle += 8)
	{
		for (float len = min_len; len < max_len; len += step)
		{
			Point2f pt, pt_mirrored;
			pt.x = thisWin.center.x + len * cos(angle * CV_PI / 180.0);
			pt.y = thisWin.center.y + len * -sin(angle * CV_PI / 180.0);
			pt_mirrored.x = thisWin.center.x - len * cos(angle * CV_PI / 180.0);
			pt_mirrored.y = thisWin.center.y - len * -sin(angle * CV_PI / 180.0);

			if (defaultImageArea.contains(pt) && !thisTanG.bpgRect.contains(pt))
			{
				int color = trueLastBPimage.at<uchar>(pt);

				if (gpu_pDist(pt, thisWin.center) > thisWin.size.width / 2)
				{
					overall_color_out += color;
					n_points_out++;
				}
				else
				{
					overall_color_in += color;
					n_points_in++;
				}
			}

			if (defaultImageArea.contains(pt_mirrored) && !thisTanG.bpgRect.contains(pt_mirrored))
			{
				int color = trueLastBPimage.at<uchar>(pt_mirrored);

				if (gpu_pDist(pt_mirrored, thisWin.center) > thisWin.size.width / 2)
				{
					overall_color_out += color;
					n_points_out++;
				}
				else
				{
					overall_color_in += color;
					n_points_in++;
				}
			}
		}
	}

	if (n_points_in > 0 && n_points_out > 0)
	{
		ret.BPcontrast = (overall_color_in / n_points_in) - (overall_color_out / n_points_out);
	}

	if (!dilated)
	{
		gpu_calculatePupilDeltas(frame, trueLastBPimage, thisWin, thisTanG, curROInum, ret, lastDPimage);
	}

	/* Return */
	if (smallEThistoryData.size() > 0)
	{
		ret.dPupilSize = abs(thisWin.size.width / 2 - smallEThistoryData.back().pupil[curROInum].radius);
		ret.dPupilPos = gpu_pDist(thisWin.center, Point2f(smallEThistoryData.back().pupil[curROInum].center_x, smallEThistoryData.back().pupil[curROInum].center_y));
	}

	return ret;
}
__host__ void gpu_calculatePupilDeltas(Mat& frame, const Mat& trueLastBPimage, const RotatedRect& thisWin, const TandemGlints& thisTanG, const int& curROInum, gpuTrackingQuality& ret, const Mat& trueLastDPimage)
{
	Rect defaultImageArea = Rect(0, 0, FRAME_WIDTH, FRAME_HEIGHT);

	int _contrast = 0;
	int arrayposs = (thisWin.size.width / 2 - MINRAD) * 8 + 0.5; //round to closest arraypos
	gpuContrastCouple general[COUPLES];

	for (int cpl = 0; cpl < COUPLES; cpl++)
	{
		GpuPoint2f in{ thisWin.center.x + precalculatedStorage1[arrayposs * (COUPLES * 4) + cpl * 4 + 0], thisWin.center.y + precalculatedStorage1[arrayposs * (COUPLES * 4) + cpl * 4 + 1] };
		GpuPoint2f out{ thisWin.center.x + precalculatedStorage1[arrayposs * (COUPLES * 4) + cpl * 4 + 2], thisWin.center.y + precalculatedStorage1[arrayposs * (COUPLES * 4) + cpl * 4 + 3] };

		if (!defaultImageArea.contains(Point(in.x, in.y)) || !defaultImageArea.contains(Point(out.x, out.y)))
		{
			general[cpl] = gpuContrastCouple();
		}
		else
		{
			general[cpl].jumped = jumpOverGlint(thisTanG, in, out);

			//round to closest int
			general[cpl].in_x = in.x + 0.5f;
			general[cpl].in_y = in.y + 0.5f;
			general[cpl].out_x = out.x + 0.5f;
			general[cpl].out_y = out.y + 0.5f;
			general[cpl].contrast = frame.at<uchar>(Point(general[cpl].in_x, general[cpl].in_y)) - frame.at<uchar>(Point(general[cpl].out_x, general[cpl].out_y));
		}
	}

	sort(general, general + COUPLES, [](const gpuContrastCouple& one, const gpuContrastCouple& two)
	{
		return one.contrast < two.contrast;
	});

	gpuContrastCouple ccp[RAY_TRACES];
	copy(general, general + RAY_TRACES, ccp);

	int maxSteps = 0;
	dx[curROInum] = 0;
	dy[curROInum] = 0;
	ds[curROInum] = 0;

	/* Bananas inside checking. */
	int percentage = 40;
	int requiredDots = CMP_POINTS;
	float proportion = thisWin.size.width / (MINRAD * 2);

	Rect2f rectOfInterest;
	Point2f basicColorPoint;

	if (curROInum == 0)
	{
		rectOfInterest = Rect2f(Point2f(thisWin.center.x, thisWin.center.y - thisWin.size.height / 2 * 0.7),
			Size2f(thisWin.size.width / 2 * 0.7, thisWin.size.height * 0.7));

		basicColorPoint = Point2f(rectOfInterest.x - rectOfInterest.width / 2 + proportion,
			rectOfInterest.y + rectOfInterest.height / 2);
	}
	else
	{
		rectOfInterest = Rect2f(Point2f(thisWin.center.x - thisWin.size.width / 2 * 0.7, thisWin.center.y - thisWin.size.height / 2 * 0.7),
			Size2f(thisWin.size.width / 2 * 0.7, thisWin.size.height * 0.7));

		basicColorPoint = Point(rectOfInterest.x + rectOfInterest.width + rectOfInterest.width / 2 - proportion,
			rectOfInterest.y + rectOfInterest.height / 2);
	}

	Rect2f goodSideLimits = Rect2f(basicColorPoint.x - (rectOfInterest.width - proportion) / 2,
		basicColorPoint.y - (rectOfInterest.height - proportion) / 2,
		rectOfInterest.width - proportion,
		rectOfInterest.height - proportion);

	/* Check for intersection with Glints areas */
	Rect2f bpgROI = goodSideLimits & thisTanG.bpgRect;
	Rect2f dpgROI = goodSideLimits & thisTanG.dpgRect;
	frame(bpgROI) = 0;
	frame(dpgROI) = 0;

	Rect2f goodSideROI = goodSideLimits;

	int contrasts = 0;
	int gottenDots = 0;
	int allColors[CMP_POINTS] = { 0 };

	/* Go inside goodSide discarding bpg and dpg areas for a searching of basicColor points */
	for (int x = goodSideROI.x; x < goodSideROI.x + goodSideROI.width; x++)
	{
		for (int y = goodSideROI.y; y < goodSideROI.y + goodSideROI.height; y++)
		{
			if (defaultImageArea.contains(Point(x, y)) && frame.at<uchar>(Point(x, y)) > 0)
			{
				contrasts += frame.at<uchar>(Point(x, y));
				allColors[gottenDots] = trueLastBPimage.at<uchar>(Point(x, y));
				//allColors[gottenDots] = frame.at<uchar>(Point(x, y));
				gottenDots++;

				if (gottenDots == requiredDots)
				{
					break;
				}
			}
		}
		if (gottenDots == requiredDots)
		{
			break;
		}
	}

	/* All points are gotten */
	if (gottenDots == requiredDots)
	{
		int basicColor = contrasts / requiredDots;

		for (int idx = 1; idx <= RAY_TRACES; idx++)
		{
			if (defaultImageArea.contains(Point(ccp[idx - 1].out_x, ccp[idx - 1].out_y)));
			{
				float perc = (float)abs(frame.at<uchar>(Point(ccp[idx - 1].out_x, ccp[idx - 1].out_y)) - basicColor) / basicColor * 100;

				float maxPerc = 0.3;
				int lastN = 3;
				float averperc = 120;

				if (cp[idx][curROInum].size() >= lastN)
				{
					averperc = 0;

					for (int coupleContrast = 1; coupleContrast <= lastN; coupleContrast++)
					{
						averperc += cp[idx][curROInum].at(cp[idx][curROInum].size() - coupleContrast);
					}
				}

				averperc = averperc / lastN;

				cp[idx][curROInum].push_back(perc);
				if (cp[idx][curROInum].size() > 100)
				{
					cp[idx][curROInum].pop_front();
				}

				/* Less value = more sensitivity to shoot traces */
				if ((averperc - perc) / averperc > maxPerc)
				{
					ccp[idx - 1].jumped = false;
					int stepX = ccp[idx - 1].out_x - ccp[idx - 1].in_x == 0 ? 0 : ccp[idx - 1].out_x - ccp[idx - 1].in_x < 0 ? -1 : 1;
					int stepY = ccp[idx - 1].out_y - ccp[idx - 1].in_y == 0 ? 0 : ccp[idx - 1].out_y - ccp[idx - 1].in_y < 0 ? -1 : 1;

					int localMaxSteps = 0;
					int localMaxStepsX = 0;
					int localMaxStepsY = 0;

					while (perc < percentage && (stepX != 0 || stepY != 0))
					{
						localMaxSteps++;
						localMaxStepsX += stepX;
						localMaxStepsY += stepY;

						if (localMaxSteps > MAXRAD * 2)
						{
							localMaxSteps = localMaxStepsX = localMaxStepsY = 0;
							break;
						}

						ccp[idx - 1].out_x += stepX;
						if (ccp[idx - 1].out_x >= FRAME_WIDTH || ccp[idx - 1].out_x < 0) break;

						ccp[idx - 1].out_y += stepY;
						if (ccp[idx - 1].out_y >= FRAME_HEIGHT || ccp[idx - 1].out_y < 0) break;

						perc = (float)abs(frame.at<uchar>(Point(ccp[idx - 1].out_x, ccp[idx - 1].out_y)) - basicColor) / basicColor * 100;
					}

					ccp[idx - 1].out_x += stepX * 2;
					ccp[idx - 1].out_y += stepY * 2;

					if (localMaxSteps > maxSteps)
					{
						dx[curROInum] = (float)localMaxStepsX / 2;
						dy[curROInum] = (float)localMaxStepsY / 2;
						float len = sqrt(dx[curROInum] * dx[curROInum] + dy[curROInum] * dy[curROInum]);
						float len2 = (int)(len * 1000) - (int)(len * 1000) % (int)(0.125 * 1000);
						ds[curROInum] = len2 / 1000;
						maxSteps = localMaxSteps;
					}
				}
			}
		}
	}
}

__host__ void ETdataFixTiming()
{
	//Check each new result
	//Some pupil data exists in current package. Change values in according glints timestamp in case of difference 
	if (entireEThistoryData.back().etData[0].pupilsTimestamp > 0 && (entireEThistoryData.back().etData[0].pupilsTimestamp != entireEThistoryData.back().etData[0].glintsTimestamp))
	{
		for (int idx = entireEThistoryData.size() - 1; idx >= 0; idx--)
		{
			//According glints timestamp found
			if (entireEThistoryData[idx].etData[0].glintsTimestamp == entireEThistoryData.back().etData[0].pupilsTimestamp)
			{
				last_fixed_index = idx;

				//Replace all pupil-related data
				for (int curROInum = 0; curROInum < 2; curROInum++)
				{
					entireEThistoryData.at(idx).etData[curROInum].rawPupilEllipse = entireEThistoryData.back().etData[curROInum].rawPupilEllipse;
					entireEThistoryData.at(idx).etData[curROInum].BPcontrast = entireEThistoryData.back().etData[curROInum].BPcontrast;
					entireEThistoryData.at(idx).etData[curROInum].DPcontrast = entireEThistoryData.back().etData[curROInum].DPcontrast;
					entireEThistoryData.at(idx).etData[curROInum].pupilEllipse = entireEThistoryData.back().etData[curROInum].pupilEllipse;
					entireEThistoryData.at(idx).etData[curROInum].rawRotPoint = entireEThistoryData.back().etData[curROInum].rawRotPoint;
					entireEThistoryData.at(idx).etData[curROInum].rotPoint = entireEThistoryData.back().etData[curROInum].rotPoint;
					entireEThistoryData.at(idx).etData[curROInum].calibShape = entireEThistoryData.back().etData[curROInum].calibShape;
					entireEThistoryData.at(idx).etData[curROInum].pursuitValues = entireEThistoryData.back().etData[curROInum].pursuitValues;
					entireEThistoryData.at(idx).etData[curROInum].threeDangle = entireEThistoryData.back().etData[curROInum].threeDangle;
					entireEThistoryData.at(idx).etData[curROInum].pupilsTimestamp = entireEThistoryData.at(idx).etData[curROInum].glintsTimestamp;
					entireEThistoryData.at(idx).etData[curROInum].pupilsTimestampGL = entireEThistoryData.at(idx).etData[curROInum].glintsTimestampGL;
					entireEThistoryData.at(idx).etData[curROInum].round = entireEThistoryData.back().etData[curROInum].round;
					entireEThistoryData.at(idx).etData[curROInum].dilatedTandemGlints = entireEThistoryData.back().etData[curROInum].dilatedTandemGlints;

					entireEThistoryData.back().etData[curROInum].rawPupilEllipse = RotatedRect();
					entireEThistoryData.back().etData[curROInum].BPcontrast = 0;
					entireEThistoryData.back().etData[curROInum].DPcontrast = 0;
					entireEThistoryData.back().etData[curROInum].pupilEllipse = RotatedRect();
					entireEThistoryData.back().etData[curROInum].rawRotPoint = Point2f();
					entireEThistoryData.back().etData[curROInum].rotPoint = Point2f();
					entireEThistoryData.back().etData[curROInum].pursuitValues = PursuitValues();
					entireEThistoryData.back().etData[curROInum].threeDangle = ThreeDangle();
					entireEThistoryData.back().etData[curROInum].pupilsTimestamp = 0;
					entireEThistoryData.back().etData[curROInum].pupilsTimestampGL = 0;
					entireEThistoryData.back().etData[curROInum].round = -1;
					entireEThistoryData.back().etData[curROInum].dilatedTandemGlints = TandemGlints();
				}

				break;
			}
		}
	}
	else if (entireEThistoryData.back().etData[0].pupilsTimestamp == entireEThistoryData.back().etData[0].glintsTimestamp)
	{
		last_fixed_index = -1;
	}
	else if (entireEThistoryData.back().etData[0].pupilsTimestamp == 0)
	{
		last_fixed_index = -2;
	}
}
__host__ void TrackingHolesFilling()
{
	//Check indexes. According should be same
	int first_nz[2] = { -1,-1 }, last_nz[2] = { -1,-1 };
	for (int curROInum = 0; curROInum < 2; curROInum++)
	{
		for (int idx = entireEThistoryData.size() - 1; idx >= 0; idx--)
		{
			if (entireEThistoryData[idx].etData[curROInum].pupilsTimestamp > 0 && last_nz[curROInum] == -1)
			{
				if (first_nz[curROInum] == -1)
				{
					first_nz[curROInum] = idx;
				}
				else
				{
					last_nz[curROInum] = idx;
					break;
				}
			}
		}
	}

	if ((first_nz[0] != -1) && (last_nz[0] != -1) && (first_nz[0] == first_nz[1]) && (last_nz[0] == last_nz[1]))
	{
		if (entireEThistoryData[first_nz[0]].etData[0].pupilsTimestamp != 0 && entireEThistoryData[first_nz[0]].etData[0].pupilsTimestamp == filled_untill)
		{
			return;
		}
		else
		{
			filled_untill = entireEThistoryData[first_nz[0]].etData[0].pupilsTimestamp;
		}

		vector<ETData> ODetdata, OSetdata;
		vector<camFrame> camFrames;

		for (int curROInum = 0; curROInum < 2; curROInum++)
		{
			int zero_cntr = first_nz[curROInum] - last_nz[curROInum] - 1;

			double time_diff = entireEThistoryData[first_nz[curROInum]].etData[curROInum].pupilsTimestampGL - entireEThistoryData[last_nz[curROInum]].etData[curROInum].pupilsTimestampGL;
			float x_center_diff = entireEThistoryData[first_nz[curROInum]].etData[curROInum].rawPupilEllipse.center.x - entireEThistoryData[last_nz[curROInum]].etData[curROInum].rawPupilEllipse.center.x;
			float y_center_diff = entireEThistoryData[first_nz[curROInum]].etData[curROInum].rawPupilEllipse.center.y - entireEThistoryData[last_nz[curROInum]].etData[curROInum].rawPupilEllipse.center.y;
			float x_center_diff_dpgdilated_raw = entireEThistoryData[first_nz[curROInum]].etData[curROInum].dilatedTandemGlints.dpGraw.x - entireEThistoryData[last_nz[curROInum]].etData[curROInum].dilatedTandemGlints.dpGraw.x;
			float y_center_diff_dpgdilated_raw = entireEThistoryData[first_nz[curROInum]].etData[curROInum].dilatedTandemGlints.dpGraw.y - entireEThistoryData[last_nz[curROInum]].etData[curROInum].dilatedTandemGlints.dpGraw.y;
			float x_center_diff_dpgdilated = entireEThistoryData[first_nz[curROInum]].etData[curROInum].dilatedTandemGlints.dpG.x - entireEThistoryData[last_nz[curROInum]].etData[curROInum].dilatedTandemGlints.dpG.x;
			float y_center_diff_dpgdilated = entireEThistoryData[first_nz[curROInum]].etData[curROInum].dilatedTandemGlints.dpG.y - entireEThistoryData[last_nz[curROInum]].etData[curROInum].dilatedTandemGlints.dpG.y;
			float size_diff = entireEThistoryData[first_nz[curROInum]].etData[curROInum].rawPupilEllipse.size.width - entireEThistoryData[last_nz[curROInum]].etData[curROInum].rawPupilEllipse.size.width;
			double time_step = time_diff / (zero_cntr + 1);
			float x_step = x_center_diff / (zero_cntr + 1);
			float y_step = y_center_diff / (zero_cntr + 1);
			float x_step_dpgdilated_raw = x_center_diff_dpgdilated_raw / (zero_cntr + 1);
			float y_step_dpgdilated_raw = y_center_diff_dpgdilated_raw / (zero_cntr + 1);
			float x_step_dpgdilated = x_center_diff_dpgdilated / (zero_cntr + 1);
			float y_step_dpgdilated = y_center_diff_dpgdilated / (zero_cntr + 1);
			float size_step = size_diff / (zero_cntr + 1);

			//start_NEW
			Point2f prev_vs_pixel = entireEThistoryData[last_nz[curROInum]].etData[curROInum].threeDangle.vsPixel;
			Point2f prev_rotated_bpg = gpu_convertCoords2(entireEThistoryData[last_nz[curROInum]].etData[curROInum].tandemGlints.bpGraw, curROInum);
			Point2f prev_rotated_dpg = gpu_convertCoords2(entireEThistoryData[last_nz[curROInum]].etData[curROInum].tandemGlints.dpGraw, curROInum);
			Point2f prev_rotated_midglint = (prev_rotated_bpg + prev_rotated_dpg) / 2;
			Point2f last_vs_pixel = entireEThistoryData[first_nz[curROInum]].etData[curROInum].threeDangle.vsPixel;
			Point2f last_rotated_bpg = gpu_convertCoords2(entireEThistoryData[first_nz[curROInum]].etData[curROInum].tandemGlints.bpGraw, curROInum);
			Point2f last_rotated_dpg = gpu_convertCoords2(entireEThistoryData[first_nz[curROInum]].etData[curROInum].tandemGlints.dpGraw, curROInum);
			Point2f last_rotated_midglint = (last_rotated_bpg + last_rotated_dpg) / 2;
			float x_glint_dist = last_rotated_midglint.x - prev_rotated_midglint.x;
			float y_glint_dist = last_rotated_midglint.y - prev_rotated_midglint.y;
			float x_pix_dist = last_vs_pixel.x - prev_vs_pixel.x;
			float y_pix_dist = last_vs_pixel.y - prev_vs_pixel.y;
			//end_NEW

			//idx is an index with zero pupil
			Point2f prev_raw_rot = entireEThistoryData[last_nz[curROInum]].etData[curROInum].rawRotPoint;

			for (int zeroc = 1; zeroc <= zero_cntr; zeroc++)
			{
				int local_idx = last_nz[curROInum] + zeroc;

				if (curROInum == 0)
					camFrames.push_back(entireEThistoryData[local_idx].frame);

				ETData _loc_et_data = entireEThistoryData[local_idx].etData[curROInum];
				_loc_et_data.pupilsTimestamp = _loc_et_data.glintsTimestamp;
				_loc_et_data.pupilsTimestampGL = _loc_et_data.glintsTimestampGL;
				//double this_add_time = zeroc * time_step;

				//Fill with zeros in case of zeros on extremum
				if (entireEThistoryData[last_nz[curROInum]].etData[curROInum].rawPupilEllipse.size.width == 0.f ||
					entireEThistoryData[first_nz[curROInum]].etData[curROInum].rawPupilEllipse.size.width == 0.f)
				{
					if (curROInum == 0)
					{
						ODetdata.push_back(_loc_et_data);
					}
					else
					{
						OSetdata.push_back(_loc_et_data);
					}
					continue;
				}

				//start_NEW
				Point2f this_rotated_bpg = gpu_convertCoords2(entireEThistoryData[local_idx].etData[curROInum].tandemGlints.bpGraw, curROInum);
				Point2f this_rotated_dpg = gpu_convertCoords2(entireEThistoryData[local_idx].etData[curROInum].tandemGlints.dpGraw, curROInum);
				Point2f this_rotated_midglint = (this_rotated_bpg + this_rotated_dpg) / 2;
				float this_x_glint_dist = this_rotated_midglint.x - prev_rotated_midglint.x;
				float this_y_glint_dist = this_rotated_midglint.y - prev_rotated_midglint.y;
				float this_x_dist_part = 0, float this_y_dist_part = 0;
				if (x_glint_dist != 0.f)
				{
					this_x_dist_part = this_x_glint_dist / x_glint_dist;
				}
				if (y_glint_dist != 0.f)
				{
					this_y_dist_part = this_y_glint_dist / y_glint_dist;
				}
				//end_NEW

				float this_add_x = zeroc * x_step;
				float this_add_y = zeroc * y_step;
				float this_add_size = zeroc * size_step;
				float this_add_x_dpgdilated_raw = zeroc * x_step_dpgdilated_raw;
				float this_add_y_dpgdilated_raw = zeroc * y_step_dpgdilated_raw;
				float this_add_x_dpgdilated = zeroc * x_step_dpgdilated;
				float this_add_y_dpgdilated = zeroc * y_step_dpgdilated;

				Point2f loc_center = Point2f(entireEThistoryData[last_nz[curROInum]].etData[curROInum].rawPupilEllipse.center.x + this_add_x, entireEThistoryData[last_nz[curROInum]].etData[curROInum].rawPupilEllipse.center.y + this_add_y);
				float loc_size = entireEThistoryData[last_nz[curROInum]].etData[curROInum].rawPupilEllipse.size.width + this_add_size;
				Point2f loc_bpg_raw = entireEThistoryData[local_idx].etData[curROInum].tandemGlints.bpGraw;
				Point2f loc_dpgdilated_raw = Point2f(entireEThistoryData[last_nz[curROInum]].etData[curROInum].dilatedTandemGlints.dpGraw.x + this_add_x_dpgdilated_raw, entireEThistoryData[last_nz[curROInum]].etData[curROInum].dilatedTandemGlints.dpGraw.y + this_add_y_dpgdilated_raw);
				Point2f loc_dpgdilated = Point2f(entireEThistoryData[last_nz[curROInum]].etData[curROInum].dilatedTandemGlints.dpG.x + this_add_x_dpgdilated, entireEThistoryData[last_nz[curROInum]].etData[curROInum].dilatedTandemGlints.dpG.y + this_add_y_dpgdilated);

				if (loc_bpg_raw != Point2f(0, 0))
				{
					//double loc_time = entireEThistoryData[last_nz[curROInum]].etData[curROInum].pupilsTimestampGL + this_add_time;
					Point2f loc_calib_shape = entireEThistoryData[local_idx].etData[curROInum].calibShape;
					
					Point2f loc_raw_rot;

					if (entireEThistoryData[local_idx].etData[curROInum].isPicBright)
					{
						loc_raw_rot = gpu_getRotPoint_center(loc_center, loc_bpg_raw);
						prev_raw_rot = loc_raw_rot;
					}
					else
					{
						loc_raw_rot = prev_raw_rot;
					}

					ThreeDangle loc_vspix = gpu_get3DangleData(loc_raw_rot, loc_center, loc_calib_shape, curROInum);
					float this_vs_pixel_x = prev_vs_pixel.x + x_pix_dist * this_x_dist_part;
					float this_vs_pixel_y = prev_vs_pixel.y + y_pix_dist * this_y_dist_part;

					_loc_et_data.rawPupilEllipse.center = loc_center;
					_loc_et_data.rawPupilEllipse.size.width = loc_size;
					_loc_et_data.rawPupilEllipse.size.height = loc_size;
					_loc_et_data.dilatedTandemGlints.dpGraw = loc_dpgdilated_raw;
					_loc_et_data.dilatedTandemGlints.dpG = loc_dpgdilated;
					_loc_et_data.dilatedTandemGlints.bpGraw = loc_dpgdilated_raw;
					_loc_et_data.dilatedTandemGlints.bpG = loc_dpgdilated;
					_loc_et_data.BPcontrast = entireEThistoryData[last_nz[curROInum]].etData[curROInum].BPcontrast;
					_loc_et_data.DPcontrast = entireEThistoryData[last_nz[curROInum]].etData[curROInum].DPcontrast;

					if (abs(x_glint_dist) > 2)
					{
						_loc_et_data.threeDangle.vsPixel.x = this_vs_pixel_x;
					}
					else
					{
						_loc_et_data.threeDangle.vsPixel.x = loc_vspix.vsPixel.x;
					}
					if (abs(y_glint_dist) > 2)
					{
						_loc_et_data.threeDangle.vsPixel.y = this_vs_pixel_y;
					}
					else
					{
						_loc_et_data.threeDangle.vsPixel.y = loc_vspix.vsPixel.y;
					}
				}

				if (curROInum == 0)
				{
					ODetdata.push_back(_loc_et_data);
				}
				else
				{
					OSetdata.push_back(_loc_et_data);
				}
			}
		}

		if (ODetdata.size() != OSetdata.size() || ODetdata.size() != camFrames.size())
		{
			cerr << "Datasizes = " << ODetdata.size() << " " << OSetdata.size() << " " << camFrames.size() << endl;
			throw;
		}
		else
		{
			//for (int idx = 0; idx < ODetdata.size(); idx++)
			//{
			//	ETResult _tmp = ETResult{ {ODetdata.at(idx), OSetdata.at(idx)},  camFrames.at(idx) };
			//	filled_et_data.push(_tmp);
			//}
			for (int idx = ODetdata.size() - 1; idx >= 0; idx--)
			{
				//cout << idx << "---" << ODetdata.at(idx).glintsTimestamp << "---" << filled_et_data.empty() << endl;
				ETResult _tmp = ETResult{ {ODetdata.at(idx), OSetdata.at(idx)},  camFrames.at(idx) };
				filled_et_data.push(_tmp);
			}

			//FixStack();
		}
	}
}
__host__ void FixStack()
{
	vector<ETResult> chkStack = vector<ETResult>();

	//Load vector
	while (!filled_et_data.empty())
	{
		ETResult stackData;
		filled_et_data.pop(stackData);
		if (stackData.etData.size() == 2)
		{
			chkStack.push_back(stackData);
		}
	}

	if (chkStack.size() > 0)
	{
		//Sort vector from MIN to MAX of etData[0].glintsTimestamp
		sort(chkStack.begin(), chkStack.end(), [](const ETResult& a, const ETResult& b) {
			return a.etData[0].glintsTimestamp < b.etData[0].glintsTimestamp;
			});

		//Insert vector to stack
		for (int idx = chkStack.size() - 1; idx >= 0; idx--)
		{
			filled_et_data.push(chkStack.at(idx));
		}
	}
	//chkStack.~vector();
}

template<typename T> __host__ float gpu_pDist(const T& p1, const T& p2)
{
	return sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));
}
template<typename T> __host__ float gpu_pAng(const T& p1, const T& p2)
{
	return fastAtan2(p2.y - p1.y, p2.x - p1.x);
}
__host__ float gpu_cam_pix_to_mm(float pix)
{
	return pix / 14.5;
}
__host__ float gpu_cam_mm_to_pix(float mm)
{
	return mm * 14.5;
}
__host__ float gpu_scr_pix_to_mm(float pix)
{
	return pix / 15.87;
}
__host__ float gpu_scr_mm_to_pix(float mm)
{
	return mm * 15.87;
}

__host__ void freeAllocatedGPUmemorySpace(AllocatedGPUmemorySpace& agms)
{
	cudaFree(agms.deviceStorage1);
	cudaFree(agms.deviceStorage2);
	cudaFree(agms.gpuAllocatedImage);
	cudaFree(agms.gpuAllocatedPupils);
}

