#pragma once
#include <fstream>
#include <chrono>
#include <filesystem>
#include <mutex>
#include "Tests.h"

extern CPTGCamera* ptgreycam;
extern CTest* currentTest;
extern atomic<bool> g_calibrationIsGoing;
extern atomic<bool> g_calibrationForcedStop;
extern int offset;

#define SENSORTEST_VERSION "1.0.20221027"

class SensorTest : public CTest
{
public:
	SensorTest();
	void inCaseOfCalibration();
	void finishCalibration(bool forced);

private:
	fstream m_history;

	string m_calibrationPath;
	string m_resourcesPath;
	Mat m_dpImg, m_bpImg, m_toDrawOn;
	Point2f m_results[2];
	Point2f m_glintsCenters[2];

	void performTest();
	double medium_brightness(Mat& frame);
	void setCamera();
	void readLastStains();
	void detectStains();
	void drawResults();
	void setCamBack();
};

SensorTest::SensorTest()
{
	webComm->openLog();
	g_calibrationIsGoing = true;
	cout << "Calibration is executing. SensorTest module" << endl;

	char path[MAX_PATH];
	HMODULE hModule = GetModuleHandleA(NULL);
	GetModuleFileNameA(hModule, path, MAX_PATH);
	PathRemoveFileSpecA(path);
	m_calibrationPath = path;
	m_calibrationPath += "\\calibration";
	m_resourcesPath = m_calibrationPath + "\\resources";

	nlohmann::json js;
	js["testType"] = "SENSOR_TEST";
	js["message_type"] = "START";
	webComm->sendJSON(js);
}

double SensorTest::medium_brightness(Mat& frame)
{
	uint64_t color_sum = 0;
	int n_pixels_engaged = 0;
	for (int i = 0; i < frame.cols; i+=2)
	{
		vector<Point2f> point_sum;

		uchar* begin_ptr = frame.ptr<uchar>(0);
		begin_ptr += i;

		for (int j = 0; j < frame.rows; j+=2)
		{
			uchar grayscale = *begin_ptr;
			begin_ptr += frame.step;

			//Scan all besides LED itself
			if (grayscale < 250)
			{
				n_pixels_engaged++;
				color_sum += grayscale;
			}
		}
	}

	if (n_pixels_engaged > 0)
	{
		return (double)color_sum / n_pixels_engaged;
	}

	return 0;
}

void SensorTest::performTest()
{
	ptgreycam->expandROI();
	ptgreycam->setExposure(2000);

	vector<int> scr_colors[2];
	vector<double> illuminations[2];

	for (int scr_color = 0; scr_color < 255; scr_color++)
	{
		//Test is interrupted from outside
		if (g_calibrationForcedStop)
		{
			return;
		}

		display.setBackgroundR(1.0 / 255 * scr_color, 1.0 / 255 * scr_color, 1.0 / 255 * scr_color);
		display.redrawPending = true;
		this_thread::sleep_for(0.1s);
		ptgreycam->getImage().copyTo(m_dpImg);
		double illumination = medium_brightness(m_dpImg(Rect(74, 410, 410, 365)));
		illuminations[0].push_back(illumination);
		scr_colors[0].push_back(scr_color);
		cout << "Right screen brightness = " << scr_color << endl;
		cout << "Right eye illumination = " << illumination << endl;

		nlohmann::json js;
		js["testType"] = "SENSOR_TEST";
		js["message_type"] = "DATA_PACKAGE";
		js["eye"] = 0;
		js["x"] = scr_color;
		js["y"] = illumination;
		webComm->sendJSON(js);
	}

	display.setBackgroundR(0, 0, 0);
	display.redrawPending = true;
	this_thread::sleep_for(0.1s);

	for (int scr_color = 0; scr_color < 255; scr_color++)
	{
		//Test is interrupted from outside
		if (g_calibrationForcedStop)
		{
			return;
		}

		display.setBackgroundL(1.0 / 255 * scr_color, 1.0 / 255 * scr_color, 1.0 / 255 * scr_color);
		display.redrawPending = true;
		this_thread::sleep_for(0.1s);
		ptgreycam->getImage().copyTo(m_dpImg);
		double illumination = medium_brightness(m_dpImg(Rect(714, 410, 410, 365)));
		illuminations[1].push_back(illumination);
		scr_colors[1].push_back(scr_color);
		cout << "Left screen brightness = " << scr_color << endl;
		cout << "Left eye illumination = " << illumination << endl;

		nlohmann::json js;
		js["testType"] = "SENSOR_TEST";
		js["message_type"] = "DATA_PACKAGE";
		js["eye"] = 1;
		js["x"] = scr_color;
		js["y"] = illumination;
		webComm->sendJSON(js);
	}

	fstream sensor_test;
	sensor_test.open(m_calibrationPath + "\\last_sensor_test_diagram.csv", ios_base::out);

	for (int eye = 0; eye < 2; eye++)
	{
		for (int index = 0; index < scr_colors[eye].size(); index++)
		{
			sensor_test << eye << ";" << scr_colors[eye].at(index) << ";" << illuminations[eye].at(index) << endl;
		}
	}

	sensor_test.close();
}

void SensorTest::setCamBack()
{
	ptgreycam->shrinkROI(offset);
	ptgreycam->setExposure(2000);
	ptgreycam->switchDPBP();
}

void SensorTest::inCaseOfCalibration()
{
	performTest();
	finishCalibration(g_calibrationForcedStop);
}

void SensorTest::finishCalibration(bool forced)
{
	setCamBack();

	m_history.open(m_resourcesPath + "\\history.txt", ios_base::app);

	if (!m_history.is_open() || forced)
	{
		cout << "ERROR: Cannot open history.txt file or forced stop" << endl;
	}
	else
	{
		uint64_t l_stopTime = std::chrono::system_clock::now().time_since_epoch().count();
		m_history << "SensorTest;" << l_stopTime << endl;

		m_history.close();
	}

	nlohmann::json js;
	js["testType"] = "SENSOR_TEST";
	js["message_type"] = "STOP";
	webComm->sendJSON(js);

	webComm->closeLog();
	cout << "Calibration is finished. SensorTest module" << endl;
}