#include "lensdistortion.h"

Lens::Lens(int lensholder, float powerOfLens)
{
	lensHolder = lensholder;
	dioptersOS = powerOfLens;
	dioptersOD = powerOfLens;
}

Point2d Lens::posfromanglOS(Point2d deg)
{
	Point2d posOS;

	double Azm = deg.x * CV_PI / 180;
	double Elv = deg.y * CV_PI / 180;

	double dIPD = lensHolder;
	double dScrn2Eyes = 160.5;
	double szScrnX = 121.0;
	double szScrnY = 68.0;
	double dEye2Lens = 32.424;
	double tiltVdeg = 10 * CV_PI / 180;
	double tiltHdeg = 7 * CV_PI / 180;

	double OSeyeX = szScrnX / 2 - dIPD / 2;
	double OSeyeY = szScrnY / 2;
	double OSeyeZ = dScrn2Eyes;
	double ODeyeX = szScrnX / 2 + dIPD / 2;
	double ODeyeY = szScrnY / 2;
	double ODeyeZ = dScrn2Eyes;

	double dEye2cntScrn = sqrt(pow(dIPD / 2, 2) + pow(dScrn2Eyes, 2));
	double dIPDline2cntScrn = dScrn2Eyes * (1 - dEye2Lens / dEye2cntScrn);
	double dIPDline2ILDline = dScrn2Eyes - dIPDline2cntScrn;
	double dILD = dIPD * (1 - dEye2Lens / dEye2cntScrn);

	double OSlnsX = szScrnX / 2 - dILD / 2;
	double OSlnsY = szScrnY / 2;
	double OSlnsZ = dIPDline2cntScrn;
	double ODlnsX = szScrnX / 2 + dILD / 2;
	double ODlnsY = szScrnY / 2;
	double ODlnsZ = dIPDline2cntScrn;

	double xlOS = OSeyeX + (dIPDline2ILDline * cos(tiltHdeg) + (OSlnsX - OSeyeX) * sin(tiltHdeg)) * sin(Azm) / cos(Azm - tiltHdeg);
	double zlOSx = dScrn2Eyes - (dIPDline2ILDline * cos(tiltHdeg) + (OSlnsX - OSeyeX) * sin(tiltHdeg)) * cos(Azm) / cos(Azm - tiltHdeg);
	double thetaHOS = atan(tan(Azm - tiltHdeg) - (dioptersOS / 1000) * (OSlnsX - OSeyeX - dIPDline2ILDline * tan(Azm)) / (cos(tiltHdeg) + sin(tiltHdeg) * tan(Azm)));
	posOS.x = xlOS + zlOSx * tan(thetaHOS + tiltHdeg);

	double ylOS = OSlnsY + dIPDline2ILDline * sin(Elv) / cos(Elv - tiltVdeg);
	double zlOSy = dScrn2Eyes - dIPDline2ILDline * cos(Elv) / cos(Elv - tiltVdeg);
	double thetaVOS = atan(tan(Elv - tiltVdeg) + (dioptersOS / 1000) * (dIPDline2ILDline * tan(Elv)) / (cos(tiltVdeg) + sin(tiltVdeg) * tan(Elv)));
	posOS.y = ylOS + zlOSy * tan(thetaVOS + tiltVdeg);

	return posOS;
}

Point2d Lens::posfromanglOD(Point2d deg)
{
	Point2d posOD;

	double Azm = deg.x * CV_PI / 180;
	double Elv = deg.y * CV_PI / 180;

	double dIPD = lensHolder;
	double dScrn2Eyes = 160.5;
	double szScrnX = 121.0;
	double szScrnY = 68.0;
	double dEye2Lens = 32.424;
	double tiltVdeg = 10 * CV_PI / 180;
	double tiltHdeg = 7 * CV_PI / 180;

	double OSeyeX = szScrnX / 2 - dIPD / 2;
	double OSeyeY = szScrnY / 2;
	double OSeyeZ = dScrn2Eyes;
	double ODeyeX = szScrnX / 2 + dIPD / 2;
	double ODeyeY = szScrnY / 2;
	double ODeyeZ = dScrn2Eyes;

	double dEye2cntScrn = sqrt(pow(dIPD / 2, 2) + pow(dScrn2Eyes, 2));
	double dIPDline2cntScrn = dScrn2Eyes * (1 - dEye2Lens / dEye2cntScrn);
	double dIPDline2ILDline = dScrn2Eyes - dIPDline2cntScrn;
	double dILD = dIPD * (1 - dEye2Lens / dEye2cntScrn);

	double OSlnsX = szScrnX / 2 - dILD / 2;
	double OSlnsY = szScrnY / 2;
	double OSlnsZ = dIPDline2cntScrn;
	double ODlnsX = szScrnX / 2 + dILD / 2;
	double ODlnsY = szScrnY / 2;
	double ODlnsZ = dIPDline2cntScrn;

	double xlOD = ODeyeX - (dIPDline2ILDline * cos(tiltHdeg) - (ODlnsX - ODeyeX) * sin(tiltHdeg)) * sin(Azm) / cos(Azm - tiltHdeg);
	double zlODx = dScrn2Eyes - (dIPDline2ILDline * cos(tiltHdeg) - (ODlnsX - ODeyeX) * sin(tiltHdeg)) * cos(Azm) / cos(Azm - tiltHdeg);
	double thetaHOD = atan(tan(Azm - tiltHdeg) + (dioptersOD / 1000) * (ODlnsX - ODeyeX + dIPDline2ILDline * tan(Azm)) / (cos(tiltHdeg) + sin(tiltHdeg) * tan(Azm)));
	posOD.x = xlOD - zlODx * tan(thetaHOD + tiltHdeg);

	double ylOD = ODlnsY + dIPDline2ILDline * sin(Elv) / cos(Elv - tiltVdeg);
	double zlODy = dScrn2Eyes - dIPDline2ILDline * cos(Elv) / cos(Elv - tiltVdeg);
	double thetaVOD = atan(tan(Elv - tiltVdeg) + (dioptersOD / 1000) * (dIPDline2ILDline * tan(Elv)) / (cos(tiltVdeg) + sin(tiltVdeg) * tan(Elv)));
	posOD.y = ylOD + zlODy * tan(thetaVOD + tiltVdeg);

	return posOD;
}

Point2d Lens::anglefromposOS(Point2d stim)
{
	Point2d os;
	Point2d anglDeg;

	anglDeg.x = 11.65;
	anglDeg.y = 0.0;

	double alph = 0.01;
	double errX = 2;
	double errY = 2;
	while (abs(errX) > 0.01)
	{
		os = posfromanglOS(anglDeg);
		errX = stim.x - os.x;
		anglDeg.x = anglDeg.x + alph * errX;
	}
	while (abs(errY) > 0.01)
	{
		os = posfromanglOS(anglDeg);
		errY = stim.y - os.y;
		anglDeg.y = anglDeg.y + alph * errY;
	}

	return anglDeg;
}

Point2d Lens::anglefromposOD(Point2d stim)
{
	Point2d od;
	Point2d anglDeg;

	anglDeg.x = -11.65;
	anglDeg.y = 0.0;

	double alph = 0.01;
	double errX = 2;
	double errY = 2;
	while (abs(errX) > 0.01)
	{
		od = posfromanglOD(anglDeg);
		errX = stim.x - od.x;
		anglDeg.x = anglDeg.x - alph * errX;
	}
	while (abs(errY) > 0.01)
	{
		od = posfromanglOD(anglDeg);
		errY = stim.y - od.y;
		anglDeg.y = anglDeg.y + alph * errY;
	}

	return anglDeg;
}

Point2d Lens::jumpTo(Point2d currentPosition, Point2d jumpAngle)
{
	Point2d stimpos;
	if (currentPosition.x > 1920.f)
	{
		stimpos.x = pixtomm(3840 - currentPosition.x);
		stimpos.y = pixtomm(currentPosition.y);
		Point2d ap = anglefromposOD(stimpos);
		Point2d afp = posfromanglOD(ap + jumpAngle);
		Point2d afpinv = mmtopix(afp);
		afpinv.x = 3840 - afpinv.x;
		return afpinv;
	}
	else
	{
		stimpos.x = pixtomm(1920 - currentPosition.x);
		stimpos.y = pixtomm(currentPosition.y);
		Point2d ap = anglefromposOS(stimpos);
		Point2d afp = posfromanglOS(ap + jumpAngle);
		Point2d afpinv = mmtopix(afp);
		afpinv.x = 1920 - afpinv.x;
		return afpinv;
	}
}