#pragma once
#include <numeric>
#include<iostream>
#include<iomanip>
#include<cmath>
#include<vector>
#include <algorithm>
#include <queue>

using namespace std;
using namespace cv;

extern string PathToExecutable;

class LuxConverter
{
public:
	double luxToGrayScale(double lux);
	double grayScaleToLux(double grayScale);
    void plot(string fn);
   vector<double> x, y;
   void loadTable(string filename);
    void fit();
    void fitI(vector<double> xValues, vector<double> yValues, double& a, double& b, double& c);
private:
    double luxToGrayScaleBig(double lux);
    double luxToGrayScaleSmall(double lux);
    double luxToGrayScaleTable(double lux);
    double k_a_s, k_b_s, k_c_s;
    double k_a_b, k_b_b, k_c_b;
    vector<pair<double, double>> luxTable;
};

void LuxConverter::plot(string fn)
{
    Mat testMat = Mat::zeros(1080, 1920, CV_8UC3);
    circle(testMat, Point(0 * 4, 1080 - luxToGrayScale(0) * 2), 3, Scalar(255, 255, 0), -1);
    for (int i = 1; i < 400; i += 2)
    {
        circle(testMat, Point(i * 4, 1080 - luxToGrayScale(i) * 2 * 255), 3, Scalar(255, 255, 0), -1);
        line(testMat, Point(i * 4, 1080 - luxToGrayScale(i) * 2 * 255), Point((i - 2) * 4, 1080 - luxToGrayScale(i - 2) * 2 * 255), Scalar(255, 255, 0));
//        cout << i << ";" << luxToGrayScale(i)*255 << endl;
    }

    for (int i = 1; i < x.size(); i ++)
    {
        circle(testMat, Point(y[i] * 4, 1080 - x[i] * 2), 3, Scalar(0, 255, 0), -1);
        line(testMat, Point(y[i] * 4, 1080 - x[i] * 2), Point(y[i-1] * 4, 1080 - x[i-1] * 2), Scalar(0, 255, 0));
//        cout << i << ";" << luxToGrayScale(i)*255 << endl;
    }

    imwrite("c:\\testvideos\\"+fn, testMat);
}

void LuxConverter::fitI(vector<double> xValues, vector<double> yValues, double& ka, double& kb, double& kc)
{
    int N=xValues.size();
    const int n = 2; //degree of polynomial
    vector<double> X;                        //Array that will store the values of sigma(xi),sigma(xi^2),sigma(xi^3)....sigma(xi^2n)
    X.resize(2 * n + 1);
    for (int i = 0; i < 2 * n + 1; i++)
    {
        X[i] = 0;
        for (int j = 0; j < N; j++)
            X[i] = X[i] + pow(xValues[j], i);        //consecutive positions of the array will store N,sigma(xi),sigma(xi^2),sigma(xi^3)....sigma(xi^2n)
    }
    double B[n + 1][n + 2], a[n + 1];            //B is the Normal matrix(augmented) that will store the equations, 'a' is for value of the final coefficients
    for (int i = 0; i <= n; i++)
        for (int j = 0; j <= n; j++)
            B[i][j] = X[i + j];            //Build the Normal matrix by storing the corresponding coefficients at the right positions except the last column of the matrix
    double Y[n + 1];                    //Array to store the values of sigma(yi),sigma(xi*yi),sigma(xi^2*yi)...sigma(xi^n*yi)
    for (int i = 0; i < n + 1; i++)
    {
        Y[i] = 0;
        for (int j = 0; j < N; j++)
            Y[i] = Y[i] + pow(xValues[j], i) * yValues[j];        //consecutive positions will store sigma(yi),sigma(xi*yi),sigma(xi^2*yi)...sigma(xi^n*yi)
    }
    for (int i = 0; i <= n; i++)
        B[i][n + 1] = Y[i];                //load the values of Y as the last column of B(Normal Matrix but augmented)
   
    int nn = n + 1;                //n is made n+1 because the Gaussian Elimination part below was for n equations, but here n is the degree of polynomial and for n degree we get n+1 equations
    for (int i = 0; i < nn; i++)                    //From now Gaussian Elimination starts(can be ignored) to solve the set of linear equations (Pivotisation)
        for (int k = i + 1; k < nn; k++)
            if (B[i][i] < B[k][i])
                for (int j = 0; j <= nn; j++)
                {
                    double temp = B[i][j];
                    B[i][j] = B[k][j];
                    B[k][j] = temp;
                }

    for (int i = 0; i < nn - 1; i++)            //loop to perform the gauss elimination
        for (int k = i + 1; k < nn; k++)
        {
            double t = B[k][i] / B[i][i];
            for (int j = 0; j <= nn; j++)
                B[k][j] = B[k][j] - t * B[i][j];    //make the elements below the pivot elements equal to zero or elimnate the variables
        }
    for (int i = nn - 1; i >= 0; i--)                //back-substitution
    {                           //x is an array whose values correspond to the values of x,y,z..
        a[i] = B[i][nn];                //make the variable to be calculated equal to the rhs of the last equation
        for (int j = 0; j < nn; j++)
            if (j != i)            //then subtract all the lhs values except the coefficient of the variable whose value                                   is being calculated
                a[i] = a[i] - B[i][j] * a[j];
        a[i] = a[i] / B[i][i];            //now finally divide the rhs by the coefficient of the variable to be calculated
    }

    cout << "\nHence the fitted Polynomial is given by:\ny=";
    for (int i = 0; i < nn; i++)
        cout << " + (" << a[i] << ")" << "x^" << i;
    cout << endl;

    ka = a[2];
    kb = a[1];
    kc = a[0];

//    for (int i = 0; i < 255; i += 2)
//       cout << i << ";" << luxToGrayScale(i) << endl;
}

void LuxConverter::fit()
{
    vector<double> xs, ys, xb, yb;
    for (int i = 0; i < x.size(); i++)
        if (x[i] < 64)
        {
            xs.push_back(x[i]);
            ys.push_back(y[i]);
        }
        else
        {
            xb.push_back(x[i]);
            yb.push_back(y[i]);
        }
    fitI(xs, ys, k_a_s, k_b_s, k_c_s);
    fitI(xb, yb, k_a_b, k_b_b, k_c_b);
}

std::vector<std::string>
resplitstring(const std::string& s, std::string rgx_str = "<SP>") {


	std::vector<std::string> elems;

	std::regex rgx(rgx_str);

	std::sregex_token_iterator iter(s.begin(), s.end(), rgx, -1);
	std::sregex_token_iterator end;

	while (iter != end) {
		elems.push_back(*iter);
		++iter;
	}

	return elems;

}

void LuxConverter::loadTable(string filename)
{
    ifstream datfl;
    std::string datfilename = PathToExecutable;
    datfilename.append("\\"+ filename);
    datfl.open(datfilename);
    string ld;
    int ln = 0;
    while (getline(datfl, ld))
    {
        vector<string> params = resplitstring(ld, ";");
        pair<double, double> tmp;
        tmp.first = atof(params[0].c_str());
        tmp.second = atof(params[1].c_str());
        luxTable.push_back(tmp);
        ln++;
    }
}

vector<pair<double, double>> findClosestbyLux(vector<pair<double, double>> arr, double lux,
    int k = 2)
{
    vector<pair<double, double>> ret;
    priority_queue<pair<double, double> > pq;
    for (int i = 0; i < k; i++)
        pq.push({ abs(arr[i].second - lux), i });

    for (int i = k; i < arr.size(); i++) {

        int diff = abs(arr[i].second - lux);

        if (diff > pq.top().first)
            continue;

        pq.pop();
        pq.push({ diff, i });
    }

    // Print contents of heap.
    while (pq.empty() == false) {
//        cout << arr[pq.top().second].first << " = " << arr[pq.top().second].second << endl;
        ret.push_back({ arr[pq.top().second].first, arr[pq.top().second].second });
        pq.pop();
    }
    return ret;
}

vector<pair<double, double>> findClosestbyRGB(vector<pair<double, double>> arr, double rgb,
    int k = 2)
{
    vector<pair<double, double>> ret;
    priority_queue<pair<double, double> > pq;
    for (int i = 0; i < k; i++)
        pq.push({ abs(arr[i].first - rgb), i });

    for (int i = k; i < arr.size(); i++) {

        int diff = abs(arr[i].first - rgb);

        if (diff > pq.top().first)
            continue;

        pq.pop();
        pq.push({ diff, i });
    }

    // Print contents of heap.
    while (pq.empty() == false) {
//        cout << arr[pq.top().second].first << " = " << arr[pq.top().second].second << endl;
        ret.push_back({ arr[pq.top().second].first, arr[pq.top().second].second });
        pq.pop();
    }
    return ret;
}

double interpolateByRGB(double givenRGB, vector<pair<double, double>> refValues)
{
    struct {
        bool operator()(pair<double, double> a, pair<double, double> b) const { return a.first < b.first; }
    } sortByLuxAsc;
    std::sort(refValues.begin(), refValues.end(), sortByLuxAsc);
    double ret = refValues[0].second + (givenRGB - refValues[0].first) * ((refValues[1].second - refValues[0].second) / (refValues[1].first - refValues[0].first));
    return ret;
}

double LuxConverter::luxToGrayScaleTable(double lux)
{


};

double LuxConverter::luxToGrayScaleBig(double lux)
{
    double a = k_a_b, b = k_b_b, c = k_c_b - lux;
    double discriminant = b * b - 4 * a * c;
    if (discriminant >= 0) {
        double x1 = (-b + sqrt(discriminant)) / (2 * a);
        double x2 = (-b - sqrt(discriminant)) / (2 * a);
        return max(x1, x2) / 255.;
    }
    else if (discriminant == 0) {
        double x1 = -b / (2 * a);
        return x1 / 255.;
    }
    else return 0;
};

double LuxConverter::luxToGrayScaleSmall(double lux)
{
    double a = k_a_s, b = k_b_s, c = k_c_s - lux;
    double discriminant = b * b - 4 * a * c;
    if (discriminant >= 0) {
        double x1 = (-b + sqrt(discriminant)) / (2 * a);
        double x2 = (-b - sqrt(discriminant)) / (2 * a);
        return max(x1, x2) / 255.;
    }
    else if (discriminant == 0) {
        double x1 = -b / (2 * a);
        return x1 / 255.;
    }
    else return 0;
};

double LuxConverter::luxToGrayScale(double lux)
{
    for (int i = 0; i < luxTable.size(); i++)
        if (luxTable[i].second == lux)
            return luxTable[i].first/255.;
    vector<pair<double, double>> refValues = findClosestbyLux(luxTable, lux);
    struct {
        bool operator()(pair<double, double> a, pair<double, double> b) const { return a.second < b.second; }
    } sortByLuxAsc;
    std::sort(refValues.begin(), refValues.end(), sortByLuxAsc);
    double ret = refValues[0].first + (lux - refValues[0].second) * ((refValues[1].first - refValues[0].first) / (refValues[1].second - refValues[0].second));
    cout << lux << "lux -> " << ret << "rgb" << endl;
    return ret/255.;

//    double s = luxToGrayScaleSmall(lux);
//    double b = luxToGrayScaleBig(lux);
//    if ((s < 0.25 && b < 0.25)||b==0)
//        return s;
//    else
//        return b;
};

double LuxConverter::grayScaleToLux(double grayScale)
{
//    if (grayScale < 64)
//        return k_a_s * pow(grayScale, 2) + k_b_s * grayScale + k_c_s;
//    else
//        return k_a_b * pow(grayScale, 2) + k_b_b * grayScale + k_c_b;

    for (int i = 0; i < luxTable.size(); i++)
        if (luxTable[i].first == grayScale)
            return luxTable[i].second;
    double ret = interpolateByRGB(grayScale, findClosestbyRGB(luxTable, grayScale));
    cout << grayScale << "rgb -> " << ret << "lux" << endl;
    return ret;
};
