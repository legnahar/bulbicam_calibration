#pragma once
#include <fstream>
#include <chrono>
#include <filesystem>
#include <mutex>
#include "Tests.h"

extern CPTGCamera* ptgreycam;
extern CTest* currentTest;
extern atomic<bool> g_calibrationIsGoing;
extern atomic<bool> g_calibrationForcedStop;
extern int offset;

#define SCREENTEST_VERSION "1.0.20221027"

class ScreenTest : public CTest
{
public:
	ScreenTest(uint distance);
	void inCaseOfCalibration();
	void finishCalibration(bool forced);

private:
	string m_calibrationPath;
	string m_resourcesPath;
	ofstream m_history;
	uint m_distance;
};

ScreenTest::ScreenTest(uint distance) : m_distance(distance)
{
	g_calibrationIsGoing = true;
	display.stimuliType = DisplayStimuliType::STIMULI;
	cout << "Calibration is executing. ScreenTest module. Distance = " << distance << "m" << endl;

	char path[MAX_PATH];
	HMODULE hModule = GetModuleHandleA(NULL);
	GetModuleFileNameA(hModule, path, MAX_PATH);
	PathRemoveFileSpecA(path);
	m_calibrationPath = path;
	m_calibrationPath += "\\calibration";
	m_resourcesPath = m_calibrationPath + "\\resources";
}

void ScreenTest::inCaseOfCalibration()
{
	while (!g_calibrationForcedStop)
	{
		Mat l_leftTexture = imread(m_resourcesPath + "\\" + to_string(m_distance) + "m.jpg");
		Mat l_resized;
		resize(l_leftTexture, l_resized, Size(1920, 1080));
		Mat l_rightTexture;
		flip(l_resized, l_rightTexture, RotateFlags::ROTATE_180);
		display.setTextures(l_resized, l_rightTexture);
		display.redrawPending = true;
	}

	finishCalibration(false);
}

void ScreenTest::finishCalibration(bool forced)
{
	m_history.open(m_resourcesPath + "\\history.txt", ios_base::app);
	if (!m_history.is_open() || forced)
	{
		cout << "ERROR: Cannot open history.txt file or forced stop" << endl;
	}
	else
	{
		uint64_t l_stopTime = std::chrono::system_clock::now().time_since_epoch().count();
		m_history << "ScreenTest;" << l_stopTime << ";" << to_string(m_distance) << "m" << endl;
		m_history.close();
	}

	cout << "Calibration is finished. ScreenTest module" << endl;
}