//#define EMULATECAMERA

//#define ALVICAMERA
#define VERSION "0.90.20221019"
#include <WinSock2.h>

#include <iostream>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include "ptgcamera.h"
#include <thread>
#include <chrono>
#include <mutex>
#include <atomic>
#include "WebCommunicate.h"
#include <nlohmann\json.hpp>
#include "Configuration.h"
#include "eyetracking.h"
#include "display.h"

//#include "tests.h"
//#include "tests.h"
#include <regex>
#include "gpu_eyetracking.h"

#include "amsler.h"
#include "antisaccade.h"
#include "memorysaccade.h"
#include "acodapt.h"
#include "nystagmus.h"
#include "fixation.h"
#include "pupiltest2.h"
#include "pupiltest4.h"
#include "recordingtool.h"
#include "vergence.h"
#include "visualfield.h"
#include "w4d.h"
#include "pursuits.h"
#include "saccades.h"
#include "ptosis.h"
#include "calibration_lux.h"
#include "luxconverter.h"
#include "oculomotor.h"

#include "techstaff_info.h"

#include <Shlwapi.h>
#include "comdef.h"
#include <boost/lockfree/stack.hpp>
#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/archive/iterators/transform_width.hpp>
//#include <boost/algorithm/string.hpp>

#pragma region VLV_CALIBRATION_TESTS

#include "ScreenTest.h"
#include "SensorTest.h"
#include "TimingTest.h"
#include "VPosTest.h"
#include "PatternTest.h"

#pragma endregion VLV_CALIBRATION_TESTS

struct lux
{
	int os, od;
	double lux;
};

float p4_sequenceDuration;

string communicationLogsPath;
string picturesLogsPath;
string testLogsPath;
string testVideosPath;
string recToolVideosPath;
string qualityLogsPath;
string screenshotsPath;


//AMD PARAMETERS
float amd_acuityInitBGClrR;
float amd_acuityInitBGClrG;
float amd_acuityInitBGClrB;
int amd_trackNSEye;

int amd_contrastPxNumber;
float amd_contrastBGClrR;
float amd_contrastBGClrG;
float amd_contrastBGClrB;

float amd_darkadaptationBleachDurationFrames;

float amd_darkadaptationFixedContrastClr[3];
float amd_darkadaptationFixedFrequencyFrequency;

float amd_medialLimit;
float amd_lateralLimit;
float amd_topLimit;
float amd_bottomLimit;

float amd_radius;
int amd_speed;

float amd_initNSEyeBGClrR;
float amd_initNSEyeBGClrG;
float amd_initNSEyeBGClrB;

int vf_greenFixationFixedCount;
int vf_greenFixationRandomCount;
float vf_greenFixationRadius;
int vf_whiteFixationCount;
float vf_whiteFixationRadius;
float vf_srtRadius;
float st_targetRadius;
float st_memoryT4Time;
int st_antisaccadeShowCentralAlways;
int st_prosaccadeShowCentralAlways;

float pas_calibrationThreshold;

std::chrono::time_point<std::chrono::system_clock> starttesttime;
std::chrono::time_point<std::chrono::system_clock> lastBlink;
std::chrono::time_point<std::chrono::system_clock> ptosisTimer;

bool picBP;
bool canceltraining;
int vftrainresults[8];
ofstream qualityLog;

uint64_t camtsGlints = 0;
uint64_t camtsPupils = 0;
double gltsGlints = 0.;
double gltsPupils = 0.;

string PathToExecutable;
patientInformation patientInfo;

atomic<bool> allowPull = false;

void dataRead2(vector<pair<string, string>> commands, bool fromPreassesment);
vector<pair<string, string>> savedCommands;

ofstream testLog;
vector<frameForAI> savedFrames; 
bool saveFrameSignal;
int savedFramesCount;
int savedFramesCurrentID;

int et_round[2] = { 0 };

#ifdef EMULATECAMERA
VideoCapture emVC;
#endif

#ifndef ALVICAMERA
CPTGCamera* ptgreycam;
#else
CAVCamera* avcam;
#endif

CDisplay display;
int exposureTime;

CTest* currentTest;
LuxConverter luxConverter[2];

int offset;

EyeTracker et;
ETInitStructure etis;
WebCommunicate* webComm;
Configuration config;
Lens* lensDist;
std::string versionInfoForHub;

float pxToDegreeOS, pxToDegreeOD;

float oslens = 0, odlens = 0;
//int patientAge;

bool eyetrackingOn;
bool showQuality = true;

bool recbpinverted = true;

std::chrono::time_point<std::chrono::system_clock> calib_prevts;
int maxpixg = -1;

int same_bp = 0, same_dp = 0;
bool start_calc = false;
bool exposureTestDone = false;
bool waitingForETTest = false;
bool preassesmentDone = false;

float shapeAngleOS, shapeAngleOD;
int shapePosOSx, shapePosOSy;
int shapePosODx, shapePosODy;

float pxtommrat;

string lastFoundValue;

std::chrono::time_point<std::chrono::system_clock> fps_timer;

atomic<bool> ptosis_pic1sent, ptosis_pic2sent, ptosis_pic3sent, ptosis_needpic, ptosis_waitingForBlink;

using namespace boost::lockfree;

deque<etQueue> smallEThistoryData;
vector<ETResult> entireEThistoryData;
boost::lockfree::stack<camFrame, boost::lockfree::capacity<100>> frame_queue;
boost::lockfree::stack<gtQueue, boost::lockfree::capacity<1>> gt_queue;
boost::lockfree::stack<etQueue, boost::lockfree::capacity<1>> et_queue;
boost::lockfree::stack<ETResult, boost::lockfree::capacity<100>> filled_et_data;
int last_fixed_index = 0;
bool dilated = false;
bool needDilatedSizeCalculation = false;
bool historyCleared = false;
float dilated_diam[2] = { 0,0 };

#pragma region VLV_CALIBRATION_TESTS

atomic<bool> g_calibrationIsGoing = false;
extern atomic<bool> g_calibrationForcedStop = false;

#pragma endregion VLV_CALIBRATION_TESTS

void openQualityLog()
{
	std::ostringstream oss;
	oss << qualityLogsPath << patientInfo.examID << "_" << std::chrono::system_clock::now().time_since_epoch().count() << ".csv";
	qualityLog.open(oss.str());
}


inline bool comma(char c) {
	return c == ',';
}

inline bool notcomma(char c) {
	return c != ',';
}

std::vector<std::string> splitCommas(const std::string& s)
{
	typedef std::string::const_iterator iter;
	std::vector<std::string> ret;
	iter i = s.begin();
	while (i != s.end())
	{
		i = std::find_if(i, s.end(), notcomma);
		iter j = std::find_if(i, s.end(), comma);
		if (i != s.end())
		{
			ret.push_back(std::string(i, j));
			i = j;
		}
	}
	return ret;
}

vector<nystagmusPoints> SplitNystagmusString(std::string& str)
{
	size_t first, last = 0;
	//	str.erase(std::remove(str.begin(), str.end(), ' '), str.end());
	str.erase(std::remove(str.begin(), str.end(), '\"'), str.end());
	vector<vector<string>> pointParams;
	while (last < str.length() - 2)
	{
		first = str.find_first_of('{', last);
		last = str.find_first_of('}', first);
		pointParams.push_back(splitCommas(str.substr(first + 1, last - first - 1)));
	}

	for (int i = 0; i < pointParams.size(); i++)
		for (int j = 0; j < pointParams[i].size(); j++)
		{
			first = pointParams[i][j].find_first_of(':');

		}
	vector<nystagmusPoints> ret;

	for (int i = 0; i < pointParams.size(); i++)
	{
		nystagmusPoints tmp;
		tmp.nType = 0;
		tmp.name = "NONAME";
		tmp.os.x = 777;
		tmp.os.y = 777;
		tmp.od.x = 777;
		tmp.od.y = 777;
		tmp.nType = 0;
		tmp.large = 0;
		for (int j = 0; j < pointParams[i].size(); j++)
		{
			first = pointParams[i][j].find_first_of(':');
			string name = pointParams[i][j].substr(0, first);
			string val = pointParams[i][j].substr(first + 1, pointParams[i][j].length() - first - 1);
			if (name == "name")
				tmp.name = val;
			if (name == "xos")
				tmp.os.x = atof(val.c_str());
			if (name == "xod")
				tmp.od.x = atof(val.c_str());
			if (name == "yos")
				tmp.os.y = atof(val.c_str());
			if (name == "yod")
				tmp.od.y = atof(val.c_str());
			if (name == "stimulus")
				tmp.nType = atoi(val.c_str());
			if (name == "large")
				tmp.large = atoi(val.c_str());
		}
		ret.push_back(tmp);
	}
	return ret;
}

vector<nystagmusPoints> SplitNystagmusJSONString(std::string& str)
{
	vector<nystagmusPoints> ret;
	int last = 0, first = 0;
	vector<pair<string, string>> tmpPairs;
	str.erase(std::remove(str.begin(), str.end(), '\"'), str.end());
	str.erase(std::remove(str.begin(), str.end(), '['), str.end());
	str.erase(std::remove(str.begin(), str.end(), ']'), str.end());
	vector<vector<string>> pointParams;
	while (last < str.length() - 2)
	{
		first = str.find_first_of('{', last);
		last = str.find_first_of('}', first);
		pointParams.push_back(resplitstring(str.substr(first + 1, last - first - 1), ","));
	}

	for (int i = 0; i < pointParams.size(); i++)
	{
		nystagmusPoints tmp;
		tmp.nType = 0;
		tmp.name = "NONAME";
		tmp.os.x = 777;
		tmp.os.y = 777;
		tmp.od.x = 777;
		tmp.od.y = 777;
		tmp.nType = 0;
		tmp.large = 0;
		for (int j = 0; j < pointParams[i].size(); j++)
		{
			first = pointParams[i][j].find_first_of(':');
			string name = pointParams[i][j].substr(0, first);
			string val = pointParams[i][j].substr(first + 1, pointParams[i][j].length() - first - 1);
			if (name == "name")
				tmp.name = val;
			if (name == "xos")
				tmp.os.x = atof(val.c_str());
			if (name == "xod")
				tmp.od.x = atof(val.c_str());
			if (name == "yos")
				tmp.os.y = atof(val.c_str());
			if (name == "yod")
				tmp.od.y = atof(val.c_str());
			if (name == "stimulus")
				tmp.nType = atoi(val.c_str());
			if (name == "large")
				tmp.large = atoi(val.c_str());
		}
		ret.push_back(tmp);
	}
	return ret;
}

vector<lux> SplitLUXJSONString(std::string& str)
{
	vector<lux> ret;
	int last = 0, first = 0;
	vector<pair<string, string>> tmpPairs;
	str.erase(std::remove(str.begin(), str.end(), '\"'), str.end());
	str.erase(std::remove(str.begin(), str.end(), '['), str.end());
	str.erase(std::remove(str.begin(), str.end(), ']'), str.end());
	vector<vector<string>> pointParams;
	while (last < str.length() - 2)
	{
		first = str.find_first_of('{', last);
		last = str.find_first_of('}', first);
		pointParams.push_back(resplitstring(str.substr(first + 1, last - first - 1), ","));
	}

	for (int i = 0; i < pointParams.size(); i++)
	{
		lux tmp;
		tmp.os = 0;
		tmp.od = 0;
		tmp.lux = 0;
		for (int j = 0; j < pointParams[i].size(); j++)
		{
			first = pointParams[i][j].find_first_of(':');
			string name = pointParams[i][j].substr(0, first);
			string val = pointParams[i][j].substr(first + 1, pointParams[i][j].length() - first - 1);
			if (name == "OS")
				tmp.os = atof(val.c_str());
			if (name == "OD")
				tmp.od = atof(val.c_str());
			if (name == "lux_value")
				tmp.lux = atof(val.c_str());
		}
		ret.push_back(tmp);
	}
	return ret;
}

void SplitVergenceString(std::string& str)
{
	size_t first, last = 0;
	str.erase(std::remove(str.begin(), str.end(), '\"'), str.end());
	vector<vector<string>> pointParams;
	while (last < str.length() - 2)
	{
		first = str.find_first_of('{', last);
		last = str.find_first_of('}', first);
		pointParams.push_back(splitCommas(str.substr(first + 1, last - first - 1)));
	}

	for (int i = 0; i < pointParams.size(); i++)
		for (int j = 0; j < pointParams[i].size(); j++)
		{
			first = pointParams[i][j].find_first_of(':');

		}
	int tam, ttp;
	for (int i = 0; i < pointParams.size(); i++)
	{
		for (int j = 0; j < pointParams[i].size(); j++)
		{
			first = pointParams[i][j].find_first_of(':');
			string name = pointParams[i][j].substr(0, first);
			string val = pointParams[i][j].substr(first + 1, pointParams[i][j].length() - first - 1);
			if (name == "type")
				if (val == "Pediatric")
					ttp = 0;
				else
					ttp = -1;
			if (name == "trials")
				tam = atoi(val.c_str());
		}
	}
	currentTest = new CVergenceTest(64, maxpixg, ttp, tam);
	display.stimuliType = DisplayStimuliType::STIMULI;
	SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
	currentTest->testType = "CONVERGENCE";
}


void startTest(int testnumber)
{}

bool findCommand(vector<pair<string, string>> commands, string comtofind)
{
	bool ret = false;
	for (int i = 0; i < commands.size(); i++)
	{
		if (commands[i].first == comtofind)
		{
			ret = true;
			if (commands[i].second == "N/A" || commands[i].second == "NaN")
				lastFoundValue = "0";
			else
			{
				lastFoundValue = commands[i].second;
			}
		}
	}
	return ret;
}

void dataRead2(vector<pair<string, string>> commands, bool fromPreassesment)
{
	if (!currentTest)
	{
		if (findCommand(commands, "EXAMID"))
		{
			webComm->sendmes("OK");
			if (patientInfo.examID != lastFoundValue)
			{
				patientInfo.examID = lastFoundValue;
#ifndef EMULATECAMERA
#ifndef ALVICAMERA
				if (ptgreycam->sn != "17391407")
					preassesmentDone = false;

#endif // !ALVICAMERA
#endif // !EMULATECAMERA

			}
		}
		if (findCommand(commands, "IS_DILATED"))
		{
			//			cout << "\"" << lastFoundValue << "\"" << endl;
			if (lastFoundValue == "true")
			{
				cout << "DILATED EYETRACKING SWITCHED ON\n";
//				patientInfo.dilated = true;
				dilated = true;
				needDilatedSizeCalculation = true;
				historyCleared = false;
				dilated_diam[0] = 0;
				dilated_diam[1] = 0;
			}
			else
			{
				cout << "DILATED EYETRACKING SWITCHED OFF\n";
				dilated = false;
				needDilatedSizeCalculation = false;
//				patientInfo.dilated = false;
			}
		}
		if (findCommand(commands, "AGE"))
		{
			patientInfo.age = atoi(lastFoundValue.c_str());
		}
		if (findCommand(commands, "DIAGNOSIS"))
		{
			patientInfo.diagnosis = atoi(lastFoundValue.c_str());
		}
		if (findCommand(commands, "LENSOD"))
		{
			odlens = -atof(lastFoundValue.c_str());
			lensDist->dioptersOD = odlens;
		}
		if (findCommand(commands, "LENSOS"))
		{
			oslens = -atof(lastFoundValue.c_str());
			lensDist->dioptersOS = oslens;
		}
		if (findCommand(commands, "LH"))
		{
			lensDist->lensHolder = atoi(lastFoundValue.c_str());
		}
		if (findCommand(commands, "LUX_CALIBRATION"))
		{
			luxConverter[0].x.clear();
			luxConverter[0].y.clear();
			luxConverter[1].x.clear();
			luxConverter[1].y.clear();
			vector<lux> tmp = SplitLUXJSONString(lastFoundValue);
			for (int i = 0; i < tmp.size(); i++)
			{
				if (tmp[i].os == 0 && tmp[i].od == 0)
				{
					luxConverter[0].x.push_back(tmp[i].od);
					luxConverter[0].y.push_back(tmp[i].lux);

					luxConverter[1].x.push_back(tmp[i].os);
					luxConverter[1].y.push_back(tmp[i].lux);
				}
				else if (tmp[i].os > 0)
				{
					luxConverter[1].x.push_back(tmp[i].os);
					luxConverter[1].y.push_back(tmp[i].lux);
				}
				else if (tmp[i].od > 0)
				{
					luxConverter[0].x.push_back(tmp[i].od);
					luxConverter[0].y.push_back(tmp[i].lux);
				}
			}
//			luxConverter[0].loadTable("luxtableod.csv");
//			luxConverter[1].loadTable("luxtableos.csv");
//			luxConverter[0].fit();
//			luxConverter[0].plot("fOS.png");
//			luxConverter[1].fit();
//			luxConverter[1].plot("fOD.png");
		}
		if (findCommand(commands, "PPD"))
		{
			lensDist->PPD = atoi(lastFoundValue.c_str());
#ifndef EMULATECAMERA
#ifndef ALVICAMERA
			if (ptgreycam->sn != "17391407")
				preassesmentDone = false;
#else
			avcam->setOffset(140, ((int)((shapePosOSy + shapePosODy) / 2 - 200 + (60 - (lensDist->PPD - 12.6)) * pxtommrat / 2) / 2) * 2);

#endif 
#endif
			et.offset.height = ((int)((shapePosOSy + shapePosODy) / 2 - 200 + (60 - (lensDist->PPD - 12.6)) * pxtommrat / 2) / 2) * 2;
		}
	}
	else
	{
		webComm->sendmes("OK");
	}

	if (findCommand(commands, "command"))
	{
		if (lastFoundValue == "START")
		{
			if (!currentTest)
			{
				if (!fromPreassesment)
				{

				}

				if (findCommand(commands, "testType"))
				{
					openQualityLog();
					if (lastFoundValue == "AMSLER_GRID_TEST")
					{
						webComm->sendmes("OK");
						currentTest = new CAmslerTest();
						display.stimuliType = DisplayStimuliType::AMSLER;
						currentTest->testType = "AMSLER_GRID_TEST";
					}
					if (lastFoundValue == "FIXATION_TEST")
					{
						int testLength = 0;
						int breakLength = 0;
						int repetitions = 1;
						if (findCommand(commands, "testLength"))
							testLength = atof(lastFoundValue.c_str());
						if (findCommand(commands, "breakLength"))
							breakLength = atof(lastFoundValue.c_str());
						if (findCommand(commands, "repetitions"))
							repetitions = atoi(lastFoundValue.c_str());
						display.st_redTargetColor[0] = 1;
						display.st_redTargetColor[1] = 0;
						display.st_redTargetColor[2] = 0;
						currentTest = new CFixTest(1, testLength, breakLength, repetitions);
						display.stimuliType = DisplayStimuliType::SACCADETASK;
						SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
						currentTest->testType = "FIXATION_TEST";
					}
					if (lastFoundValue == "SACCADE_MERGED")
					{
						int testedEye = 0;
						int trainingDots = 0;
						int testDots = 0;
						if (findCommand(commands, "oculus"))
						{
							if (lastFoundValue == "OS")
								testedEye = 1;
							else if (lastFoundValue == "OD")
								testedEye = 0;
						}
						if (findCommand(commands, "training"))
							trainingDots = atoi(lastFoundValue.c_str());
						if (findCommand(commands, "stimuli"))
							testDots = atoi(lastFoundValue.c_str());

						if (findCommand(commands, "type"))
							if (lastFoundValue == "memory")
							{
								currentTest = new CMSTest(testedEye, trainingDots, testDots);
								display.st_redTargetColor[0] = 1;
								display.st_redTargetColor[1] = 0;
								display.st_redTargetColor[2] = 0;
								display.stimuliType = DisplayStimuliType::SACCADETASK;
								SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
								currentTest->testType = "SACCADE_MERGED";
							}
							else if (lastFoundValue == "pro")
							{
								display.st_redTargetColor[0] = 0;
								display.st_redTargetColor[1] = 1;
								display.st_redTargetColor[2] = 0;
								currentTest = new CASTest(testedEye, trainingDots, testDots, "PROSACCADE");
								display.stimuliType = DisplayStimuliType::SACCADETASK;
								SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
								currentTest->testType = "SACCADE_MERGED";

							}
							else if (lastFoundValue == "anti")
							{
								display.st_redTargetColor[0] = 1;
								display.st_redTargetColor[1] = 0;
								display.st_redTargetColor[2] = 0;
								currentTest = new CASTest(testedEye, trainingDots, testDots, "ANTISACCADE");
								display.stimuliType = DisplayStimuliType::SACCADETASK;
								SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
								currentTest->testType = "SACCADE_MERGED";

							}
					}
					if (lastFoundValue == "CONVERGENCE")
					{
						int ttp = -1;
						int tam = 1;
						if (findCommand(commands, "type"))
							if (lastFoundValue == "Pediatric")
								ttp = 0;
							else
								ttp = -1;
						if (findCommand(commands, "trials"))
							tam = atoi(lastFoundValue.c_str());
						currentTest = new CVergenceTest(64, maxpixg, ttp, tam);
						display.stimuliType = DisplayStimuliType::STIMULI;
						SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
						currentTest->testType = "CONVERGENCE";
					}
					if (lastFoundValue == "RECORDING_TOOL")
					{
						patientInfo.diagnosis = "nan";
						if (findCommand(commands, "IDC"))
							if (lastFoundValue == "")
								patientInfo.diagnosis = "nan";
							else
								patientInfo.diagnosis = lastFoundValue;
						currentTest = new RecordingTool();
						SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
						currentTest->testType = "RECORDING_TOOL";
					}
					if (lastFoundValue == "FUNCTIONAL_SCREENING")
					{
						int testedEye = 0;
						if (findCommand(commands, "oculus"))
						{
							if (lastFoundValue == "OS")
								testedEye = 1;
							else if (lastFoundValue == "OD")
								testedEye = 0;
						}
						currentTest = new COculoMotorTest(testedEye);
						savedFramesCount = 0;
						SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
						currentTest->testType = "FUNCTIONAL_SCREENING";
					}
					if (lastFoundValue == "DARK_ADAPTAION_AMD")
					{
						bool testAcuity = true, testContrast = true, testFF = true, testFC = true, testOS = true, testOD = true;
						if (findCommand(commands, "visualAcuity"))
							if (lastFoundValue == "false")
								testAcuity = false;
						if (findCommand(commands, "contrastWithoutBleach"))
							if (lastFoundValue == "false")
								testContrast = false;
						if (findCommand(commands, "bleachRecovery"))
							if (lastFoundValue == "false")
								testFC = false;
						if (findCommand(commands, "contrastWithBleach"))
							if (lastFoundValue == "false")
								testFF = false;
						if (findCommand(commands, "target"))
						{
							if (lastFoundValue == "OS")
							{
								testOS = true;
								testOD = false;
							}
							else if (lastFoundValue == "OD")
							{
								testOS = false;
								testOD = true;
							}
							else if (lastFoundValue == "OU")
							{
								testOS = true;
								testOD = true;
							}
						}
						currentTest = new CAMDTest(testAcuity, testContrast, testFF, testFC, testOD, testOS);
						SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
						currentTest->testType = "DARK_ADAPTAION_AMD";
					}
					if (lastFoundValue == "NYSTAGMUS_EVALUATION")
					{
						webComm->sendmes("OK");
						int testDuration = 4;
						if (findCommand(commands, "lengthInSeconds"))
							testDuration = atoi(lastFoundValue.c_str());
						findCommand(commands, "config");
						vector<nystagmusPoints> nparr = SplitNystagmusJSONString(lastFoundValue);
						currentTest = new CNystagmusTest(nparr, testDuration);
						currentTest->testType = "NYSTAGMUS_EVALUATION";
						display.stimuliType = DisplayStimuliType::NYSTAGMUS;
					}
					if (lastFoundValue == "PUPILLARY_EVALUATION2")
					{
						int seqnum = 2;
						if (findCommand(commands, "xSequences"))
							seqnum = atoi(lastFoundValue.c_str());
						currentTest = new CPupil2Test(false, false);
						savedFramesCurrentID = 0;
						currentTest->testType = "PUPILLARY_EVALUATION2";
						display.stimuliType = DisplayStimuliType::STIMULI;
					}
					if (lastFoundValue == "PUPILLARY_EVALUATION3")
					{
						savedFramesCurrentID = 0;
						if (findCommand(commands, "oculus"))
						{
							if (lastFoundValue == "OD")
								currentTest = new CPupil2Test(true, false);
							if (lastFoundValue == "OS")
								currentTest = new CPupil2Test(true, false);
							if (lastFoundValue == "OU")
								currentTest = new CPupil2Test(true, false);
							currentTest->testType = "PUPILLARY_EVALUATION3";
							display.stimuliType = DisplayStimuliType::STIMULI;
						}
					}
					if (lastFoundValue == "VISUAL_FIELD_MERGED")
					{
						int oculus;
						int diameter;
						int stimuliPositions;
						bool training;
						int bgr= 128, bgg= 128, bgb= 128;
						int str= 255, stg= 255, stb=255;
						int onsetType;
						float onsetTime;
						bool stFlickerStatus;
						float stFlickerFrequency;
						float onsetAdditionalWaitingLength;
						bool onsetAdditionalWaitingStatus;
						bool greenDotAnimationStatus;
						float greenDotAnimationConcentric;
						bool animateColor, animatePos, animateSize, animateLetter;
						float fullIntensity=-1,bgIntensity=-1;

						findCommand(commands, "stimuliPositions");
						stimuliPositions = atoi(lastFoundValue.c_str());
						findCommand(commands, "oculus");
						if (lastFoundValue == "OD")
							oculus = 0;
						if (lastFoundValue == "OS")
							oculus = 1;
						if (lastFoundValue == "OU")
							oculus = 2;

						findCommand(commands, "training");
						if (lastFoundValue == "true")
							training = true;
						if (lastFoundValue == "false")
							training = false;

						findCommand(commands, "diameter");
						diameter = atoi(lastFoundValue.c_str());

						if(findCommand(commands, "bgr"))
							bgr = atof(lastFoundValue.c_str());
						if (findCommand(commands, "bgg"))
							bgg = atof(lastFoundValue.c_str());
						if (findCommand(commands, "bgb"))
							bgb = atof(lastFoundValue.c_str());

						if (findCommand(commands, "str"))
							str = atof(lastFoundValue.c_str());
						if (findCommand(commands, "stg"))
							stg = atof(lastFoundValue.c_str());
						if (findCommand(commands, "stb"))
							stb = atof(lastFoundValue.c_str());

						findCommand(commands, "onsetType");
						if (lastFoundValue == "Instant")
							onsetType = 0;
						if (lastFoundValue == "Linear")
							onsetType = 1;
						if (lastFoundValue == "Logarithmic")
							onsetType = 2;

						findCommand(commands, "onsetTime");
						onsetTime = atof(lastFoundValue.c_str());

						findCommand(commands, "onsetAdditionalWaitingStatus");
						if (lastFoundValue == "true")
							onsetAdditionalWaitingStatus = true;
						if (lastFoundValue == "false")
							onsetAdditionalWaitingStatus = false;

						findCommand(commands, "onsetAdditionalWaitingLength");
						onsetAdditionalWaitingLength = atof(lastFoundValue.c_str());

						findCommand(commands, "stFlickerStatus");
						if (lastFoundValue == "true")
							stFlickerStatus = true;
						if (lastFoundValue == "false")
							stFlickerStatus = false;

						findCommand(commands, "greenDotAnimationStatus");
						if (lastFoundValue == "true")
							greenDotAnimationStatus = true;
						if (lastFoundValue == "false")
							greenDotAnimationStatus = false;

						findCommand(commands, "greenDotAnimationConcentric");
						greenDotAnimationConcentric = atof(lastFoundValue.c_str()) / 1000.f;

						findCommand(commands, "color");
						if (lastFoundValue == "true")
							animateColor = true;
						if (lastFoundValue == "false")
							animateColor = false;

						findCommand(commands, "movement");
						if (lastFoundValue == "true")
							animatePos = true;
						if (lastFoundValue == "false")
							animatePos = false;

						findCommand(commands, "size");
						if (lastFoundValue == "true")
							animateSize = true;
						if (lastFoundValue == "false")
							animateSize = false;

						findCommand(commands, "reading");
						if (lastFoundValue == "true")
							animateLetter = true;
						if (lastFoundValue == "false")
							animateLetter = false;

						findCommand(commands, "stFlickerFrequency");
						stFlickerFrequency = atof(lastFoundValue.c_str());

						if(findCommand(commands, "backgroundLuxlevel"))
							bgIntensity = atof(lastFoundValue.c_str());
						if(findCommand(commands, "fullIntensityLuxLevel"))
							fullIntensity = atof(lastFoundValue.c_str());

						currentTest = new CVFMTest(oculus, stimuliPositions, training, diameter, bgr, bgg, bgb, str, stg, stb, onsetType, onsetTime, stFlickerStatus, stFlickerFrequency, greenDotAnimationConcentric, animateColor, animateSize, animatePos, animateLetter, greenDotAnimationConcentric, onsetAdditionalWaitingStatus, onsetAdditionalWaitingLength,fullIntensity,bgIntensity);
						display.stimuliType = DisplayStimuliType::STIMULI;
						starttesttime = std::chrono::system_clock::now();
						SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
						currentTest->testType = "VISUAL_FIELD_MERGED";
					}

					if (lastFoundValue == "PURSUITS_AND_SACCADES")
					{
						if (findCommand(commands, "target"))
						{
							if (lastFoundValue == "SMOOTH_PURSUIT")
								currentTest = new CPSTest();
							if (lastFoundValue == "SACCADES")
								currentTest = new CSaccadesTest();
							currentTest->testType = "PURSUITS_AND_SACCADES";
							display.stimuliType = DisplayStimuliType::STIMULI;
							starttesttime = std::chrono::system_clock::now();
							SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
						}
					}


					if (lastFoundValue == "WORTH_FOUR_DOT_TEST")
					{
						currentTest = new CW4DTest();
						currentTest->testType = "WORTH_FOUR_DOT_TEST";
						webComm->sendmes("OK");
					}

				}
				if (findCommand(commands, "test"))
				{
					if (lastFoundValue == "LUX_CALIBRATION")
					{
						int backOS = 0, backOD = 0;
						if (findCommand(commands, "BackgroundOS"))
							backOS = atoi(lastFoundValue.c_str());
						if (findCommand(commands, "BackgroundOD"))
							backOD = atoi(lastFoundValue.c_str());
						currentTest = new CCalibrationLux(backOD, backOS);
						currentTest->testType = "LUX_CALIBRATION";
						webComm->sendmes("OK");
					}

#pragma region VLV_CALIBRATION_TESTS

					if (lastFoundValue == "SCREEN_TEST")
					{
						g_calibrationForcedStop = false;

						int screen_test_dist = 0;
						if (findCommand(commands, "distance"))
							screen_test_dist = atoi(lastFoundValue.c_str());

						currentTest = new ScreenTest(screen_test_dist);
						//display.stimuliType = DisplayStimuliType::STIMULI;
						SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
						currentTest->testType = "SCREEN_TEST";
						std::thread(&calibrationProcessor).detach();
					}

					if (lastFoundValue == "PATTERN_TEST")
					{
						g_calibrationForcedStop = false;
						currentTest = new PatternTest();
						display.stimuliType = DisplayStimuliType::STIMULI;
						SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
						currentTest->testType = "PATTERN_TEST";
						std::thread(&calibrationProcessor).detach();
					}

					if (lastFoundValue == "VPOS_TEST")
					{
						g_calibrationForcedStop = false;
						currentTest = new VPosTest();
						display.stimuliType = DisplayStimuliType::STIMULI;
						SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
						currentTest->testType = "VPOS_TEST";
						std::thread(&calibrationProcessor).detach();
					}

					if (lastFoundValue == "SHORT_TIMING_TEST")
					{
						g_calibrationForcedStop = false;
						currentTest = new TimingTest(true);
						display.stimuliType = DisplayStimuliType::STIMULI;
						SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
						currentTest->testType = "SHORT_TIMING_TEST";
						std::thread(&calibrationProcessor).detach();
					}

					if (lastFoundValue == "LONG_TIMING_TEST")
					{
						g_calibrationForcedStop = false;
						currentTest = new TimingTest(false);
						display.stimuliType = DisplayStimuliType::STIMULI;
						SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
						currentTest->testType = "LONG_TIMING_TEST";
						std::thread(&calibrationProcessor).detach();
					}

					if (lastFoundValue == "SENSOR_TEST")
					{
						g_calibrationForcedStop = false;
						currentTest = new SensorTest();
						display.stimuliType = DisplayStimuliType::STIMULI;
						SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
						currentTest->testType = "SENSOR_TEST";
						std::thread(&calibrationProcessor).detach();
					}

#pragma endregion VLV_CALIBRATION_TESTS

				}
			}
		}
		else if (lastFoundValue == "STOP")
		{
			webComm->sendmes("OK");
			if (currentTest)
			{
				if (testLog.is_open())
					testLog.close();
				nlohmann::json m6;
				m6["chartTypeString"] = currentTest->testType;
				m6["message_type"] = MESSAGE_TYPE::STOP_TEST;
				m6["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
				webComm->sendJSON(m6);
				eyetrackingOn = false;
				display.setDragonfly();
				qualityLog.close();
				currentTest = nullptr;
				webComm->closeLog();

#pragma region VLV_CALIBRATION_TESTS

				g_calibrationForcedStop = true;

#pragma endregion VLV_CALIBRATION_TESTS

			}
		}
		else if (lastFoundValue == "STOP_TRAINING")
		{
			canceltraining = true;
		}
		else if (lastFoundValue == "GETSERIAL")
		{
#ifndef EMULATECAMERA
#ifndef ALVICAMERA
		nlohmann::json m6;
		m6["metadata"] = ptgreycam->sn;
		webComm->sendJSON(m6);
#else
		nlohmann::json m6;
		m6["metadata"] = avcam->sn;
		webComm->sendJSON(m6);
#endif // !EMULATECAMERA
#else
		nlohmann::json m6;
		m6["metadata"] = 0;
		webComm->sendJSON(m6);
#endif // !EMULATECAMERA

		}
		else
		{
			webComm->sendmes("OK");
			if (findCommand(commands, "testType"))
			{
				if (lastFoundValue == "WORTH_FOUR_DOT_TEST")
				{
					findCommand(commands, "command");
					if (currentTest)
					{
						if (currentTest->testType == "WORTH_FOUR_DOT_TEST")
						{
							CW4DTest* wdt = static_cast<CW4DTest*>(currentTest);
							if (lastFoundValue == "INFINITE")
							{
								wdt->w4d_distance = W4DD_FAR;
								wdt->w4d_type = W4DT_NORMAL;
								wdt->redraw();
							}
							if (lastFoundValue == "DEFINED")
							{
								wdt->w4d_distance = W4DD_CLOSE;
								wdt->w4d_type = W4DT_NORMAL;
								wdt->redraw();
							}
							if (lastFoundValue == "BACKGROUND")
							{
								wdt->switchBG();
							}
							if (lastFoundValue == "A")
							{
								wdt->w4d_type = W4DT_A;
								wdt->redraw();
							}
							if (lastFoundValue == "B")
							{
								wdt->w4d_type = W4DT_B;
								wdt->redraw();
							}
							if (lastFoundValue == "C")
							{
								wdt->w4d_type = W4DT_C;
								wdt->redraw();
							}
							if (lastFoundValue == "D")
							{
								wdt->w4d_type = W4DT_D;
								wdt->redraw();
							}
							if (lastFoundValue == "E")
							{
								wdt->w4d_type = W4DT_E;
								wdt->redraw();
							}
							if (lastFoundValue == "UNDEFINED")
							{
								wdt->w4d_type = W4DT_UNDEFINED;
								wdt->redraw();
							}
							if (lastFoundValue == "OS")
							{
								wdt->w4d_eye = W4DE_OS;
								wdt->redraw();
							}
							if (lastFoundValue == "OD")
							{
								wdt->w4d_eye = W4DE_OD;
								wdt->redraw();
							}
						}
					}

				}
				if (lastFoundValue == "AMSLER_GRID_TEST")
				{
					if (currentTest)
					{
						if (currentTest->testType == "AMSLER_GRID_TEST")
						{

							findCommand(commands, "command");
							CAmslerTest* amt = static_cast<CAmslerTest*>(currentTest);
							webComm->sendmes("OK");
							if (lastFoundValue == "FIXATIONDOT")
							{
								display.dotsz = !display.dotsz;
								display.redrawPending = true;
							}
							if (lastFoundValue == "TOGGLEAREAID")
							{
								display.redon = !display.redon;
								display.redrawPending = true;
							}
							if (lastFoundValue == "TOGGLEAREA")
							{
								findCommand(commands, "areaNumber");
								if (display.aminitpos[0] <= 1920)
									display.osblues[atoi(lastFoundValue.c_str()) - 1] = 1 - display.osblues[atoi(lastFoundValue.c_str()) - 1];
								else
									display.odblues[atoi(lastFoundValue.c_str()) - 1] = 1 - display.odblues[atoi(lastFoundValue.c_str()) - 1];
								display.redrawPending = true;
							}
							if (lastFoundValue == "SHOWGRID")
							{
								findCommand(commands, "oculus");
								if (lastFoundValue == "OFF")
								{
									display.jw = 0;
									display.dotsz = false;
									display.redon = false;
								}
								if (lastFoundValue == "OS")
								{
									display.jw = 1;
									display.dotsz = false;
									display.redon = false;
									display.aminitpos[0] = 960 + lensDist->mmtopix(lensDist->PPD * 0.25f);
									display.aminitpos[1] = 540;
								}
								if (lastFoundValue == "OD")
								{
									display.jw = 1;
									display.dotsz = false;
									display.redon = false;
									display.aminitpos[0] = 2880 - lensDist->mmtopix(lensDist->PPD * 0.25f);
									display.aminitpos[1] = 540;
								}
								display.redrawPending = true;
							}
						}
					}
				}
				if (lastFoundValue == "LUX_CALIBRATION")
				{
					if (currentTest)
					{
						int backOS = 0, backOD = 0;
						if (findCommand(commands, "BackgroundOS"))
							backOS = atoi(lastFoundValue.c_str());
						if (findCommand(commands, "BackgroundOD"))
							backOD = atoi(lastFoundValue.c_str());
						CCalibrationLux* clt = static_cast<CCalibrationLux*>(currentTest);
						clt->changeBackgroundColors(backOD, backOS);
					}
				}

			}
			if (findCommand(commands, "test"))
			{
				if (lastFoundValue == "LUX_CALIBRATION")
				{
					if (currentTest)
					{
						int backOS = 0, backOD = 0;
						if (findCommand(commands, "BackgroundOS"))
							backOS = atoi(lastFoundValue.c_str());
						if (findCommand(commands, "BackgroundOD"))
							backOD = atoi(lastFoundValue.c_str());
						CCalibrationLux* clt = static_cast<CCalibrationLux*>(currentTest);
						clt->changeBackgroundColors(backOD, backOS);
					}
				}

			}
		}

	}
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);
	if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
	{
		canceltraining = true;
	}
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (key == GLFW_KEY_ENTER && action == GLFW_PRESS)
	{
		if (!dilated)
		{
			dilated = true;
			needDilatedSizeCalculation = true;
			historyCleared = false;
			dilated_diam[0] = 0;
			dilated_diam[1] = 0;
		}
		else
		{
			dilated = false;
			needDilatedSizeCalculation = false;
		}
	}
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		testLog.close();
		qualityLog.close();
		webComm->closeLog();
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
	if (key == GLFW_KEY_1 && action == GLFW_PRESS)
	{
		currentTest = new RecordingTool();
		SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
		currentTest->testType = "RECORDING_TOOL";
	}
	if (key == GLFW_KEY_2 && action == GLFW_PRESS)
	{
		currentTest = new CASTest(0, 2, 3, "PROSACCADE");
		display.stimuliType = DisplayStimuliType::SACCADETASK;
		SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
		currentTest->testType = "SACCADE_MERGED";
	}
	if (key == GLFW_KEY_3 && action == GLFW_PRESS)
	{
		currentTest = new CMSTest(0,5,5);
		currentTest->testType = "MEMORYSACCADE";
		display.stimuliType = DisplayStimuliType::STIMULI;
	}
	if (key == GLFW_KEY_4 && action == GLFW_PRESS)
	{
	}
	if (key == GLFW_KEY_5 && action == GLFW_PRESS)
	{
		string sssss = "<SP>{\"command\":\"START\",\"testType\":\"DARK_ADAPTAION_AMD\",\"target\":\"OD\",\"visualAcuity\":false,\"contrastWithoutBleach\":true,\"contrastWithBleach\":false,\"bleachRecovery\":false}";
		webComm->emulaterecv(sssss);
		for (int i = 0; i < 400; i ++)
		{
//			cout << i << "  =  " << luxConverter[0].luxToGrayScale(i) << endl;
//			cout << luxConverter[0].luxToGrayScale(i) << "  =  " << luxConverter[0].grayScaleToLux(luxConverter[0].luxToGrayScale(i)) << endl;
		}
	}
	if (key == GLFW_KEY_0 && action == GLFW_PRESS)
	{
		webComm->sendmes("OK");
		if (currentTest)
		{
			if (testLog.is_open())
				testLog.close();
			nlohmann::json m6;
			m6["chartTypeString"] = currentTest->testType;
			m6["message_type"] = MESSAGE_TYPE::STOP_TEST;
			m6["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
			webComm->sendJSON(m6);
			eyetrackingOn = false;
			display.setDragonfly();
			qualityLog.close();
			currentTest = nullptr;
			webComm->closeLog();
		}
	}

#pragma region VLV_CALIBRATION_TESTS

	if (key == GLFW_KEY_Q && action == GLFW_PRESS)
	{
		if (!currentTest)
		{
			g_calibrationForcedStop = false;
			currentTest = new PatternTest();
			display.stimuliType = DisplayStimuliType::STIMULI;
			SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
			currentTest->testType = "CALIBRATION_SHAPE_RECOGNITION";
			std::thread(&calibrationProcessor).detach();
		}
		else if (currentTest && currentTest->testType == "CALIBRATION_SHAPE_RECOGNITION")
		{
			g_calibrationForcedStop = true;
		}
	}
	if (key == GLFW_KEY_W && action == GLFW_PRESS)
	{
		if (!currentTest)
		{
			g_calibrationForcedStop = false;
			currentTest = new ScreenTest(1);
			display.stimuliType = DisplayStimuliType::STIMULI;
			SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
			currentTest->testType = "CALIBRATION_SCREEN_TEST";
			std::thread(&calibrationProcessor).detach();
		}
		else if (currentTest && currentTest->testType == "CALIBRATION_SCREEN_TEST")
		{
			g_calibrationForcedStop = true;
		}
	}
	if (key == GLFW_KEY_E && action == GLFW_PRESS)
	{
		if (!currentTest)
		{
			g_calibrationForcedStop = false;
			currentTest = new VPosTest();
			display.stimuliType = DisplayStimuliType::STIMULI;
			SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
			currentTest->testType = "VIRTUAL_POSITION_TEST";
			std::thread(&calibrationProcessor).detach();
		}
		else if (currentTest && currentTest->testType == "VIRTUAL_POSITION_TEST")
		{
			g_calibrationForcedStop = true;
		}
	}
	if (key == GLFW_KEY_R && action == GLFW_PRESS)
	{
		if (!currentTest)
		{
			g_calibrationForcedStop = false;
			currentTest = new TimingTest(true);
			display.stimuliType = DisplayStimuliType::STIMULI;
			SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
			currentTest->testType = "SHORT_TIMING_TEST";
			std::thread(&calibrationProcessor).detach();
		}
		else if (currentTest && currentTest->testType == "SHORT_TIMING_TEST")
		{
			g_calibrationForcedStop = true;
		}
	}
	if (key == GLFW_KEY_T && action == GLFW_PRESS)
	{
		if (!currentTest)
		{
			g_calibrationForcedStop = false;
			currentTest = new TimingTest(false);
			display.stimuliType = DisplayStimuliType::STIMULI;
			SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
			currentTest->testType = "LONG_TIMING_TEST";
			std::thread(&calibrationProcessor).detach();
		}
		else if (currentTest && currentTest->testType == "LONG_TIMING_TEST")
		{
			g_calibrationForcedStop = true;
		}
	}
	if (key == GLFW_KEY_Y && action == GLFW_PRESS)
	{
		if (!currentTest)
		{
			g_calibrationForcedStop = false;
			currentTest = new SensorTest();
			display.stimuliType = DisplayStimuliType::STIMULI;
			SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
			currentTest->testType = "SENSOR_TEST";
			std::thread(&calibrationProcessor).detach();
		}
		else if (currentTest && currentTest->testType == "SENSOR_TEST")
		{
			g_calibrationForcedStop = true;
		}
	}
#pragma endregion VLV_CALIBRATION_TESTS

}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	try
	{
#ifndef ALVICAMERA
		if (ptgreycam)
		{
			ptgreycam->setOffset(140, ptgreycam->yOffset + yoffset * 4);
			et.offset.height = ptgreycam->yOffset;
		}
#else
		if (avcam)
		{
			avcam->setOffset(140, avcam->yOffset + yoffset * 4);
			et.offset.height = avcam->yOffset;
		}

#endif // !1
	}
	catch (exception& e)
	{
		cerr << e.what() << endl;
	}
}

#ifndef EMULATECAMERA
#ifndef ALVICAMERA
void imageProcessor()
{
	camFrame tmpFrame;
	Mat camPicture;
	uint64_t lineStatus, camTS, frameID, prevFID = 0;

	while (true)
	{

#pragma region VLV_CALIBRATION_TESTS

		if (g_calibrationIsGoing)
		{
			continue;
		}

#pragma endregion VLV_CALIBRATION_TESTS

		camPicture = ptgreycam->getImage(lineStatus, camTS, frameID);
		if (ptgreycam->sn == "17391407")
		{
			if (lineStatus == 10)
				lineStatus = 12;
			else
				lineStatus = 14;
		}

		if (lineStatus != 14)
			picBP = true;
		else
			picBP = false;

		prevFID = frameID;
		tmpFrame.linestatus = lineStatus;
		tmpFrame.frameID = frameID;
		tmpFrame.picBP = picBP;
		tmpFrame.timestamp = camTS;
		tmpFrame.picture = camPicture.clone();

		frame_queue.push(tmpFrame);
	}
}
#endif
#else
void imageProcessor()
{
	camFrame tmpFrame;
	Mat camPicture;
	uint64_t lineStatus, frameID = 0;

	while (true)
	{
		if (!emVC.read(camPicture))
		{
			emVC.set(CAP_PROP_POS_FRAMES, 0);
			emVC.read(camPicture);
		}
		cvtColor(camPicture, camPicture, COLOR_RGB2GRAY);

		if (camPicture.at<uchar>(Point(7, 7)) < 128)
		{
			picBP = 0 ^ recbpinverted;
		}
		else
		{
			picBP = 1 ^ recbpinverted;
		}
		if (picBP)
			lineStatus = 12;
		else
			lineStatus = 14;
		frameID++;

		tmpFrame.frameID = frameID;
		tmpFrame.picBP = picBP;
		tmpFrame.timestamp = clock();
		tmpFrame.picture = camPicture.clone();
		tmpFrame.linestatus = lineStatus;

		frame_queue.push(tmpFrame);
	}
}
#endif

void errorCallback(int error, const char* description)
{
	std::cerr << description << std::endl;
}

void initET()
{
	etis.BPGthreshold = 250;
	etis.DPGthreshold = 250;
	etis.inverted = true;
	etis.frameSize = Size(1000, 400);

#ifndef EMULATECAMERA
#ifndef ALVICAMERA

	etis.offset = Size(140, ptgreycam->yOffset);
#else
	etis.offset = Size(140, avcam->yOffset);

#endif
#else
	etis.offset = Size(140, 400);
#endif

	etis.anglesOfShapes[0] = shapeAngleOD;
	etis.anglesOfShapes[1] = shapeAngleOS;
	etis.centersOfShapes[0] = Point2f(shapePosODx, shapePosODy);
	etis.centersOfShapes[1] = Point2f(shapePosOSx, shapePosOSy);

	et = EyeTracker(etis);
}

void initDisplays()
{
	display.errorCallback = errorCallback;
	display.keyCallback = key_callback;
	display.scrollCallback = scroll_callback;
	display.mouseCallback = mouse_button_callback;
	display.initGL(1000, 400);
	cout << "GL init ok" << endl;
	display.initWindows(true, true);
	cout << "Windows init ok" << endl;
	display.setDragonfly();
	currentTest = nullptr;
}

void loadConfig()
{
	exposureTime = atoi(config.GetConfigSetting("exposuretime").c_str());
	offset = atoi(config.GetConfigSetting("offset").c_str());

	communicationLogsPath = config.GetConfigSetting("communicationLogsPath");
	picturesLogsPath = config.GetConfigSetting("picturesLogsPath");
	testLogsPath = config.GetConfigSetting("testLogsPath");
	testVideosPath = config.GetConfigSetting("testVideosPath");
	recToolVideosPath = config.GetConfigSetting("recToolVideosPath");
	qualityLogsPath = config.GetConfigSetting("qualityLogsPath");
	screenshotsPath = config.GetConfigSetting("screenshotsPath");


	shapeAngleOS = atof(config.GetConfigSetting("shapeangleos").c_str());
	shapeAngleOD = atof(config.GetConfigSetting("shapeangleod").c_str());
	shapePosOSx = atoi(config.GetConfigSetting("shapepososx").c_str());
	shapePosOSy = atoi(config.GetConfigSetting("shapepososy").c_str());
	shapePosODx = atoi(config.GetConfigSetting("shapeposodx").c_str());
	shapePosODy = atoi(config.GetConfigSetting("shapeposody").c_str());

	//ACUITY
	amd_acuityInitBGClrR = atof(config.GetConfigSetting("amd_acuityInitBGClrR").c_str());
	amd_acuityInitBGClrG = atof(config.GetConfigSetting("amd_acuityInitBGClrG").c_str());
	amd_acuityInitBGClrB = atof(config.GetConfigSetting("amd_acuityInitBGClrB").c_str());

	//CONTRAST
	amd_contrastPxNumber = atoi(config.GetConfigSetting("amd_contrastPxNumber").c_str());
	amd_contrastBGClrR = atof(config.GetConfigSetting("amd_contrastBGClrR").c_str());
	amd_contrastBGClrG = atof(config.GetConfigSetting("amd_contrastBGClrG").c_str());
	amd_contrastBGClrB = atof(config.GetConfigSetting("amd_contrastBGClrB").c_str());

	//DARK_ADAPTATION
	amd_darkadaptationBleachDurationFrames = atof(config.GetConfigSetting("amd_darkadaptationBleachDurationFrames").c_str());

	amd_darkadaptationFixedContrastClr[0] = atof(config.GetConfigSetting("amd_darkadaptationFixedContrastClrR").c_str());
	amd_darkadaptationFixedContrastClr[1] = atof(config.GetConfigSetting("amd_darkadaptationFixedContrastClrG").c_str());
	amd_darkadaptationFixedContrastClr[2] = atof(config.GetConfigSetting("amd_darkadaptationFixedContrastClrB").c_str());

	amd_darkadaptationFixedFrequencyFrequency = atof(config.GetConfigSetting("amd_darkadaptationFixedFrequencyFrequency").c_str());

	pas_calibrationThreshold = atof(config.GetConfigSetting("pas_calibrationThreshold").c_str());


	//SCREEN AREA
	amd_medialLimit = atof(config.GetConfigSetting("amd_medialLimit").c_str());
	amd_lateralLimit = atof(config.GetConfigSetting("amd_lateralLimit").c_str());
	amd_topLimit = atof(config.GetConfigSetting("amd_topLimit").c_str());
	amd_bottomLimit = atof(config.GetConfigSetting("amd_bottomLimit").c_str());

	//STIMULI MOVEMENT
	amd_radius = atof(config.GetConfigSetting("amd_radius").c_str());
	amd_speed = atoi(config.GetConfigSetting("amd_speed").c_str());

	//NON STIMULI EYE
	amd_initNSEyeBGClrR = atof(config.GetConfigSetting("amd_initNSEyeBGClrR").c_str());
	amd_initNSEyeBGClrG = atof(config.GetConfigSetting("amd_initNSEyeBGClrG").c_str());
	amd_initNSEyeBGClrB = atof(config.GetConfigSetting("amd_initNSEyeBGClrB").c_str());
	amd_trackNSEye = atoi(config.GetConfigSetting("amd_trackNSEye").c_str());

	pxtommrat = atof(config.GetConfigSetting("calibration_pixels_to_mm_ratio").c_str());
	p4_sequenceDuration = atof(config.GetConfigSetting("p4_sequenceDuration").c_str());

	vf_greenFixationFixedCount = atoi(config.GetConfigSetting("vf_greenFixationFixedCount").c_str());
	vf_greenFixationRadius = atof(config.GetConfigSetting("vf_greenFixationRadius").c_str());
	vf_greenFixationRandomCount = atoi(config.GetConfigSetting("vf_greenFixationRandomCount").c_str());
	vf_greenFixationRadius = atof(config.GetConfigSetting("vf_greenFixationRadius").c_str());
	vf_whiteFixationCount = atoi(config.GetConfigSetting("vf_whiteFixationCount").c_str());
	vf_whiteFixationRadius = atof(config.GetConfigSetting("vf_whiteFixationRadius").c_str());
	vf_srtRadius = atof(config.GetConfigSetting("vf_srtRadius").c_str());

	st_targetRadius = atof(config.GetConfigSetting("st_targetRadius").c_str());
	st_memoryT4Time = atof(config.GetConfigSetting("st_memoryT4Time").c_str());
	st_antisaccadeShowCentralAlways = atoi(config.GetConfigSetting("st_antisaccadeShowCentralAlways").c_str());
	st_prosaccadeShowCentralAlways = atof(config.GetConfigSetting("st_prosaccadeShowCentralAlways").c_str());
}

void initWebComm()
{
	webComm = new WebCommunicate();
	webComm->dataRead2 = dataRead2;
	std::thread(&WebCommunicate::startServer, webComm).detach();
}

#ifdef ALVICAMERA
void frameRecieved(cv::Mat picture, bool picBP, uint64_t timestamp, uint64_t frameID)
{
	camFrame tmpFrame;
	if (picBP)
		tmpFrame.linestatus = 12;
	else
		tmpFrame.linestatus = 14;
	tmpFrame.frameID = frameID;
	tmpFrame.picBP = picBP;
	tmpFrame.timestamp = timestamp;
	tmpFrame.picture = picture.clone();
	frame_queue.push(tmpFrame);
}

#endif


#ifndef ALVICAMERA
void initCamera(int exposureTime)
{
	ptgreycam = new CPTGCamera(offset);
	ptgreycam->setExposure(exposureTime);
	ptgreycam->startAcquisition();
	ptgreycam->switchDPBP();
}
#else
void initCamera(int exposureTime)
{
	avcam = new CAVCamera(offset, frameRecieved);
	avcam->setExposure(exposureTime);
}
#endif

void VLVtestProcessorRoutine(ETResult& etResult)
{
	for (int eye = 0; eye < 2; eye++)
	{
		if (etResult.etData[eye].rawPupilEllipse.size != Size2f(0, 0) && etResult.etData[eye].rawPupilEllipse.center != Point2f(0, 0))
		{
			etResult.etData[eye].distanceToOptimum = pDist(etResult.etData[eye].rawPupilEllipse.center, Point2f(250 + 500 * eye, 200));

			int bpStainsInPupil = 0;
			int dpStainsInPupil = 0;

			for (int bpgcand = 0; bpgcand < et.bpgCands[eye].size(); bpgcand++)
			{
				if (pDist(et.bpgCands[eye].at(bpgcand), etResult.etData[eye].rawPupilEllipse.center) < etResult.etData[eye].rawPupilEllipse.size.width / 2)
				{
					bpStainsInPupil++;
				}
			}
			for (int dpgcand = 0; dpgcand < et.dpgCands[eye].size(); dpgcand++)
			{
				if (pDist(et.dpgCands[eye].at(dpgcand), etResult.etData[eye].rawPupilEllipse.center) < etResult.etData[eye].rawPupilEllipse.size.width / 2)
				{
					dpStainsInPupil++;
				}
			}

			if (bpStainsInPupil > 1 && dpStainsInPupil > 1)
			{
				etResult.etData[eye].isIOL = true;
			}
		}
	}
	if (etResult.etData[0].rawPupilEllipse.size != Size2f(0, 0) && etResult.etData[1].rawPupilEllipse.size != Size2f(0, 0))
	{
		if (et.initCentersOfShapes[0].y == 0)
		{
			throw;
		}
		float intersectionZone = 1024 * 2 - et.initCentersOfShapes[0].y - et.initCentersOfShapes[1].y - (60 * 14.5);
		float pixIPD = 1024 * 2 - (et.offset.height * 2 + etResult.etData[0].rawPupilEllipse.center.y + etResult.etData[1].rawPupilEllipse.center.y) - intersectionZone;
		etResult.etData[0].IPD = pixIPD / 14.5;
	}
}

void gtProcessor()
{
	uint64_t updateCounter = 0;
	uint64_t prevFID = 0;
	bool prevBP = false;
	bool picBP;

	camFrame tmpFrame;
	vector<ETData> etData;
	int total_miss = 0;

	while (true)
	{
		//if (!allowPull)
		{
			if (!frame_queue.empty())
			{
				frame_queue.pop(tmpFrame);
				if (tmpFrame.frameID > prevFID)
				{
					if (tmpFrame.frameID - prevFID > 1)
					{
						total_miss += tmpFrame.frameID - prevFID - 1;
						//cout << "Total miss = " << total_miss << endl;
					}
					prevFID = tmpFrame.frameID;
					prevBP = tmpFrame.picBP;
					picBP = tmpFrame.picBP;

					++updateCounter;
					++et.frameNum;
					etData = et.getTrackingData(tmpFrame.picture, tmpFrame.linestatus, false);

					uint64_t timestamp = tmpFrame.timestamp;
					gtQueue _this_gt_queue{ et.trueLastBPimage, et.lastBPDPimage, et.lastDPimage, timestamp, tmpFrame.glts, etData[0].tandemGlints, etData[1].tandemGlints,
						etData[0].calibShape, etData[1].calibShape, et.defaultBorders[0],  et.defaultBorders[1],
						et.goodGlintsSequence[0], et.goodGlintsSequence[1], etData[0].isPicBright };

					gt_queue.push(_this_gt_queue);

					etQueue _tmpQ;
					if (!et_queue.empty())
					{
						et_queue.pop(_tmpQ);
						etData[0].rawPupilEllipse = RotatedRect(Point2f(_tmpQ.pupil[0].center_x, _tmpQ.pupil[0].center_y), Size2f(_tmpQ.pupil[0].radius * 2, _tmpQ.pupil[0].radius * 2), 0);
						etData[1].rawPupilEllipse = RotatedRect(Point2f(_tmpQ.pupil[1].center_x, _tmpQ.pupil[1].center_y), Size2f(_tmpQ.pupil[1].radius * 2, _tmpQ.pupil[1].radius * 2), 0);
						etData[0].rawRotPoint = _tmpQ.rotPoint[0];
						etData[1].rawRotPoint = _tmpQ.rotPoint[1];
						etData[0].BPcontrast = _tmpQ.BPcontrast[0];
						etData[1].BPcontrast = _tmpQ.BPcontrast[1];
						etData[0].DPcontrast = _tmpQ.DPcontrast[0];
						etData[1].DPcontrast = _tmpQ.DPcontrast[1];
						etData[0].threeDangle = _tmpQ.threeDdata[0];
						etData[1].threeDangle = _tmpQ.threeDdata[1];
						etData[0].dilatedTandemGlints = _tmpQ.tandemGlints[0];
						etData[1].dilatedTandemGlints = _tmpQ.tandemGlints[1];
					}

					gltsGlints = _this_gt_queue.glts;
					gltsPupils = _tmpQ.glts;
					camtsGlints = timestamp;
					camtsPupils = _tmpQ.timestamp;

					etData[1].glintsTimestampGL = etData[0].glintsTimestampGL = gltsGlints;
					etData[1].pupilsTimestampGL = etData[0].pupilsTimestampGL = gltsPupils;
					etData[1].glintsTimestamp = etData[0].glintsTimestamp = camtsGlints;
					etData[1].pupilsTimestamp = etData[0].pupilsTimestamp = camtsPupils;

					if (needDilatedSizeCalculation)
					{
						if (!historyCleared)
						{
							entireEThistoryData.clear();
							historyCleared = true;
							last_fixed_index = 0;
						}
						if (dilated_diam[0] == 0)
						{
							dilated_diam[0] = calculateDilatedPupilSize(0);
						}

						if (dilated_diam[1] == 0)
						{
							dilated_diam[1] = calculateDilatedPupilSize(1);
						}

						if (webComm != nullptr && dilated_diam[0] > 0 && dilated_diam[1] > 0)
						{
							needDilatedSizeCalculation = false;
							cout << dilated_diam[0] << "  " << dilated_diam[1] << endl;
							//webComm->sendmes("OK");
						}
					}

					entireEThistoryData.push_back({ { etData[0], etData[1] }, tmpFrame });
					if (entireEThistoryData.size() > 1000)
					{
						entireEThistoryData.erase(entireEThistoryData.begin());
					}
					ETdataFixTiming();

					ETResult etrtmp;
					if (last_fixed_index > -1)
					{
						etrtmp = entireEThistoryData.at(last_fixed_index);
						filled_et_data.push(etrtmp);
					}
					else if (last_fixed_index == -1)
					{
						etrtmp = entireEThistoryData.back();
						filled_et_data.push(etrtmp);
					}

					TrackingHolesFilling();
					FixStack();

					allowPull = true;
				}
			}
		}
	}
}

void etProcessor()
{
	AllocatedGPUmemorySpace agms = allocateGPUmemorySpace();
	uint64_t time_limit = 10000000;

	Point2f last_succesfull_rots[2] = { Point2f() };
	ThreeDangle last_successfull_3dangle[2];
	gtQueue tmpQueue;

	while (true)
	{
		if (!gt_queue.empty())
		{
			gt_queue.pop(tmpQueue);
			etQueue _tmp_et;

			if (dilated)
			{
				if (!tmpQueue.isBP)
				{
					bool erasingRequred[2] = { false, false };

					for (int curROInum = 0; tmpQueue.lastBPDPimage.size().width > 0 && curROInum <= 1; curROInum++)
					{
						Pupil candidate = getDilatedCandidatePupil(agms, tmpQueue, time_limit, dilated_diam[curROInum], curROInum);

						if (candidate.radius > 0)
						{
							_tmp_et.pupil[curROInum] = candidate;

							RotatedRect _temp = RotatedRect(Point2f(_tmp_et.pupil[curROInum].center_x, _tmp_et.pupil[curROInum].center_y), Size2f(_tmp_et.pupil[curROInum].radius * 2, _tmp_et.pupil[curROInum].radius * 2), 0);
							gpuTrackingQuality tq = gpuQualityControl(smallEThistoryData, tmpQueue.lastBPDPimage, tmpQueue.lastDPimage, tmpQueue.trueLastBPimage, curROInum, _tmp_et.rotPoint[curROInum], _temp, tmpQueue.tgs[curROInum], dilated);
							_tmp_et.BPcontrast[curROInum] = tq.BPcontrast;
							_tmp_et.DPcontrast[curROInum] = tq.DPcontrast;

							// bad quality
							if (_tmp_et.DPcontrast[curROInum] > 0 || tq.dPupilPos > 30)
							{
								erasingRequred[curROInum] = true;
							}
							else
							{
								TandemGlints only_dpg = et.DPglintTracking(tmpQueue.lastDPimage, tmpQueue.trueLastBPimage, _temp, curROInum);
								if (only_dpg.dpGraw != Point2f())
								{
									_tmp_et.tandemGlints[curROInum] = only_dpg;
								}
								else
								{
									erasingRequred[curROInum] = true;
								}
							}
						}
					}

					smallEThistoryData.push_back(_tmp_et);
					if (smallEThistoryData.size() > 100)
					{
						smallEThistoryData.pop_front();
					}

					if (erasingRequred[0])
					{
						_tmp_et.eraseTrackingData(0);
					}
					if (erasingRequred[1])
					{
						_tmp_et.eraseTrackingData(1);
					}

					_tmp_et.timestamp = tmpQueue.timestamp;
					_tmp_et.glts = tmpQueue.glts;
					et_queue.push(_tmp_et);
				}
			}
			else
			{
				for (int curROInum = 0; tmpQueue.lastBPDPimage.size().width > 0 && curROInum <= 1; curROInum++)
				{
					Pupil candidate = getCandidatePupil(agms, smallEThistoryData, tmpQueue, last_succesfull_rots[curROInum], time_limit, curROInum);
					gpuTrackingQuality tq;

					if (candidate.radius > 0)
					{
						_tmp_et.pupil[curROInum] = candidate;

						if (tmpQueue.isBP || last_succesfull_rots[curROInum] == Point2f())
						{
							_tmp_et.rotPoint[curROInum] = gpu_getRotPoint(_tmp_et.pupil[curROInum], tmpQueue.tgs[curROInum].bpGraw);
							last_succesfull_rots[curROInum] = _tmp_et.rotPoint[curROInum];
						}
						else
						{
							_tmp_et.rotPoint[curROInum] = last_succesfull_rots[curROInum];
						}

						_tmp_et.threeDdata[curROInum] = gpu_get3DangleData(_tmp_et.rotPoint[curROInum], Point2f(candidate.center_x, candidate.center_y), tmpQueue.calibShape[curROInum], curROInum);

						RotatedRect _temp = RotatedRect(Point2f(_tmp_et.pupil[curROInum].center_x, _tmp_et.pupil[curROInum].center_y), Size2f(_tmp_et.pupil[curROInum].radius * 2, _tmp_et.pupil[curROInum].radius * 2), 0);
						tq = gpuQualityControl(smallEThistoryData, tmpQueue.lastBPDPimage, tmpQueue.lastDPimage, tmpQueue.trueLastBPimage, curROInum, _tmp_et.rotPoint[curROInum], _temp, tmpQueue.tgs[curROInum], dilated);
						_tmp_et.BPcontrast[curROInum] = tq.BPcontrast;
						_tmp_et.DPcontrast[curROInum] = tq.DPcontrast;

						//Bad quality
						if (et_round[curROInum] == 1 && tq.dPupilSize >= 4)
						{
							_tmp_et.eraseTrackingData(curROInum);
							et_round[curROInum] = 0;
						}
					}
				}

				_tmp_et.timestamp = tmpQueue.timestamp;
				_tmp_et.glts = tmpQueue.glts;
				et_queue.push(_tmp_et);
				smallEThistoryData.push_back(_tmp_et);
				if (smallEThistoryData.size() > 100)
				{
					smallEThistoryData.pop_front();
				}
			}
		}
	}

	freeAllocatedGPUmemorySpace(agms);
}

void testProcessor()
{
	Mat ptosisPic;
	uint64_t updateCounter = 0;
cv:Mat qiPic = cv::Mat(400, 1000, CV_8UC3);
	vector<eyelids> nn;
	RotatedRect last_nonzero_pupil[2]; //VLV - HaPe wants fake size

	while (true)
	{
		//		std::cout << "1517\n";
		if (allowPull)
		{
			if (!filled_et_data.empty())
			{
				ETResult etResult;
				filled_et_data.pop(etResult);

				if (etResult.etData.size() != 2)
				{
					cerr << "etData.size() = " << etResult.etData.size() << endl;
					continue;
				}

				if (etResult.etData[0].glintsTimestamp != etResult.etData[0].pupilsTimestamp && etResult.etData[0].pupilsTimestamp != 0)
				{
					cerr << "Timestamps = " << etResult.etData[0].glintsTimestamp << "  " << etResult.etData[0].pupilsTimestamp << " " << entireEThistoryData.size() << " " << last_fixed_index << endl;
					continue;
				}

//				VLVtestProcessorRoutine(etResult);
//				else 
				if (etResult.etData.size() > 1 && currentTest)
				{
					if (qualityLog.is_open())
					{
						qualityLog << etResult.etData[0].glintsTimestamp << ";" << // 1
							etResult.etData[0].pupilsTimestamp << ";" << // 2
							etResult.etData[1].glintsTimestamp << ";" << // 3
							etResult.etData[1].pupilsTimestamp << ";" << // 4
							etResult.etData[0].tandemGlints.bpGraw.x << ";" << // 5
							etResult.etData[0].tandemGlints.bpGraw.y << ";" << // 6
							etResult.etData[0].tandemGlints.dpGraw.x << ";" << // 7
							etResult.etData[0].tandemGlints.dpGraw.y << ";" << // 8
							etResult.etData[0].dilatedTandemGlints.dpGraw.x << ";" << // 7
							etResult.etData[0].dilatedTandemGlints.dpGraw.y << ";" << // 8
							etResult.etData[0].rawPupilEllipse.center.x << ";" << // 9
							etResult.etData[0].rawPupilEllipse.center.y << ";" << // 10
							max(etResult.etData[0].rawPupilEllipse.size.width, etResult.etData[0].rawPupilEllipse.size.height) << ";" << // 11
							etResult.etData[0].rawRotPoint.x << ";" << // 12
							etResult.etData[0].rawRotPoint.y << ";" << // 13
							etResult.etData[0].threeDangle.vsPixel.x << ";" << // 14
							etResult.etData[0].threeDangle.vsPixel.y << ";" << // 15
							etResult.etData[1].tandemGlints.bpGraw.x << ";" << // 16
							etResult.etData[1].tandemGlints.bpGraw.y << ";" << // 17
							etResult.etData[1].tandemGlints.dpGraw.x << ";" << // 18
							etResult.etData[1].tandemGlints.dpGraw.y << ";" << // 19
							etResult.etData[1].dilatedTandemGlints.dpGraw.x << ";" << // 7
							etResult.etData[1].dilatedTandemGlints.dpGraw.y << ";" << // 8
							etResult.etData[1].rawPupilEllipse.center.x << ";" << // 20
							etResult.etData[1].rawPupilEllipse.center.y << ";" << // 21
							max(etResult.etData[1].rawPupilEllipse.size.width, etResult.etData[1].rawPupilEllipse.size.height) << ";" << // 22
							etResult.etData[1].rawRotPoint.x << ";" << // 23
							etResult.etData[1].rawRotPoint.y << ";" << // 24
							etResult.etData[1].threeDangle.vsPixel.x << ";" << // 25
							etResult.etData[1].threeDangle.vsPixel.y << ";" << // 26
							currentTest->testType << // 27
							endl;
					}
					if (etResult.etData[0].tandemGlints.bpGraw.x == 0 && etResult.etData[1].tandemGlints.bpGraw.x == 0)
					{
						lastBlink = std::chrono::system_clock::now();
						ptosis_waitingForBlink = false;
					}
					if (ptosis_needpic || saveFrameSignal)
					{
						if (!ptosis_waitingForBlink && !etResult.frame.picBP)
						{
							if (etResult.etData[0].rawPupilEllipse.center.x > 0 &&
								etResult.etData[1].rawPupilEllipse.center.x > 0 &&
								etResult.etData[0].tandemGlints.bpGraw.x > 0 &&
								etResult.etData[0].tandemGlints.bpGraw.x > 0)
							{
								if ((abs(etResult.etData[0].tandemGlints.bpGraw.x - etResult.etData[0].rawPupilEllipse.center.x) < 20
									&& abs(etResult.etData[1].tandemGlints.bpGraw.x - etResult.etData[1].rawPupilEllipse.center.x) < 20
									&& abs(etResult.etData[0].tandemGlints.bpGraw.x - etResult.etData[0].rawPupilEllipse.center.x) > 8
									&& abs(etResult.etData[1].tandemGlints.bpGraw.x - etResult.etData[1].rawPupilEllipse.center.x) > 8)
									|| saveFrameSignal)
								{
									if (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - lastBlink).count() > 100)
									{
										ptosisPic = etResult.frame.picture.clone();
										ptosisTimer = std::chrono::system_clock::now();
										ptosis_waitingForBlink = true;
										if (ptosis_needpic)
											nn = detectEyelids(etResult.frame.picture, etResult.etData);
									}
								}
								else
								{
									ptosisPic = Mat::zeros(Size(ptosisPic.cols, ptosisPic.rows), CV_8UC1);
									ptosis_waitingForBlink = false;
								}
							}
						}
						else if (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - ptosisTimer).count() > 100)
						{
							if (ptosis_needpic)
							{
								if (ptosisPic.cols && ptosisPic.rows)
								{
									Mat picToSend = Mat::zeros(ptosisPic.cols / 2, ptosisPic.rows * 2, CV_8UC1);
									rotate(ptosisPic(Rect(0, 0, ptosisPic.cols / 2, ptosisPic.rows)),
										picToSend(Rect(0, 0, picToSend.cols / 2, picToSend.rows)),
										2);
									rotate(ptosisPic(Rect(ptosisPic.cols / 2, 0, ptosisPic.cols / 2, ptosisPic.rows)),
										picToSend(Rect(picToSend.cols / 2, 0, picToSend.cols / 2, picToSend.rows)),
										0);

									std::vector<uchar> buff;
									std::vector<int> param(2);
									param[0] = cv::IMWRITE_JPEG_QUALITY;
									param[1] = 30;
									cv::imencode(".jpg", picToSend, buff, param);
									std::string tmp = std::string(buff.begin(), buff.end());
									using namespace boost::archive::iterators;
									using It = base64_from_binary<transform_width<std::string::const_iterator, 6, 8>>;
									auto s = std::string(It(std::begin(tmp)), It(std::end(tmp)));
									s.append((3 - tmp.size() % 3) % 3, '=');

									std::ostringstream fnShort;
									fnShort << "ptosis_" << patientInfo.examID << "_" << std::chrono::system_clock::now().time_since_epoch().count() << ".png";
									std::ostringstream fnFull;
									fnFull << picturesLogsPath << fnShort.str();
									cv::imwrite(fnFull.str(), picToSend);


									nlohmann::json js_ptosis;
									js_ptosis["chartTypeString"] = "PTOSIS_EVALUATION";
									js_ptosis["message_type"] = 6;
									js_ptosis["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
									js_ptosis["picture"] = s.c_str();

									if (nn.size() == 2)
									{
										js_ptosis["ODBottomX"] = nn[0].lowerEyelid.x;
										js_ptosis["OSBottomX"] = nn[1].lowerEyelid.x;
										js_ptosis["ODCenterX"] = nn[0].centerPupil.x;
										js_ptosis["OSCenterX"] = nn[1].centerPupil.x;
										js_ptosis["ODTopX"] = nn[0].upperEyelid.x;
										js_ptosis["OSTopX"] = nn[1].upperEyelid.x;

										js_ptosis["ODBottomY"] = nn[0].lowerEyelid.y;
										js_ptosis["OSBottomY"] = nn[1].lowerEyelid.y;
										js_ptosis["ODCenterY"] = nn[0].centerPupil.y;
										js_ptosis["OSCenterY"] = nn[1].centerPupil.y;
										js_ptosis["ODTopY"] = nn[0].upperEyelid.y;
										js_ptosis["OSTopY"] = nn[1].upperEyelid.y;

										js_ptosis["ODSize"] = nn[0].pupilSize;
										js_ptosis["OSSize"] = nn[1].pupilSize;

										js_ptosis["filename"] = fnShort.str();

										webComm->sendJSON(js_ptosis);
									}
									if (!ptosis_pic1sent)
										ptosis_pic1sent = true;
									else if (!ptosis_pic2sent)
										ptosis_pic2sent = true;
									else if (!ptosis_pic3sent)
										ptosis_pic3sent = true;
									ptosis_needpic = false;
									ptosis_waitingForBlink = false;
									lastBlink = std::chrono::system_clock::now();
								}
							}
							else
							{
								frameForAI tmpf;
								tmpf.picture = ptosisPic.clone();
								tmpf.ts = std::chrono::system_clock::now().time_since_epoch().count();
								savedFrames.push_back(tmpf);
								savedFramesCurrentID++;
								savedFramesCount++;
								if (currentTest->testType != "FUNCTIONAL_SCREENING" || savedFramesCount == 2)
								{
									savedFramesCount = 0;
									saveFrameSignal = false;
									ptosis_waitingForBlink = false;
									lastBlink = std::chrono::system_clock::now();
								}
							}
						}
					}
					if (currentTest)
					{
						//						cout << "this2" << endl;
						if (currentTest->testType == "VISUAL_FIELD_MERGED")
						{
							CVFMTest* vft = static_cast<CVFMTest*>(currentTest);
							vft->ProcessETData(etResult.etData, etResult.frame.timestamp, etResult.frame.frameID);
						}
						else if (currentTest->testType == "NYSTAGMUS_EVALUATION")
						{
							CNystagmusTest* vft = static_cast<CNystagmusTest*>(currentTest);
							vft->ProcessETData(etResult.etData, etResult.frame.picture, etResult.frame.picBP);
						}
						else if (currentTest->testType == "RECORDING_TOOL")
						{
							RecordingTool* vft = static_cast<RecordingTool*>(currentTest);
							vft->ProcessETData(etResult.frame.picture, etResult.frame.linestatus);
						}
						else
							currentTest->ProcessETData(etResult.etData);
					}

				}

				//TechStaff displaying
				++updateCounter;
				if (updateCounter >= 31)
				{
					qiPic.setTo(CV_RGB(0.2 * 255, 0.3 * 255, 0.3 * 255));
					if (display.stimuliType != DisplayStimuliType::DRAGONFLY)
					{
						if (currentTest)
						{
							if (currentTest->testType == "VISUAL_FIELD_EVALUATION")
							{
								Scalar clr;
								for (int vfi = 0; vfi < 8; vfi++)
								{
									if (vftrainresults[vfi] == 1)
										clr = CV_RGB(255, 0, 0);
									if (vftrainresults[vfi] == 2)
										clr = CV_RGB(128, 128, 128);
									if (vftrainresults[vfi] == 3)
										clr = CV_RGB(0, 255, 0);
									circle(qiPic, Point(150 + vfi * 100, 250), 40, clr, -1);
								}
							}
						}
					}
					else
					{
						drawTSinfo(etResult.etData, qiPic, etResult.frame.picture);
					}

					Mat tsETPicture = etResult.frame.picture.clone();
					/*					if (etResult.etData[0].rawPupilEllipse.center.x > 0 &&
											etResult.etData[1].rawPupilEllipse.center.x > 0 &&
											etResult.etData[0].tandemGlints.bpGraw.x > 0 &&
											etResult.etData[0].tandemGlints.bpGraw.x > 0 && !picBP)
										{
											nn = detectEyelids(etResult.frame.picture, etResult.etData);
											circle(tsETPicture, nn[0].rawLowerEyelid, 3, Scalar(255), 3, -1);
											circle(tsETPicture, nn[0].rawUpperEyelid, 3, Scalar(255), 3, -1);
											circle(tsETPicture, nn[1].rawLowerEyelid, 3, Scalar(255), 3, -1);
											circle(tsETPicture, nn[1].rawUpperEyelid, 3, Scalar(255), 3, -1);
										}*/
					for (int pp = 0; pp < etResult.etData.size(); pp++)
					{
						circle(tsETPicture, etResult.etData[pp].tandemGlints.bpGraw, 3, Scalar(128), 2, -1);
						circle(tsETPicture, etResult.etData[pp].tandemGlints.dpGraw, 3, Scalar(64), 2, -1);
						ellipse(tsETPicture, etResult.etData[pp].rawPupilEllipse, Scalar(255));
					}
					display.updateCamPicture(tsETPicture);
					display.updateQIPicture(qiPic);
					updateCounter = 0;
				}
			}
			else
			{
				allowPull = false;
			}
		}

		//		std::cout << "1797\n";
	}
}

#pragma region VLV_CALIBRATION_TESTS

void calibrationProcessor()
{
	currentTest->inCaseOfCalibration();
	display.setDragonfly();
	currentTest = nullptr;
	g_calibrationIsGoing = false;
	cout << "Calibration thread is finished" << endl;
}

#pragma endregion VLV_CALIBRATION_TESTS

int main()
{
	std::cout << "--------------------------------------------------------------" << std::endl;
	std::cout << " BULBICAM v " << VERSION << std::endl;
	std::cout << "--------------------------------------------------------------" << std::endl << std::endl;
	//	setlocale(LC_ALL, "RUS");

	lensDist = new Lens(66, 6);
	lensDist->PPD = 64;
	loadConfig();
#ifdef EMULATECAMERA

	//bool ooo = emVC.open("D:\\vids_dilated_tracking_(test_set)\\alldailm_rec_16438722997067391_id220203121028_nan.avi");
	//bool ooo = emVC.open("D:\\vids_dilated_tracking_(test_set)\\alldailm_rec_16456160710151240_id220223163332_nan.avi");
	bool ooo = emVC.open("c:\\testvideos\\1.avi");

	emVC.set(CAP_PROP_POS_FRAMES, 1);
#else
	initCamera(exposureTime);
#endif	
	cout << "cam init ok" << endl;
	initET();
	cout << "et init ok" << endl;
	initDisplays();
	cout << "gl init ok" << endl;
	initWebComm();
	cout << "network init ok" << endl;
	ofstream versioninfofile;


	WCHAR wpath[MAX_PATH];
	HMODULE hModule = GetModuleHandleW(NULL);
	GetModuleFileNameW(hModule, wpath, MAX_PATH);
	PathRemoveFileSpec(wpath);
	std::wstring wspath = wpath;
	PathToExecutable = std::string(wspath.begin(), wspath.end());

	std::string texfilename = PathToExecutable;
	preassesmentDone = false;

	texfilename.append("\\BulbiCam_");
	texfilename.append(VERSION);
	texfilename.append(".txt");

	versioninfofile.open(texfilename);

	versioninfofile << "eyetracking version : " << ETversion << endl;
	versioninfofile << "recording tool version : " << recordingtool_version << endl;
	versioninfofile << "amsler version : " << amslerversion << endl;
	versioninfofile << "antisaccade version : " << antisaccadeversion << endl;
	versioninfofile << "display version : " << displayversion << endl;
	versioninfofile << "lens distortion version : " << lensdistortionversion << endl;
	versioninfofile << "ptgrey cam lib version : " << cameraversion << endl;
	versioninfofile << "vergence version : " << vergenceversion << endl;
	versioninfofile << "visual field version : " << visualfieldversion << endl;
	versioninfofile << "w4d version : " << w4dversion << endl;
	versioninfofile << "bulbihub communication version : " << webcversion << endl;
	versioninfofile << "pupil 2 version : " << pupil2version << endl;
	versioninfofile << "pupil 4 version : " << pupil4version << endl;
	versioninfofile << "nystagmus version : " << nystagmusversion << endl;
	versioninfofile << "acodapt version : " << contrastversion << endl;

#pragma region VLV_CALIBRATION_TESTS

	versioninfofile << "Calibration Screen Test version : " << SCREENTEST_VERSION << endl;
	versioninfofile << "Calibration Pattern Test version : " << PATTERNTEST_VERSION << endl;
	versioninfofile << "Calibration VPos Test version : " << VPOSTEST_VERSION << endl;
	versioninfofile << "Calibration Timing Test version : " << TIMINGTEST_VERSION << endl;
	versioninfofile << "Calibration Sensor Test version : " << SENSORTEST_VERSION << endl;

#pragma endregion VLV_CALIBRATION_TESTS

	versioninfofile.close();

	stringstream vifhss;
	vifhss << "eyetracking version : " << ETversion << endl
		<< "recording tool version : " << recordingtool_version << endl
		<< "amsler version : " << amslerversion << endl
		<< "antisaccade version : " << antisaccadeversion << endl
		<< "display version : " << displayversion << endl
		<< "lens distortion version : " << lensdistortionversion << endl
		<< "ptgrey cam lib version : " << cameraversion << endl
		<< "vergence version : " << vergenceversion << endl
		<< "visual field version : " << visualfieldversion << endl
		<< "w4d version : " << w4dversion << endl
		<< "bulbihub communication version : " << webcversion << endl
		<< "pupil 2 version : " << pupil2version << endl
		<< "nystagmus version : " << nystagmusversion << endl
		<< "acodapt version : " << contrastversion << endl;
	versionInfoForHub = vifhss.str();

	fps_timer = std::chrono::system_clock::now();
	
	luxConverter[0].loadTable("luxtableod.csv");
	luxConverter[1].loadTable("luxtableos.csv");
	
	display.maxLuminosityOD = luxConverter[0].luxToGrayScale(300);
	display.maxLuminosityOS = luxConverter[1].luxToGrayScale(300);
//	display.maxLuminosityOD = 0.5;
//	display.maxLuminosityOS = 0.5;
	display.changeTextures();

	DWORD dwError, dwPriClass;

	if (!SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS))
	{
		dwError = GetLastError();
		cout << "Failed to enter realtime mode, error : " << dwError << endl;
	}
	dwPriClass = GetPriorityClass(GetCurrentProcess());
	cout << "Current priority class is : " << dwPriClass << endl;

	std::thread(&etProcessor).detach();
	std::thread(&gtProcessor).detach();
	std::thread(&testProcessor).detach();

#ifndef ALVICAMERA
	std::thread(&imageProcessor).detach();
#endif


	display.displayProcessor();
}