#pragma once

#include "Tests.h"
#include <chrono>

#define antisaccadeversion "0.9.20220805"
extern CTest* currentTest;
extern ofstream qualityLog;
extern ofstream testLog;
extern string testLogsPath;

enum FT_STATES
{
	FT_TEST,
	FT_BREAK
};

class CFixTest : public CTest
{
public:
	CFixTest(int targetEye, float testTime, int breakTime, int testCount);
	void ProcessETData(vector<ETData> etData);
private:
	float ft_periodBreak, ft_periodTest;
	int ft_testAmount;
	int ft_greenDotID;
	int ft_eyeNum;
	float convertToMM(RotatedRect pupil, int eye);
	Point2d ft_centralCoord;
	int ft_testcount;
	int ft_state;
};

CFixTest::CFixTest(int targetEye, float testTime, int breakTime, int testCount)
{
	ft_state = FT_TEST;
	ft_eyeNum = targetEye;
	ft_periodBreak = breakTime;
	ft_periodTest = testTime;
	ft_testAmount = testCount;
	ft_testcount = 0;

	webComm->openLog();
	nlohmann::json js221;
	js221["chartTypeString"] = "FIXATION_TEST";
	js221["message_type"] = MESSAGE_TYPE::START_TEST;
	js221["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	webComm->sendJSON(js221);

	std::ostringstream vffn;
	vffn << testLogsPath << "ft_" << patientInfo.examID << "_" << std::chrono::system_clock::now().time_since_epoch().count() << ".csv";
	std::string vffns = vffn.str();

	testLog.open(vffns);

	glfwSetTime(0);

	display.setBackground(0.5, 0.5, 0.5);

	float ppd = min(66, lensDist->PPD);

	Point2d centreScreenToCenterOS = lensDist->mmtopix(ppd / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOS / 7.);
	Point2d centreScreenToCenterOD = lensDist->mmtopix(ppd / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOD / 7.);
	if (ft_eyeNum == 1)
		ft_centralCoord = Point(960 + centreScreenToCenterOS.x, 540);
	else
		ft_centralCoord = Point(2880 - centreScreenToCenterOD.x, 540);

	display.st_greenTargetCoords = ft_centralCoord;
	display.st_showGreen = true;
	display.st_showRed = false;

	display.redrawPending = true;
}
float CFixTest::convertToMM(RotatedRect pupil, int eye)
{
	float sz = MAX(pupil.size.height, pupil.size.width) / 14.5;
	float lll = 0;
	if (sz == 0)
		return 0;
	if (eye == 0)
		lll = -lensDist->dioptersOD;
	else
		lll = -lensDist->dioptersOS;
	float perc = 0;
	if (lll > 0)
		perc = 0.0232;
	else if (lll < 0)
		perc = 0.0178;
	float k = perc * lll;
	return sz - sz * k;
}

void CFixTest::ProcessETData(vector<ETData> etData)
{

	if (ft_state == FT_TEST && glfwGetTime() > ft_periodTest)
	{
		display.st_showGreen = false;
		display.redrawPending = true;
		glfwSetTime(0);
		ft_state = FT_BREAK;
		ft_testcount++;
		if (ft_testcount == ft_testAmount)
		{
			nlohmann::json me1;
			me1["chartTypeString"] = "FIXATION_TEST";
			me1["message_type"] = MESSAGE_TYPE::STOP_TEST;
			me1["timestamp"] = 1 / 100;
			webComm->sendJSON(me1);
			webComm->closeLog();
			qualityLog.close();
			eyetrackingOn = false;
			display.setDragonfly();
			currentTest = nullptr;
			testLog.close();
			return;
		}
		return;
	}
	if (ft_state == FT_BREAK && glfwGetTime() > ft_periodBreak)
	{
		display.st_showGreen = true;
		display.redrawPending = true;
		glfwSetTime(0);
		ft_state = FT_TEST;
		return;
	}

	nlohmann::json as_rawdata;
	as_rawdata["chartTypeString"] = "FIXATION_TEST";
	as_rawdata["message_type"] = MESSAGE_TYPE::DATA_PACKAGE;
	as_rawdata["rawOSx"] = (etData[1].tandemGlints.bpG + etData[1].tandemGlints.dpG).x / 2;
	as_rawdata["rawOSy"] = (etData[1].tandemGlints.bpG + etData[1].tandemGlints.dpG).y / 2;
	as_rawdata["rawODx"] = (etData[0].tandemGlints.bpG + etData[0].tandemGlints.dpG).x / 2;
	as_rawdata["rawODy"] = (etData[0].tandemGlints.bpG + etData[0].tandemGlints.dpG).y / 2;
	as_rawdata["ODPupilSize"] = convertPSToMM(etData[0].rawPupilEllipse, 0);
	as_rawdata["OSPupilSize"] = convertPSToMM(etData[1].rawPupilEllipse, 1);
	as_rawdata["stage"] = ft_state;
	as_rawdata["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	as_rawdata["trial"] = ft_testcount;
	as_rawdata["targettype"] = ft_eyeNum;
	webComm->sendJSON(as_rawdata);
}