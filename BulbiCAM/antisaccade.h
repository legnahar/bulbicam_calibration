#pragma once

#include "Tests.h"
#include <chrono>

#define antisaccadeversion "0.9.20210805"
extern CTest* currentTest;
extern ofstream qualityLog;
extern ofstream testLog;
extern string testLogsPath;
extern int st_prosaccadeShowCentralAlways;
extern int st_antisaccadeShowCentralAlways;

enum AS_STATES
{
	AS_CENTRAL,
	AS_PERIPHERAL
};

enum AS_DIRECTION
{
	AS_LEFT,
	AS_RIGHT
};

class CASTest : public CTest
{
public:
	CASTest(int targetEye, int trainCount, int mainCount, string saccadeTasksTestType, string saccadeTasksChartTypeString = "SACCADE_MERGED");
	void ProcessETData(vector<ETData> etData);
private:
	int as_direction;
	float as_period_white_timeout = 1.f;

	int as_greenDotID;
	int as_eyeNum;
	Point2d as_centralCoord, as_leftCoord, as_rightCoord;

	string as_testType;
	int as_testcount;
	int as_state;
	float nextDelay;
	int maintrials, traintrials;
	string as_chartTypeString;
};

CASTest::CASTest(int targetEye, int trainCount, int mainCount, string saccadeTasksTestType, string saccadeTasksChartTypeString)
{
	as_testType = saccadeTasksTestType;
	as_eyeNum = targetEye;
	maintrials = mainCount;
	traintrials = trainCount;
	webComm->openLog();
	as_chartTypeString = saccadeTasksChartTypeString;
	nlohmann::json js221;
	js221["chartTypeString"] = as_chartTypeString;
	js221["target"] = as_testType;
	js221["message_type"] = MESSAGE_TYPE::START_TEST;
	js221["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	if (saccadeTasksChartTypeString == "SACCADE_MERGED")
		webComm->sendJSON(js221);
	eyetrackingOn = true;
	as_testcount = 0;

	std::ostringstream vffn;
	vffn << testLogsPath << "as_" << patientInfo.examID << "_" << std::chrono::system_clock::now().time_since_epoch().count() << ".csv";
	std::string vffns = vffn.str();

	testLog.open(vffns);

	glfwSetTime(0);

	display.setBackground(0.5, 0.5, 0.5);
	nextDelay = 1 + rand() % 100 / 100.;

	float ppd = min(66, lensDist->PPD);

	Point2d centreScreenToCenterOS = lensDist->mmtopix(ppd / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOS / 7.);
	Point2d centreScreenToCenterOD = lensDist->mmtopix(ppd / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOD / 7.);
	if (as_eyeNum == 1)
	{
		as_centralCoord = Point(960 + centreScreenToCenterOS.x, 540);
		as_leftCoord = lensDist->jumpTo(as_centralCoord, Point2d(-10, 0));
		as_rightCoord = lensDist->jumpTo(as_centralCoord, Point2d(10, 0));

	}
	else
	{
		as_centralCoord = Point(2880 - centreScreenToCenterOD.x, 540);
		as_leftCoord = lensDist->jumpTo(as_centralCoord, Point2d(10, 0));
		as_rightCoord = lensDist->jumpTo(as_centralCoord, Point2d(-10, 0));
	}

	display.st_greenTargetCoords = as_centralCoord;
	display.st_showGreen = true;
	display.st_showRed = false;

	as_direction = rand() % 2;

	as_state = AS_CENTRAL;

	display.redrawPending = true;
}

void CASTest::ProcessETData(vector<ETData> etData)
{

	if (as_state == AS_CENTRAL && glfwGetTime()>nextDelay)
	{
		if ((st_antisaccadeShowCentralAlways && as_testType == "ANTISACCADE")||(st_prosaccadeShowCentralAlways && as_testType == "PROSACCADE"))
		{
			display.st_showGreen = true;
			display.st_showRed = true;
			if (as_direction == 0)
				display.st_redTargetCoords = as_leftCoord;
			else
				display.st_redTargetCoords = as_rightCoord;
			display.st_greenTargetCoords = as_centralCoord;
		}
		else
		{
			display.st_showGreen = false;
			display.st_showRed = true;
			if (as_direction == 0)
				display.st_redTargetCoords = as_leftCoord;
			else
				display.st_redTargetCoords = as_rightCoord;
		}


		display.redrawPending = true;

		glfwSetTime(0);
		nextDelay = 1 + rand() % 100 / 100.;
		as_state = AS_PERIPHERAL;
		return;
	}

	if (as_state == AS_PERIPHERAL && glfwGetTime() > as_period_white_timeout)
	{
		as_testcount++;
		if (as_testcount == maintrials + traintrials)
		{
			nlohmann::json me1;
			me1["chartTypeString"] = as_chartTypeString;
			me1["target"] = as_testType;
			me1["message_type"] = MESSAGE_TYPE::STOP_TEST;
			me1["timestamp"] = 1 / 100;
			webComm->sendJSON(me1);
			webComm->closeLog();
			qualityLog.close();
			eyetrackingOn = false;
			display.setDragonfly();
			currentTest = nullptr;
			testLog.close();
			return;
		}
		display.st_showGreen = true;
		display.st_showRed = false;
		display.st_greenTargetCoords = as_centralCoord;
		display.redrawPending = true;

		as_direction = rand() % 2;
		nextDelay = 1 + rand() % 100 / 100.;
		as_state = AS_CENTRAL;
		glfwSetTime(0);
	}

	nlohmann::json as_rawdata;
	as_rawdata["chartType"] = 25;
	as_rawdata["chartTypeString"] = as_chartTypeString;
	as_rawdata["target"] = as_testType;
	as_rawdata["message_type"] = MESSAGE_TYPE::DATA_PACKAGE;
	as_rawdata["rawOSx"] = (etData[1].tandemGlints.bpG + etData[1].tandemGlints.dpG).x / 2;
	as_rawdata["rawOSy"] = (etData[1].tandemGlints.bpG + etData[1].tandemGlints.dpG).y / 2;
	as_rawdata["rawODx"] = (etData[0].tandemGlints.bpG + etData[0].tandemGlints.dpG).x / 2;
	as_rawdata["rawODy"] = (etData[0].tandemGlints.bpG + etData[0].tandemGlints.dpG).y / 2;
	as_rawdata["targetDotX"] = as_direction * 20 - 10;;
	as_rawdata["training"] = as_testcount < traintrials;
	as_rawdata["stage"] = as_state;
	as_rawdata["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	as_rawdata["trial"] = as_testcount;
	as_rawdata["targettype"] = as_eyeNum;
	webComm->sendJSON(as_rawdata);
}