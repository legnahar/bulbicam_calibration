#pragma once

#include "Tests.h"
#include <chrono>

#define pursuitssaccadesversion "0.9.20211104"

extern CTest* currentTest;
extern ofstream qualityLog;
extern ofstream testLog;
extern float pas_calibrationThreshold;
extern string testLogsPath;

enum ps_states
{
	PS_PURSUIT,
	PS_REST,
	PS_SACCADES
};
enum ps_calibstates
{
	PS_LEFTOS,
	PS_LEFTOD,
	PS_RIGHTOS,
	PS_RIGHTOD
};

class CPSTest : public CTest
{
public:
	CPSTest();
	void ProcessETData(vector<ETData> etData);
private:
	float lcDist(Point2d p1, Point2d p2, int eye);
	double movegreen();
	Point2d ps_stimL[2], ps_stimC[2], ps_stimR[2], ps_stimLC[2], ps_stimRC[2], ps_stimLT[2], ps_stimRT[2];
	Point2d ps_greenOS, ps_greenOD;
	Point2d ps_stimOS, ps_stimOD;
	int ps_greenOSID, ps_greenODID;
	int ps_velocityIndex;
	float ps_velocities[4] = { 0.20, 0.5, 0.15, 0.8 };
	int ps_state;

	Point2d ps_fixPoint;
	int ps_fixCount;

	vector<Point2d> calibrationSamples;
};

float CPSTest::lcDist(Point2d p1, Point2d p2, int eye)
{

	float dist = sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));
	float lll = 0;
	if (eye == 0)
		lll = -lensDist->dioptersOD;
	else
		lll = -lensDist->dioptersOS;
	float perc = 0;
	if (lll > 0)
		perc = 0.0232;
	else if (lll < 0)
		perc = -0.0178;
	float k = perc * lll;

	return dist + dist * k;

}

CPSTest::CPSTest()
{
	ps_fixCount = 0;
	webComm->openLog();
	nlohmann::json js221;
	js221["chartTypeString"] = "PURSUITS_AND_SACCADES";
	js221["message_type"] = MESSAGE_TYPE::START_TEST;
	js221["metadata"] = versionInfoForHub;
	js221["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	js221["target"] = "SMOOTH_PURSUIT";
	webComm->sendJSON(js221);

	eyetrackingOn = true;

	std::ostringstream vffn;
	vffn << testLogsPath << "p_" << patientInfo.examID << "_" << std::chrono::system_clock::now().time_since_epoch().count() << ".csv";
	std::string vffns = vffn.str();
	testLog.open(vffns);

	glfwSetTime(0);

	ps_stimC[0] = Point(2880, 320);
	ps_stimC[1] = Point(960, 320);

	ps_stimLT[0] = lensDist->jumpTo(Point(2880, 320), Point(10, 0));
	ps_stimLT[1] = lensDist->jumpTo(Point(960, 320), Point(-10, 0));

	ps_stimRT[0] = lensDist->jumpTo(Point(2880, 320), Point(-10, 0));
	ps_stimRT[1] = lensDist->jumpTo(Point(960, 320), Point(10, 0));

	ps_stimLC[0] = lensDist->jumpTo(Point(2880, 320), Point(5, 0));
	ps_stimLC[1] = lensDist->jumpTo(Point(960, 320), Point(-5, 0));

	ps_stimRC[0] = lensDist->jumpTo(Point(2880, 320), Point(-5, 0));
	ps_stimRC[1] = lensDist->jumpTo(Point(960, 320), Point(5, 0));


	//	ps_stimL[0] = Point(2880 + extremex, 320);
//	ps_stimL[1] = Point(960 + extremex, 320);

//	ps_stimR[0] = Point(2880 - extremex, 320);
//	ps_stimR[1] = Point(960 - extremex, 320);

	ps_stimC[0].x -= lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOD / 7.);
	ps_stimC[1].x += lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOS / 7.);

	ps_stimLC[0].x -= lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOD / 7.);
	ps_stimLC[1].x += lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOS / 7.);

	ps_stimRC[0].x -= lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOD / 7.);
	ps_stimRC[1].x += lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOS / 7.);

	ps_stimLT[0].x -= lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOD / 7.);
	ps_stimLT[1].x += lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOS / 7.);

	ps_stimRT[0].x -= lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOD / 7.);
	ps_stimRT[1].x += lensDist->mmtopix(lensDist->PPD / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOS / 7.);


	StimuliObjects s1;

	s1.objectColor[0] = 0;
	s1.objectColor[1] = 1;
	s1.objectColor[2] = 0;

	ps_greenOD = ps_stimL[0];

	s1.objectCoordinates[0] = ps_stimL[0].x;
	s1.objectCoordinates[1] = ps_stimL[0].y;

	s1.objectSize = 27.f;
	ps_greenODID = display.addObject(s1);

	StimuliObjects s2;

	ps_greenOS = Point(9999, 320);

	s2.objectColor[0] = 0;
	s2.objectColor[1] = 1;
	s2.objectColor[2] = 0;
	s2.objectCoordinates[0] = 9999;
	s2.objectCoordinates[1] = 320;

	s2.objectSize = 27.f;
	ps_greenOSID = display.addObject(s2);

	display.redrawPending = true;

	ps_state = ps_states::PS_REST;
	ps_velocityIndex = 0;
	ps_greenOD = ps_stimC[0];
	ps_greenOS = ps_stimC[1];

	display.moveObject(ps_greenOSID, ps_greenOS.x, ps_greenOS.y);
	display.moveObject(ps_greenODID, ps_greenOD.x, ps_greenOD.y);

	ps_stimL[0] = ps_stimLT[0];
	ps_stimL[1] = ps_stimLT[1];
	ps_stimR[0] = ps_stimRT[0];
	ps_stimR[1] = ps_stimRT[1];

	testLog << "PARAM" << ";"
		<< pas_calibrationThreshold << endl;

}

double CPSTest::movegreen()
{
	double ggt = glfwGetTime();
	float ps_velocity;
	if (ps_velocityIndex == 3)
		ps_velocity = 0.1 + ggt * 0.05;
	else
		ps_velocity = ps_velocities[ps_velocityIndex];
	double ang = CV_2PI * ps_velocity * ggt;

	if ((ang > CV_2PI * 3 && ps_velocityIndex<3) || (ang > CV_2PI * 6 && ps_velocityIndex == 3) || (ang > CV_2PI && ps_velocityIndex == 2))
	{
		if (ps_velocityIndex == 1)
		{
			ps_stimL[0] = ps_stimLC[0];
			ps_stimL[1] = ps_stimLC[1];
			ps_stimR[0] = ps_stimRC[0];
			ps_stimR[1] = ps_stimRC[1];
		}
		else
		{
			ps_stimL[0] = ps_stimLT[0];
			ps_stimL[1] = ps_stimLT[1];
			ps_stimR[0] = ps_stimRT[0];
			ps_stimR[1] = ps_stimRT[1];
		}
		ang = CV_2PI;
		ps_state = ps_states::PS_REST;
		glfwSetTime(0);
		nlohmann::json js_stopSequenceMessage;
		js_stopSequenceMessage["chartTypeString"] = "PURSUITS_AND_SACCADES";
		js_stopSequenceMessage["message_type"] = 16;
		js_stopSequenceMessage["target"] = "SMOOTH_PURSUIT";
		js_stopSequenceMessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
		webComm->sendJSON(js_stopSequenceMessage);

		if (ps_velocityIndex == 3)
		{
			nlohmann::json pas_stop;
			pas_stop["chartTypeString"] = "PURSUITS_AND_SACCADES";
			pas_stop["message_type"] = MESSAGE_TYPE::STOP_TEST;
			pas_stop["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
			pas_stop["target"] = "SMOOTH_PURSUIT";
			webComm->sendJSON(pas_stop);
			webComm->closeLog();
			qualityLog.close();
			eyetrackingOn = false;
			display.setDragonfly();
			currentTest = nullptr;

			testLog.close();
			return 0;

		}
		else
			ps_velocityIndex++;
	}
	if (sin(ang) > 0)
	{
		ps_greenOD = Point(ps_stimC[0].x + sin(ang) * (ps_stimR[0].x - ps_stimC[0].x), 320);
		ps_greenOS = Point(ps_stimC[1].x + sin(ang) * (ps_stimR[1].x - ps_stimC[1].x), 320);
	}
	else
	{
		ps_greenOD = Point(ps_stimC[0].x + sin(ang) * (ps_stimC[0].x - ps_stimL[0].x), 320);
		ps_greenOS = Point(ps_stimC[1].x + sin(ang) * (ps_stimC[1].x - ps_stimL[1].x), 320);
	}
	display.moveObject(ps_greenOSID, ps_greenOS.x, ps_greenOS.y);
	display.moveObject(ps_greenODID, ps_greenOD.x, ps_greenOD.y);
	display.redrawPending = true;
	return ps_velocity;
}

void CPSTest::ProcessETData(vector<ETData> etData)
{
	double ggt = glfwGetTime();

	Point2d eyeOSraw = Point(0, 0);
	Point2d eyeODraw = Point(0, 0);
	if (etData[1].tandemGlints.bpG.x > 0 && etData[1].tandemGlints.dpG.x > 0)
		eyeOSraw = (etData[1].tandemGlints.bpG + etData[1].tandemGlints.dpG) / 2.;
	if (etData[0].tandemGlints.bpG.x > 0 && etData[0].tandemGlints.dpG.x > 0)
		eyeODraw = (etData[0].tandemGlints.bpG + etData[0].tandemGlints.dpG) / 2.;
	Point2d eyeOS = Point(0, 0);
	Point2d eyeOD = Point(0, 0);
	Point2f stimOS = Point2f(-((ps_stimL[1].x - ps_greenOS.x) / (ps_stimL[1].x - ps_stimR[1].x)) * 20.+10, 0);
	Point2f stimOD = Point2f(-((ps_stimL[0].x - ps_greenOD.x) / (ps_stimL[0].x - ps_stimR[0].x)) * 20.+10, 0);
	testLog << "1" << ";"
		<< eyeOSraw.x << ";"
		<< eyeOSraw.y << ";"
		<< eyeODraw.x << ";"
		<< eyeODraw.y << ";"
		<< stimOS.x << ";"
		<< stimOS.y << endl;

	double freq = 0;
	if (ps_state == PS_REST && ggt >= 3)
	{
		if (ggt >= 3)
		{
			ps_state = ps_states::PS_PURSUIT;
			nlohmann::json js_stopSequenceMessage;
			js_stopSequenceMessage["chartTypeString"] = "PURSUITS_AND_SACCADES";
			js_stopSequenceMessage["message_type"] = 15;
			js_stopSequenceMessage["stimuliOSx"] = stimOS.x;
			js_stopSequenceMessage["stimuliOSy"] = stimOS.y;
			js_stopSequenceMessage["stimuliODx"] = stimOD.x;
			js_stopSequenceMessage["target"] = "SMOOTH_PURSUIT";
			js_stopSequenceMessage["stimuliODy"] = stimOD.y;
			js_stopSequenceMessage["eyeOSx"] = eyeOS.x;
			js_stopSequenceMessage["eyeOSy"] = eyeOS.y;
			js_stopSequenceMessage["eyeODx"] = eyeOD.x;
			js_stopSequenceMessage["eyeODy"] = eyeOD.y;
			js_stopSequenceMessage["rawEyeOSx"] = eyeOSraw.x;
			js_stopSequenceMessage["rawEyeOSy"] = eyeOSraw.y;
			js_stopSequenceMessage["rawEyeODx"] = eyeODraw.x;
			js_stopSequenceMessage["rawEyeODy"] = eyeODraw.y;
			js_stopSequenceMessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
			webComm->sendJSON(js_stopSequenceMessage);

			glfwSetTime(0);
		}
		return;
	}
	if (ps_state == PS_PURSUIT)
	{
		freq = movegreen();
	}
	nlohmann::json pas_data;
	pas_data["chartTypeString"] = "PURSUITS_AND_SACCADES";
	pas_data["message_type"] = MESSAGE_TYPE::DATA_PACKAGE;
	pas_data["stimuliOSx"] = stimOS.x;
	pas_data["stimuliOSy"] = stimOS.y;
	pas_data["stimuliODx"] = stimOD.x;
	pas_data["stimuliODy"] = stimOD.y;
	pas_data["eyeOSx"] = eyeOS.x;
	pas_data["eyeOSy"] = eyeOS.y;
	pas_data["eyeODx"] = eyeOD.x;
	pas_data["eyeODy"] = eyeOD.y;
	pas_data["rawEyeOSx"] = eyeOSraw.x;
	pas_data["rawEyeOSy"] = eyeOSraw.y;
	pas_data["rawEyeODx"] = eyeODraw.x;
	pas_data["rawEyeODy"] = eyeODraw.y;
	pas_data["frequency"] = freq;
	pas_data["message_type"] = MESSAGE_TYPE::DATA_PACKAGE;
	pas_data["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	pas_data["target"] = "SMOOTH_PURSUIT";
	webComm->sendJSON(pas_data);
}