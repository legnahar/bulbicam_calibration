#pragma once

#define RES_X 1920
#define RES_Y 1080

#include <fstream>
#include <chrono>
#include "Tests.h"

class ThreedPixel : public CTest
{

public:
	ThreedPixel();
	void ProcessETData(vector<ETData> etData);

private:
	void initStimuli();
	void virtualToReal(vector<ETData> &etData);

	int objIndexes[2];
	bool objectsCreated = false;
};

ThreedPixel::ThreedPixel()
{
	initStimuli();
}

void ThreedPixel::initStimuli()
{
	StimuliObjects stimuli;

	stimuli.objectColor[0] = 0;
	stimuli.objectColor[1] = 1;
	stimuli.objectColor[2] = 0;
	stimuli.objectCoordinates[0] = 0;
	stimuli.objectCoordinates[1] = 0;
	stimuli.objectSize = 20;
	objIndexes[0] = display.addObject(stimuli);

	stimuli.objectColor[0] = 1;
	stimuli.objectColor[1] = 1;
	stimuli.objectColor[2] = 0;
	stimuli.objectCoordinates[0] = 0;
	stimuli.objectCoordinates[1] = 0;
	stimuli.objectSize = 20;
	objIndexes[1] = display.addObject(stimuli);

	display.setBackgroundL(0, 0, 0);
	display.setBackgroundR(0, 0, 0);

	objectsCreated = true;
	display.redrawPending = true;
	glfwSetTime(0);
}

void ThreedPixel::ProcessETData(vector<ETData> etData)
{
	virtualToReal(etData);

	for (int i = 0; i < 2; i++)
	{
		display.moveObject(objIndexes[i], etData[i].threeDangle.vsPixel.x, etData[i].threeDangle.vsPixel.y);
	}

	display.redrawPending = true;
}

void ThreedPixel::virtualToReal(vector<ETData> &etData)
{
	for (int i = 0; i < 2; i++)
	{
		if (etData[i].threeDangle.vsPixel.x != 0.f)
		{
			if (i == 0)
			{
				etData[i].threeDangle.vsPixel.x = RES_X * 2 - etData[i].threeDangle.vsPixel.x;
			}
			if (i == 1)
			{
				etData[i].threeDangle.vsPixel.x = RES_X - etData[i].threeDangle.vsPixel.x;
			}
		}
	}
}