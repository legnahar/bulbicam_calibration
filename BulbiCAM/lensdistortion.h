#pragma once
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#define lensdistortionversion "0.9.20200206"

enum EYE
{
	OS,
	OD
};

using namespace cv;


class Lens
{
public:
	Lens(int lensholder, float powerOfLens);
	Point2d jumpTo(Point2d currentPosition, Point2d jumpAngle);
	float dioptersOS, dioptersOD;
	int lensHolder;
	int PPD;
	Point2d posfromanglOS(Point2d deg);
	Point2d posfromanglOD(Point2d deg);
	Point2d anglefromposOS(Point2d stim);
	Point2d anglefromposOD(Point2d stim);

int mmtopix(float mm)
{
	return mm * 1920.f / 121.f;
}

Point mmtopix(Point2d mm)
{
	return Point(mmtopix(mm.x), mmtopix(mm.y));
}

float pixtomm(int px)
{
	return px * 121.f / 1920.f;
}

Point2d pixtomm(Point px)
{
	return Point(pixtomm(px.x), pixtomm(px.y));
}
};

