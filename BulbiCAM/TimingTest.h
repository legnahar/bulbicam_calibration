#pragma once
#include <fstream>
#include <chrono>
#include <filesystem>
#include <mutex>
#include "Tests.h"

extern CPTGCamera* ptgreycam;
extern CTest* currentTest;
extern atomic<bool> g_calibrationIsGoing;
extern atomic<bool> g_calibrationForcedStop;
extern int offset;

#define TIMINGTEST_VERSION "1.0.20221027"

class TimingTest : public CTest
{
public:
	TimingTest(bool shortTest);
	void inCaseOfCalibration();
	void finishCalibration(bool forced);

private:
	fstream m_history;

	bool m_shortTest = true;
	string m_calibrationPath;
	string m_resourcesPath;
	Mat m_dpImg, m_bpImg, m_toDrawOn;
	Point2f m_results[2];
	Point2f m_glintsCenters[2];

	void performTest();
	void readLastStains();
	void detectStains();
	void drawResults();
	void setCamBack();
};

TimingTest::TimingTest(bool shortTest) : m_shortTest(shortTest)
{
	webComm->openLog();
	g_calibrationIsGoing = true;
	cout << "Calibration is executing. TimingTest module" << endl;

	char path[MAX_PATH];
	HMODULE hModule = GetModuleHandleA(NULL);
	GetModuleFileNameA(hModule, path, MAX_PATH);
	PathRemoveFileSpecA(path);
	m_calibrationPath = path;
	m_calibrationPath += "\\calibration";
	m_resourcesPath = m_calibrationPath + "\\resources";

	readLastStains();

	if (this->m_shortTest)
	{
		nlohmann::json js;
		js["testType"] = "SHORT_TIMING_TEST";
		js["message_type"] = "START";
		webComm->sendJSON(js);
	}
	else
	{
		nlohmann::json js;
		js["testType"] = "LONG_TIMING_TEST";
		js["message_type"] = "START";
		webComm->sendJSON(js);
	}
}

void TimingTest::readLastStains()
{
	m_history.open(m_resourcesPath + "\\history.txt", ios_base::in);

	if (m_history.is_open())
	{
		string l_str;
		while (getline(m_history, l_str))
		{
			//cout << l_str << endl;
			size_t l_pos = 0;
			string l_token;
			bool token_found = false;
			vector<string> glints_params;

			//Check first word
			if ((l_pos = l_str.find(";")) != std::string::npos)
			{
				l_token = l_str.substr(0, l_pos);
				if (l_token == "VPosTest")
				{
					//cout << "FOUND" << endl;
					token_found = true;
					l_str.erase(0, l_pos + 1);
				}
			}

			if (token_found)
			{
				while ((l_pos = l_str.find(";")) != std::string::npos)
				{
					l_token = l_str.substr(0, l_pos);
					if (token_found)
					{
						glints_params.push_back(l_token);
						l_str.erase(0, l_pos + 1);
					}
				}

				glints_params.push_back(l_str);
				cout << glints_params.size() << endl;
				if (glints_params.size() == 13)
				{
					m_glintsCenters[0] = Point2f(atof(glints_params[3].c_str()), atof(glints_params[4].c_str()));
					m_glintsCenters[1] = Point2f(atof(glints_params[7].c_str()), atof(glints_params[8].c_str()));
				}
				else
				{
					cout << "ERROR: Wrong format of calibration pattern recording." << endl;
					finishCalibration(true);
				}
			}
		}

		if (m_glintsCenters[0] == Point2f() && m_glintsCenters[1] == Point2f())
		{
			cout << "ERROR: No recordings of VPosTest detected. Please, perfom VPos test at first." << endl;
			finishCalibration(true);
		}
		else
		{
			cout << "Glints centers are: " << m_glintsCenters[0] << m_glintsCenters[1] << endl;
		}

		m_history.close();
	}
	else
	{
		cout << "ERROR: Cannot open history.txt file" << endl;
		finishCalibration(true);
	}
}

void TimingTest::performTest()
{
	ptgreycam->expandROI();
	ptgreycam->setExposure(2000);

	std::vector<clock_t> timing[2];

	for (int eye = 0; eye < 2; eye++)
	{
		int sampleSize = m_shortTest ? 100 : 1000;
		bool whitescreen = false;

		for (int sample = 0; sample < sampleSize; sample++)
		{
			//Test is interrupted from outside
			if (g_calibrationForcedStop)
			{
				return;
			}

			if (eye == 0)
			{
				display.setBackgroundR(whitescreen, whitescreen, whitescreen);
			}
			else
			{
				display.setBackgroundL(whitescreen, whitescreen, whitescreen);
			}

			display.redrawPending = true;
			clock_t display_draw_time = clock();
			clock_t sensor_react_time = 0;

			while (true)
			{
				m_dpImg = ptgreycam->getImage();

				if (m_dpImg.at<uchar>(m_glintsCenters[eye]) > 128 && whitescreen ||
					m_dpImg.at<uchar>(m_glintsCenters[eye]) < 128 && !whitescreen)
				{
					sensor_react_time = clock();
					break;
				}

				if (clock() - display_draw_time > 1000)
				{
					cout << "Timeout" << endl;
					break;
				}
			}

			whitescreen = !whitescreen;
			clock_t measured_time = sensor_react_time - display_draw_time;
			cout << eye << "--" << sample << "--" << measured_time << endl;
			timing[eye].push_back(measured_time);
		}

		if (eye == 0)
		{
			display.setBackgroundR(0, 0, 0);
		}
		else
		{
			display.setBackgroundL(0, 0, 0);
		}
	}

	fstream timing_test;

	if (m_shortTest)
	{
		timing_test.open(m_resourcesPath + "\\last_short_timing_test_log.csv", ios_base::out);
	}
	else
	{
		timing_test.open(m_resourcesPath + "\\last_long_timing_test_log.csv", ios_base::out);
	}

	for (int eye = 0; eye < 2; eye++)
	{
		for (int index = 0; index < timing[eye].size(); index++)
		{
			timing_test << eye << ";" << index << ";" << timing[eye].at(index) << endl;
		}
	}

	timing_test.close();

	fstream timing_test_diagram;

	if (m_shortTest)
	{
		timing_test_diagram.open(m_calibrationPath + "\\last_short_timing_test_diagram.csv", ios_base::out);
	}
	else
	{
		timing_test_diagram.open(m_calibrationPath + "\\last_long_timing_test_diagram.csv", ios_base::out);
	}

	for (int eye = 0; eye < 2; eye++)
	{
		std::sort(timing[eye].begin(), timing[eye].end());
		std::map<clock_t, uint> times_met;

		for (int index = 0; index < timing[eye].size(); index++)
		{
			times_met[timing[eye].at(index)]++;
		}

		for (const pair<clock_t, uint>& p : times_met)
		{
			if (p.first > 20)
			{
				timing_test_diagram << eye << ";" << p.first << ";" << p.second << endl;

				if (this->m_shortTest)
				{
					nlohmann::json js;
					js["testType"] = "SHORT_TIMING_TEST";
					js["message_type"] = "DATA_PACKAGE";
					js["eye"] = eye;
					js["x"] = p.first;
					js["y"] = p.second;
					webComm->sendJSON(js);
				}
				else
				{
					nlohmann::json js;
					js["testType"] = "LONG_TIMING_TEST";
					js["message_type"] = "DATA_PACKAGE";
					js["eye"] = eye;
					js["x"] = p.first;
					js["y"] = p.second;
					webComm->sendJSON(js);
				}
			}
		}
	}

	timing_test_diagram.close();
}

void TimingTest::inCaseOfCalibration()
{
	performTest();
	finishCalibration(g_calibrationForcedStop);
}

void TimingTest::setCamBack()
{
	ptgreycam->shrinkROI(offset);
	ptgreycam->setExposure(2000);
	ptgreycam->switchDPBP();
}

void TimingTest::finishCalibration(bool forced)
{
	setCamBack();

	m_history.open(m_resourcesPath + "\\history.txt", ios_base::app);

	if (!m_history.is_open() || forced)
	{
		cout << "ERROR: Cannot open history.txt file or forced stop" << endl;
	}
	else
	{
		uint64_t l_stopTime = std::chrono::system_clock::now().time_since_epoch().count();
		if (m_shortTest)
		{
			m_history << "ShortTimingTest;" << l_stopTime << endl;
		}
		else
		{
			m_history << "LongTimingTest;" << l_stopTime << endl;
		}
		m_history.close();
	}

	if (this->m_shortTest)
	{
		nlohmann::json js;
		js["testType"] = "SHORT_TIMING_TEST";
		js["message_type"] = "STOP";
		webComm->sendJSON(js);
	}
	else
	{
		nlohmann::json js;
		js["testType"] = "LONG_TIMING_TEST";
		js["message_type"] = "STOP";
		webComm->sendJSON(js);
	}

	webComm->closeLog();
	cout << "Calibration is finished. TimingTest module" << endl;
}
