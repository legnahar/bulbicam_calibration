#pragma once

#include "Tests.h"
#include <Shlwapi.h>
#include "comdef.h"

#define w4dversion "0.9.20210805"

enum W4DType
{
	W4DT_NORMAL,
	W4DT_A,
	W4DT_B,
	W4DT_C,
	W4DT_D,
	W4DT_E,
	W4DT_UNDEFINED
};

enum W4DEye
{
	W4DE_OS,
	W4DE_OD
};

enum W4DDistance
{
	W4DD_FAR,
	W4DD_CLOSE
};

class CW4DTest : public CTest
{
public:
	CW4DTest();
	void redraw();
	void switchBG();

	int testNum = 8;

	int w4d_distance;
	int w4d_type;
	int w4d_eye;
	Mat greenPic, redPic;
	Mat leftTex, rightTex;

private:

	bool showBackground;
	void update();
	int farCircleSize = 8;
	int closeCircleSize = 40;

	int farCenterToCenter = 85;
	int closeCenterToCenter = 422;


	int circleSize;
	int centerShapeToCenterCircleGreen;
	int centerShapeToCenterCircleRed;
	int centerShapeGreen;
	int centerShapeRed;
	int centreScreenToCenterShapeOS;
	int centreScreenToCenterShapeOD;
	int LBColor[3];

	StimuliObjects LL;
	StimuliObjects LB;
	StimuliObjects LR;
	StimuliObjects RU;
	StimuliObjects RB;
	StimuliObjects LBl;
	StimuliObjects RBl;
};

CW4DTest::CW4DTest()
{

	WCHAR wpath[MAX_PATH];
	HMODULE hModule = GetModuleHandleW(NULL);
	GetModuleFileNameW(hModule, wpath, MAX_PATH);
	PathRemoveFileSpec(wpath);
	std::wstring wspath = wpath;
	std::string path = std::string(wspath.begin(), wspath.end());

	std::string texfilenamered = path;
	std::string texfilenamegreen = path;

	texfilenamered.append("\\media\\red.jpg");
	texfilenamegreen.append("\\media\\green.jpg");

	leftTex = Mat::zeros(1080, 1920, CV_8UC3);
	rightTex = Mat::zeros(1080, 1920, CV_8UC3);

//	Mat rrr, ggg;

//	rrr = cv::imread(texfilenamered, cv::IMREAD_UNCHANGED);
//	ggg = cv::imread(texfilenamegreen, cv::IMREAD_UNCHANGED);

//	redPic = Mat::zeros(1080, 3840, CV_8UC3);
//	greenPic = Mat::zeros(1080, 3840, CV_8UC3);

//	rrr.copyTo(redPic(Rect(420, 0, 3000, 1080)));
//	ggg.copyTo(greenPic(Rect(420, 0, 3000, 1080)));

	redPic = cv::imread(texfilenamered, cv::IMREAD_UNCHANGED);
	greenPic = cv::imread(texfilenamegreen, cv::IMREAD_UNCHANGED);

	webComm->sendmes("OK");
	w4d_distance = 0;
	w4d_type = 0;
	w4d_eye = 0;

	showBackground = true;

	display.setBackground(0.f, 0.f, 0.f);
	display.stimuliType = STIMULI;
	redraw();
}

void CW4DTest::redraw()
{
	update();

	LL.objectColor[0] = 0;
	LL.objectColor[1] = 1;
	LL.objectColor[2] = 0;

	LL.objectCoordinates[0] = centerShapeGreen + centerShapeToCenterCircleGreen;
	LL.objectCoordinates[1] = 540;

	LL.objectSize = circleSize;

	LR.objectColor[0] = 0;
	LR.objectColor[1] = 1;
	LR.objectColor[2] = 0;

	LR.objectCoordinates[0] = centerShapeGreen - centerShapeToCenterCircleGreen;
	LR.objectCoordinates[1] = 540;

	LR.objectSize = circleSize;

	LB.objectColor[0] = LBColor[0];
	LB.objectColor[1] = LBColor[1];
	LB.objectColor[2] = LBColor[2];

	LB.objectCoordinates[0] = centerShapeGreen;
	LB.objectCoordinates[1] = 540 - centerShapeToCenterCircleGreen;

	LB.objectSize = circleSize;

	RU.objectColor[0] = 1;
	RU.objectColor[1] = 0;
	RU.objectColor[2] = 0;

	RU.objectCoordinates[0] = centerShapeRed;
	RU.objectCoordinates[1] = 540 + centerShapeToCenterCircleRed;

	RU.objectSize = circleSize;

	RB.objectColor[0] = 1;
	RB.objectColor[1] = 0;
	RB.objectColor[2] = 0;

	RB.objectCoordinates[0] = centerShapeRed;
	RB.objectCoordinates[1] = 540 - centerShapeToCenterCircleRed;

	RB.objectSize = circleSize;

	RBl.objectColor[0] = 0;
	RBl.objectColor[1] = 0;
	RBl.objectColor[2] = 0;

	RBl.objectCoordinates[0] = centerShapeRed;
	RBl.objectCoordinates[1] = 540;

	RBl.objectSize = centerShapeToCenterCircleRed + circleSize*2;

	LBl.objectColor[0] = 0;
	LBl.objectColor[1] = 0;
	LBl.objectColor[2] = 0;

	LBl.objectCoordinates[0] = centerShapeGreen;
	LBl.objectCoordinates[1] = 540;

	LBl.objectSize = centerShapeToCenterCircleGreen + circleSize*2;

	display.clearObjects();
	if (w4d_type != W4DT_UNDEFINED)
	{

		if (w4d_type != W4DT_B)
		{
			display.addObject(LL);
			display.addObject(LR);
			display.addObject(LB);
		}
		if (w4d_type != W4DT_C)
		{
			display.addObject(RU);
			if (w4d_type != W4DT_A)
				display.addObject(RB);
		}
		if (w4d_type == W4DT_NORMAL && w4d_distance == W4DD_CLOSE)
		{
			display.addObject(LBl);
			display.addObject(RBl);
		}
	}
	display.redrawPending = true;
}

void CW4DTest::switchBG()
{
	showBackground = !showBackground;
	redraw();
}

void CW4DTest::update()
{
	LBColor[0] = 0;
	LBColor[1] = 1;
	LBColor[2] = 0;

	if (w4d_distance == W4DD_FAR)
	{
		circleSize = farCircleSize;
		centerShapeToCenterCircleGreen = farCenterToCenter / 2.;
		centerShapeToCenterCircleRed = farCenterToCenter / 2.;
		centreScreenToCenterShapeOS = lensDist->mmtopix(lensDist->PPD / 2.) - lensDist->mmtopix(1) - lensDist->mmtopix(10) * (-lensDist->dioptersOS / 7.);
		cout << "OS Eye = " << centreScreenToCenterShapeOS << endl;
		centreScreenToCenterShapeOD = lensDist->mmtopix(lensDist->PPD / 2.) - lensDist->mmtopix(1) - lensDist->mmtopix(10) * (-lensDist->dioptersOD / 7.);
		cout << "OD Eye = " << centreScreenToCenterShapeOD << endl;
	}
	else
	{
		circleSize = closeCircleSize;
		centerShapeToCenterCircleGreen = closeCenterToCenter / 2.;
		centerShapeToCenterCircleRed = closeCenterToCenter / 2.;
		centreScreenToCenterShapeOS = lensDist->mmtopix(lensDist->PPD / 2.)*0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOS / 7.);
		cout << "OS Eye = " << centreScreenToCenterShapeOD << endl;
		centreScreenToCenterShapeOD = lensDist->mmtopix(lensDist->PPD / 2.)*0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOD / 7.);
		cout << "OD Eye = " << centreScreenToCenterShapeOD << endl;
	}

	if (w4d_type == W4DT_NORMAL)
	{
		centerShapeGreen = 960 + centreScreenToCenterShapeOS;
		centerShapeRed = 2880 - centreScreenToCenterShapeOD;
	}
	if (showBackground)
	{
		greenPic(cv::Rect(greenPic.cols / 2 - 1920 + 960 - (lensDist->mmtopix(lensDist->PPD / 2.) - lensDist->mmtopix(1) - lensDist->mmtopix(10) * (-lensDist->dioptersOS / 7.)), 0, 1920, 1080)).copyTo(leftTex);
		redPic(cv::Rect(redPic.cols / 2 - 960 + (lensDist->mmtopix(lensDist->PPD / 2.) - lensDist->mmtopix(1) - lensDist->mmtopix(10) * (-lensDist->dioptersOD / 7.)), 0, 1920, 1080)).copyTo(rightTex);
		display.setTextures(leftTex, rightTex);
	}
	else
	{
		display.setBackground(0, 0, 0);
	}

	if (w4d_type == W4DT_A)
	{
		if (w4d_eye == W4DE_OS)
		{
			centerShapeGreen = 960 + centreScreenToCenterShapeOS;
			centerShapeRed = 960 + centreScreenToCenterShapeOS;
		}
		else
		{
			centerShapeGreen = 2880 - centreScreenToCenterShapeOD;
			centerShapeRed = 2880 - centreScreenToCenterShapeOD;
		}
		LBColor[0] = 1;
		LBColor[1] = 1;
		LBColor[2] = 1;
	}

	if (w4d_type == W4DT_B)
	{
		if (w4d_eye == W4DE_OS)
			centerShapeRed = 960 + centreScreenToCenterShapeOS;
		else
			centerShapeRed = 2880 - centreScreenToCenterShapeOD;
	}

	if (w4d_type == W4DT_C)
	{
		if (w4d_eye == W4DE_OS)
			centerShapeGreen = 960 + centreScreenToCenterShapeOS;
		else
			centerShapeGreen = 2880 - centreScreenToCenterShapeOD;
	}

	if (w4d_type == W4DT_D)
	{
		if (w4d_eye == W4DE_OS)
		{
			centerShapeGreen = 960 + centreScreenToCenterShapeOS;
			centerShapeRed = 960 + centreScreenToCenterShapeOS - 2 * centerShapeToCenterCircleGreen;
		}
		else
		{
			centerShapeGreen = 2880 - centreScreenToCenterShapeOD;
			centerShapeRed = 2880 - centreScreenToCenterShapeOD - 2 * centerShapeToCenterCircleGreen;
		}
	}

	if (w4d_type == W4DT_E)
	{
		if (w4d_eye == W4DE_OS)
		{
			centerShapeGreen = 960 + centreScreenToCenterShapeOS;
			centerShapeRed = 960 + centreScreenToCenterShapeOS + 2 * centerShapeToCenterCircleGreen;
		}
		else
		{
			centerShapeGreen = 2880 - centreScreenToCenterShapeOD;
			centerShapeRed = 2880 - centreScreenToCenterShapeOD + 2 * centerShapeToCenterCircleGreen;
		}
	}
}