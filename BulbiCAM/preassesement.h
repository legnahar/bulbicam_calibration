#pragma once

#include <fstream>
#include <chrono>
#include <boost/filesystem.hpp>
#include <mutex>
#include "Tests.h"

#define duringdragonfly_version "0.9.20210805"	//3 secs timeout + save anyway in case of bad condition
#define RES_X		1920
#define RES_Y		1080

extern CPTGCamera* ptgreycam;
extern EyeTracker et;
extern CTest* currentTest;
extern void dataRead2(vector<pair<string, string>> commands, bool fromPreassesment);
extern vector<pair<string, string>> savedCommands;


class DuringDragonfly : public CTest
{
public:
	DuringDragonfly();
	bool successfulTracking = false;
	void ProcessETData(vector<ETData> etData);

private:
	ofstream ofl;
	int waitfor;
	const int lastn = 10;
	const int saveFrames = 10;
	double timeout = 3.0;
	int nSavedFrames = 0;
	int exposureTime;
	int goodsequence[2] = { 0,0 };
	bool patientin = false;
	vector<Size2f> _greenVecs[2];
	string picsDirPath;

	float pDist(const Point2f& one, const Point2f& two);
	void isIOL(const vector<ETData>& etData, const int& eye);
	void wellCenter(const vector<ETData>& etData, const int& eye);
};

DuringDragonfly::DuringDragonfly()
{
	//Frames directory and log file
	uint64_t creationTime = std::chrono::system_clock::now().time_since_epoch().count();
//	picsDirPath = "c:\\testvideo\\green_pt_pics_" + to_string(creationTime);
//	boost::filesystem::create_directory(picsDirPath);
	std::ostringstream vffn;
	vffn << testLogsPath << "expnrot_" << creationTime << ".csv";
	std::string vffns = vffn.str();
	ofl.open(vffns);
	glfwSetTime(0);
}

float DuringDragonfly::pDist(const Point2f& p1, const Point2f& p2)
{
	return sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));
}

void DuringDragonfly::ProcessETData(vector<ETData> etData)
{
	//Go through eyes
	for (int pp = 0; pp < etData.size(); pp++)
	{
		//Check how good tracking is
		if (et.nPupils[pp].size() > 1 &&
			etData[pp].rawPupilEllipse.size.width > 0 &&
			pDist(et.nPupils[pp].back().center, et.nPupils[pp].at(et.nPupils[pp].size() - 2).center) < 30)
		{
			//If winner exists and hence, tracking is good
			goodsequence[pp]++;
		}
		else
		{
			goodsequence[pp] = 0;
		}

		//300 results and we count that patient is in
		if (goodsequence[pp] >= 150 && etData[pp].rawPupilEllipse.size.width > 0 && waitfor == 0)
		{
			isIOL(etData, pp);
			wellCenter(etData, pp);

			//Save required number of frames each 30 ms
			if (nSavedFrames < saveFrames && int(glfwGetTime() * 1000) % 30 == 0 && glfwGetTime() <= (timeout - 0.8))
			{
				nSavedFrames++;

				//If even - save DP image
				if (nSavedFrames % 2 == 0)
				{
					imwrite(picsDirPath + "\\" + to_string(nSavedFrames) + "-DP.bmp", et.lastDPimage);
				}
				else
				{
					if (et.trueLastBPimage.size)
						imwrite(picsDirPath + "\\" + to_string(nSavedFrames) + "-BP.bmp", et.trueLastBPimage);
				}
			}

			ofl << "Warning = " << et.tsquality[pp].drawWarning << endl;

			if (patientin == false)
			{
				glfwSetTime(0);
				patientin = true;
			}

			float averbri = 0;
			if (et.nBcontr[pp].size() >= lastn)
			{
				for (int id = 0; id < lastn; id++)
				{
					averbri += et.nBcontr[pp].at(et.nBcontr[pp].size() - id - 1);
				}
			}
			averbri /= lastn;

			ofl << "BRIGHTNESS = " << averbri << endl;

			if (averbri > 900 && exposureTime > 500)
			{
				exposureTime -= 50;
				try
				{
					ptgreycam->setExposure(exposureTime);
					waitfor = lastn;
					ofl << "BRIGHTNESS = " << averbri << " NEW_EXP_TIME = " << exposureTime << endl;
				}
				catch (exception ex)
				{
					ofl << "EXP_TIME_SETTING_ERROR = " << ex.what() << endl;
				}
			}
			else if (glfwGetTime() > (timeout - 1) && patientin)
			{
				if (ofl.is_open())
				{
					cout << "Pre-assesement is completed." << endl;
					ofl << "Patient was inside successfully during 2 seconds." << endl;
					ofl.close();
				}
				//webComm->sendmes("OK");
				currentTest = nullptr;
				dataRead2(savedCommands, true);
				return;
			}
		}

		if (pp == 0 && waitfor > 0)
		{
			waitfor--;
		}
	}

	//0.5 sec remaining, but not enough frames saved. Save all
	if (glfwGetTime() > (timeout - 0.8) && nSavedFrames < saveFrames && int(glfwGetTime() * 1000) % 30 == 0)
	{
		nSavedFrames++;

		//If even - save DP image
		if (nSavedFrames % 2 == 0)
		{
			imwrite(picsDirPath + "\\" + to_string(nSavedFrames) + "-DP.bmp", et.lastDPimage);
		}
		else
		{
			if (et.trueLastBPimage.size)
				imwrite(picsDirPath + "\\" + to_string(nSavedFrames) + "-BP.bmp", et.trueLastBPimage);
		}
	}
	if (glfwGetTime() > timeout && patientin)
	{
		if (ofl.is_open())
		{
			cout << "Pre-assesement is completed." << endl;
			ofl << "Patiend was inside successfully during suspiciously long time 4 sec." << endl;
			ofl.close();
		}
		//webComm->sendmes("OK");
		currentTest = nullptr;
		dataRead2(savedCommands, true);
		return;
	}
	else if (glfwGetTime() > timeout)
	{
		if (ofl.is_open())
		{
			cout << "Pre-assesement is not completed." << endl;
			ofl << "Patiend was not inside." << endl;
			ofl.close();
		}
		//webComm->sendmes("OK");
		currentTest = nullptr;
		dataRead2(savedCommands, true);
		return;
	}
}

void DuringDragonfly::isIOL(const vector<ETData>& etData, const int& eye)
{
	int n_dpgs_inside = 0;
	//int n_bpgs_inside = 0;

	for_each(et.dpgCands[eye].begin(), et.dpgCands[eye].end(), [&n_dpgs_inside, &eye, &etData, this](const Point2f& this_dpg)
	{n_dpgs_inside += pDist(etData[eye].rawPupilEllipse.center, this_dpg) < etData[eye].rawPupilEllipse.size.width / 2; });

	//for_each(et.bpgCands[eye].begin(), et.bpgCands[eye].end(), [&n_bpgs_inside, &eye, &etData, this](const Point2f& this_bpg)
	//{n_bpgs_inside += pDist(etData[eye].rawPupilEllipse.center, this_bpg) < etData[eye].rawPupilEllipse.size.width / 2; });

	if (n_dpgs_inside > 1 && et.pupType == PupilType::BPDP)
	{
		//et.pupType = PupilType::DP;
		ofl << "IOL positive. NOT Switched to Dark Pupil Tracking" << endl;
	}
}

void DuringDragonfly::wellCenter(const vector<ETData>& etData, const int& eye)
{
	Rect center = Rect(et.defaultBorders[eye].x + 100, et.defaultBorders[eye].y + 100, 150, 150);
}