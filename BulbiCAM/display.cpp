#include "display.h"
#include <Shlwapi.h>
#include "comdef.h"
#include <chrono>

extern std::chrono::time_point<std::chrono::system_clock> starttesttime;
extern std::string screenshotsPath;
extern float st_targetRadius;

void CDisplay::initGL(int framewidth, int frameheight)
{
	frameHeight = frameheight;
	frameWidth = framewidth;
	glfwSetErrorCallback(errorCallback);
	glfwInit();
	shaderTime = std::chrono::system_clock::now();

}

void CDisplay::displayProcessor()
{
	frameNumber = 0;
	glfwSwapInterval(0);
	while (!glfwWindowShouldClose(techstaffWindow))
	{
//		std::cout << "26\n";
		glfwMakeContextCurrent(techstaffWindow);
		glUseProgram(techstaffProgramID);
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		camMutex.lock();
		glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, 0, camPicture.cols, camPicture.rows, 1, GL_RED, GL_UNSIGNED_BYTE, camPicture.ptr());
		camMutex.unlock();

		screensMutex.lock();
		glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, 2, screenTex.cols, screenTex.rows, 1, GL_BGR, GL_UNSIGNED_BYTE, screenTex.ptr());
		screensMutex.unlock();

		warnMutex.lock();
		glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, 1, qiTex.cols, qiTex.rows, 1, GL_BGR, GL_UNSIGNED_BYTE, qiTex.ptr());
		warnMutex.unlock();

		glBindVertexArray(TSVAO);
		glDrawElements(GL_TRIANGLES, 30, GL_UNSIGNED_INT, 0);

		glfwSwapBuffers(techstaffWindow);

		if (stimuliType == DisplayStimuliType::DRAGONFLY || redrawPending)
		{
			frameNumber++;
			switch (stimuliType)
			{
			case DisplayStimuliType::DRAGONFLY:
				showDragonfly();
				break;
			case DisplayStimuliType::STIMULI:
				redraw();
				break;
			case DisplayStimuliType::VERGENCE:
				redrawCross();
				break;
			case DisplayStimuliType::AMSLER:
				drawAmsler();
				break;
			case DisplayStimuliType::NYSTAGMUS:
				drawNystagmus();
				break;
			case DisplayStimuliType::CONTRAST:
				drawContrast();
				break;
			case DisplayStimuliType::SACCADETASK:
				drawSaccadeTask();
				break;
			}
		}
		glfwPollEvents();
//		std::cout << "89\n";
	}
	std::cerr << "NORMAL WINDOW TERMINATION" << std::endl;
}

void CDisplay::initWindows(bool initTechSWindow, bool initSWindow)
{
	if(initTechSWindow)
		initTSWindow();
	if (initSWindow)
		initStimuliWindow();
	glfwFocusWindow(stimuliWindow);
	glfwFocusWindow(techstaffWindow);

}

void CDisplay::initTSWindow()
{
	qiTex = cv::Mat::zeros(cv::Size(frameWidth, frameHeight), CV_8UC3);

	qiTex.setTo(CV_RGB(0.2 * 255, 0.3 * 255, 0.3 * 255));

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_DECORATED, GLFW_FALSE);
	glfwWindowHint(GLFW_FLOATING, GLFW_FALSE);

	techstaffWindow = glfwCreateWindow(1920, 1080, "BulbiCAM Tech Staff", NULL, NULL);

	if (techstaffWindow == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
	}
	glfwMakeContextCurrent(techstaffWindow);
	glfwSetKeyCallback(techstaffWindow, keyCallback);
	glfwSetMouseButtonCallback(techstaffWindow, mouseCallback);
	glfwSetScrollCallback(techstaffWindow, scrollCallback);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
	}

	unsigned int vertex, fragment;
	vertex = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex, 1, &TSvertexSource, NULL);
	glCompileShader(vertex);
	//	std::cout << "VertexTS" << std::endl;
	//	checkCompilation(vertex);

	fragment = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment, 1, &TSfragmentSource, NULL);
	glCompileShader(fragment);
	//	std::cout << "FragmentTS" << std::endl;
	//checkCompilation(fragment);

	techstaffProgramID = glCreateProgram();

	glAttachShader(techstaffProgramID, vertex);
	glAttachShader(techstaffProgramID, fragment);
	glLinkProgram(techstaffProgramID);

	glDeleteShader(vertex);
	glDeleteShader(fragment);

	glGenVertexArrays(1, &TSVAO);
	glGenBuffers(1, &TSVBO);
	glGenBuffers(1, &TSEBO);

	glBindVertexArray(TSVAO);

	glBindBuffer(GL_ARRAY_BUFFER, TSVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(TSVertices), TSVertices, GL_STREAM_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, TSEBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(TSIndices), TSIndices, GL_STREAM_DRAW);

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(2 * sizeof(float)));
	glEnableVertexAttribArray(1);

	glGenTextures(1, &camTexture);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D_ARRAY, camTexture);
	glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGB, frameWidth, frameHeight, 3, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);

	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);


}

void CDisplay::updateCamPicture(cv::Mat newCamPicture)
{
	camMutex.lock();
	camPicture = newCamPicture.clone();
	camMutex.unlock();
}

void CDisplay::updateQIPicture(cv::Mat newQIPicture)
{
	warnMutex.lock();
	qiTex = newQIPicture.clone();
	warnMutex.unlock();
}

void CDisplay::initStimuliWindow()
{
	WCHAR wpath[MAX_PATH];
	HMODULE hModule = GetModuleHandleW(NULL);
	GetModuleFileNameW(hModule, wpath, MAX_PATH);
	PathRemoveFileSpec(wpath);
	std::wstring wspath = wpath;
	std::string path = std::string(wspath.begin(), wspath.end());

	std::string texfilenamel = path;
	std::string texfilenamer = path;
	std::string texfilenameb = path;
	std::string texfilenamebf = path;
	std::string texfilenamebfvid = path;
	std::string texfilenamea1vid = path;
	std::string texfilenamea2vid = path;
	std::string texfilenamea3vid = path;
	std::string texfilenamea4vid = path;
	std::string texfilenamea5vid = path;
	std::string texfilenamea6vid = path;
	std::string texfilenamea7vid = path;
	std::string texfilenamea8vid = path;
	std::string texfilenamea9vid = path;
	std::string texfilenamea10vid = path;
	std::string texfilenamet = path;
	std::string texfilenameeb = path;

	acImg = cv::Mat(1080, 5760, CV_8UC3);
	texfilenamel.append("\\media\\dragonflyL.mp4");
	texfilenamer.append("\\media\\dragonflyR.mp4");
	texfilenameb.append("\\media\\background.jpg");
	texfilenamebf.append("\\media\\butterfly.png");
	texfilenamet.append("\\media\\turtle.png");
	texfilenamea10vid.append("\\media\\cat_grey.mp4");
	texfilenamea1vid.append("\\media\\cock_grey.mp4");
	texfilenamea2vid.append("\\media\\cow_grey.mp4");
	texfilenamea3vid.append("\\media\\dog_grey.mp4");
	texfilenamea4vid.append("\\media\\elephant_grey.mp4");
	texfilenamea5vid.append("\\media\\monkey_grey.mp4");
	texfilenamea6vid.append("\\media\\panda_grey.mp4");
	texfilenamea7vid.append("\\media\\pig_grey.mp4");
	texfilenamea8vid.append("\\media\\rabbit_grey.mp4");
	texfilenamea9vid.append("\\media\\sheep_grey.mp4");
	texfilenamebfvid.append("\\media\\butterfly.mov");
	texfilenameeb.append("\\media\\eyeblink.mp4");


	for (int i = 0; i < 16; i++)
	{
		std::string texfilenamec = path;
		texfilenamec.append(cv::format("\\media\\numtex\\%d.jpg", i+1));
		numTex[i] = cv::imread(texfilenamec, cv::IMREAD_COLOR);
	}

	for (int i = 1; i <= 9; i++)
	{
		std::string texfilenamec = path;
		texfilenamec.append(cv::format("\\media\\vftex\\%d.png", i));
		numTexVF[i-1] = cv::imread(texfilenamec, cv::IMREAD_COLOR);
	}


	dragonflyOpened = dfL.open(texfilenamel) && dfR.open(texfilenamer);
	bool bvop = bfVideo.open(texfilenamebfvid);
	std::cout << "bfv : " << bvop << std::endl;
	bfVideo.set(cv::CAP_PROP_POS_FRAMES, 0);
	bfVideo.read(bflvtex);

	animalVideos[0].open(texfilenamea1vid);
	animalVideos[1].open(texfilenamea2vid);
	animalVideos[2].open(texfilenamea3vid);
	animalVideos[3].open(texfilenamea4vid);
	animalVideos[4].open(texfilenamea5vid);
	animalVideos[5].open(texfilenamea6vid);
	animalVideos[6].open(texfilenamea7vid);
	animalVideos[7].open(texfilenamea8vid);
	animalVideos[8].open(texfilenamea9vid);
	animalVideos[9].open(texfilenamea10vid);
	animalVideos[0].read(bflvtex);

	for (int i = 0; i <= 256; i++)
	{
		std::string texfilename = path;
		texfilename.append(cv::format("\\media\\p4tex\\%d.png", i));
		bgTexOS[i] = cv::imread(texfilename, cv::IMREAD_COLOR);
		bgTexOD[i] = cv::imread(texfilename, cv::IMREAD_COLOR);
	}
	eyeBlink.open(texfilenameeb);
	eyeBlink.read(ebTex);
	ebTex1 = ebTex.clone();
	cv::cvtColor(ebTex1, ebTex1, cv::COLOR_BGR2BGRA);

	ssrawdata = new GLubyte[3840 * 1080 * 3];
	screenPicture = cv::Mat::zeros(1080, 1920 * 2, CV_8UC3);

	resolutionX = 1920;
	resolutionY = 1080;
	distanceToScreen = 160;
	physicalSizeX = 121;
	physicalSizeY = 68;

	mmToPix = (float)resolutionX / 121.f;
	pxTomm = 121.f / (float)resolutionX;
	degToPix = (float)((float)resolutionX * (160.f*tan(6.283185307179586 / 360.f))) / 121.f;
	pdToPix = (float)(resolutionX * 160.f / 100.f) / 121.f;
	pdTomm = (float)(float)160.f / 100.f;


	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_DECORATED, GLFW_FALSE);
	glfwWindowHint(GLFW_FLOATING, GLFW_TRUE);

	stimuliWindow = glfwCreateWindow(resolutionX * 2, resolutionY, "BulbiCAM Stimuli", NULL, NULL);
	glfwSetWindowPos(stimuliWindow, resolutionX, 0);
	if (stimuliWindow == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
	}
	glfwMakeContextCurrent(stimuliWindow);


	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
	}

	stimuliSTProgramID = glCreateProgram();
	unsigned int vertexST, fragmentST;
	vertexST = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexST, 1, &stimuli_VertexSource, NULL);
	glCompileShader(vertexST);
	fragmentST = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentST, 1, &saccadetask_FragmentSource, NULL);
	glCompileShader(fragmentST);
	GLint isCompiled = 0;
	glGetShaderiv(fragmentST, GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetShaderiv(fragmentST, GL_INFO_LOG_LENGTH, &maxLength);

		// The maxLength includes the NULL character
		std::vector<GLchar> errorLog(maxLength);
		glGetShaderInfoLog(fragmentST, maxLength, &maxLength, &errorLog[0]);
		std::cout << "ERROR 1:" << &errorLog[0] << std::endl;
	}
	glAttachShader(stimuliSTProgramID, vertexST);
	glAttachShader(stimuliSTProgramID, fragmentST);
	glLinkProgram(stimuliSTProgramID);
	glDeleteShader(fragmentST);
	glDeleteShader(vertexST);

	glGenVertexArrays(1, &VAOST);
	glGenBuffers(1, &VBOST);
	glGenBuffers(1, &EBOST);

	glBindVertexArray(VAOST);

	glBindBuffer(GL_ARRAY_BUFFER, VBOST);
	glBufferData(GL_ARRAY_BUFFER, sizeof(stimVertices), stimVertices, GL_STREAM_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBOST);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(stimIndices), stimIndices, GL_STREAM_DRAW);

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	stimuliCAProgramID = glCreateProgram();
	glGenTextures(1, &amd_bgtex);
	unsigned int vertexCA, fragmentCA;
	vertexCA = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexCA, 1, &stimuli_VertexSource, NULL);
	glCompileShader(vertexCA);
	fragmentCA = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentCA, 1, &contrast_FragmentSource, NULL);
	glCompileShader(fragmentCA);
	isCompiled = 0;
	glGetShaderiv(fragmentCA, GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetShaderiv(fragmentCA, GL_INFO_LOG_LENGTH, &maxLength);

		// The maxLength includes the NULL character
		std::vector<GLchar> errorLog(maxLength);
		glGetShaderInfoLog(fragmentCA, maxLength, &maxLength, &errorLog[0]);
		std::cout << "ERROR 3:" << &errorLog[0] << std::endl;
	}
	glAttachShader(stimuliCAProgramID, vertexCA);
	glAttachShader(stimuliCAProgramID, fragmentCA);
	glLinkProgram(stimuliCAProgramID);
	glDeleteShader(fragmentCA);
	glDeleteShader(vertexCA);

	glGenVertexArrays(1, &VAOCA);
	glGenBuffers(1, &VBOCA);
	glGenBuffers(1, &EBOCA);

	glBindVertexArray(VAOCA);

	glBindBuffer(GL_ARRAY_BUFFER, VBOCA);
	glBufferData(GL_ARRAY_BUFFER, sizeof(stimVertices), stimVertices, GL_STREAM_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBOCA);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(stimIndices), stimIndices, GL_STREAM_DRAW);

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	glGenTextures(1, &acTexture);
	acImg = cv::imread(texfilenameb, cv::IMREAD_UNCHANGED);

	cv::Mat acbg = cv::Mat(1080, 3840, CV_8UC3);
	acImg(cv::Rect(0, 0, 1920, 1080)).copyTo(acbg(cv::Rect(0, 0, 1920, 1080)));
	acImg(cv::Rect(mmToPix * 64, 0, 1920, 1080)).copyTo(acbg(cv::Rect(1920, 0, 1920, 1080)));

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, acTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//	cv::imshow("www", acImg);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, acbg.cols, acbg.rows, 0, GL_BGR, GL_UNSIGNED_BYTE, acbg.ptr());

	glGenTextures(1, &bgTextures);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D_ARRAY, bgTextures);
	glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGB, 1920, 1080, 2, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);

	glGenTextures(1, &vfnumTextures);
	unsigned int vertexC, fragmentC;
	vertexC = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexC, 1, &stimuli_VertexSource, NULL);
	glCompileShader(vertexC);

	fragmentC = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentC, 1, &stimuli_FragmentSource, NULL);
	glCompileShader(fragmentC);
	glGetShaderiv(fragmentC, GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetShaderiv(fragmentC, GL_INFO_LOG_LENGTH, &maxLength);

		// The maxLength includes the NULL character
		std::vector<GLchar> errorLog(maxLength);
		glGetShaderInfoLog(fragmentC, maxLength, &maxLength, &errorLog[0]);
		std::cout << "ERROR 4:" << &errorLog[0] << std::endl;
	}
	stimuliProgramID = glCreateProgram();

	glAttachShader(stimuliProgramID, vertexC);
	glAttachShader(stimuliProgramID, fragmentC);
	glLinkProgram(stimuliProgramID);

	glDeleteShader(vertexC);
	glDeleteShader(fragmentC);

	glGenVertexArrays(1, &VAOC);
	glGenBuffers(1, &VBOC);
	glGenBuffers(1, &EBOC);

	glBindVertexArray(VAOC);

	glBindBuffer(GL_ARRAY_BUFFER, VBOC);
	glBufferData(GL_ARRAY_BUFFER, sizeof(stimVertices), stimVertices, GL_STREAM_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBOC);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(stimIndices), stimIndices, GL_STREAM_DRAW);

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	unsigned int vertexM, fragmentM;
	vertexM = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexM, 1, &vergence_VertexSource, NULL);
	glCompileShader(vertexM);

	fragmentM = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentM, 1, &vergence_FragmentSource, NULL);
	glCompileShader(fragmentM);
	glGetShaderiv(fragmentM, GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetShaderiv(fragmentM, GL_INFO_LOG_LENGTH, &maxLength);

		// The maxLength includes the NULL character
		std::vector<GLchar> errorLog(maxLength);
		glGetShaderInfoLog(fragmentM, maxLength, &maxLength, &errorLog[0]);
		std::cout << "ERROR 5:" << &errorLog[0] << std::endl;
	}

	stimuliMProgramID = glCreateProgram();

	glAttachShader(stimuliMProgramID, vertexM);
	glAttachShader(stimuliMProgramID, fragmentM);
	glLinkProgram(stimuliMProgramID);

	glDeleteShader(vertexM);
	glDeleteShader(fragmentM);

	glGenVertexArrays(1, &VAOM);
	glGenBuffers(1, &VBOM);
	glGenBuffers(1, &EBOM);

	glGenTextures(1, &numTextures);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D_ARRAY, numTextures);
	glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGB, 60, 60, 16, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);

	unsigned int vertexA, fragmentA;
	vertexA = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexA, 1, &amsler_VertexSource, NULL);
	glCompileShader(vertexA);

	fragmentA = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentA, 1, &amsler_FragmentSource, NULL);
	glCompileShader(fragmentA);
	glGetShaderiv(fragmentA, GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetShaderiv(fragmentA, GL_INFO_LOG_LENGTH, &maxLength);

		// The maxLength includes the NULL character
		std::vector<GLchar> errorLog(maxLength);
		glGetShaderInfoLog(fragmentA, maxLength, &maxLength, &errorLog[0]);
		std::cout << "ERROR 6:" << &errorLog[0] << std::endl;
	}

	stimuliAProgramID = glCreateProgram();

	glAttachShader(stimuliAProgramID, vertexA);
	glAttachShader(stimuliAProgramID, fragmentA);
	glLinkProgram(stimuliAProgramID);

	glDeleteShader(vertexA);
	glDeleteShader(fragmentA);

	glGenVertexArrays(1, &VAOA);
	glGenBuffers(1, &VBOA);
	glGenBuffers(1, &EBOA);

	glBindVertexArray(VAOA);

	glBindBuffer(GL_ARRAY_BUFFER, VBOA);
	glBufferData(GL_ARRAY_BUFFER, sizeof(stimVertices), stimVertices, GL_STREAM_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBOA);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(stimIndices), stimIndices, GL_STREAM_DRAW);

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	backgroundColorL[0] = 0.0;
	backgroundColorL[1] = 0.0;
	backgroundColorL[2] = 0.0;
	backgroundColorR[0] = 0.0;
	backgroundColorR[1] = 0.0;
	backgroundColorR[2] = 0.0;


	glGenTextures(1, &dfTextures);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D_ARRAY, dfTextures);
	glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGB, 1920, 1080, 2, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);

	unsigned int vertexDF, fragmentDF;
	vertexDF = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexDF, 1, &dragonfly_VertexSource, NULL);
	glCompileShader(vertexDF);

	fragmentDF = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentDF, 1, &dragonfly_FragmentSource, NULL);
	glCompileShader(fragmentDF);

	stimuliDFProgramID = glCreateProgram();

	glAttachShader(stimuliDFProgramID, vertexDF);
	glAttachShader(stimuliDFProgramID, fragmentDF);
	glLinkProgram(stimuliDFProgramID);

	glDeleteShader(vertexDF);
	glDeleteShader(fragmentDF);

	glGenVertexArrays(1, &VAODF);
	glGenBuffers(1, &VBODF);
	glGenBuffers(1, &EBODF);	
	
	glGenTextures(1, &p2Textures);

	unsigned int vertexP2, fragmentP2;
	vertexP2 = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexP2, 1, &pupil2_VertexSource, NULL);
	glCompileShader(vertexP2);

	fragmentP2 = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentP2, 1, &pupil2_FragmentSource, NULL);
	glCompileShader(fragmentP2);

	stimuliP2ProgramID = glCreateProgram();

	glAttachShader(stimuliP2ProgramID, vertexP2);
	glAttachShader(stimuliP2ProgramID, fragmentP2);
	glLinkProgram(stimuliP2ProgramID);

	glDeleteShader(vertexP2);
	glDeleteShader(fragmentP2);

	glGenVertexArrays(1, &VAOP2);
	glGenBuffers(1, &VBOP2);
	glGenBuffers(1, &EBOP2);

	turTex = cv::imread(texfilenamebf, cv::IMREAD_UNCHANGED);
	bfTex = cv::imread(texfilenamet, cv::IMREAD_UNCHANGED);

	glGenTextures(1, &wcTextures);

	objCounter = 0;

	glGenTextures(1, &nTexture);
	unsigned int vertexN, fragmentN;
	vertexN = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexN, 1, &nystagmus_VertexSource, NULL);
	glCompileShader(vertexN);

	fragmentN = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentN, 1, &nystagmus_FragmentSource, NULL);
	glCompileShader(fragmentN);
	glGetShaderiv(fragmentN, GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetShaderiv(fragmentN, GL_INFO_LOG_LENGTH, &maxLength);

		// The maxLength includes the NULL character
		std::vector<GLchar> errorLog(maxLength);
		glGetShaderInfoLog(fragmentN, maxLength, &maxLength, &errorLog[0]);
		std::cout << "ERROR 8:" << &errorLog[0] << std::endl;
	}
	stimuliNProgramID = glCreateProgram();

	glAttachShader(stimuliNProgramID, vertexN);
	glAttachShader(stimuliNProgramID, fragmentN);
	glLinkProgram(stimuliNProgramID);

	glDeleteShader(vertexN);
	glDeleteShader(fragmentN);

	glGenVertexArrays(1, &VAON);
	glGenBuffers(1, &VBON);
	glGenBuffers(1, &EBON);

	objCounter = 0;
}

void CDisplay::loadBlackAnimals()
{
	WCHAR wpath[MAX_PATH];
	HMODULE hModule = GetModuleHandleW(NULL);
	GetModuleFileNameW(hModule, wpath, MAX_PATH);
	PathRemoveFileSpec(wpath);
	std::wstring wspath = wpath;
	std::string path = std::string(wspath.begin(), wspath.end());

	std::string texfilenamea1vid = path;
	std::string texfilenamea2vid = path;
	std::string texfilenamea3vid = path;
	std::string texfilenamea4vid = path;
	std::string texfilenamea5vid = path;
	std::string texfilenamea6vid = path;
	std::string texfilenamea7vid = path;
	std::string texfilenamea8vid = path;
	std::string texfilenamea9vid = path;
	std::string texfilenamea10vid = path;

	texfilenamea8vid.append("\\media\\cat_black.mp4");
	texfilenamea7vid.append("\\media\\cock_black.mp4");
	texfilenamea1vid.append("\\media\\cow_black.mp4");
	texfilenamea10vid.append("\\media\\dog_black.mp4");
	texfilenamea3vid.append("\\media\\elephant_black.mp4");
	texfilenamea5vid.append("\\media\\monkey_black.mp4");
	texfilenamea4vid.append("\\media\\panda_black.mp4");
	texfilenamea9vid.append("\\media\\pig_black.mp4");
	texfilenamea2vid.append("\\media\\rabbit_black.mp4");
	texfilenamea6vid.append("\\media\\sheep_black.mp4");

	animalVideos[0].open(texfilenamea1vid);
	animalVideos[1].open(texfilenamea2vid);
	animalVideos[2].open(texfilenamea3vid);
	animalVideos[3].open(texfilenamea4vid);
	animalVideos[4].open(texfilenamea5vid);
	animalVideos[5].open(texfilenamea6vid);
	animalVideos[6].open(texfilenamea7vid);
	animalVideos[7].open(texfilenamea8vid);
	animalVideos[8].open(texfilenamea9vid);
	animalVideos[9].open(texfilenamea10vid);
	animalVideos[0].read(bflvtex);
}

void CDisplay::loadGrayAnimals()
{
	WCHAR wpath[MAX_PATH];
	HMODULE hModule = GetModuleHandleW(NULL);
	GetModuleFileNameW(hModule, wpath, MAX_PATH);
	PathRemoveFileSpec(wpath);
	std::wstring wspath = wpath;
	std::string path = std::string(wspath.begin(), wspath.end());

	std::string texfilenamea1vid = path;
	std::string texfilenamea2vid = path;
	std::string texfilenamea3vid = path;
	std::string texfilenamea4vid = path;
	std::string texfilenamea5vid = path;
	std::string texfilenamea6vid = path;
	std::string texfilenamea7vid = path;
	std::string texfilenamea8vid = path;
	std::string texfilenamea9vid = path;
	std::string texfilenamea10vid = path;

	texfilenamea10vid.append("\\media\\cat_grey.mp4");
	texfilenamea1vid.append("\\media\\cock_grey.mp4");
	texfilenamea2vid.append("\\media\\cow_grey.mp4");
	texfilenamea3vid.append("\\media\\dog_grey.mp4");
	texfilenamea4vid.append("\\media\\elephant_grey.mp4");
	texfilenamea5vid.append("\\media\\monkey_grey.mp4");
	texfilenamea6vid.append("\\media\\panda_grey.mp4");
	texfilenamea7vid.append("\\media\\pig_grey.mp4");
	texfilenamea8vid.append("\\media\\rabbit_grey.mp4");
	texfilenamea9vid.append("\\media\\sheep_grey.mp4");

	animalVideos[0].open(texfilenamea1vid);
	animalVideos[1].open(texfilenamea2vid);
	animalVideos[2].open(texfilenamea3vid);
	animalVideos[3].open(texfilenamea4vid);
	animalVideos[4].open(texfilenamea5vid);
	animalVideos[5].open(texfilenamea6vid);
	animalVideos[6].open(texfilenamea7vid);
	animalVideos[7].open(texfilenamea8vid);
	animalVideos[8].open(texfilenamea9vid);
	animalVideos[9].open(texfilenamea10vid);
	animalVideos[0].read(bflvtex);
}

int CDisplay::addObject(StimuliObjects object)
{
	std::unique_lock<std::mutex> lck(mtx);
	object.objectID = objCounter;
	objCounter++;
	stimuliObjects.push_back(object);
	stimuliType = DisplayStimuliType::STIMULI;
	return object.objectID;
}

void CDisplay::moveObject(int objectID, int x, int y)
{
	std::unique_lock<std::mutex> lck(mtx);
	for (int i = 0; i < stimuliObjects.size(); i++)
	{
		if (stimuliObjects[i].objectID == objectID)
		{
			stimuliObjects[i].objectCoordinates[0] = x;
			stimuliObjects[i].objectCoordinates[1] = y;
			break;
		}
	}
	stimuliType = DisplayStimuliType::STIMULI;
}

void CDisplay::deleteObject(int objectID)
{
	std::unique_lock<std::mutex> lck(mtx);
	for (int i = 0; i < stimuliObjects.size(); i++)
	{
		if (stimuliObjects[i].objectID == objectID)
		{
			stimuliObjects.erase(stimuliObjects.begin() + i);
			break;
		}
	}
	stimuliType = DisplayStimuliType::STIMULI;
}

void CDisplay::clearObjects()
{
	std::unique_lock<std::mutex> lck(mtx);
	stimuliObjects.clear();
	stimuliType = DisplayStimuliType::STIMULI;
}

void CDisplay::paintObject(int objectID, float R, float G, float B)
{
	std::unique_lock<std::mutex> lck(mtx);
	for (int i = 0; i < stimuliObjects.size(); i++)
	{
		if (stimuliObjects[i].objectID == objectID)
		{
			stimuliObjects[i].objectColor[0] = R;
			stimuliObjects[i].objectColor[1] = G;
			stimuliObjects[i].objectColor[2] = B;
			break;
		}
	}
	stimuliType = DisplayStimuliType::STIMULI;
}

void CDisplay::resizeObject(int objectID, int size)
{
	std::unique_lock<std::mutex> lck(mtx);
	for (int i = 0; i < stimuliObjects.size(); i++)
	{
		if (stimuliObjects[i].objectID == objectID)
		{
			stimuliObjects[i].objectSize = size;
			break;
		}
	}
	stimuliType = DisplayStimuliType::STIMULI;
}

void CDisplay::setBackground(float R, float G, float B)
{
	std::unique_lock<std::mutex> lck(mtx);
	backgroundColorL[0] = R;
	backgroundColorL[1] = G;
	backgroundColorL[2] = B;
	backgroundColorR[0] = R;
	backgroundColorR[1] = G;
	backgroundColorR[2] = B;
	backgroundColorA[0] = R;
	backgroundColorA[1] = G;
	backgroundColorA[2] = B;
	backtex = false;
	//	stimuliType = DisplayStimuliType::STIMULI;
}

void CDisplay::setBackgroundAA(float R, float G, float B)
{
	std::unique_lock<std::mutex> lck(mtx);
	backgroundColorAA[0] = R;
	backgroundColorAA[1] = G;
	backgroundColorAA[2] = B;
}

void CDisplay::setBackgroundL(float R, float G, float B)
{
	std::unique_lock<std::mutex> lck(mtx);
	backgroundColorL[0] = R;
	backgroundColorL[1] = G;
	backgroundColorL[2] = B;
	backtex = false;
	//	stimuliType = DisplayStimuliType::STIMULI;
}

void CDisplay::setBackgroundR(float R, float G, float B)
{
	std::unique_lock<std::mutex> lck(mtx);
	backgroundColorR[0] = R;
	backgroundColorR[1] = G;
	backgroundColorR[2] = B;
	backtex = false;
	//	stimuliType = DisplayStimuliType::STIMULI;
}

void CDisplay::setTextures(cv::Mat leftTex, cv::Mat rightTex)
{
	std::unique_lock<std::mutex> lck(mtx);
	lBGTex = leftTex.clone();
	rBGTex = rightTex.clone();
	backtex = true;
	//	stimuliType = DisplayStimuliType::STIMULI;
}

void CDisplay::drawMCross(float xr1, float xr2, float size)
{
	float x = xr1 / (float)resolutionX - 1.f;
	float x1 = xr2 / (float)resolutionX - 1.f;
	float y = 0.f;

	float szx = size / (float)resolutionX;
	float szy = size / ((float)resolutionY / 2);
	float mcvertices[170] = {
			x,  y,1,1,1,

			x - (szx - szx / 5.f), y,1,1,1,

			x, y + (szy - szy / 5.f),1,1,1,

			x + (szx - szx / 5.f),  y,1,1,1,

			x, y - (szy - szy / 5.f),1,1,1,

			x - szx, y - szy / 3.f,1,1,1,
			x - szx, y + szy / 3.f,1,1,1,

			x - szx / 3.f, y + szy,1,1,1,
			x + szx / 3.f, y + szy,1,1,1,

			x + szx, y + szy / 3.f,1,1,1,
			x + szx, y - szy / 3.f,1,1,1,

			x + szx / 3.f, y - szy,1,1,1,
			x - szx / 3.f, y - szy,1,1,1,

			x1,  y,1,1,1,

			x1 - (szx - szx / 5.f), y,1,1,1,

			x1, y + (szy - szy / 5.f),1,1,1,

			x1 + (szx - szx / 5.f),  y,1,1,1,

			x1, y - (szy - szy / 5.f),1,1,1,

			x1 - szx, y - szy / 3.f,1,1,1,
			x1 - szx, y + szy / 3.f,1,1,1,

			x1 - szx / 3.f, y + szy,1,1,1,
			x1 + szx / 3.f, y + szy,1,1,1,

			x1 + szx, y + szy / 3.f,1,1,1,
			x1 + szx, y - szy / 3.f,1,1,1,

			x1 + szx / 3.f, y - szy,1,1,1,
			x1 - szx / 3.f, y - szy,1,1,1,

			x + szx / 7.f, y, 1, 0, 0,
			x, y + szy / 7.f, 1, 0, 0,
			x - szx / 7.f, y, 1, 0, 0,
			x, y - szy / 7.f, 1, 0, 0,

			x1 + szx / 7.f, y, 1, 0, 0,
			x1, y + szy / 7.f, 1, 0, 0,
			x1 - szx / 7.f, y, 1, 0, 0,
			x1, y - szy / 7.f, 1, 0, 0

	};
	mcMutex.lock();
	for (int i = 0; i < 170; i++)
		mcVertices[i] = mcvertices[i];
	mcMutex.unlock();

	stimuliType = DisplayStimuliType::VERGENCE;
}

void CDisplay::redrawCross()
{
	mcMutex.lock();
	glfwMakeContextCurrent(stimuliWindow);

	glUseProgram(stimuliMProgramID);

	glBindVertexArray(VAOM);


	if (verg_targettype == -1)
	{

		glBindBuffer(GL_ARRAY_BUFFER, VBOM);
		glBufferData(GL_ARRAY_BUFFER, sizeof(mcVertices), mcVertices, GL_STREAM_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBOM);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(mcIndices), mcIndices, GL_STREAM_DRAW);

		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(0);

		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(2 * sizeof(float)));
		glEnableVertexAttribArray(1);

		glClearColor(0.0, 0.0, 0.0, 1.0);
		glClear(GL_COLOR_BUFFER_BIT);
//		glDisable(GL_TEXTURE);

		glUniform1i(glGetUniformLocation(stimuliMProgramID, "ntype"), verg_targettype);
		glUniform1i(glGetUniformLocation(stimuliMProgramID, "centralPic"), 0);
		glUniform1i(glGetUniformLocation(stimuliMProgramID, "coordlx"), nCoordL.x);
		glUniform1i(glGetUniformLocation(stimuliMProgramID, "coordly"), nCoordL.y);
		glUniform1i(glGetUniformLocation(stimuliMProgramID, "coordrx"), nCoordR.x);
		glUniform1i(glGetUniformLocation(stimuliMProgramID, "coordry"), nCoordR.y);

		glBindVertexArray(VAOM);
		glDrawElements(GL_TRIANGLES, 60, GL_UNSIGNED_INT, 0);

	}
	else
	{
		const float vvertices[20] = {
			1.f,  1.f, 0, 0, 0,
			1.f, -1.f, 0, 0, 0,
			-1.f, -1.f,0, 0, 0,
			-1.f,  1.f,0, 0, 0
		};
		const unsigned int vindices[6] = {
			0, 1, 3,
			1, 2, 3,
		};

		glBindBuffer(GL_ARRAY_BUFFER, VBOM);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vvertices), vvertices, GL_STREAM_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBOM);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(vindices), vindices, GL_STREAM_DRAW);

		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(0);

		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(2 * sizeof(float)));
		glEnableVertexAttribArray(1);

		glClearColor(0.0, 0.0, 0.0, 1.0);
		glClear(GL_COLOR_BUFFER_BIT);
		glActiveTexture(GL_TEXTURE0);
		
		glBindTexture(GL_TEXTURE_2D, nTexture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		if (!animalVideos[verg_targettype].read(bflvtex))
		{
			animalVideos[verg_targettype].set(cv::CAP_PROP_POS_FRAMES, 0);
			animalVideos[verg_targettype].read(bflvtex);
		}

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bflvtex.cols, bflvtex.rows, 0, GL_BGR, GL_UNSIGNED_BYTE, bflvtex.ptr());

		glUniform1i(glGetUniformLocation(stimuliMProgramID, "ntype"), verg_targettype);
		glUniform1i(glGetUniformLocation(stimuliMProgramID, "centralPic"), 0);
		glUniform1i(glGetUniformLocation(stimuliMProgramID, "coordlx"), nCoordL.x);
		glUniform1i(glGetUniformLocation(stimuliMProgramID, "coordly"), nCoordL.y);
		glUniform1i(glGetUniformLocation(stimuliMProgramID, "coordrx"), nCoordR.x);
		glUniform1i(glGetUniformLocation(stimuliMProgramID, "coordry"), nCoordR.y);

		glBindVertexArray(VAOM);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	}

	
	glReadPixels(0, 0, 1920 * 2, 1080, GL_BGR, GL_UNSIGNED_BYTE, ssrawdata);
	screenPicture.data = ssrawdata;
	screensMutex.lock();
	cv::resize(screenPicture, screenTex, cv::Size(frameWidth, frameHeight));
	cv::flip(screenTex, screenTex, -1);
	screensMutex.unlock();

	glfwSwapBuffers(stimuliWindow);
	mcMutex.unlock();
	redrawPending = false;

}

void CDisplay::amsler(int eye, int type, float ipd)
{
	amse = eye;
	amst = type;
	aipd = ipd;
	stimuliType = DisplayStimuliType::AMSLER;
	redrawPending = true;
}

void CDisplay::drawAmsler()
{
	glfwMakeContextCurrent(stimuliWindow);

	glUseProgram(stimuliAProgramID);

	GLint uniformLoc;
	char uniformNameAr[64];
	char * uniformName = uniformNameAr;


	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, numTextures);
	for (int i = 0; i < 16; i++)
		glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, i, 60, 60, 1, GL_BGR, GL_UNSIGNED_BYTE, numTex[i].data);

	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	uniformLoc = glGetUniformLocation(stimuliAProgramID, "jw");
	glUniform1i(uniformLoc, jw);

	for (int i = 0; i < 16; i++)
	{
		sprintf(uniformName, "blues[%i]", i);
		uniformLoc = glGetUniformLocation(stimuliAProgramID, uniformName);
		if(aminitpos[0]<=1920)
			glUniform1i(uniformLoc, osblues[i]);
		else
			glUniform1i(uniformLoc, odblues[i]);

	}

	uniformLoc = glGetUniformLocation(stimuliAProgramID, "dtp");
	glUniform1i(uniformLoc, 30);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D_ARRAY, numTextures);
	glUniform1i(glGetUniformLocation(stimuliAProgramID, "ourTexture"), 0);

	uniformLoc = glGetUniformLocation(stimuliAProgramID, "cds");
	if(dotsz)
		glUniform1i(uniformLoc, 20);
	else
		glUniform1i(uniformLoc, 10);

	uniformLoc = glGetUniformLocation(stimuliAProgramID, "redon");
	glUniform1i(uniformLoc, redon);

	uniformLoc = glGetUniformLocation(stimuliAProgramID, "initpos");
	glUniform2fv(uniformLoc, 1, aminitpos);

	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT);

	glBindVertexArray(VAOA);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

	glReadPixels(0, 0, 1920 * 2, 1080, GL_BGR, GL_UNSIGNED_BYTE, ssrawdata);
	screenPicture.data = ssrawdata;
	screensMutex.lock();
	cv::resize(screenPicture, screenTex, cv::Size(frameWidth, frameHeight));
	cv::flip(screenTex, screenTex, -1);
	screensMutex.unlock();
	glfwSwapBuffers(stimuliWindow);
}

GLubyte* CDisplay::doScreenshot()
{
	glfwMakeContextCurrent(stimuliWindow);
	GLubyte* ret = new GLubyte[3840 * 1080 * 3];
	glReadPixels(0, 0, 1920 * 2, 1080, GL_BGR, GL_UNSIGNED_BYTE, ret);
	return ret;
}

void CDisplay::switchShowTex(int id, int val)
{
	std::unique_lock<std::mutex> lck(mtx);
	for (int i = 0; i < stimuliObjects.size(); i++)
	{
		if (stimuliObjects[i].objectID == id)
		{
			stimuliObjects[i].numTexture = val;
			break;
		}
	}
	stimuliType = DisplayStimuliType::STIMULI;
}

void CDisplay::saveScreenshot()
{
	glfwMakeContextCurrent(stimuliWindow);
	GLubyte* ret = new GLubyte[3840 * 1080 * 3];
	glReadPixels(0, 0, 1920 * 2, 1080, GL_BGR, GL_UNSIGNED_BYTE, ret);
	cv::Mat ss = cv::Mat::zeros(1080, 1920 * 2, CV_8UC3);
	ss.data = ret;
	std::ostringstream vffn;
	vffn << screenshotsPath << std::chrono::system_clock::now().time_since_epoch().count() << ".png";
	cv::imwrite(vffn.str(), ss);
}

void CDisplay::showDragonfly()
{
	glfwMakeContextCurrent(stimuliWindow);

	glUseProgram(stimuliDFProgramID);

	int texW = 1920, texH = 1080;

	if (!dfL.read(dfLtex) || !dfR.read(dfRtex))
	{
		dfL.set(cv::CAP_PROP_POS_FRAMES, 0);
		dfL.read(dfLtex);
		dfR.set(cv::CAP_PROP_POS_FRAMES, 0);
		dfR.read(dfRtex);
	}

	glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, 0, texW, texH, 1, GL_BGR, GL_UNSIGNED_BYTE, dfLtex.data);
	glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, 1, texW, texH, 1, GL_BGR, GL_UNSIGNED_BYTE, dfRtex.data);

	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glBindVertexArray(VAODF);

	glBindBuffer(GL_ARRAY_BUFFER, VBODF);
	glBufferData(GL_ARRAY_BUFFER, sizeof(dfVertices), &dfVertices, GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBODF);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(dfIndices), &dfIndices, GL_DYNAMIC_DRAW);

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (void*)0);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(2 * sizeof(float)));
	glEnableVertexAttribArray(1);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D_ARRAY, dfTextures);
	glUniform1i(glGetUniformLocation(stimuliDFProgramID, "ourTexture"), 0);

	glBindVertexArray(VAODF);
	glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, 0);

	glReadPixels(0, 0, 1920 * 2, 1080, GL_BGR, GL_UNSIGNED_BYTE, ssrawdata);
	screenPicture.data = ssrawdata;
	screensMutex.lock();
	cv::resize(screenPicture, screenTex, cv::Size(frameWidth, frameHeight));
	cv::flip(screenTex, screenTex, -1);
	screensMutex.unlock();
	glfwSwapBuffers(stimuliWindow);
}

void CDisplay::drawSaccadeTask()
{
	std::unique_lock<std::mutex> lck(mtx);
	glfwMakeContextCurrent(stimuliWindow);

	glUseProgram(stimuliSTProgramID);
	glfwSwapInterval(0);

	glUniform3fv(glGetUniformLocation(stimuliSTProgramID, "redTargetColor"), 1, st_redTargetColor);
	glUniform3fv(glGetUniformLocation(stimuliSTProgramID, "backgroundColorL"), 1, backgroundColorL);
	glUniform3fv(glGetUniformLocation(stimuliSTProgramID, "backgroundColorR"), 1, backgroundColorR);
//		"uniform bool showRedTarget;\n"
	GLfloat gtc[2];
	gtc[0] = st_greenTargetCoords.x;
	gtc[1] = st_greenTargetCoords.y;
	glUniform2fv(glGetUniformLocation(stimuliSTProgramID, "greenTargetCoords"), 1, gtc);
	GLfloat rtc[2];
	rtc[0] = st_redTargetCoords.x;
	rtc[1] = st_redTargetCoords.y;
	glUniform2fv(glGetUniformLocation(stimuliSTProgramID, "redTargetCoords"), 1, rtc);
	glUniform1i(glGetUniformLocation(stimuliSTProgramID, "showRedTarget"), st_showRed);
	glUniform1f(glGetUniformLocation(stimuliSTProgramID, "targetSize"), st_targetRadius);
	glUniform1i(glGetUniformLocation(stimuliSTProgramID, "showGreenTarget"), st_showGreen);

	std::cout << st_redTargetColor << "  " << gtc << "  " << rtc << "  " << st_showRed << "  " << st_showGreen << "  " << st_targetRadius << std::endl;

	glBindVertexArray(VAOST);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

	glReadPixels(0, 0, 1920 * 2, 1080, GL_BGR, GL_UNSIGNED_BYTE, ssrawdata);
	screenPicture.data = ssrawdata;
	screensMutex.lock();
	cv::resize(screenPicture, screenTex, cv::Size(frameWidth, frameHeight));
	cv::flip(screenTex, screenTex, -1);
	screensMutex.unlock();
	glfwSwapBuffers(stimuliWindow);
	glFinish();
}

void CDisplay::drawNystagmus()
{
	glfwMakeContextCurrent(stimuliWindow);

	glUseProgram(stimuliNProgramID);
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glBindVertexArray(VAON);

	glBindBuffer(GL_ARRAY_BUFFER, VBON);
	glBufferData(GL_ARRAY_BUFFER, sizeof(P2Verticestur), &P2Verticestur, GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBON);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(P2Indices), &P2Indices, GL_DYNAMIC_DRAW);

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (void*)0);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(2 * sizeof(float)));
	glEnableVertexAttribArray(1);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, nTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	
	int txsz = 23, tysz = 23;
	if (nTargetType == 1)
	{
		if (!animalVideos[nAnilmalNum].read(bflvtex))
		{
			animalVideos[nAnilmalNum].set(cv::CAP_PROP_POS_FRAMES, 0);
			animalVideos[nAnilmalNum].read(bflvtex);
		}
		txsz = 64;
		tysz = 36;
	}
	if (bflvtex.channels() == 4)
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, bflvtex.cols, bflvtex.rows, 0, GL_BGRA, GL_UNSIGNED_BYTE, bflvtex.ptr());
	}
	else
	{
		glDisable(GL_BLEND);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bflvtex.cols, bflvtex.rows, 0, GL_BGR, GL_UNSIGNED_BYTE, bflvtex.ptr());
	}

	glUniform1i(glGetUniformLocation(stimuliNProgramID, "centralPic"), 0);

	glUniform1i(glGetUniformLocation(stimuliNProgramID, "coordlx"), nCoordL.x);
	glUniform1i(glGetUniformLocation(stimuliNProgramID, "coordly"), nCoordL.y);
	glUniform1i(glGetUniformLocation(stimuliNProgramID, "coordrx"), nCoordR.x);
	glUniform1i(glGetUniformLocation(stimuliNProgramID, "coordry"), nCoordR.y);
	glUniform1i(glGetUniformLocation(stimuliNProgramID, "ntype"), nTargetType);
//	glUniform1i(glGetUniformLocation(stimuliNProgramID, "ntype"), 1);
	glUniform1i(glGetUniformLocation(stimuliNProgramID, "nshowl"), (int)nShowL);
	glUniform1i(glGetUniformLocation(stimuliNProgramID, "nshowr"), (int)nShowR);
	if (nysLarge)
	{
		txsz = 3 * txsz;
		tysz = 3 * tysz;
	}
	glUniform1i(glGetUniformLocation(stimuliNProgramID, "tszx"), txsz);
	glUniform1i(glGetUniformLocation(stimuliNProgramID, "tszy"), tysz);
//	glUniform1i(glGetUniformLocation(stimuliNProgramID, "ntype"), nystagmusType);
	glBindVertexArray(VAON);
	glDrawElements(GL_TRIANGLES, 24, GL_UNSIGNED_INT, 0);

	glReadPixels(0, 0, 1920 * 2, 1080, GL_BGR, GL_UNSIGNED_BYTE, ssrawdata);
	screenPicture.data = ssrawdata;
	screensMutex.lock();
	cv::resize(screenPicture, screenTex, cv::Size(frameWidth, frameHeight));
	cv::flip(screenTex, screenTex, -1);
	screensMutex.unlock();
	glfwSwapBuffers(stimuliWindow);
}

void CDisplay::setDragonfly()
{
	setBackground(0, 0, 0);
	stimuliObjects.clear();
	stimuliType = DisplayStimuliType::DRAGONFLY;
	SetPriorityClass(GetCurrentProcess(), NORMAL_PRIORITY_CLASS);
//	currentTest = nullptr;
}

void CDisplay::changeTextures()
{
	for (int i = 0; i < 257; i++)
	{
		bgTexOS[i] = bgTexOS[i] * maxLuminosityOS;
		bgTexOD[i] = bgTexOD[i] * maxLuminosityOD;
	}
}

void CDisplay::redraw()
{
	std::unique_lock<std::mutex> lck(mtx);
	glfwMakeContextCurrent(stimuliWindow);

	glUseProgram(stimuliProgramID);
	glfwSwapInterval(0);


	if (backtex)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D_ARRAY, bgTextures);
		glUniform1i(glGetUniformLocation(stimuliProgramID, "ourTexture"), 0);
		if (!p4)
		{
			glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, 0, lBGTex.cols, lBGTex.rows, 1, GL_BGR, GL_UNSIGNED_BYTE, lBGTex.ptr());
			glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, 1, rBGTex.cols, rBGTex.rows, 1, GL_BGR, GL_UNSIGNED_BYTE, rBGTex.ptr());
		}
		else
		{
			glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, 0, 1920, 1080, 1, GL_BGR, GL_UNSIGNED_BYTE, bgTexOS[p4_texL].ptr());
			glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, 1, 1920, 1080, 1, GL_BGR, GL_UNSIGNED_BYTE, bgTexOD[p4_texR].ptr());
		}

		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	}
	else //if(showvftex)
	{
		glActiveTexture(GL_TEXTURE1);
		glUniform1i(glGetUniformLocation(stimuliProgramID, "vfTextures"), 1);
		glBindTexture(GL_TEXTURE_2D, vfnumTextures);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, numTex[vftexnum].cols, numTex[vftexnum].rows, 0, GL_BGR, GL_UNSIGNED_BYTE, numTex[vftexnum].ptr());
	}


	glUniform1i(glGetUniformLocation(stimuliProgramID, "numc"), stimuliObjects.size());
	glUniform1i(glGetUniformLocation(stimuliProgramID, "backTex"), backtex);
	glUniform1i(glGetUniformLocation(stimuliProgramID, "vfTex"), showvftex);
	glUniform1i(glGetUniformLocation(stimuliProgramID, "hollowCircle"), hollowCircle);

	GLfloat hcc[2];
	hcc[0] = hollowCircleCoords.x;
	hcc[1] = hollowCircleCoords.y;
	glUniform2fv(glGetUniformLocation(stimuliProgramID, "hcCorrds"), 1, hcc);
	glUniform1f(glGetUniformLocation(stimuliProgramID, "hcR1"), hollowCircleR1);
	glUniform1f(glGetUniformLocation(stimuliProgramID, "hcR2"), hollowCircleR2);

	for (int i = 0; i < stimuliObjects.size(); i++)
	{
		GLint uniformLoc;
		char uniformNameAr[64];
		char* uniformName = uniformNameAr;

		sprintf(uniformName, "circles[%i]", i);
		uniformLoc = glGetUniformLocation(stimuliProgramID, uniformName);
		glUniform2fv(uniformLoc, 1, stimuliObjects[i].objectCoordinates);

		sprintf(uniformName, "colors[%i]", i);
		uniformLoc = glGetUniformLocation(stimuliProgramID, uniformName);
		glUniform3fv(uniformLoc, 1, stimuliObjects[i].objectColor);

		sprintf(uniformName, "sizes[%i]", i);
		uniformLoc = glGetUniformLocation(stimuliProgramID, uniformName);
		glUniform1f(uniformLoc, stimuliObjects[i].objectSize);

		sprintf(uniformName, "stex[%i]", i);
		uniformLoc = glGetUniformLocation(stimuliProgramID, uniformName);
		glUniform1i(uniformLoc, stimuliObjects[i].numTexture);
	}

	glUniform3fv(glGetUniformLocation(stimuliProgramID, "backclrl"), 1, backgroundColorL);
	glUniform3fv(glGetUniformLocation(stimuliProgramID, "backclrr"), 1, backgroundColorR);
	glUniform1i(glGetUniformLocation(stimuliProgramID, "bleech"), vfbleech);

	glBindVertexArray(VAOC);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

	glReadPixels(0, 0, 1920 * 2, 1080, GL_BGR, GL_UNSIGNED_BYTE, ssrawdata);
	screenPicture.data = ssrawdata;
	screensMutex.lock();
	cv::resize(screenPicture, screenTex, cv::Size(frameWidth, frameHeight));
	cv::flip(screenTex, screenTex, -1);
	screensMutex.unlock();
	glfwSwapBuffers(stimuliWindow);
	glFinish();
	if (vfbleech)
	{
		vfbleech = false;
		afterbleech = true;
	}
	else
	{
		redrawPending = false;
		if (afterbleech)
			vf_srtstart = std::chrono::system_clock::now();
	}
}

void CDisplay::drawContrast()
{
	glfwMakeContextCurrent(stimuliWindow);

	glUseProgram(stimuliCAProgramID);
	if (ac_hollowCircle)
	{
		glUniform1i(glGetUniformLocation(stimuliCAProgramID, "hollowCircle"), ac_hollowCircle);
		glUniform1f(glGetUniformLocation(stimuliCAProgramID, "hcR1"), ac_hollowCircleR1);
		glUniform1f(glGetUniformLocation(stimuliCAProgramID, "hcR2"), ac_hollowCircleR2);
	}
	else
		glUniform1i(glGetUniformLocation(stimuliCAProgramID, "hollowCircle"), ac_hollowCircle);
	if (contrast_amdstage == 1)
	{
		if (contrast_tPosition.x <= 1920)
			glUniform3fv(glGetUniformLocation(stimuliCAProgramID, "wclr"), 1, backgroundColorL);
		else
			glUniform3fv(glGetUniformLocation(stimuliCAProgramID, "wclr"), 1, backgroundColorR);
		glUniform3fv(glGetUniformLocation(stimuliCAProgramID, "bclr"), 1, contrast_tClr);
		//std::cout << contrast_tClr[0] << std::endl;
		glUniform1i(glGetUniformLocation(stimuliCAProgramID, "purewpx"), 0);
		glUniform1i(glGetUniformLocation(stimuliCAProgramID, "purebpx"), sqrt(contract_tPx));
		glUniform1i(glGetUniformLocation(stimuliCAProgramID, "showRim"), 0);
		GLfloat whtOS[3] = { maxLuminosityOS,maxLuminosityOS,maxLuminosityOS };
		GLfloat whtOD[3] = { maxLuminosityOD,maxLuminosityOD,maxLuminosityOD };
		
		if (contrast_bleachFramesL > 0)
		{
			std::cout << "OS " << whtOS[0] << std::endl;
			glUniform3fv(glGetUniformLocation(stimuliCAProgramID, "backClrL"), 1, whtOS);
			contrast_bleachFramesL--;
			glUniform3fv(glGetUniformLocation(stimuliCAProgramID, "bclr"), 1, whtOS);
		}
		else
			glUniform3fv(glGetUniformLocation(stimuliCAProgramID, "backClrL"), 1, backgroundColorL);

		if (contrast_bleachFramesR > 0)
		{
			std::cout << "OD " << whtOD[0] << std::endl;
			glUniform3fv(glGetUniformLocation(stimuliCAProgramID, "backClrR"), 1, whtOD);
			contrast_bleachFramesR--;
			glUniform3fv(glGetUniformLocation(stimuliCAProgramID, "bclr"), 1, whtOD);
		}
		else
			glUniform3fv(glGetUniformLocation(stimuliCAProgramID, "backClrR"), 1, backgroundColorR);
	}
	else
	{
		bool showrim;
		float wclr, bclr;
		float c_degrees = pow(10, contrast_logmarindex) / 60.;
		float c_length = c_degrees * 60.;
		float c_area = pow(c_length, 2);
		int purepxcount_prev = pow(floor(c_length) + 1, 2);
		int purepxcount = pow(c_length, 2);
		if (c_length >= 1)
			purepxcount = pow(floor(c_length), 2);
		float dclr = 0.5 - ((c_area - purepxcount) / (purepxcount_prev - purepxcount)) * 0.5;
		//		std::cout << dclr << std::endl;
		if (contrast_logmarindex >= 0)
		{
			contrast_bPx = sqrt(purepxcount);
			contrast_wPx = sqrt(purepxcount);
//			contrast_gbclr = dclr;
//			contrast_gwclr = 1 - dclr;
//			wclr = 1;
//			bclr = 0;
			showrim = 1;
		}
		else if (contrast_logmarindex < 0)

		{
			contrast_bPx = 1;
			contrast_wPx = 0;
//			contrast_gbclr = 0.5;
//			contrast_gwclr = 0.5;
//			wclr = 0.5;
//			bclr = dclr;
			showrim = 0;
		}
		else if (contrast_logmarindex == 0)
		{
			contrast_bPx = 1;
			contrast_wPx = 0;
//			contrast_gbclr = 0.5;
//			contrast_gwclr = 0.5;
//			wclr = 0.5;
//			bclr = 0;
			showrim = 0;
		}

//		contrast_wClr[0] = wclr;
//		contrast_wClr[1] = wclr;
//		contrast_wClr[2] = wclr;

//		contrast_bClr[0] = bclr;
//		contrast_bClr[1] = bclr;
//		contrast_bClr[2] = bclr;
		GLfloat bpc[3] = { contrast_wClr,contrast_wClr,contrast_wClr };
		GLfloat wpc[3] = { contrast_bClr,contrast_bClr,contrast_bClr };

//		std::cout << contrast_bPx << " ; " 
//				<< contrast_wPx << " ; "
//				<< contrast_gwclr << " ; "
//				<< contrast_gbclr << " ; "
//				<< contrast_wClr << " ; "
//				<< contrast_bClr << std::endl;
		glUniform1f(glGetUniformLocation(stimuliCAProgramID, "gwclr"), contrast_gwclr);
		glUniform1f(glGetUniformLocation(stimuliCAProgramID, "gbclr"), contrast_gbclr);
		glUniform3fv(glGetUniformLocation(stimuliCAProgramID, "wclr"), 1, wpc);
		glUniform3fv(glGetUniformLocation(stimuliCAProgramID, "bclr"), 1, bpc);
		glUniform1i(glGetUniformLocation(stimuliCAProgramID, "purewpx"), contrast_wPx);
		glUniform1i(glGetUniformLocation(stimuliCAProgramID, "purebpx"), contrast_bPx);
		GLfloat wht[3] = { 1,1,1 };
		glUniform3fv(glGetUniformLocation(stimuliCAProgramID, "backClrL"), 1, backgroundColorL);
		glUniform3fv(glGetUniformLocation(stimuliCAProgramID, "backClrR"), 1, backgroundColorR);
		glUniform1i(glGetUniformLocation(stimuliCAProgramID, "showRim"), showrim);
	}

	GLfloat tsp[2];
	tsp[0] = contrast_tPosition.x;
	tsp[1] = contrast_tPosition.y;

	glUniform2fv(glGetUniformLocation(stimuliCAProgramID, "tPosition"), 1, tsp);


	glBindVertexArray(VAOCA);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

	glReadPixels(0, 0, 1920 * 2, 1080, GL_BGR, GL_UNSIGNED_BYTE, ssrawdata);
	screenPicture.data = ssrawdata;
	screensMutex.lock();
	cv::resize(screenPicture, screenTex, cv::Size(frameWidth, frameHeight));
	cv::flip(screenTex, screenTex, -1);
	screensMutex.unlock();
	glfwSwapBuffers(stimuliWindow);
	redrawPending = false;
}

CDisplay::~CDisplay()
{
	glDeleteVertexArrays(1, &TSVAO);
	glDeleteBuffers(1, &TSVBO);
	glDeleteBuffers(1, &TSEBO);
	glfwTerminate();
}