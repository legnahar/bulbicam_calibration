#pragma once

#include "Tests.h"

#define amslerversion "0.9.20210805"

class CAmslerTest : public CTest
{
public:
	CAmslerTest();
	void ProcessETData();
	int amsler_eye;
	int amsler_type;
};

CAmslerTest::CAmslerTest()
{
	display.amst = 7;
	display.amse = 0;
	display.aipd = lensDist->PPD/2;
	amsler_eye = 0;
	amsler_type = 0;
	eyetrackingOn = false;
	webComm->sendmes("OK");
	display.redrawPending = true;
	for (int i = 0; i < 16; i++)
	{
		display.osblues[i] = 0;
		display.odblues[i] = 0;
	}
	display.redon = false;
	display.dotsz = false;
	display.jw = 0;
}

void CAmslerTest::ProcessETData()
{
//	display.drawAmsler(amsler_eye, amsler_type, 64);
}