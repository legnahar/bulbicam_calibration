#pragma once

#include "Tests.h"
#include <chrono>

#define antisaccadeversion "0.9.20210805"
extern CTest* currentTest;
extern ofstream qualityLog;
extern ofstream testLog;
extern float pxToDegreeOS, pxToDegreeOD;
extern string testLogsPath;
extern float st_memoryT4Time;


enum MS_STATES
{
	MS_T1,
	MS_T2,
	MS_T3,
	MS_T4
};

enum MS_TARGET_SEEN_TYPE
{
	MS_UNSEEN,
	MS_INSPECT,
	MS_SEEN
};

enum MS_COLORS
{
	MS_WHITE,
	MS_GREEN,
	MS_RED
};

class CMSTest : public CTest
{
public:
	CMSTest(int eye, int trainingdots, int testdots);
	void ProcessETData(vector<ETData> etData);
private:
	int ms_eye;
	void drawMSDots();
	void pickRed();
	float ms_randomTime;
	long ms_redInitFrame;
	int ms_testCount;
	bool ms_redDrawn;
	int ms_currentGreenDot;
	int ms_currentRedDot;
	int ms_state;
	int ms_trainingDots, ms_testDots;
	Point2d ms_centralCoord;
};

void CMSTest::pickRed()
{
	if (ms_currentGreenDot == 0)
		ms_currentRedDot = 1;
	else if (ms_currentGreenDot == 4)
		ms_currentRedDot = 3;
	else
		ms_currentRedDot = ms_currentGreenDot + 1 - (rand() % 2)*2;
}

void CMSTest::drawMSDots()
{
	if (ms_state == MS_T2)
		display.st_showRed = true;
	else
		display.st_showRed = false;
	if (ms_state == MS_T4)
		display.st_showGreen = false;
	else
		display.st_showGreen = true;
	int gx, rx;
	if (ms_eye == 0)
	{
		gx = lensDist->jumpTo(ms_centralCoord, Point((ms_currentGreenDot-2) * 5, 0)).x;
		rx = lensDist->jumpTo(ms_centralCoord, Point((ms_currentRedDot-2) * 5, 0)).x;
		display.st_greenTargetCoords.x = gx;
		display.st_redTargetCoords.x = rx;
	}
	else
	{
		gx = lensDist->jumpTo(ms_centralCoord, Point((2 - ms_currentGreenDot) * 5, 0)).x;
		rx = lensDist->jumpTo(ms_centralCoord, Point((2 - ms_currentRedDot) * 5, 0)).x;
	}
	display.st_greenTargetCoords.x = gx;
	display.st_redTargetCoords.x = rx;
	display.st_greenTargetCoords.y = 540;
	display.st_redTargetCoords.y = 540;
	display.redrawPending = true;
}

CMSTest::CMSTest(int eye, int trainingdots, int testdots)
{
	webComm->openLog();
	nlohmann::json ms_startTestMessage;
	ms_startTestMessage["chartTypeString"] = "SACCADE_MERGED";
	ms_startTestMessage["target"] = "MEMORYSACCADE";
	ms_startTestMessage["message_type"] = MESSAGE_TYPE::START_TEST;
	ms_startTestMessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	webComm->sendJSON(ms_startTestMessage);

	ms_eye = eye;
	ms_trainingDots = trainingdots;
	ms_testDots = testdots;
	ms_testCount = 0;
	ms_redDrawn = false;
	ms_currentGreenDot = 2;
	ms_state = 0;
	pickRed();

	float ppd = max(66, lensDist->PPD);
	Point2d centreScreenToCenterOS = lensDist->mmtopix(ppd / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOS / 7.);
	Point2d centreScreenToCenterOD = lensDist->mmtopix(ppd / 2.) * 0.6 - lensDist->mmtopix(6.5) * (-lensDist->dioptersOD / 7.);
	if (ms_eye == 1)
		ms_centralCoord = Point(960 + centreScreenToCenterOS.x, 540);
	else
		ms_centralCoord = Point(2880 - centreScreenToCenterOD.x, 540);


	display.setBackground(0, 0, 0);
	if(ms_eye==0)
		display.setBackgroundR(0.5, 0.5, 0.5);
	else
		display.setBackgroundL(0.5, 0.5, 0.5);
	drawMSDots();
	glfwSetTime(0);

}

void CMSTest::ProcessETData(vector<ETData> etData)
{
	float ggt = glfwGetTime();

	nlohmann::json ms_dataMessage;
	ms_dataMessage["chartTypeString"] = "SACCADE_MERGED";
	ms_dataMessage["message_type"] = MESSAGE_TYPE::DATA_PACKAGE;
	ms_dataMessage["rawOSx"] = (etData[1].tandemGlints.bpG + etData[1].tandemGlints.dpG).x / 2;
	ms_dataMessage["rawOSy"] = (etData[1].tandemGlints.bpG + etData[1].tandemGlints.dpG).y / 2;
	ms_dataMessage["rawODx"] = (etData[0].tandemGlints.bpG + etData[0].tandemGlints.dpG).x / 2;
	ms_dataMessage["rawODy"] = (etData[0].tandemGlints.bpG + etData[0].tandemGlints.dpG).y / 2;
	ms_dataMessage["greenDotIndex"] = ms_currentGreenDot;
	ms_dataMessage["redDotIndex"] = ms_currentRedDot;
	ms_dataMessage["training"] = ms_testCount < ms_trainingDots;
	ms_dataMessage["stage"] = ms_state;
	ms_dataMessage["trial"] = ms_testCount;
	ms_dataMessage["target"] = "MEMORYSACCADE";
	ms_dataMessage["targettype"] = ms_eye;
	ms_dataMessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	webComm->sendJSON(ms_dataMessage);

	if (ms_state == MS_T1)
	{
		if (ggt > 1)
		{
			if (ms_testCount > ms_trainingDots + ms_testDots-1)
			{
				nlohmann::json ms_stopTestMessage;
				ms_stopTestMessage["chartTypeString"] = "SACCADE_MERGED";
				ms_stopTestMessage["target"] = "MEMORYSACCADE";
				ms_stopTestMessage["message_type"] = MESSAGE_TYPE::STOP_TEST;
				ms_stopTestMessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
				webComm->sendJSON(ms_stopTestMessage);
				webComm->closeLog();
				qualityLog.close();
				display.setDragonfly();
				currentTest = nullptr;
				return;
			}
			ms_state = MS_T2;
			glfwSetTime(0);
			ms_redInitFrame = display.frameNumber;
			drawMSDots();
		}
	}
	else if (ms_state == MS_T2)
	{
		if (display.frameNumber - ms_redInitFrame > 1)
		{
			ms_state = MS_T3;
			glfwSetTime(0);
			ms_randomTime = rand() % 100 / 100.;
		}
		drawMSDots();
	}
	else if(ms_state == MS_T3)
	{
		if(ggt > 1 + ms_randomTime)
		{
			ms_state = MS_T4;
			glfwSetTime(0);
			drawMSDots();
		}
	}
	else if (ms_state == MS_T4)
	{
		if (ggt > st_memoryT4Time)
		{
			ms_testCount++;
			ms_currentGreenDot = ms_currentRedDot;
			ms_state = MS_T1;
			glfwSetTime(0);
			drawMSDots();
			pickRed();
		}
	}

}