#pragma once

#include "Tests.h"

#define pupil4version "0.9.20210902"

extern bool picBP;
extern string PathToExecutable;
extern float p4_sequenceDuration;
extern ofstream qualityLog;
extern ofstream testLog;
extern string picturesLogsPath;
extern bool saveFrameSignal;

struct BGColor
{
	int OSBackground;
	int ODBackground;
};

class CPupil4Test : public CTest
{
public:
	CPupil4Test();
	void ProcessETData(vector<ETData> etData);
	int testNum = 444;
private:

	vector<BGColor> pupil_bgc;

/*	const int pupil_bgc[25][2] =
	{ { 0,0 },
	{ 0,0 },
	{ 0,0 },
	{ 0,0 },
	{ 2,0 },
	{ 4,0 },
	{ 8,0 },
	{ 16,0 },
	{ 32,0},
	{ 64,0},
	{ 128,0},
	{ 256,0},
	{ 0,0},
	{ 0,0 },
	{ 0,0 },
	{ 0,0 },
	{ 0,2 },
	{ 0,4 },
	{ 0,8 },
	{ 0,16 },
	{ 0,32 },
	{ 0,64 },
	{ 0,128 },
	{ 0,256 },
	{ 0,0 }
	};*/
	float pupil_stimDuration;
	int pupil_state;
	float convertToMM(RotatedRect pupil, int eye);
	int p2tp;
	int pupil_whitecircleIDs[2];
	int pupil_redcircleIDs[2];
	int p4_seqCount;
	bool picSavedThisRound;
};

CPupil4Test::CPupil4Test()
{
	p4_seqCount = 0;
	ifstream datfl;
	std::string datfilename = PathToExecutable;
	datfilename.append("\\pupil4sequence.conf");
	datfl.open(datfilename);
	string ld;
	while (getline(datfl, ld))
	{
		string s1 = ld.substr(0, ld.find(','));
		string s2 = ld.substr(ld.find(',')+1);
		BGColor tmp;
		tmp.ODBackground = atoi(s1.c_str());
		tmp.OSBackground = atoi(s2.c_str());
		pupil_bgc.push_back(tmp);
	}

	std::ostringstream vffn;
	vffn << testLogsPath << "p4_" << patientInfo.examID << "_" << std::chrono::system_clock::now().time_since_epoch().count() << ".csv";
	std::string vffns = vffn.str();

	testLog.open(vffns);

	display.stimuliType = STIMULI;


	pupil_state = 0;

	float ppd2 = (lensDist->PPD / 2.) - 0.5;
	ppd2 = 15;
	//	if (ppd2 > 28)
//		ppd2 = 28;
	StimuliObjects t1;

	t1.objectColor[0] = 1;
	t1.objectColor[1] = 0;
	t1.objectColor[2] = 0;

	t1.objectCoordinates[0] = 960 + mmtopixels(ppd2);
	t1.objectCoordinates[1] = 540;

	t1.objectSize = 20;

	pupil_redcircleIDs[0] = display.addObject(t1);

	StimuliObjects t2;

	t2.objectColor[0] = 1;
	t2.objectColor[1] = 0;
	t2.objectColor[2] = 0;

	t2.objectCoordinates[0] = 2880 - mmtopixels(ppd2);
	t2.objectCoordinates[1] = 540;

	t2.objectSize = 20;

	pupil_redcircleIDs[1] = display.addObject(t2);

	display.p4 = true;
//	display.rBGTex = bgTex[128].clone();
//	display.lBGTex = bgTex[128].clone();
	display.p4_texL = pupil_bgc[0].OSBackground;
	display.p4_texR = pupil_bgc[0].ODBackground;
	display.backtex = true;
	display.redrawPending = true;


	nlohmann::json pupil_startpupildatamessage;
	pupil_startpupildatamessage["chartTypeString"] = "PUPILLARY_EVALUATION2";
	pupil_startpupildatamessage["message_type"] = MESSAGE_TYPE::START_PUPIL_DATA_TRANSMISSION;
	pupil_startpupildatamessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	webComm->sendJSON(pupil_startpupildatamessage);

	nlohmann::json pupil_startbgdatamessageOS;
	pupil_startbgdatamessageOS["chartTypeString"] = "PUPILLARY_EVALUATION2";
	pupil_startbgdatamessageOS["message_type"] = MESSAGE_TYPE::START_BACKGROUND_DATA_TRANSMISSION;
	pupil_startbgdatamessageOS["backgroundColorOS"] = pupil_bgc[0].OSBackground;
	pupil_startbgdatamessageOS["backgroundColorOD"] = pupil_bgc[0].ODBackground;
	pupil_startbgdatamessageOS["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	webComm->sendJSON(pupil_startbgdatamessageOS);

	glfwSetTime(0);
}

float CPupil4Test::convertToMM(RotatedRect pupil, int eye)
{
	float sz = max(pupil.size.height,pupil.size.width)/14.5;
	float lll = 0;
	if (eye == 0)
		lll = -lensDist->dioptersOD;
	else
		lll = -lensDist->dioptersOS;
	float perc = 0;
	if (lll > 0)
		perc = 0.0232;
	else if (lll < 0)
		perc = 0.0178;
	float k = perc * lll;
	return sz - sz * k;
}

void CPupil4Test::ProcessETData(vector<ETData> etData)
{
	pupil_stimDuration = glfwGetTime();

	if (pupil_stimDuration > p4_sequenceDuration)
	{
		pupil_state++;
		if (pupil_state >= pupil_bgc.size())
		{
			p4_seqCount++;
			if (p4_seqCount == 3)
			{
				nlohmann::json pupil_stopbgdatamessage;
				pupil_stopbgdatamessage["chartTypeString"] = "PUPILLARY_EVALUATION2";
				pupil_stopbgdatamessage["message_type"] = MESSAGE_TYPE::STOP_BACKGROUND_DATA_TRANSMISSION;
				pupil_stopbgdatamessage["pulseduration"] = 2.5;
				pupil_stopbgdatamessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
				webComm->sendJSON(pupil_stopbgdatamessage);

				nlohmann::json pupil_stoppupildatamessage;
				pupil_stoppupildatamessage["chartTypeString"] = "PUPILLARY_EVALUATION2";
				pupil_stoppupildatamessage["message_type"] = MESSAGE_TYPE::STOP_PUPIL_DATA_TRANSMISSION;
				pupil_stoppupildatamessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
				webComm->sendJSON(pupil_stoppupildatamessage);

				nlohmann::json pupil_stoptestmessage;
				pupil_stoptestmessage["chartTypeString"] = "PUPILLARY_EVALUATION2";
				pupil_stoptestmessage["message_type"] = MESSAGE_TYPE::STOP_TEST;
				pupil_stoptestmessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
				webComm->sendJSON(pupil_stoptestmessage);
				webComm->closeLog();
				qualityLog.close();

//				for (int i = 0; i < savedFrames.size(); i++)
//				{
//					std::ostringstream vffn2;
//					vffn2 << picturesLogsPath << "pupil_" << patientInfo.examID << "_" << std::chrono::system_clock::now().time_since_epoch().count() << format("_%d", i) << ".png";
//					if(savedFrames[i].rows&& savedFrames[i].cols)
//						cv::imwrite(vffn2.str(), savedFrames[i]);
//				}
//				savedFrames.clear();

				display.backtex = false;
				testLog.close();
				display.setDragonfly();
				eyetrackingOn = false;
				currentTest = nullptr;
				return;
			}
			else
			{
				pupil_state = 0;

				//display.lBGTex = bgTex[pupil_bgc[pupil_state].OSBackground].clone();
				//display.rBGTex = bgTex[pupil_bgc[pupil_state].ODBackground].clone();
				display.p4_texL = pupil_bgc[pupil_state].OSBackground;
				display.p4_texR = pupil_bgc[pupil_state].ODBackground;
//				cout << " E " << pupil_state << endl;
//				cout << " E " << bgTex[pupil_bgc[pupil_state].OSBackground].cols << "  " << bgTex[pupil_bgc[pupil_state].ODBackground].cols << endl;
				display.backtex = true;
				display.redrawPending = true;

				nlohmann::json pupil_stopbgdatamessage;
				pupil_stopbgdatamessage["chartTypeString"] = "PUPILLARY_EVALUATION2";
				pupil_stopbgdatamessage["message_type"] = MESSAGE_TYPE::STOP_BACKGROUND_DATA_TRANSMISSION;
				pupil_stopbgdatamessage["pulseduration"] = 2.5;
				pupil_stopbgdatamessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
				webComm->sendJSON(pupil_stopbgdatamessage);

				nlohmann::json pupil_stoppupildatamessage;
				pupil_stoppupildatamessage["chartTypeString"] = "PUPILLARY_EVALUATION2";
				pupil_stoppupildatamessage["message_type"] = MESSAGE_TYPE::STOP_PUPIL_DATA_TRANSMISSION;
				pupil_stoppupildatamessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
				webComm->sendJSON(pupil_stoppupildatamessage);

				nlohmann::json pupil_startpupildatamessage;
				pupil_startpupildatamessage["chartTypeString"] = "PUPILLARY_EVALUATION2";
				pupil_startpupildatamessage["message_type"] = MESSAGE_TYPE::START_PUPIL_DATA_TRANSMISSION;
				pupil_startpupildatamessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
				webComm->sendJSON(pupil_startpupildatamessage);

				nlohmann::json pupil_startbgdatamessage;
				pupil_startbgdatamessage["chartTypeString"] = "PUPILLARY_EVALUATION2";
				pupil_startbgdatamessage["message_type"] = MESSAGE_TYPE::START_BACKGROUND_DATA_TRANSMISSION;
				pupil_startbgdatamessage["backgroundColorOS"] = pupil_bgc[0].OSBackground;
				pupil_startbgdatamessage["backgroundColorOD"] = pupil_bgc[0].ODBackground;
				pupil_startbgdatamessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
				webComm->sendJSON(pupil_startbgdatamessage);
				glfwSetTime(0);
				return;

			}

		}
//		display.lBGTex = bgTex[0].clone();
		//display.lBGTex = bgTex[pupil_bgc[pupil_state].OSBackground].clone();
//		display.rBGTex = bgTex[0].clone();
		//display.rBGTex = bgTex[pupil_bgc[pupil_state].ODBackground].clone();
		display.p4_texL = pupil_bgc[pupil_state].OSBackground;
		display.p4_texR = pupil_bgc[pupil_state].ODBackground;
		display.backtex = true;

		nlohmann::json pupil_stopbgdatamessage;
		pupil_stopbgdatamessage["chartTypeString"] = "PUPILLARY_EVALUATION2";
		pupil_stopbgdatamessage["message_type"] = MESSAGE_TYPE::STOP_BACKGROUND_DATA_TRANSMISSION;
		pupil_stopbgdatamessage["pulseduration"] = 2.5;
		pupil_stopbgdatamessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
		webComm->sendJSON(pupil_stopbgdatamessage);

		nlohmann::json pupil_startbgdatamessage;
		pupil_startbgdatamessage["chartTypeString"] = "PUPILLARY_EVALUATION2";
		pupil_startbgdatamessage["message_type"] = MESSAGE_TYPE::START_BACKGROUND_DATA_TRANSMISSION;
		pupil_startbgdatamessage["backgroundColorOS"] = pupil_bgc[pupil_state].OSBackground;
		pupil_startbgdatamessage["backgroundColorOD"] = pupil_bgc[pupil_state].ODBackground;
		pupil_startbgdatamessage["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
		webComm->sendJSON(pupil_startbgdatamessage);

		glfwSetTime(0);
		display.redrawPending = true;

	}

	nlohmann::json js;

	float ossize = 0, odsize = 0;
	odsize = convertToMM(etData[0].rawPupilEllipse, 0);
	ossize = convertToMM(etData[1].rawPupilEllipse, 1);

	js["pupilxOS"] = 0;
	js["pupilyOS"] = 0;
	js["bpgxOS"] = 0;
	js["bpgyOS"] = 0;
	js["dpgxOS"] = 0;
	js["dpgyOS"] = 0;
	js["pupilxOD"] = 0;
	js["pupilyOD"] = 0;
	js["bpgxOD"] = 0;
	js["bpgyOD"] = 0;
	js["dpgxOD"] = 0;
	js["dpgyOD"] = 0;

	testLog << pupil_bgc[pupil_state].OSBackground << ";"
		<< pupil_bgc[pupil_state].ODBackground << ";"
		<< pupil_stimDuration << ";"
		<< odsize << ";"
		<< ossize << endl;

	if (pupil_stimDuration > 0.5 && !picSavedThisRound)
	{
		picSavedThisRound = true;
		saveFrameSignal = true;
	}


	if (etData[0].rawPupilEllipse.center.x > 0 && etData[0].rawPupilEllipse.center.y > 0)
	{
		js["pupilxOD"] = etData[0].pupilEllipse.center.x;
		js["pupilyOD"] = etData[0].pupilEllipse.center.y;
	}
	if (etData[1].rawPupilEllipse.center.x > 0 && etData[1].rawPupilEllipse.center.y > 0)
	{
		js["pupilxOS"] = etData[1].pupilEllipse.center.x;
		js["pupilyOS"] = etData[1].pupilEllipse.center.y;
	}
	if (picBP)
	{
		if (etData[0].tandemGlints.bpGraw.x > 0 && etData[0].tandemGlints.bpGraw.y > 0)
		{
			js["bpgxOD"] = etData[0].tandemGlints.bpG.x;
			js["bpgyOD"] = etData[0].tandemGlints.bpG.y;
		}
		if (etData[1].tandemGlints.bpGraw.x > 0 && etData[1].tandemGlints.bpGraw.y > 0)
		{
			js["bpgxOS"] = etData[1].tandemGlints.bpG.x;
			js["bpgyOS"] = etData[1].tandemGlints.bpG.y;
		}
	}
	else
	{
		if (etData[0].tandemGlints.dpGraw.x > 0 && etData[0].tandemGlints.dpGraw.y > 0)
		{
			js["dpgxOD"] = etData[0].tandemGlints.dpG.x;
			js["dpgyOD"] = etData[0].tandemGlints.dpG.y;
		}
		if (etData[1].tandemGlints.dpGraw.x > 0 && etData[1].tandemGlints.dpGraw.y > 0)
		{
			js["dpgxOS"] = etData[1].tandemGlints.dpG.x;
			js["dpgyOS"] = etData[1].tandemGlints.dpG.y;
		}
	}

	js["chartTypeString"] = "PUPILLARY_EVALUATION2";
	js["message_type"] = MESSAGE_TYPE::DATA_PACKAGE;
	js["backgroundColorOS"] = pupil_bgc[pupil_state].OSBackground;
	js["measurementOD"] = odsize;
	js["measurementOS"] = ossize;
	js["backgroundColorOD"] = pupil_bgc[pupil_state].ODBackground;
	js["timestamp"] = std::chrono::system_clock::now().time_since_epoch().count();
	webComm->sendJSON(js);

}